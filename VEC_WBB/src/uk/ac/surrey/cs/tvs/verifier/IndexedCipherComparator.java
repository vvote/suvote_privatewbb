/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.verifier;

import java.math.BigInteger;
import java.util.Comparator;

/**
 * Comparator for comparing two IndexCiphers using their ECPoint arrays to compare them. Each ECPoint array contains two elements, the first
 * representing g^r, the second my^r.
 * 
 * @author Chris Culnane
 * 
 */
public class IndexedCipherComparator implements Comparator<IndexedCipher> {

  /**
   * Compare two IndexCiphers by comparing their underlying ECPoint arrays, each representing a cipher, to allow them to be ordered
   * during a sort.
   * 
   * To perform the actual comparison we get the encoded form of the points and place them in a BigInteger before then comparing the
   * BigInteger of each of the cipher elements. 
   * 
   * The most important point of the comparator is that it performs the same sort as the ballot generation.
   * 
   * @param o1
   *          IndexCipher containing ECPoint array with two elements, [g^r,my^r]
   * @param o2
   *          IndexCipher containing ECPoint array with two elements, [g^r,my^r] returns an int of the comparison as per the
   *          comparable specification <0 less than, 0 equals, >0 greater than
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(IndexedCipher o1, IndexedCipher o2) {
    BigInteger grOne = new BigInteger(1,o1.getCipher()[0].getEncoded());
    BigInteger myrOne = new BigInteger(1,o1.getCipher()[1].getEncoded());
    BigInteger grTwo = new BigInteger(1,o2.getCipher()[0].getEncoded());
    BigInteger myrTwo = new BigInteger(1,o2.getCipher()[1].getEncoded());

    if (grOne.equals(grTwo)) {
      return myrOne.compareTo(myrTwo);
    }
    else {
      // Compare the g^r
      return grOne.compareTo(grTwo);
    }
  }
}
