/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.verifier;

import org.bouncycastle.math.ec.ECPoint;

/**
 * Simple wrapper class to make checking the re-encryption and sort produce the correct permutation. When first created the
 * re-encrypted ciphers are wrapped in this class, along with the initial index of the base cipher for that race. When these
 * IndexedCiphers are then sorted we can iterate over the sorted list and extract the permutation directly.
 * 
 * @author Chris Culnane
 * 
 */
public class IndexedCipher {

  /**
   * int holding the candidate index
   */
  private int       index;

  /**
   * ECPoint array of two elements g^r and my^r that represent a cipher
   */
  private ECPoint[] cipher;

  /**
   * Constructs a new IndexedCipher wrapper. The passed in ECPoint array should be a re-encrypted ECPoint array and the index should
   * be the original candidate index for the race
   * 
   * @param cipher ECPoint[] containing two elements, the first g^r the second my^r
   * @param index int candidate index for use in creating a permutation string
   */
  public IndexedCipher(ECPoint[] cipher, int index) {
    this.cipher = cipher;
    this.index = index;
  }

  /**
   * Gets the original candidate index related to this cipher
   * @return int candidate index
   */
  public int getIndex() {
    return this.index;
  }

  /**
   * Gets the ECPoint array containing the actual cipher. The first element is g^r the second is my^r
   * @return ECPoint[] of two elements
   */
  public ECPoint[] getCipher() {
    return this.cipher;
  }

}
