/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents a candidate in the data structure
 * 
 * @author Chris Culnane
 * 
 */
public class Candidate {

  /**
   * String containing the name of the candidates
   */
  private String name;

  /**
   * Constructs a new candidate object with the specified JSONObject
   * 
   * @param candidateObj
   *          JSONObject containing the candidate data
   * @throws JSONException
   * 
   */
  public Candidate(JSONObject candidateObj) throws JSONException {
    super();

    this.name = candidateObj.getString("name");
  }

  /**
   * Returns the candidate name as a string
   */
  @Override
  public String toString() {
    return "\"" + this.name + "\"";
  }
}
