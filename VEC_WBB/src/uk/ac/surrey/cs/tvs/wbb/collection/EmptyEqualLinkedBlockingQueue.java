/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.collection;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This is a subclass of a LinkedBlockingQueue that has the additional property that all empty queues are considered to be equal,
 * even if they are different objects.
 * 
 * This is primarily intended to be used with the ConcurrentHashMap remove(key,value) method that performs an atomic remove if the
 * key exists and equals the value. We define a static EMPTY_QUEUE in this class that can be used to atomically remove the queue
 * from the list if it is empty. This allows us to use the internal concurrency of ConcurrentHashMap to control the removal of empty
 * lists, without having to use external synchronisation.
 * 
 * @author Chris Culnane
 * 
 */
public class EmptyEqualLinkedBlockingQueue<E> extends LinkedBlockingQueue<E> {

  /**
   * Static EMPTY QUEUE that can be used to compare against all other empty queues
   */
  public static final EmptyEqualLinkedBlockingQueue<?> EMPTY_QUEUE      = new EmptyEqualLinkedBlockingQueue<Object>(1);

  /**
   * Auto-generated serialVersionID
   */
  private static final long                            serialVersionUID = 238186681608639568L;

  /**
   * Overridden hashCode method that returns an identical hashCode for all empty queues, otherwise it defaults to the superclass
   * implementation. The identical hashCodes are achieved by returned the hashCode for the string "Empty"
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    if (this.isEmpty()) {
      return "Empty".hashCode();
    }
    else {
      return super.hashCode();
    }
  }

  /**
   * Overridden method to return true if both queues are empty, irrespective of whether they are the same object. In all other cases
   * defaults to the superclass.
   * 
   * Indicates whether some other object is "equal to" this one.
   * @param obj - the reference object with which to compare.
   * @return true if this object is the same as the obj argument or if both are empty queues; false otherwise.
   * 
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    @SuppressWarnings("unchecked")
    EmptyEqualLinkedBlockingQueue<E> other = (EmptyEqualLinkedBlockingQueue<E>) obj;
    if (other.isEmpty() && this.isEmpty()) {
      return true;
    }
    return super.equals(obj);
  }

  /**
   * Creates an EmptyEqualLinkedBlockingQueue with a capacity of Integer.MAX_VALUE. The EmptyEqualLinkedBlockingQueue is a subclass
   * of LinkedBlockingQueue that has the property that all empty queues are considered equal.
   */
  public EmptyEqualLinkedBlockingQueue() {
    super();
  }

  /**
   * Creates a EmptyEqualLinkedBlockingQueue with a capacity of Integer.MAX_VALUE, initially containing the elements of the given
   * collection, added in traversal order of the collection's iterator.
   * 
   * The EmptyEqualLinkedBlockingQueue is a subclass of LinkedBlockingQueue that has the property that all empty queues are
   * considered equal.
   * 
   * @param c
   *          - the collection of elements to initially contain
   */
  public EmptyEqualLinkedBlockingQueue(Collection<? extends E> c) {
    super(c);
  }

  /**
   * Creates a EmptyEqualLinkedBlockingQueue with the given (fixed) capacity.
   * 
   * The EmptyEqualLinkedBlockingQueue is a subclass of LinkedBlockingQueue that has the property that all empty queues are
   * considered equal.
   * 
   * @param capacity
   *          - the capacity of this queue
   */
  public EmptyEqualLinkedBlockingQueue(int capacity) {
    super(capacity);
  }

}
