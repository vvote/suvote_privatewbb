/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

/**
 * Enum for database fields.
 * 
 * @author Chris Culnane
 * 
 */
public enum DBFieldName {
  SIGNATURE_COUNT, SENT_SIGNATURE, CHECKED_SIGS, MY_SIGNATURE, SENT_TIMEOUT, CLIENT_MESSAGE, SIGNATURES_TO_CHECK, QUEUED_MESSAGE, FROM_PEER, COMMIT_TIME, POST_TIMEOUT, SK2_SIGNATURE, ID
}
