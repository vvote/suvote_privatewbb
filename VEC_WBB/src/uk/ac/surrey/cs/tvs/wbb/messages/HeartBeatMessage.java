/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * 
 * @author Chris Culnane
 * 
 */
public class HeartBeatMessage extends ExternalMessage {

  /**
   * Telemetry Logger
   */
  private static final Logger telemetry    = LoggerFactory.getLogger("Telemetry");
  /**
   * Logger
   */
  private static final Logger logger       = LoggerFactory.getLogger(HeartBeatMessage.class);
  private static final String HEARTBEAT_ID = "HEARTBEAT";

  /**
   * Constructor to create HeartBeatMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public HeartBeatMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create HeartBeatMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public HeartBeatMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructs a response to a message with a signature.
   * 
   * @param peer
   *          The WBB peer.
   * @return The response.
   * @throws TVSSignatureException
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#constructResponseAndSign(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public JSONObject constructResponseAndSign(WBBPeer peer) throws TVSSignatureException, JSONException {
    TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getSK2());
    tvsSig.update(this.getExternalSignableContent());

    JSONObject response = this.constructResponseWithoutSignature(peer);
    response.put(MessageFields.JSONWBBMessage.PEER_SIG, tvsSig.signAndEncode(EncodingType.BASE64));

    return response;
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return this.getInternalSignableContent();
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() throws JSONException {
    if (this.internalSignableContent == null) {
      // Sign cancel and id
      StringBuffer signableContentSB = new StringBuffer();
      signableContentSB.append(this.getTypeString());

      this.internalSignableContent = signableContentSB.toString();
      this.externalSignableContent = this.internalSignableContent;
    }

    return this.internalSignableContent;
  }

  /**
   * Initialise object
   * 
   * Extracts the id and sets the type as well as calculating the signable content
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.HEARTBEAT;
    this.id = HEARTBEAT_ID;
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Validate the JSON against the Cancel Schema
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.HEARTBEAT))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Checks client signature
      if (!this.validateClientSignature(peer)) {
        logger.warn("EBM signature is not valid:{}", this.msg);
        throw new MessageVerificationException("EBM signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }
  }

  /**
   * StartEVM messages are not to be included in the commit directly, only via the VoteMessage
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sendingPeer
   *          The sending WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws CommitProcessingException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#processAsPartOfCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      org.json.JSONObject, java.lang.String, java.lang.String)
   */
  @Override
  public void processAsPartOfCommitR2(WBBPeer peer, JSONObject record, String sendingPeer, String safeUploadDir)
      throws CommitProcessingException {
    throw new CommitProcessingException("HeartBeat should not be included in Commit Process");
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    // Try to store the message

    telemetry.info("MSGHEARTBEAT#{}", this);
    JSONObject response = this.constructResponseAndSign(peer);
    peer.sendAndClose(this.getMsg().getString(MessageFields.UUID), response.toString());

    return true;

  }

  /**
   * Stores the incoming message in the database.
   * 
   * @param peer
   *          The WBB peer.
   * @param signature
   *          The signature of the message.
   * @return True if the message was successfully stored.
   * @throws IOException
   * @throws UnknownDBException
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#storeIncomingMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected boolean storeIncomingMessage(WBBPeer peer, String signature) throws NoSuchAlgorithmException, InvalidKeyException,
      SignatureException, JSONException, IOException, UnknownDBException {
    throw new IOException("Method should not be called on HeartBeat Message");
  }

  /**
   * /** Stores an incoming checked peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          The message to store.
   * @param isValid
   *          Whether the message is valid.
   * @param ignoreTimeout
   *          Ignore the timeout block?
   * @param isConflict
   *          Store in the conflict collection?
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#submitIncomingPeerMessageChecked(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage, boolean, boolean, boolean)
   */
  @Override
  public void submitIncomingPeerMessageChecked(WBBPeer peer, PeerMessage msg, boolean isValid, boolean ignoreTimeout,
      boolean isConflict) throws AlreadyReceivedMessageException, AlreadySentTimeoutException {
    throw new AlreadyReceivedMessageException("Method should not be called on HeartBeat Message");
  }

  /**
   * Validates the EBM signature
   * 
   * @param peer
   *          WBBPeer this is running on - needed for access to keys
   * @return boolean of output from validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validateClientSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSCertificate clientCert = peer.getCertificateFromCerts(CertStore.EVM,this.msg.getString(JSONWBBMessage.SENDER_ID));
      if(clientCert.getBLSPublicKey()==null){
        clientCert = peer.getCertificateFromCerts(CertStore.VPS,this.msg.getString(JSONWBBMessage.SENDER_ID));
      }
      
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, clientCert);
      tvsSig.update(this.msg.getString(JSONWBBMessage.TYPE));
      
      return tvsSig.verify(this.msg.getString(JSONWBBMessage.SENDER_SIG), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }
}
