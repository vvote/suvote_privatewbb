/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipOutputStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyRespondedException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message;
import uk.ac.surrey.cs.tvs.wbb.messages.FileMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;
import uk.ac.surrey.cs.tvs.wbb.messages.types.CommitFiles;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.utils.CommitComparator;

/**
 * Performs the initial Commit action. This is called by the Timer function when a commit is due, or by any function that calls an
 * arbitrary commit.
 * 
 * It prepares a commit as if it will need to share the whole database, but doesn't initial share it. This is to ensure that the
 * contents of the commit is consistent to what this peer is committing to, even if further votes/data comes in after round 1.
 * 
 * It should be remembered that this process could be called after this peer has already received incoming commit messages from
 * other peers. As such, it will check if there are messages to processed at the end of preparing our own commit.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitProcessor extends MessageProcessor implements Runnable {

  /** Commit file full prefix. */
  private static final String COMMIT_FILE_FULL_PREFIX   = "commitFull";

  /** Commit file full extension. */
  private static final String COMMIT_FILE_FULL_EXT      = ".json";

  /** Commit file prefix. */
  private static final String COMMIT_FILE_PREFIX        = "commit";

  /** Commit file extension. */
  private static final String COMMIT_FILE_EXT           = ".json";

  /** Commit file ZIP prefix. */
  private static final String COMMIT_FILE_ZIP_PREFIX    = "commitFiles";

  /** Commit file ZIP extension. */
  private static final String COMMIT_FILE_ZIP_EXT       = ".zip";

  /**
   * Commit message type.
   */
  private static final String FINAL_COMMIT_MESSAGE_TYPE = "Commit";

  /**
   * Logger
   */
  private static final Logger logger                    = LoggerFactory.getLogger(CommitProcessor.class);

  /**
   * String to hold reference to commitTime
   */
  private String              targetCommitTime          = null;

  /**
   * String to describe the nature of the commit - this is used for extra forced commits -for example empty ballot commits
   */
  private String              commitDesc                = null;

  /**
   * Default constructor to create a CommitProcessor
   * 
   * This obtains a commitTime by calling getPreviousCommitTime. This will return the commitTime prior to the current active
   * commitTime.
   * 
   * @param peer
   *          Reference to the WBBPeer this is running on - needed to access central resources
   */
  public CommitProcessor(WBBPeer peer) {
    super(peer);
    
    //ms after midnight for the commit
    int commitHour = peer.getConfig().getTime(WBBConfig.COMMIT_TIME, TimeUnit.HOURS);
    int commitMinute =peer.getConfig().getTime(WBBConfig.COMMIT_TIME, TimeUnit.MINUTES);
    long commitMsAfterMidnight= TimeUnit.HOURS.toMillis(commitHour) + TimeUnit.MINUTES.toMillis(commitMinute);
    
    //ms after midnight for the running the commit
    int hours = peer.getConfig().getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.HOURS);
    int minutes =peer.getConfig().getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.MINUTES);
    long msAfterMidnight= TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(minutes);
    
    long time = System.currentTimeMillis();
    long days = TimeUnit.MILLISECONDS.toDays(time);
    long msLeft = time - TimeUnit.DAYS.toMillis(days);
    
    if(msLeft>commitMsAfterMidnight && msLeft<msAfterMidnight){
      //we are between the commitTime and runAtTime, so we use the previous commit
      this.targetCommitTime = peer.getPreviousCommitTime();
    }else{
      //The previous commit should have run by now, so we want to use the current commit
      this.targetCommitTime = peer.getCommitTime();
    }
    logger.info("Created commit processor for commit with ID {}",this.targetCommitTime);
    
  }

  /**
   * Constructor that allows a Commit process to be started on a specific commitTime.
   * 
   * This could be of use if trying to recover from a failed commit.
   * 
   * @param peer
   *          peer Reference to the WBBPeer this is running on - needed to access central resources
   * @param commitTimeToCommit
   *          Commit time to use
   */
  public CommitProcessor(WBBPeer peer, String commitTimeToCommit) {
    super(peer);

    this.targetCommitTime = commitTimeToCommit;
    logger.info("Created commit processor for commit with ID {}",this.targetCommitTime);
  }

  /**
   * Constructor that allows a Commit process to be started on a specific commitTime with a specific description. This is the entry
   * point for commits that are not end of day commits, i.e. extra commits
   * 
   * 
   * 
   * @param peer
   *          peer Reference to the WBBPeer this is running on - needed to access central resources
   * @param commitTimeToCommit
   *          Commit time to use
   */
  public CommitProcessor(WBBPeer peer, String commitTimeToCommit, String commitDesc) {
    super(peer);

    this.targetCommitTime = commitTimeToCommit;
    this.commitDesc = commitDesc;
    logger.info("Created commit processor for commit with ID {}",this.targetCommitTime);
  }

  /**
   * Utility method for checking and storing Round 1 commit messages from other peers.
   * 
   * 
   * @param myHash
   *          the hash of the data held by this peer
   * @param msg
   *          the message received from another peer
   * @param commitTime
   *          the commitTime this is related to
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   */
  protected void checkAndStoreCommitMessage(String myHash, CommitR1Message msg, String commitTime, String commitDesc)
      throws JSONException, AlreadyReceivedMessageException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, this.peer.getCertificateFromCerts(CertStore.PEER, msg.getMsg()
          .getString(MessageFields.Commit.PEER_ID) + WBBConfig.SIGNING_KEY_SK1_SUFFIX));
      tvsSig.update(IOUtils.decodeData(EncodingType.BASE64, myHash));
      tvsSig.update(commitTime);
      if (commitDesc != null) {
        tvsSig.update(commitDesc);

      }

      boolean valid = tvsSig.verify(msg.getMsg().getString(MessageFields.Commit.SIGNATURE), EncodingType.BASE64);

      this.peer.getDB().submitIncomingPeerCommitR1Checked(msg, valid);
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing cancel message", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing cancel message", e);
    }
  }

  /**
   * Performs the commit as a runnable action.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      JSONObject peerMsg = CommitProcessor.prepareCommitR1(this.peer, this.targetCommitTime, this.commitDesc);
      this.peer.sendToAllPeers(peerMsg.toString());

      String digest = peerMsg.getString(MessageFields.Commit.HASH);

      // Check if we have received any commitment messages from other peers
      ArrayList<CommitR1Message> sigsToSign = this.peer.getDB().getCurrentCommitSigsToCheck();
      for (CommitR1Message sigToSign : sigsToSign) {
        try {
          this.checkAndStoreCommitMessage(digest, sigToSign, this.targetCommitTime, this.commitDesc);
        }
        catch (AlreadyReceivedMessageException e) {
          logger.warn("Already received message: {} from {}. Will ignore", peerMsg.getString(MessageFields.Commit.COMMIT_TIME),
              sigToSign.getMsg().getString(MessageFields.FROM_PEER));
        }
      }
      CommitProcessor.checkCommitThreshold(this.peer);
    }
    catch (JSONException e) {
      logger.error("JSON Exception whilst trying to start a commit - a message must be corrupt", e);
    }
    catch (MessageJSONException e) {
      logger.error("Message Exception whilst trying to start a commit, indicates an error when trying to parse a message", e);
    }
    catch (AlreadyReceivedMessageException e) {
      System.err.println("A commit has already been started. It must either complete or be forced closed");
      logger.error("A commit has already been started. It must either complete or be forced closed", e);
    }
    catch (IOException e) {
      logger.error("General IOException whilst trying to start a commit", e);
    }
    catch (CommitProcessingException e) {
      logger.error("Exception whilst processing a commit", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception generating signature for commit", e);
    }
  }

  /**
   * Adds the commit files to the ZIP used in the commit message.
   * 
   * @param peer
   *          The WBB peer.
   * @param commit
   *          The commit data.
   * @param bw
   *          output file.
   * @param zos
   *          The ZIP.
   * @param md
   *          The message digest for signing.
   * @throws IOException
   * @throws JSONException
   */
  private static void addMessagesToZipFile(WBBPeer peer, ArrayList<JSONWBBMessage> commit, BufferedWriter bw, ZipOutputStream zos,
      MessageDigest md) throws IOException, JSONException {
    // Loop through each message
    for (JSONWBBMessage msg : commit) {
      // Write the message to the JSON File
      bw.write(msg.toString());
      bw.newLine();

      // Get the signable contents of the messages
      md.update(msg.getInternalSignableContent().getBytes());

      // Check for any file messages that will need including
      if (msg.getType() == Message.FILE_MESSAGE || msg.getType() == Message.BALLOT_AUDIT_COMMIT
          || msg.getType() == Message.BALLOT_GEN_COMMIT || msg.getType() == Message.MIX_RANDOM_COMMIT) {
        // We may have received this message in a previous commit attempt that has failed. As such, it could be held in
        // safeUploadDir
        File attached;

        if (msg.getMsg().has(MessageFields.PeerFile.SAFE_UPLOAD_DIR)) {
          attached = new File(msg.getMsg().getString(MessageFields.PeerFile.SAFE_UPLOAD_DIR) + File.separator
              + ((FileMessage) msg).getFilename());
        }
        else {
          attached = new File(peer.getUploadDir().getAbsolutePath() + File.separator + ((FileMessage) msg).getFilename());
        }

        ZipUtil.addFileToZip(attached, md, zos);
      }
    }
  }

  /**
   * Checks that the commit threshold has been reached and if it has, sends the commit data to the Public WBB.
   * 
   * @param peer
   *          The peer WBB peer.
   * @throws JSONException
   * @throws MessageJSONException
   * @throws TVSSignatureException
   */
  public static void checkCommitThreshold(WBBPeer peer) throws JSONException, MessageJSONException, TVSSignatureException {
    // Check if we have reached the threshold - note the threshold is all peers
    if (peer.getDB().checkCommitThreshold(peer.getPeerCount() + 1)) { // +1 to include self peer.
      try {
        CommitProcessor.sendAndStoreCompletedCommitSK2(peer);
      }
      catch (NoCommitExistsException e) {
        logger
            .error(
                "Current Commit is null even though signatures have been validated. Database must be corrupt or underlying current commit changed whilst running protocol. ",
                e);
      }
    }
    else {
      // We haven't reached consensus yet, check if we still can
      if (!peer.getDB().canReachCommitConsensus(peer.getPeerCount() + 1, peer.getPeerCount() + 1)
          && peer.getDB().shouldRunR2Commit()) {
        // check we haven't already started R2
        try {
          String commitID = peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_ID);
          String targetCommitTime = peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_TIME);

          // Send the full database to all other peers
          String commitFileFull = peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_FILE_FULL);
          String commitAttachments = peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_ATTACHMENTS);
          peer.sendR2CommitFileToAllPeers(commitFileFull, commitAttachments, commitID, targetCommitTime);

          // Process any waiting Round 2 messages from other peers
          ArrayList<JSONWBBMessage> queue = peer.getDB().getR2Queue();
          for (JSONWBBMessage m : queue) {
            peer.getCommitExecutor().execute(new InternalMessageProcessor(m, peer));
          }
        }
        catch (NoCommitExistsException e) {
          logger.error("Checked whether to run R2 of commit and got True, however, the commit has been closed. Should not happen.",
              e);
        }
      }
    }
  }

  /**
   * Creates the files needed for a commit.
   * 
   * @param peer
   *          The WBB Peer.
   * @return A map of the files and their locations.
   * @throws IOException
   */
  public static HashMap<CommitFiles, File> createCommitFiles(WBBPeer peer) throws IOException {
    HashMap<CommitFiles, File> files = new HashMap<CommitFiles, File>();

    File commitsFolder = new File(peer.getConfig().getString(WBBConfig.COMMIT_DIRECTORY));
    IOUtils.checkAndMakeDirs(commitsFolder);
    
    files.put(CommitFiles.COMMIT_FOLDER, commitsFolder);

    // Prepare fullCommitFile. Note that it is not a temporary file, the method is used to generated a unique name
    File commitFullFile = File.createTempFile(CommitProcessor.COMMIT_FILE_FULL_PREFIX, CommitProcessor.COMMIT_FILE_FULL_EXT,
        commitsFolder);
    files.put(CommitFiles.COMMIT_FILE_FULL, commitFullFile);
    File zipCommitFull = File.createTempFile(CommitProcessor.COMMIT_FILE_ZIP_PREFIX, CommitProcessor.COMMIT_FILE_ZIP_EXT,
        commitsFolder);
    files.put(CommitFiles.COMMIT_FILE_FULL_ZIP, zipCommitFull);
    File commitFile = File.createTempFile(CommitProcessor.COMMIT_FILE_PREFIX, CommitProcessor.COMMIT_FILE_EXT, commitsFolder);
    files.put(CommitFiles.COMMIT_FILE, commitFile);

    return files;
  }

  /**
   * Gets all of the data needed for a commit session.
   * 
   * @param peer
   *          The WBB peer.
   * @param commitFullFile
   *          The output commit file.
   * @param targetCommitTime
   *          The commit session.
   * @return The commit messages.
   * @throws CommitProcessingException
   */
  private static ArrayList<JSONWBBMessage> getCommitData(WBBPeer peer, File commitFullFile, String targetCommitTime)
      throws CommitProcessingException {
    // We will need to sort the contents of the commit to ensure a uniform order across peers
    ArrayList<JSONWBBMessage> commit = new ArrayList<JSONWBBMessage>();

    try {
      // Get commit data from the DB and write the full set of data to the full file
      commit.addAll(peer.getDB().getCommitData(peer.getThreshold(), commitFullFile, targetCommitTime));
    }
    catch (MessageJSONException | IOException e1) {
      logger
          .error(
              "Exception thrown whilst reading commit data. This indicates invalid JSON message is stored in DB. DB is potentially corrupted. Can't commit.",
              e1);
      throw new CommitProcessingException("Can't commit because data in the DB is invalid", e1);
    }

    // Sort the data
    Collections.sort(commit, new CommitComparator());

    return commit;
  }

  /**
   * Packages the commit files into a signed ZIP.
   * 
   * @param peer
   *          The WBB peer.
   * @param targetCommitTime
   *          The target commit session.
   * @param files
   *          The prepared files.
   * @return The signed ZIP of the commit files.
   * @throws CommitProcessingException
   */
  public static byte[] prepareCommitFiles(WBBPeer peer, String targetCommitTime, HashMap<CommitFiles, File> files)
      throws CommitProcessingException {
    try {
      // Get the commit messages (and store them in the full file).
      ArrayList<JSONWBBMessage> commit = CommitProcessor.getCommitData(peer, files.get(CommitFiles.COMMIT_FILE_FULL),
          targetCommitTime);

      Collections.sort(commit, new CommitComparator());
      // Prepare a zip file that can contain any FileMessages
      ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(files.get(CommitFiles.COMMIT_FILE_FULL_ZIP)));

      // Prepare a digest for the Round 1 commitment
      MessageDigest md = peer.getConfig().getMessageDigest();

      // Again, not an actual temp file, just a unique file
      BufferedWriter bw = new BufferedWriter(new FileWriter(files.get(CommitFiles.COMMIT_FILE)));

      try {
        CommitProcessor.addMessagesToZipFile(peer, commit, bw, zos, md);
      }
      finally {
        try {
          bw.close();
        }
        finally {
          zos.close();
        }
      }

      // Calculate digest
      return md.digest();
    }
    catch (IOException | JSONException | NoSuchAlgorithmException e) {
      logger.error("Exception whilst preparing commit files", e);
      throw new CommitProcessingException("Exception whilst preparing commit file", e);
    }
  }

  /**
   * Prepares for a commit round 1.
   * 
   * @param peer
   *          The WBB peer.
   * @param targetCommitTime
   *          The target commit session.
   * @param commitDesc
   *          Description of the commit or null if end of day commit
   * @return The commit message to be sent.
   * @throws IOException
   * @throws CommitProcessingException
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws MessageJSONException
   * @throws AlreadyReceivedMessageException
   */
  public static JSONObject prepareCommitR1(WBBPeer peer, String targetCommitTime, String commitDesc) throws IOException,
      CommitProcessingException, TVSSignatureException, JSONException, MessageJSONException, AlreadyReceivedMessageException {
    HashMap<CommitFiles, File> files = CommitProcessor.createCommitFiles(peer);
    // Package up all of the files
    byte[] digest = CommitProcessor.prepareCommitFiles(peer, targetCommitTime, files);

    // Calculate digest
    TVSSignature tvssig = new TVSSignature(SignatureType.DEFAULT, peer.getSK1());

    tvssig.update(digest);
    tvssig.update(targetCommitTime);
    if (commitDesc != null) {
      tvssig.update(commitDesc);
    }
    String signature = tvssig.signAndEncode(EncodingType.BASE64);
    String commitID = UUID.randomUUID().toString();

    JSONObject peerMsg = new JSONObject();
    peerMsg.put(MessageFields.TYPE, CommitR1Message.TYPE_STRING);
    peerMsg.put(MessageFields.Commit.COMMIT_ID, commitID);
    peerMsg.put(MessageFields.Commit.HASH, IOUtils.encodeData(EncodingType.BASE64, digest));
    peerMsg.put(MessageFields.Commit.SIGNATURE, signature);
    peerMsg.put(MessageFields.Commit.COMMIT_TIME, targetCommitTime);
    peerMsg.put(MessageFields.Commit.PEER_ID, peer.getID());
    peerMsg.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    if (commitDesc != null) {
      peerMsg.put(MessageFields.Commit.COMMIT_DESC, commitDesc);
    }

    JSONWBBMessage msgToSend = JSONWBBMessage.parseMessage(peerMsg);

    // Record our commitment
    peer.getDB().createNewCommitRecord(msgToSend, files.get(CommitFiles.COMMIT_FILE).getName(),
        files.get(CommitFiles.COMMIT_FILE_FULL).getName(), files.get(CommitFiles.COMMIT_FILE_FULL_ZIP).getName(), targetCommitTime,
        commitDesc);

    return peerMsg;
  }

  /**
   * Sends and stores the commit signature.
   * 
   * @param peer
   *          The WBB peer.
   * @throws NoCommitExistsException
   * @throws TVSSignatureException
   * @throws JSONException
   */
  public static void sendAndStoreCompletedCommitSK2(WBBPeer peer) throws NoCommitExistsException, TVSSignatureException,
      JSONException {
    // If we have we can construct the SK2 signature and send the message to the Public WBB.
    String myHash = peer.getDB().getCurrentCommitHash();
    TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getSK2());
    tvsSig.update(CommitProcessor.FINAL_COMMIT_MESSAGE_TYPE);
    tvsSig.update(peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_TIME).getBytes());
    tvsSig.update(IOUtils.decodeData(EncodingType.BASE64, myHash));
    String commitDesc = peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_DESC);
    if (commitDesc != null) {
      logger.info("Commit Description is non-null: {}", commitDesc);

      tvsSig.update(commitDesc);
    }

    JSONObject response = new JSONObject();
    response.put(MessageFields.TYPE, CommitProcessor.FINAL_COMMIT_MESSAGE_TYPE);
    response.put(MessageFields.CommitSK2.HASH, myHash);
    response.put(MessageFields.CommitSK2.COMMIT_TIME, peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_TIME));
    response.put(MessageFields.CommitSK2.PEER_ID, peer.getID());
    if (commitDesc != null) {
      response.put(MessageFields.CommitSK2.COMMIT_DESC, commitDesc);
    }
    String encSig = tvsSig.signAndEncode(EncodingType.BASE64);
    response.put(MessageFields.CommitSK2.PEER_SIG, encSig);
    File commitsFolder = new File(peer.getConfig().getString(WBBConfig.COMMIT_DIRECTORY));
    File commitFile = new File(commitsFolder, peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_FILE));
    File commitFileAttachments = new File(commitsFolder, peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_ATTACHMENTS));
    response.put(MessageFields.CommitSK2.FILE_SIZE, commitFile.length());
    response.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, commitFileAttachments.length());

    try {
      if (!peer.getDB().setCommitSentSig(encSig)) {
        throw new NoCommitExistsException("Exception whilst setting commit, record either doesn't exist or is corrupt");
      }
      peer.sendToPublicWBB(response.toString());
    }
    catch (AlreadyRespondedException e) {
      logger.info("Have already recorded an SK2 signature, will not repeat: {}", e.getMessage());
    }
  }

  /**
   * Called to force close the current commit
   * 
   * @param peer
   *          WBBPeer reference to the peer we are running on
   * @return boolean true if successful, false if not completely successful
   * @throws NoCommitExistsException
   */
  public static boolean forceCloseCurrentCommit(WBBPeer peer) throws NoCommitExistsException {
    boolean result = peer.getDB().forceCloseCurrentCommit();
    logger.error("A commit was force closed: {}. This shouldn't normally happen, hence it is logged as an error", result);
    return result;
  }
}
