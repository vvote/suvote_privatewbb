/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.validation;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;

/**
 * Utility class for loading and accessing schemas
 * 
 * The schemas are only loaded once to improve performance. They are not of sufficient size for us to worry about the memory cost of
 * caching.
 * 
 * They are reported to be thread safe so can be used concurrently by multiple threads.
 * 
 * @author Chris Culnane
 * 
 */
public class JSONValidator {

  /**
   * Logger
   */
  private static final Logger           logger      = LoggerFactory.getLogger(JSONValidator.class);

  /**
   * Hashmap indexed by the JSONSchema enum that stores the relevant String of the json Schema
   */
  protected HashMap<JSONSchema, String> schemas     = new HashMap<JSONSchema, String>();

  /**
   * Schema ID field.
   */
  private static final String           SCHEMA_ID   = "id";

  /**
   * Schema file field.
   */
  private static final String           SCHEMA_FILE = "schemaFile";

  /**
   * Constructor that loads the schemas.
   * 
   */
  public JSONValidator(WBBPeer peer) {
    super();

    try {
      JSONArray schemaArray = IOUtils.readJSONArrayFromFile(peer.getConfig().getString(WBBConfig.SCHEMA_LIST));
      for (int i = 0; i < schemaArray.length(); i++) {

        JSONObject schema = schemaArray.getJSONObject(i);
        try {
          this.schemas.put(JSONSchema.valueOf(schema.getString(JSONValidator.SCHEMA_ID)),
              IOUtils.readStringFromFile(schema.getString(JSONValidator.SCHEMA_FILE)));
        }
        catch (IOException e) {
          logger.error("Exception whilst loading schema {}", schema.getString(JSONValidator.SCHEMA_FILE), e);
        }
      }
    }
    catch (JSONIOException | JSONException eio) {
      logger.error("Exception whilst loading schema list file  {}", peer.getConfig().getString(WBBConfig.SCHEMA_LIST), eio);
    }
  }

  /**
   * Gets the requested schema from the cache
   * 
   * @param schema
   *          the JSONSchema type to get
   * @return JsonSchema
   */
  public String getSchema(JSONSchema schema) {
    return this.schemas.get(schema);
  }
}
