/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.config;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;

/**
 * Provides access to the JSON Configuration for this peer.
 * 
 * A number of values are pre-loaded and cached to save having to read them every time.
 * 
 * @author Chris Culnane
 * 
 */
public class WBBConfig {

  /**
   * Configuration fields.
   */
  public static final String          MESSAGE_DIGEST                            = "messageDigest";
  public static final String          TIME_STRING_SEPARATOR                     = ":";
  public static final String          SUBMISSION_TIMEOUT                        = "submissionTimeout";
  public static final String          SSL_TIMEOUT_MAX                           = "sslTimeoutMax";
  public static final String          SOCKET_TIMEOUT_INITIAL                    = "socketTimeoutInitial";
  public static final String          SOCKET_TIMEOUT_MAX                        = "socketTimeoutMax";
  public static final String          SSL_TIMEOUT_INITIAL                       = "sslTimeoutInitial";

  public static final String          COMMIT_UPLOAD_ATTACHMENT_DIRECTORY_PREFIX = "CommitAttachments";
  public static final String          COMMIT_UPLOAD_ATTACHMENT_PREFIX           = "CommitUploadAttachment";
  public static final String          COMMIT_UPLOAD_ATTACHMENT_EXTENSION        = ".zip";
  public static final String          DEFAULT_MESSAGE_DIGEST                    = SystemConstants.DEFAULT_MESSAGE_DIGEST;
  public static final String          BUFFER_BOUND                              = "bufferBound";
  public static final String          DISTRICT_CONFIG                           = "districtConf";
  public static final String          BASE_ENCRYPTED_IDS_FILE                   = "baseEncryptedIds";

  public static final String          BALLOT_TIMEOUT                            = "ballotTimeout";
  public static final String          ADDRESS                                   = "address";
  public static final String          PORT                                      = "port";
  public static final String          PEER_ID                                   = "id";
  public static final String          PATH_TO_KEYSTORE                          = "pathToKeyStore";
  public static final String          KEYSTORE_PASSWORD                         = "keyStorePwd";
  public static final String          UPLOAD_DIR                                = "uploadDirectory";
  public static final String          COMMIT_UPLOAD_DIR                         = "commitUploadDirectory";
  public static final String          CERTIFICATE_KEYSTORE                      = "certificateKeyStore";
  public static final String          CERTIFICATE_KEYSTORE_PASSWORD             = "certificateKeyStorePwd";

  public static final String          SIGNING_KEY_KEYSTORE                      = "pathToSigningKey";
  public static final String          SIGNING_KEY_KEYSTORE_PASSWORD             = "signingKeyPwd";
  public static final String          SIGNING_KEY_ALIAS                         = "alias";
  public static final String          SIGNING_KEY_SK1_PASSWORD                  = "sk1Pwd";
  public static final String          SIGNING_KEY_SK2_PASSWORD                  = "sk2Pwd";
  public static final String          SIGNING_KEY_SK2_SUFFIX                    = SystemConstants.SIGNING_KEY_SK2_SUFFIX;
  public static final String          SIGNING_KEY_SK1_SUFFIX                    = SystemConstants.SIGNING_KEY_SK1_SUFFIX;

  public static final String          SSL_KEY_SUFFIX                            = "_SSL";
  public static final String          EXTERNAL_PORT                             = "extPort";

  public static final String          HOST_ADDRESS                              = "address";
  public static final String          HOST_PORT                                 = "port";

  public static final String          COMMIT_TIME                               = "commitTime";
  public static final String          RUN_COMMIT_AT                             = "runCommitAt";
  public static final String          COMMIT_DIRECTORY                          = "commitDirectory";
  public static final String          THRESHOLD                                 = "threshold";
  public static final String          SCHEMA_LIST                               = "schemaList";
  public static final String          FILE_UPLOAD_TIMEOUT                       = "fileUploadTimeout";
  public static final String          PEERS                                     = "peers";

  public static final String          MAX_ZIP_ENTRY                             = "max_zip_entry";

  public static final String          SSL_CONFIG                                = "SSLConfig";
  public static final String          SSL_CIPHER_DEFAULT                        = "default";

  public static final String          CERT_STORES_SPEC                          = "certStores";
  public static final String          RACES                                     = "races";
  public static final String          CANDIDATE_COUNT                           = "candidates";
  public static final String          LIVE_CHECK_POD                            = "liveCheckPOD";
  public static final String          ELECTION_PUBLIC_KEY                       = "electionPublicKey";
  public static final String          PUBLIC_WBB_ADDRESS                        = "publicWBBAddress";
  public static final String          PUBLIC_WBB_PORT                           = "publicWBBPort";
  public static final String          CRL_FOLDER                           = "CRLFolder";

  /**
   * Stores a JSON object for each Peer listed in the config file. The JSON object provides access to the address and port of the
   * peer. It is indexed by the Peer ID
   */
  private HashMap<String, JSONObject> peers                                     = new HashMap<String, JSONObject>();

  /**
   * Initial timeout for a generic socket failure. This is not an an uncommon occurrence, for example, during startup peers will be
   * unable to connect to each other until they are all running. This time represents the initial wait period following an error.
   * Like the SSL Wait time it will grow logarithmically up to the max time.
   */
  private int                         socketFailTimeoutInitial                  = 2000;

  /**
   * Max delay the socket will wait before trying to reconnect. This should not be too large, since it is not uncommon for socket
   * errors to occur. This delay will determine the maximum amount of time that will need to be waited until the connection is tried
   * again.
   */
  private int                         socketFailTimeoutMax                      = 2000;

  /**
   * Represents the period within which a message should have reached consensus across the peers and a response sent. If it expires
   * a failure message will be returned to the client.
   */
  private int                         submissionTimeout                         = 30;

  /**
   * int of the threshold of valid responses required - must be strictly more than 2/3 of the number of peers
   */
  private int                         threshold;

  /**
   * Logger
   */
  private static final Logger         logger                                    = LoggerFactory.getLogger(WBBConfig.class);

  /**
   * JSONObject with system wide district configuration, which specifies the number of candidates in each district and race
   */
  private JSONObject                  districtConf;

  /**
   * Underlying JSON object that contains the configuration file
   */
  private JSONObject                  config;

  /**
   * JSONArray of base encrypted candidate IDs - these are the candidate IDs encrypted with a fixed randomness and used during
   * ballot generation
   */
  private JSONArray                   baseEncryptedIDs;
  /**
   * The maximum size allowed for the read Buffer, this is effectively the maximum amount of data that can be sent in a message
   */
  private int                         bufferBound;

  /**
   * Static variable to point towards schema for the config file
   */
  private static final String         SCHEMA_LOCATION                           = "./schemas/wbbconfigschema.json";

  /**
   * boolean that determines whether to check ballot reductions in real time
   */
  private boolean                     liveCheckPOD                              = false;

  /**
   * ECPoint containing the election public key
   */
  private ECPoint                     electionPublicKey;

  /**
   * Instance of ECUtils to be used throughout - saves having to construct multiple instances
   */
  private ECUtils                     ecUtils                                   = new ECUtils();

  /**
   * integer array to pre-cache the sizes of the races, used during Ballot Reduction
   */
  private int[]                       raceSizes                                 = null;

  /**
   * Constructor for WBBConfig
   * 
   * Loads the JSON file and sets the hard-coded properties.
   * 
   * 
   * @param file
   *          path to JSON configuration file
   * @throws JSONSchemaValidationException
   */
  public WBBConfig(String file) throws JSONSchemaValidationException {
    try {
      logger.info("Starting load of configuration file: {}", file);

      // Loads the configuration file - this is a trusted file so we don't use a bounded reader object
      String configString = IOUtils.readStringFromFile(file);

      // Check the config file validates
      if (!JSONUtils.validateSchema(JSONUtils.loadSchema(WBBConfig.SCHEMA_LOCATION), configString)) {
        throw new JSONSchemaValidationException("Config file failed schema validation");
      }

      // Load the string into a JSON object
      this.config = new JSONObject(configString);

      this.bufferBound = this.config.getInt(WBBConfig.BUFFER_BOUND);
      this.threshold = this.config.getInt(WBBConfig.THRESHOLD);
      // Load the district configuration
      this.districtConf = IOUtils.readJSONObjectFromFile(this.config.getString(WBBConfig.DISTRICT_CONFIG));
      // Extract the peers from the configuration file
      JSONArray hostArray = this.config.getJSONArray(WBBConfig.PEERS);
      for (int i = 0; i < hostArray.length(); i++) {
        JSONObject host = hostArray.getJSONObject(i);
        String hostIndex = host.getString(WBBConfig.PEER_ID);
        this.peers.put(hostIndex, host);
      }
      this.electionPublicKey = this.ecUtils.pointFromJSON(IOUtils.readJSONObjectFromFile(this.config
          .getString(WBBConfig.ELECTION_PUBLIC_KEY)));
      this.baseEncryptedIDs = IOUtils.readJSONArrayFromFile(this.config.getString(WBBConfig.BASE_ENCRYPTED_IDS_FILE));

      // Set hard-coded properties from the config file
      this.socketFailTimeoutInitial = this.config.getInt(WBBConfig.SOCKET_TIMEOUT_INITIAL);
      this.socketFailTimeoutMax = this.config.getInt(WBBConfig.SOCKET_TIMEOUT_MAX);
      this.submissionTimeout = this.config.getInt(WBBConfig.SUBMISSION_TIMEOUT);
      this.liveCheckPOD = this.config.getBoolean(WBBConfig.LIVE_CHECK_POD);
      JSONArray raceArr = this.config.getJSONArray(WBBConfig.RACES);
      this.raceSizes = new int[raceArr.length()];
      for (int i = 0; i < raceArr.length(); i++) {
        JSONObject raceObj = raceArr.getJSONObject(i);
        this.raceSizes[i] = raceObj.getInt(WBBConfig.CANDIDATE_COUNT);
      }
      logger.info("Finished loading Peer config: {}", file);
    }
    catch (IOException e) {
      logger.error("Failed to load WBBConfig", e);
    }
    catch (JSONException e) {
      logger.error("WBBConfig is malformed", e);
    }
    catch (JSONIOException e) {
      logger.error("JSON Config file is malformed", e);
    }
  }

  /**
   * Checks whether the system should be performing live checking of ballot reductions
   * 
   * @return boolean true if live checking should be performed, false if not
   */
  public boolean doLivePODCheck() {
    return this.liveCheckPOD;
  }

  /**
   * Gets a JSONArray of base encrypted Candidate IDs. These are the IDs that are encrypted with a fixed randomness and are used as
   * the input to the ballot generation. These are required for live checking the ballot reduction.
   * 
   * @return JSONArray containing the base encrypted Candidate IDs
   */
  public JSONArray getBaseEncryptedIDs() {
    return this.baseEncryptedIDs;
  }

  /**
   * @return The buffer bound.
   */
  public int getBufferBound() {
    return this.bufferBound;
  }

  /**
   * @return The district configuration.
   */
  public JSONObject getDistrictConf() {
    return this.districtConf;
  }

  /**
   * Gets the instance of ECUtils used throughout the MBB. This saves having to construct multiple instances across the other
   * classes
   * 
   * @return ECUtils instance
   */
  public ECUtils getECUtils() {
    return this.ecUtils;
  }

  /**
   * Gets the election public key - this is important for performing live ballot reduction
   * 
   * @return ECPoint containing the Election Public Key
   */
  public ECPoint getElectionPublicKey() {
    return this.electionPublicKey;
  }

  /**
   * Gets the JSON object for the requested host
   * 
   * @param id
   *          PeerID of the required host
   * @return JSONObject containing host address and port
   */
  public JSONObject getHostDetails(String id) {
    return this.peers.get(id);
  }

  /**
   * Gets an int property value
   * 
   * @param key
   *          name of property to get
   * @return int value of the requested property or 0 if it does not exist or is not an integer
   */
  public int getInt(String key) {
    try {
      return this.config.getInt(key);
    }
    catch (JSONException e) {
      logger.error("WBBConfig JSON Exception when trying to get {} from WBBConfig", key, e);
    }
    return 0;
  }

  /**
   * @return The message digest.
   * @throws NoSuchAlgorithmException
   */
  public MessageDigest getMessageDigest() throws NoSuchAlgorithmException {
    if (this.config.has(WBBConfig.MESSAGE_DIGEST)) {
      try {
        return MessageDigest.getInstance(this.config.getString(WBBConfig.MESSAGE_DIGEST));
      }
      catch (JSONException e) {
        logger.error("Exception whilst getting message digest from WBB Config. Using {} instead", WBBConfig.DEFAULT_MESSAGE_DIGEST,
            e);
        return MessageDigest.getInstance(WBBConfig.DEFAULT_MESSAGE_DIGEST);
      }
    }
    else {
      return MessageDigest.getInstance(WBBConfig.DEFAULT_MESSAGE_DIGEST);
    }
  }

  /**
   * @return The map of peers.
   */
  public HashMap<String, JSONObject> getPeers() {
    return this.peers;
  }

  /**
   * Gets the integer array of race sizes<br>
   * 0 - district race size<br>
   * 1 - region race ATL size <br>
   * 2 - region race BTL size <br>
   * 
   * @return integer array of races sizes
   */
  public int[] getRaceSizes() {

    return this.raceSizes;
  }

  /**
   * Gets the initial wait time for a general socket error
   * 
   * @return milliseconds to wait before retrying
   */
  public int getSocketFailTimeoutInitial() {
    return this.socketFailTimeoutInitial;
  }

  /**
   * Get the maximum amount of time to wait after successive socket failures
   * 
   * @return milliseconds to wait before retrying
   */
  public int getSocketFailTimeoutMax() {
    return this.socketFailTimeoutMax;
  }

  /**
   * Gets a string property value
   * 
   * @param key
   *          name of property to get
   * @return String value of the property or empty string if property does not exist or is not a string
   */
  public String getString(String key) {
    try {
      return this.config.getString(key);
    }
    catch (JSONException e) {
      logger.error("WBBConfig JSON Exception when trying to get {} from WBBConfig", key, e);
    }
    return null;
  }

  /**
   * Gets the submission timeout - period to wait before return error to client
   * 
   * @return seconds to wait before timing out
   */
  public int getSubmissionTimeout() {
    return this.submissionTimeout;
  }

  /**
   * @return The threshold.
   */
  public int getThreshold() {
    return this.threshold;
  }

  /**
   * Gets requested time property extracts the requested TimeUnit from it
   * 
   * @param key
   *          name of property to get from config
   * @param unit
   *          TimeUnit (Hours or Minutes) to return
   * @return int representing the relevant time unit or -1 if invalid TimeUnit or key is requested
   */
  public int getTime(String key, TimeUnit unit) {
    if (this.config.has(key)) {
      try {
        if (unit == TimeUnit.HOURS) {
          String value = this.config.getString(key);
          return Integer.parseInt(value.substring(0, value.indexOf(WBBConfig.TIME_STRING_SEPARATOR)));
        }
        else if (unit == TimeUnit.MINUTES) {
          String value = this.config.getString(key);
          return Integer.parseInt(value.substring(value.indexOf(WBBConfig.TIME_STRING_SEPARATOR) + 1));
        }
        else {
          return -1;
        }
      }
      catch (JSONException e) {
        logger.error("Exception whilst getting time from WBB Config", e);
      }
    }

    return -1;
  }
}
