/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a message sent from the BallotManager who is constructing a request for authorisation to use a ballot from a threshold
 * set of POD Peers
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class PODMessage extends ExternalMessage {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(PODMessage.class);

  /**
   * Constructor to create PODMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public PODMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create PODMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public PODMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Checks if we have reached consensus on a cancellation message and if not, whether it is possible to do so.
   * 
   * @param peer
   *          The WBB peer.
   * @param threshold
   *          integer of the threshold we need to reach
   * @param peerCount
   *          integer of total number of peers
   * @return boolean - true if have or can reach consensus, false otherwise
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#canOrHaveReachedConsensus(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer, int, int)
   */
  @Override
  protected boolean canOrHaveReachedConsensus(WBBPeer peer, int threshold, int peerCount) {
    return peer.getDB().canOrHaveReachedConsensus(DBRecordType.POD, this.getID(), threshold, peerCount);
  }

  /**
   * Checks whether we have reached a consensus on a message and if we have sent a response.
   * 
   * @param peer
   *          The WBB peer.
   * @param threshold
   *          integer threshold to check.
   * @return boolean - true if we have reached consensus and should send response, otherwise false
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#checkThresholdAndResponse(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer, int)
   */
  @Override
  protected boolean checkThresholdAndResponse(WBBPeer peer, int threshold) {
    return peer.getDB().checkThresholdAndResponse(DBRecordType.POD, this.getID(), peer.getThreshold(),
        peer.getConfig().getInt(WBBConfig.BALLOT_TIMEOUT));
  }

  /**
   * Gets the original client message.
   * 
   * @param peer
   *          The WBB peer.
   * @param id
   *          The message id.
   * @return A String representation of the message.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#getClientMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer, java.lang.String)
   */
  @Override
  protected String getClientMessage(WBBPeer peer, String id) {
    return peer.getDB().getClientMessage(DBRecordType.POD, id);
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    if (this.externalSignableContent == null) {
      // we sign the serialNo and district
      StringBuffer signableContentSB = new StringBuffer();
      signableContentSB.append(this.msg.getString(JSONWBBMessage.ID));
      signableContentSB.append(this.msg.getString(JSONWBBMessage.DISTRICT));
      this.externalSignableContent = signableContentSB.toString();
    }

    return this.externalSignableContent;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() throws JSONException {
    if (this.internalSignableContent == null) {
      // we sign the serialNo, district, ballot reductions and commitTime
      StringBuffer signableContentSB = new StringBuffer();
      signableContentSB.append(this.msg.getString(JSONWBBMessage.ID));
      signableContentSB.append(this.msg.getString(JSONWBBMessage.DISTRICT));
      signableContentSB.append(this.msg.getJSONArray(MessageFields.PODMessage.BALLOT_REDUCTIONS).toString());
      signableContentSB.append(this.commitTime);
      this.internalSignableContent = signableContentSB.toString();
    }

    return this.internalSignableContent;
  }

  /**
   * Gets an ArrayList of PeerMessages that need to be checked
   * 
   * @param peer
   *          The WBB peer.
   * @param id
   *          The message id.
   * @return ArrayList of PeerMessages to check, or an empty list
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#getSigsToCheck(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer, java.lang.String)
   */
  @Override
  protected ArrayList<? extends PeerMessage> getSigsToCheck(WBBPeer peer, String id) throws MessageJSONException {
    return peer.getDB().getSigsToCheck(DBRecordType.POD, id);
  }

  /**
   * Init method for this message. Sets the type, extracts the serialNo
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.POD_MESSAGE;
    this.id = this.msg.getString(JSONWBBMessage.ID);
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Validate the message against a JSON Schema
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.POD))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Check the POD Client signature
      if (!this.validatePODClientSignature(peer)) {
        logger.warn("POD Client signature is not valid:{}", this.msg);
        throw new MessageVerificationException("POD Client signature is not valid");
      }

      // Check the Ballot Reduction - not full check
      if (!this.validateBallotReduction(peer)) {
        logger.warn("POD Client ballot reduction is not valid:{}", this.msg);
        throw new MessageVerificationException("POD Client ballot reduction is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }

  }

  /**
   * Processes the messages as part of commit round 2.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sendingPeer
   *          The sending WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#processAsPartOfCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      org.json.JSONObject, java.lang.String, java.lang.String)
   */
  @Override
  public void processAsPartOfCommitR2(WBBPeer peer, JSONObject record, String sendingPeer, String safeUploadDir) {
    try {
      this.doProcessAsPartOfCommitR2(peer, record, sendingPeer, safeUploadDir, DBRecordType.POD);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("No such algorithm exception whilst checking and storing cancel message", e);
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing cancel message", e);
    }
    catch (MessageJSONException e) {
      logger.error("Message Exception when processing R2 POD message record - indicates malformed message - will ignore record", e);
    }
    catch (MessageVerificationException e) {
      logger
          .error(
              "Message Verification Exception when processing R2 POD message record - indicates malformed message - will ignore record",
              e);
    }
    catch (AlreadyReceivedMessageException e) {
      // We don't log this because it is normal if our database is consistent
    }
    catch (JSONException e) {
      logger.error("JSON Exception whilst processing R2 POD record - indicates malformed message - record discarded", e);
    }
    catch (AlreadySentTimeoutException e) {
      logger
          .error(
              "Timeout Exception occured whilst processing an R2 record - this shouldn't happen, timeouts should be overridden during commit",
              e);
    }
    catch (JSONSchemaValidationException e) {
      logger.error(
          "JSON Schema Validation failed when processing R2 POD message record - indicates malformed message - will ignore record",
          e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing cancel message", e);
    }
  }

  /**
   * Stores the incoming message in the database.
   * 
   * @param peer
   *          The WBB peer.
   * @param signature
   *          The signature of the message.
   * @return True if the message was successfully stored.
   * @throws IOException
   * @throws UnknownDBException
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#storeIncomingMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected boolean storeIncomingMessage(WBBPeer peer, String signature) throws IOException, UnknownDBException,
      NoSuchAlgorithmException, InvalidKeyException, SignatureException, JSONException {
    try {
      peer.getDB().submitIncomingClientMessage(DBRecordType.POD, this, signature);

      // If we get here the message has been stored, otherwise an exception would have been thrown
      return true;
    }
    catch (AlreadyReceivedMessageException e) {
      // Serial number already used or message already received - either way the message is rejected.
      logger.warn("Serial No {} already used. Sending error message to Client. msg:{}", this.getID(), this.msg);
      SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.SERIALNO_USED);

      return false;
    }
  }

  /**
   * Stores an incoming checked peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          The message to store.
   * @param isValid
   *          Whether the message is valid.
   * @param ignoreTimeout
   *          Ignore the timeout block?
   * @param isConflict
   *          Store in the conflict collection?
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#submitIncomingPeerMessageChecked(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage, boolean, boolean, boolean)
   */
  @Override
  public void submitIncomingPeerMessageChecked(WBBPeer peer, PeerMessage msg, boolean isValid, boolean ignoreTimeout,
      boolean isConflict) throws AlreadyReceivedMessageException, AlreadySentTimeoutException {
    peer.getDB().submitIncomingPeerMessageChecked(DBRecordType.POD, msg, isValid, ignoreTimeout, isConflict);
  }

  /**
   * Validates the ballot reductions in the message.
   * 
   * @param peer
   *          The WBB peer.
   * @return boolean true if the ballot reduction is value, false if not
   */
  public boolean validateBallotReduction(WBBPeer peer) {
    int[] raceSizes = peer.getConfig().getRaceSizes();

    try {
      JSONObject districtData = peer.getConfig().getDistrictConf().getJSONObject(this.msg.getString(JSONWBBMessage.DISTRICT));
      
      JSONArray ballotReductions = this.msg.getJSONArray(MessageFields.PODMessage.BALLOT_REDUCTIONS);
      JSONObject ballot = peer.getDB().getBallotRecord(this.id);
      JSONArray baseEncIDs = peer.getConfig().getBaseEncryptedIDs();
      ECUtils ecUtils = peer.getConfig().getECUtils();

      if (ballot == null) {
        logger.warn("No ballot record present, cannot check ciphers");
        return false;
      }

      int cipherOffset = 0;

      for (int raceCount = 0; raceCount < ballotReductions.length(); raceCount++) {
        JSONArray raceReductions = ballotReductions.getJSONArray(raceCount);
        int candidateCount = districtData.getInt(MessageFields.PODMessage.RACE_ORDER[raceCount]);

        if (raceReductions.length() != (raceSizes[raceCount] - candidateCount)) {
          logger.warn("Incorrect number of ballot reductions received");
          return false;
        }

        for (int i = 0; i < raceReductions.length(); i++) {
          JSONObject ballotReduction = raceReductions.getJSONObject(i);

          if (!(ballotReduction.getInt(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX) >= candidateCount && ballotReduction
              .getInt(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX) < raceSizes[raceCount])) {
            logger.warn("Candidate index in ballot reduction is either too large or too small");
            return false;
          }

          if (peer.getConfig().doLivePODCheck()) {
            ECPoint[] cipher = ecUtils.jsonToCipher(baseEncIDs.getJSONObject(cipherOffset
                + ballotReduction.getInt(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX)));

            byte[] randomness = IOUtils.decodeData(EncodingType.BASE64,
                ballotReduction.getString(MessageFields.PODMessage.BALLOT_REDUCTIONS_RANDOMNESS));
            ECPoint[] checkedCipher = ecUtils.reencrypt(cipher, peer.getConfig().getElectionPublicKey(), new BigInteger(1,
                randomness));
            ECPoint[] committedCipher = ecUtils.jsonToCipher(ballot.getJSONArray(MessageFields.Cipher.CIPHERS).getJSONObject(
                (cipherOffset + ballotReduction.getInt(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX))));

            if (!(committedCipher[0].equals(checkedCipher[0]) && committedCipher[1].equals(checkedCipher[1]))) {
              logger.error("Ciphers and BallotReduction randomness are not consistent");
              return false;
            }
          }
        }

        logger.info("Ciphers in race {} are consistent", raceCount);

        cipherOffset = cipherOffset + (raceSizes[raceCount]);
      }
    }
    catch (JSONException e) {
      logger.warn("JSON Exception whilst checking ballot reductions", e);
      return false;
    }

    return true;
  }

  /**
   * Validates that this message and its contents were signed by the BallotManager
   * 
   * @param peer
   *          WBBPeer this is running on - needed for access to keys
   * @return boolean of the outcome of the validation process
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validatePODClientSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {

      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.VPS,
          this.msg.getString(JSONWBBMessage.SENDER_ID)));
      tvsSig.update(this.msg.getString(JSONWBBMessage.ID));
      tvsSig.update(this.msg.getString(JSONWBBMessage.DISTRICT));

      return tvsSig.verify(this.msg.getString(JSONWBBMessage.SENDER_SIG), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }
}
