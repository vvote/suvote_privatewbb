/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation

 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.setup;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;

/**
 * Utility class for importing all the BLS Public Certificate Keystores into a specified KeyStore
 * @author Chris Culnane
 * 
 */
public class ImportAllBLSCertsInFolder {

  /**
   * Simple Utility for importing all BLS Public Certificate KeyStores into the specified KeyStore. This is useful when setting up,
   * when all the devices will provide their BLS public key, they can all be put in one folder and imported at once
   * 
   * @param args String[] of two arguments, the first is the keystore, the second is the folder of certificates to import
   * @throws KeyStoreException
   * @throws IOException
   */
  public static void main(String[] args) throws KeyStoreException, IOException {
    if (args.length != 2) {
      printUsage();
    }
    else {
      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStore.load(args[0], "".toCharArray());
      File certs = new File(args[1]);
      if (certs.isDirectory()) {
        File[] files = certs.listFiles();
        for (File temp : files) {
          try {
            TVSKeyStore tempStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
            tempStore.load(temp.getAbsolutePath(), "".toCharArray());
            tvsKeyStore.addBLSKeyStoreEntries(tempStore);
            System.out.println("Importing " + temp.getName());
          }
          catch (KeyStoreException e) {
            System.out.println("Exception import " + temp.getName());
          }
        }
        tvsKeyStore.store(args[0]);
      }
      else {
        System.err.println("Path is not a directory");
      }
    }

  }

  public static void printUsage() {
    System.out.println("ImportAllBLSCertsInFolder imports all the BLS public");
    System.out.println("certificate keystores into the specified keystore.");
    System.out.println("Usage:");
    System.out.println("ImportAllBLSCertsInFolder KeyStorePath CertsFolderToImport");
  }

}
