/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CertificateManagerException;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileChangeListener;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileSystemWatcher;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileWatcherException;

/**
 * CertificateManager provides a wrapper around the JSON based certificate stores. It preloads them and holds them in memory as well
 * as monitoring the file system for changes. If one of the certificate stores changes it will attempt to reload it, if successful,
 * it will update the in-memory copy of the key store. Due to the design of ConcurrentHashMap, which we use to store references to
 * our TVSKeyStores, if a read is in progress whilst an update is taking place the read will get the last completed update. As such,
 * the updated keystore will only be available once the update to ConcurrentHashMap is complete.
 * 
 * @author Chris Culnane
 * 
 */
public class CertificateManager implements FileChangeListener {

  /**
   * Logger
   */
  private static final Logger                       logger      = LoggerFactory.getLogger(CertificateManager.class);
  /**
   * ConcurrentHashMap<CertStore,KeyStore> that holds a cache of the different keystores, indexed by the different types of
   * CertStore
   */
  private ConcurrentHashMap<CertStore, TVSKeyStore> certStores  = new ConcurrentHashMap<CertStore, TVSKeyStore>();

  /**
   * HashMap that maps the canonical paths of the keystores to their cert store types - we need this to map back the file change
   * events. It does not need to be concurrent because once it is setup it is never changed
   */
  private HashMap<String, CertStore>                fileToIDMap = new HashMap<String, CertStore>();

  /**
   * Underlying FileSystemWatcher to monitor the certificates directory
   */
  private FileSystemWatcher                         fsWatcher;

  /**
   * Constructs a new CertificateManager using the certificate location information in the WBBConfig. It reads the config file to
   * get the CertStore list and then loads all the cert stores specified in it. All cert stores must be stored in the same folder so
   * we can monitor changes. If cert stores are listed in different folders this will throw an exception.
   * 
   * Note: This does not start monitoring the file for changes. That only happens startMonitoring is called. Once the
   * FileSystemWatcher is finished with stopMonitoring must be called to release resources
   * 
   * @param config
   *          WBBConfig that contains this peers config file
   * @throws CertificateManagerException
   */
  public CertificateManager(WBBConfig config) throws CertificateManagerException {
    try {
      logger.info("Loading certstores");
      JSONArray certsArray = IOUtils.readJSONArrayFromFile(config.getString(WBBConfig.CERT_STORES_SPEC));
      File watcherDir = null;

      // Get the list of certs stores from the cert stores spec
      for (int i = 0; i < certsArray.length(); i++) {
        // Loop through each entry, getting the id and file location
        JSONObject certStore = certsArray.getJSONObject(i);
        File certFile = new File(certStore.getString(CertStore.CERT_KEYSTORE));
        logger.info("Loading certstore: {}", certFile);
        File folderDir = certFile.getCanonicalFile().getParentFile().getCanonicalFile();
        if (watcherDir == null) {
          // If this the first one, set the watcher directory to the parent of this file
          watcherDir = folderDir;
          logger.info("Creating FileSystemWatcher on {}", watcherDir);
          this.fsWatcher = new FileSystemWatcher(watcherDir);
        }
        else if (!folderDir.equals(watcherDir)) {
          // Check we don't have a different parent, if we do we can't monitor changes
          throw new CertificateManagerException(
              "Cannot watch multiple directories, certificate stores should all be in the same directory");
        }
        this.fileToIDMap.put(certFile.getCanonicalPath(), CertStore.valueOf(certStore.getString(CertStore.CERT_ID)));
        this.fsWatcher.addFileListener(certFile, this);
        this.certStores.put(CertStore.valueOf(certStore.getString(CertStore.CERT_ID)),
            CryptoUtils.loadTVSKeyStore(certFile.getAbsolutePath()));
        logger.info("CertStore {} loaded and added to map", CertStore.valueOf(certStore.getString(CertStore.CERT_ID)));
      }
    }
    catch (CryptoIOException | JSONIOException | JSONException | FileWatcherException | IOException e) {
      throw new CertificateManagerException("Exception whilst loading certificate manager stores");
    }
  }

  /**
   * Called when a certificate store file we are monitoring has changed. This provides the filePath of the change file. We compare
   * it with our map of cert store to files to find which one to update. If there is a mapping we load the relevant file into a new
   * keystore. If there is an exception we log it and abandon any change. This could happen, for example, if half way through
   * editing a file the user saves it, in such situations the file will be incomplete and will fail to load. In such situations we
   * will not change the underlying certificate stores.
   */
  @Override
  public void fileChanged(File filePath) {
    try {
      logger.info("File changed:{}", filePath);
      if (this.fileToIDMap.containsKey(filePath.getCanonicalPath())) {
        CertStore certStore = this.fileToIDMap.get(filePath.getCanonicalPath());
        logger.info("File is {} certificate store", certStore);
        TVSKeyStore updatedKeyStore = CryptoUtils.loadTVSKeyStore(filePath.getAbsolutePath());
        logger.info("Reloaded keystore");
        this.certStores.put(certStore, updatedKeyStore);
        logger.info("Updated cert stores");
      }
      else {
        logger.warn("Cannot find file in certStore map, will ignore");
      }
    }
    catch (IOException e) {
      logger.error("Exception updating keystore, no changes made: {}", e.getMessage());
    }
    catch (CryptoIOException e) {
      logger.error("Exception loading updated keystore - possible incomplete file, no changes made: {}", e.getMessage());
    }
  }

  /**
   * Gets one of the Certificate Stores the CertificateManager has loaded.
   * 
   * @param certStore
   *          CertStore to retrieve
   * @return TVSKeyStore of the requested CertStore, or null if it doesn't exist
   */
  public TVSKeyStore getCertificateStore(CertStore certStore) {
    return this.certStores.get(certStore);
  }

  /**
   * Called to start the monitoring of the files - this is required in order to start the monitoring thread
   * 
   * @throws FileWatcherException
   */
  public void startMonitoring() throws FileWatcherException {
    if (this.fsWatcher != null) {
      this.fsWatcher.startWatching();
    }
  }

  /**
   * Called to stop the file watcher and clean up the resources - it is essential this is called to clean up resources once the file
   * watcher is no longer needed
   * 
   * @throws FileWatcherException
   */
  public void stopMonitoringAndShutdown() throws FileWatcherException {
    if (this.fsWatcher != null) {
      this.fsWatcher.stopWatching();
    }
  }
}
