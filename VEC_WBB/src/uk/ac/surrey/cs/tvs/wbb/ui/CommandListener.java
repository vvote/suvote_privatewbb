/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener;
import uk.ac.surrey.cs.tvs.wbb.core.CommitProcessor;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.CLICommand;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIMessage;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIRequest;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIResponse;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

/**
 * Example class for interacting with the WBB via the command line
 * 
 * Currently only reacts to a commit request
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class CommandListener extends AppenderBase<ILoggingEvent> implements Runnable, VECWebSocketListener {

  /**
   * The WBBPeer we are running commands on
   */
  private WBBPeer                       peer;

  /**
   * Logger
   */
  private static final org.slf4j.Logger logger                 = org.slf4j.LoggerFactory.getLogger(CommandListener.class);

  /**
   * Set to true to shutdown gracefully
   */
  private volatile boolean                       shutdown               = false;

  /**
   * StatSender provides Runtime information
   */
  private StatSender                    statSender;

  /**
   * Thread to run the StatSender in
   */
  private Thread                        statSenderThread       = null;

  /**
   * Thread to run the peerstat sender in
   */
  private Thread                        peerStatSenderThread   = null;

  /**
   * PeerStatSender provides information about the state of this peer
   */
  private PeerStatSender                peerStatSender;

  /**
   * Logging Pattern
   */
  private PatternLayout                 pl;

  /**
   * ThresdholdFilter for Logging, allows logging of only certain levels of message
   */
  private ThresholdFilter               tf;

  /**
   * Listener web socket.
   */
  private WebSocket                     ws;
  /**
   * Is logging turned on?
   */
  private boolean                       isLogging              = false;

  /**
   * Default delay for sending telemetry data
   */
  private static final int              DEFAULT_DELAY          = 1000;

  /**
   * Default level of logging messages to send
   */
  private static final String           DEFAULT_LOGGING_LEVEL  = "warn";

  /**
   * Default logging root - the package within which we wnat to log
   */
  private static final String           LOGGING_ROOT           = "uk";

  /**
   * Logging pattern to use when getting the log data
   */
  private static final String           LOGGING_PATTERN_LAYOUT = "%d %5p %t [%c:%L] %m%n)";

  /**
   * boolean that stores whether the command listener thread has been started
   */
  private boolean                       commandListenerStarted = false;

  /**
   * Construct a commandListener and starts a daemon thread
   * 
   * @param peer
   *          the WBBPeer we run commands on
   */
  public CommandListener(WBBPeer peer) {
    super();

    this.peer = peer;

    // Getting logging content to register this as an appender
    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    
    this.pl = new PatternLayout();
    this.pl.setPattern(LOGGING_PATTERN_LAYOUT);
    this.pl.setContext(lc);
    this.pl.start();

    this.peer = peer;
   
    this.setContext(lc);

    this.tf = new ThresholdFilter();
    this.tf.setLevel(DEFAULT_LOGGING_LEVEL);
    this.tf.start();

    this.addFilter(this.tf);

    


  }

  /**
   * Starts the command listener
   */
  public void startCL() {
    if (!commandListenerStarted) {
      this.start();
      // Listens to all logging messages with root of uk (we use classnames as all ours start with uk).
      ((Logger) LoggerFactory.getLogger(LOGGING_ROOT)).addAppender(this);
      Thread t = new Thread(this, "CommandListener");
      t.setDaemon(true);
      t.start();
      this.commandListenerStarted = true;
      logger.info("CommandListener started");
    }
  }

  /**
   * Appends a log event to the log. Extracts details of the JSON message for the log.
   * 
   * @param event
   *          The log event.
   * 
   * @see ch.qos.logback.core.AppenderBase#append(java.lang.Object)
   */
  @Override
  protected void append(ILoggingEvent event) {
    try {
      if (this.isLogging) {
        JSONObject obj = new JSONObject();
        obj.put(UIMessage.UI_TYPE, UIResponse.UI_TYPE_LOGGING);
        obj.put(UIResponse.UI_VALUE_MESSAGE, event.getFormattedMessage());
        obj.put(UIResponse.UI_VALUE_LEVEL, event.getLevel().toString());
        obj.put(UIResponse.UI_VALUE_THREAD, event.getThreadName());
        obj.put(UIResponse.UI_VALUE_DATETIME, event.getTimeStamp());
        this.ws.send(obj.toString());
      }
    }
    catch (JSONException e) {
      logger.error("Exception whilst responding with Logging data", e);
    }
  }

  /**
   * Processes an incoming message.
   * 
   * @param message
   *          The received message.
   * @param ws
   *          The websocket it is received on.
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener#processMessage(org.json.JSONObject, org.java_websocket.WebSocket)
   */
  @Override
  public void processMessage(JSONObject message, WebSocket ws) {
    if (!message.has(UIMessage.UI_TYPE)) {
      logger.warn("Received message without type:{}", message.toString());
      return;
    }

    try {
      this.ws = ws;
      String type = message.getString(UIMessage.UI_TYPE);
      String delaystr;
      int delay;

      switch (type) {
        case UIRequest.CMD_START_LOGGING:
          this.isLogging = true;
          if (message.has(UIRequest.VALUE_LEVEL)) {
            this.tf.setLevel(message.getString(UIRequest.VALUE_LEVEL));
          }
          break;

        case UIRequest.CMD_STOP_LOGGING:
          this.isLogging = false;
          break;

        case UIRequest.CMD_START_MEMORY:
          if (this.statSenderThread != null) {
            this.statSender.shutdown();
            if (this.statSenderThread.isAlive()) {
              this.statSenderThread.interrupt();
            }
          }
          delaystr = message.getString(UIRequest.VALUE_DELAY);
          delay = CommandListener.DEFAULT_DELAY;
          try {
            // Custom delay - to reduce resource use
            delay = Integer.parseInt(delaystr);
          }
          catch (NumberFormatException e) {
            logger.warn("Received invalid number from front-end", e);
          }
          this.statSender = new StatSender(ws, delay);
          this.statSenderThread = new Thread(this.statSender, "StatSender");
          this.statSenderThread.start();
          break;

        case UIRequest.CMD_STOP_MEMORY:
          if (this.statSender != null) {
            this.statSender.shutdown();
          }
          break;

        case UIRequest.CMD_START_PEER_STAT:
          if (this.peerStatSenderThread != null) {
            this.peerStatSender.shutdown();
            if (this.peerStatSenderThread.isAlive()) {
              this.peerStatSenderThread.interrupt();
            }
          }
          delaystr = message.getString(UIRequest.VALUE_DELAY);
          delay = 1000;
          try {
            delay = Integer.parseInt(delaystr);
          }
          catch (NumberFormatException e) {
            logger.warn("Received invalid number from front-end", e);
          }
          this.peerStatSender = new PeerStatSender(ws, delay, this.peer);
          (new Thread(this.peerStatSender, "PeerStatSender")).start();
          break;

        case UIRequest.CMD_STOP_PEER_STAT:
          // Stop sending peer stats and shutdown thread
          if (this.peerStatSender != null) {
            this.peerStatSender.shutdown();
          }
          break;

        case UIRequest.CMD_GET_COMMIT_DATA:
          try {
            // List all available commits
            JSONObject obj = this.peer.getDB().getCommitTable();
            obj.put(UIMessage.UI_TYPE, UIResponse.UI_TYPE_COMMIT);
            ws.send(obj.toString());
          }
          catch (JSONException e) {
            e.printStackTrace();
          }
          break;
        case UIRequest.CMD_GET_NEXT_COMMIT:
          try {
            // List all available commits
            JSONObject resp = new JSONObject();
            resp.put(UIResponse.UI_VALUE_COMMIT_TIME, this.peer.getCommitTime());
            resp.put(UIMessage.UI_TYPE, UIResponse.UI_TYPE_COMMIT_TIME);
            ws.send(resp.toString());
          }
          catch (JSONException e) {
            e.printStackTrace();
          }
          break;

        case UIRequest.CMD_DO_EXTRA_COMMIT:
          try {
            // List all available commits
            String commitTime = message.getString(UIRequest.VALUE_COMMIT_TIME);
            String commitDesc = null;
            if (message.has(UIRequest.VALUE_COMMIT_DESC)) {
              commitDesc = message.getString(UIRequest.VALUE_COMMIT_DESC);
              this.peer.runCommit(commitTime, commitDesc);
            }
            else {
              this.peer.runCommit(commitTime);
            }

          }
          catch (JSONException e) {
            e.printStackTrace();
          }
          break;

      }
    }
    catch (JSONException e) {
      logger.error("Exception whilst processing UI Message, will ignore it", e);
    }
  }

  /**
   * Continuously responds to command line input until shutdown.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    while (!this.shutdown) {
      // Prepare reader for standard.in
      BufferedReader br = null;
      String line = null;

      try {
        br = new BufferedReader(new InputStreamReader(System.in));
        // Read a line
        while ((line = br.readLine()) != null) {
          // Tokenize the line
          StringTokenizer st = new StringTokenizer(line);

          if (st.hasMoreTokens()) {
            // The first value is the command, the rest are arguments
            String command = st.nextToken();

            switch (command) {
              case CLICommand.CLI_COMMIT:
                logger.info("Commit requested via CLI");

                if (st.hasMoreTokens()) {
                  String commitTime = st.nextToken();
                  String commitDesc = null;
                  if (st.hasMoreTokens()) {
                    commitDesc = st.nextToken();
                    logger.info("CommitTime specified, starting Commit on {} with Description: {}", commitTime, commitDesc);
                    System.out.println("Starting Commit on: " + commitTime + " with description:" + commitDesc);
                    this.peer.runCommit(commitTime, commitDesc);
                  }
                  else {
                    logger.info("CommitTime specified, starting Commit on {}", commitTime);
                    System.out.println("Starting Commit on: " + commitTime);
                    this.peer.runCommit(commitTime);
                  }

                }
                else {
                  logger.info("Starting Commit");
                  System.out.println("Starting Commit");
                  this.peer.runCommit();
                }
                break;

              case CLICommand.CLI_SHUTDOWN:
                if (st.hasMoreTokens()) {
                  if (st.nextToken().equals("force")) {
                    this.peer.shutdown(false);
                  }
                }

                this.peer.shutdown(true);
                System.out.println("Called shutdown on peer. Shutting down UI and CLI");
                this.shutdown();
                System.out.println("CLI Shutdown - awaiting termination...");
                break;
              case CLICommand.CLI_FORCE_CLOSE_COMMIT:
                System.out.println("About to force close commit");
                try {
                  System.out.println("Successful outcome of forceClose:" + CommitProcessor.forceCloseCurrentCommit(this.peer));
                }
                catch (NoCommitExistsException e) {
                  logger.error("Exception whilst trying to force close", e);
                  System.out.println("There was an exception whilst trying to force close");
                }
                break;
              default:
                System.out.println("Unknown command");
            }
          }
        }
      }
      catch (IOException e) {
        logger.warn("IOException received on CommandListener. Will reinitialise", e);
      }
      /**
       * finally{ //Removed the br.close, since this closes System.in which means it will never reinitialise if(br!=null){ try {
       * br.close(); } catch (IOException e) { logger.error("Exception whilst closing System.in reader",e); } } }
       */
    }
  }

  /**
   * Shuts down the CommandListener at the next opportunity - note, this will not interrupt a current wait
   */
  public void shutdown() {
    this.shutdown = true;
  }
}
