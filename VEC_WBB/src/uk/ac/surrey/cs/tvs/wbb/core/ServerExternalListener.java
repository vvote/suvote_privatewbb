/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.MapMaker;

/**
 * Listens for connections on the external facing port. The only internal connections are those from other WBBPeers, all others are
 * treated as external connections. As such, the following components will connect through this listener:
 * <ul>
 * <li>EBM</li>
 * <li>PODClient - for print on demand/audit/ballot gen/cancellation</li>
 * <li>MixNetPeer - for transmitting proofs, randomness</li>
 * </ul>
 * 
 * Connection are accepted and immediately handed off to an Executor ThreadPool. No processing is performed on this thread, it is
 * purely to receive the connection. The validity of the connection is checked later.
 * 
 * @author Chris Culnane
 * 
 */
public class ServerExternalListener implements Runnable {

  /**
   * Logger
   */
  private static final Logger                                            logger      = LoggerFactory
                                                                                         .getLogger(ServerExternalListener.class);

  /**
   * Set to true to shutdown the listener
   */
  private boolean                                                        shutdown    = false;

  /**
   * Reference to the peer we are listening for
   */
  private WBBPeer                                                        peer;

  /**
   * A weak keys map to enforce that messages related to the same serial number are processed
   * sequentially, not concurrently.
   */
  private ConcurrentMap<String, SerialExecutor> serialMap = new MapMaker().weakKeys().makeMap();
  /**
   * Constructor for external listener. Each peer should only have one such listener
   * 
   * @param peer
   *          the WBBPeer this listener is attached to
   */
  public ServerExternalListener(WBBPeer peer) {
    super();

    this.peer = peer;
  }

  /**
   * Continuously listen for messages and hand them off to the external processor.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    // Loop until shutdown
    while (!this.shutdown) {
      try {
        // Get the external socket from the peer and call accept, which blocks until a connection is received
        Socket socket = this.peer.getPeerExternalServerSocket().accept();

        // Create ExternalPeerThread (it is a Runnable, not a pure thread)
        WBBExternalPeerThread wbbPeerThread = new WBBExternalPeerThread(this.peer, socket, this.serialMap);

        // Add the wbbPeerThread processor the executor
        this.peer.getExternalListenerExecutor().execute(wbbPeerThread);
      }
      catch (IOException e) {
        logger.error("IOException whilst accepting connection on External Server Socket", e);
      }
    }
  }

  /**
   * Shuts down the listener.
   */
  public void shutdown() {
    this.shutdown = true;
  }
}
