/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import org.java_websocket.WebSocket;

/**
 * Abstract class which encapsulates sending a message via a web socket.
 * 
 * @author Chris Culnane
 * 
 */
public abstract class MsgSender implements Runnable {

  /**
   * Set to true to shutdown gracefully
   */
  protected boolean   shutdown = false;

  /**
   * WebSocket to send data on
   */
  protected WebSocket ws;

  /**
   * Delay between sending update messages
   */
  protected long      delay    = 1000;

  /**
   * Construct a new Message Sender
   * 
   * @param ws
   *          WebSocket to send message on
   * @param delay
   *          long delay between sending updated messages
   */
  public MsgSender(WebSocket ws, long delay) {
    super();

    this.ws = ws;
    this.delay = delay;
  }

  /**
   * Abstract implementation should send the message.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public abstract void run();

  /**
   * Request a shutdown.
   */
  public void shutdown() {
    this.shutdown = true;
  }
}