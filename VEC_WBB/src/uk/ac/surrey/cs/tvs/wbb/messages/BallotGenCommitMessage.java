/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a FileMessage sent from a client device to the WBB
 * 
 * This will be utilised by PODPeers to periodically submit a zip file of proofs covering key transformation. It will also be
 * utlised by the MixNetPeer to bulk upload its proofs, mixes, decryptions etc.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class BallotGenCommitMessage extends BallotFileMessage {

  /**
   * Directory prefix used for ballot upload.
   */
  private static final String UPLOAD_DIR_PREFIX = "BallotGenCommitAttachments";

  /**
   * Logger
   */
  private static final Logger logger            = LoggerFactory.getLogger(BallotGenCommitMessage.class);

  /**
   * Constructor to create FileMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public BallotGenCommitMessage(JSONObject msg) throws MessageJSONException {
    super(msg);
    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create FileMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public BallotGenCommitMessage(String msgString) throws MessageJSONException {
    super(msgString);
    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Gets the filename of the underlying file
   * 
   * @return string of the fileName
   */
  @Override
  public String getFilename() {
    return this.fileName;
  }

  /**
   * Gets the fileSize of the underlying file
   * 
   * @return long value of the file size
   */
  @Override
  public long getFileSize() {
    return this.fileSize;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.internalSignableContent;
  }

  /**
   * Init the message
   * 
   * Sets the type, extracts the id from submissionID, extracts the fileSize and checks whether the underlying message has a
   * _fileName attribute. Having mentioned above that a message does not have a _fileName it may seem odd to load one from the JSON
   * during initialisation. However, this handles loading a message from our own local database when we perform the commit. Note
   * that a message will have been rejected if it was received with a _fileName attribute
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.BALLOT_GEN_COMMIT;
    this.id = this.msg.getString(MessageFields.FileMessage.ID);
    this.fileSize = this.msg.getInt(MessageFields.FileMessage.FILESIZE);

    if (this.msg.has(MessageFields.FileMessage.INTERNAL_FILENAME)) {
      this.fileName = this.msg.getString(MessageFields.FileMessage.INTERNAL_FILENAME);
    }

    if (this.msg.has(MessageFields.FileMessage.INTERNAL_DIGEST)) {
      this.updateDigest(this.msg.getString(MessageFields.FileMessage.INTERNAL_DIGEST));
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Perform JSONSchema validation on the underlying message
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.BALLOTGENCOMMIT))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Check the sender signature is valid with the digest sent in the message
      if (!this.validateSenderSignature(peer)) {
        logger.warn("Sender signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Sender signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }
  }

  /**
   * Post processes a message as a part of the commit round 2 processing. This is only for submissions that impact on the wider
   * database, for example, ciphers.
   * 
   * @param peer
   *          The WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#postprocessAsPartofCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected void postprocessAsPartofCommitR2(WBBPeer peer, String safeUploadDir) throws NoSuchAlgorithmException, JSONException,
      MessageJSONException {
    this.processCipherFiles(peer, true);
  }

  /**
   * Pre-processes an incoming message by saving the file data to a file and by validating the message and verifying the message
   * signature.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream for the message.
   * @param socket
   *          The socket used for the send.
   * @return True if the message was pre-processed and validated successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.FileMessage#preProcessMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream, java.net.Socket)
   */
  @Override
  public boolean preProcessMessage(WBBPeer peer, BoundedBufferedInputStream in, Socket socket) throws IOException, JSONException,
      NoSuchAlgorithmException {
    // If it is a file message we need to download the file from the stream
    logger.info("Message is Ballot Gen Commit upload");

    return this.doFileProcessing(peer, in, socket, BallotGenCommitMessage.UPLOAD_DIR_PREFIX);
  }

  /**
   * Processes the cipher files for generated ballots. Assumes that this is not being processed as part of a commit.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if processed correctly.
   * @throws JSONException
   */
  private boolean processCipherFiles(WBBPeer peer) throws JSONException {
    return this.processCipherFiles(peer, false);
  }

  /**
   * Processes the cipher files for generated ballots.
   * 
   * @param peer
   *          The WBB peer.
   * @param inCommit
   *          Is this being processed during a commit?
   * @return True if processed correctly.
   * @throws JSONException
   */
  private boolean processCipherFiles(WBBPeer peer, boolean inCommit) throws JSONException {
    if (this.outputDir.list().length == 1) {

      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(this.outputDir.listFiles()[0]));

        String line;
        while ((line = br.readLine()) != null) {
          JSONObject cipher = new JSONObject(line);
          try {
            peer.getDB().submitCipher(cipher);
          }
          catch (AlreadyReceivedMessageException e) {
            if (!inCommit) {
              SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.SERIALNO_USED);
              logger.warn("Exception whilst insert cipher, will stop processing any more", e);
              return false;
            }
            {
              logger.warn("Cipher already stored, will ignore");
            }
          }
        }
      }
      catch (IOException e1) {
        logger.error("Exception whilst processing message", e1);
        return false;
      }
      finally {
        try {
          if (br != null) {
            br.close();
          }
        }
        catch (IOException e) {
          logger.error("Exception closing cipher file", e);
        }
      }

      return true;
    }
    else {
      logger.warn("OutputDir has {} files when it should only have 1", this.outputDir.length());
      return false;
    }
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    boolean messageAccepted = false;
    String signature = this.createInternalSignature(peer);
    messageAccepted = this.storeIncomingMessage(peer, signature);

    if (messageAccepted) {
      // We could have received internal peer messages related to this serial number before we received the original message from
      // the client. Therefore we check if there is anything queued up waiting for us to verify
      ArrayList<PeerMessage> sigsToSign = peer.getDB().getSigsToCheck(DBRecordType.GENERAL, this.getID());

      for (PeerMessage sigToSign : sigsToSign) {
        try {
          // Check the internal peer message and store it in the database
          this.checkAndStoreMessage(peer, this, sigToSign);
        }
        catch (AlreadyReceivedMessageException e) {
          // We've already received that peer message, will ignore it.
          logger.warn("Already received message: {} from {}. Will ignore", this.getID(),
              this.getMsg().getString(MessageFields.FROM_PEER));
        }
      }

      if (this.outputDir == null) {
        logger.warn("OutputDir is null when it should have been set during pre-processing. Cannot process non-existent files");
        return false;
      }
      else {
        if (!this.processCipherFiles(peer)) {
          return false;
        }
      }
      // Prepare a message to send to all other peers (SK1) message
      JSONObject peermsg = this.constructPeerMessage(peer, signature);

      // Send it to all peers
      peer.sendToAllPeers(peermsg.toString());
      if (!this.checkThreshold(peer)) {
        return false;
      }
    }

    return true;
  }
}
