/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;

/**
 * Represents an external message to the peer.
 * 
 * @author Chris Culnane
 * 
 */
public abstract class ExternalMessage extends JSONWBBMessage {

  /**
   * Telemetry Logger
   */
  private static final Logger         telemetry             = LoggerFactory.getLogger("Telemetry");
  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ExternalMessage.class);

  /**
   * Signable content string represents the parts of the internal message to be signed or used in verification
   */
  protected String            internalSignableContent;

  /**
   * Signable content string represents the parts of the external message to be signed or used in verification
   */
  protected String            externalSignableContent;

  /**
   * Constructor for JSONWBBMessage takes message as a JSONObject.
   * 
   * @param msg
   *          the JSONObject of the message
   * @throws MessageJSONException
   */
  public ExternalMessage(JSONObject msg) throws MessageJSONException {
    super(msg);
  }

  /**
   * Constructor for a JSONWBBMessage.
   * 
   * Should never be called directly
   * 
   * @param msgString
   *          the underlying message string
   * @throws MessageJSONException
   */
  public ExternalMessage(String msgString) throws MessageJSONException {
    super(msgString);
  }

  /**
   * Checks if we have reached consensus on a cancellation message and if not, whether it is possible to do so.
   * 
   * @param peer
   *          The WBB peer.
   * @param threshold
   *          integer of the threshold we need to reach
   * @param peerCount
   *          integer of total number of peers
   * @return boolean - true if have or can reach consensus, false otherwise
   */
  protected boolean canOrHaveReachedConsensus(WBBPeer peer, int threshold, int peerCount) {
    return peer.getDB().canOrHaveReachedConsensus(DBRecordType.GENERAL, this.getID(), threshold, peerCount);
  }

  /**
   * Performs signature verification on the message and stores the result to the database.
   * 
   * The message being checked is from another peer. That message will contain a serial number and a signature, nothing else. As
   * such, the original message it is referring to is passed in to verify that the signature from the peer matches the data we have
   * locally.
   * 
   * @param messageFromClient
   *          generic message from a client that is used to perform the check
   * @param msg
   *          the message from the peer that is being checked
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   *           the message has already been stored in the database
   */
  public void checkAndStoreMessage(WBBPeer peer, JSONWBBMessage messageFromClient, PeerMessage msg) throws JSONException,
      AlreadyReceivedMessageException {
    try {
      boolean valid = messageFromClient.checkPeerMessageSignature(peer, msg);

      // Irrespective of the whether signature is valid or not we store it. The only exception is if the signature checking throws
      // and exception. In which case we do not store it to the database. Whilst we could in theory rely on the SSL connection to
      // perform authentication, from a protocol point of view we do not. If the signature is well formed but incorrect we know the
      // peer received the wrong data. If the signature is not well formed we cannot tell
      this.submitIncomingPeerMessageChecked(peer, msg, valid, false, false);

    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing message", e);
    }
    catch (AlreadySentTimeoutException e) {
      try {
        // Indicates we received a peer message after the timeout. We want to store it, but not include it within the set of valid
        // messages
        peer.getDB().submitPostTimeoutMessage(msg);
      }
      catch (UnknownDBException e1) {
        logger.error("Error trying store message to postTimeout set", e1);
      }
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing message", e);
    }
  }

  /**
   * Performs signature verification on the message and stores the result to the database.
   * 
   * The message being checked is from another peer. That message will contain a serial number and a signature, nothing else. As
   * such, the original message it is referring to is passed in to verify that the signature from the peer matches the data we have
   * locally.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          generic message from a client that is used to perform the check
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#checkAndStoreMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage)
   */
  @Override
  public void checkAndStoreMessage(WBBPeer peer, PeerMessage msg) throws JSONException, AlreadyReceivedMessageException {
    this.checkAndStoreMessage(peer, this, msg);
  }

  /**
   * Verifies the signature of a external peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @param sendingPeer
   *          The sending peer.
   * @param signature
   *          The signature to verify.
   * @return True if the signature is valid.
   * @throws KeyStoreException
   * @throws TVSSignatureException
   * @throws JSONException
   */
  public boolean checkMessageFromExternalPeer(WBBPeer peer, String sendingPeer, String signature) throws KeyStoreException,
      TVSSignatureException, JSONException {

    TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateFromCerts(CertStore.PEER, sendingPeer
        + WBBConfig.SIGNING_KEY_SK1_SUFFIX));
    tvsSig.update(this.getInternalSignableContent());

    return tvsSig.verify(signature, EncodingType.BASE64);
  }

  /**
   * Checks whether we have reached a consensus on a message and sends the response.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message sent. Consensus may not have been reached in which case an error response was sent.
   * @throws MessageJSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws JSONException
   * @throws InvalidKeyException
   * @throws IOException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#checkThreshold(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean checkThreshold(WBBPeer peer) throws MessageJSONException, NoSuchAlgorithmException, SignatureException,
      JSONException, InvalidKeyException, IOException, TVSSignatureException {
    // Check if we have reached the required threshold, if we have set the record to show we are going to send an SK2 signature
    if (this.checkThresholdAndResponse(peer, peer.getThreshold())) {
      // Get the original message from the client - strictly speaking we already have it above.
      String clientMessage = this.getClientMessage(peer, this.getID());

      if (clientMessage != null) {
        JSONWBBMessage clientMsg = JSONWBBMessage.parseMessage(clientMessage);

        // Create SK2 signature
        JSONObject response = clientMsg.constructResponseAndSign(peer);

        clientMsg.addAdditionalContentToResponse(response);

        // Construct response and send back to client
        peer.sendAndClose(clientMsg.getMsg().getString(MessageFields.UUID), response.toString());

        return true;
      }
      else {
        logger.error("Client message is null even though signatures have been validated. Database must be corrupt. msg:{}",
            this.msg);
        return false;
      }
    }
    else {
      // Check whether it is still possible to reach a successful consensus, if not send an error to the client
      if (!this.canOrHaveReachedConsensus(peer, peer.getThreshold(), peer.getPeerCount() + 1)) {
        SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.NO_CONSENSUS_POSSIBLE);
      }

      return true;
    }
  }

  /**
   * Checks whether we have reached a consensus on a message and if we have sent a response.
   * 
   * @param peer
   *          The WBB peer.
   * @param threshold
   *          integer threshold to check.
   * @return boolean - true if we have reached consensus and should send response, otherwise false
   */
  protected boolean checkThresholdAndResponse(WBBPeer peer, int threshold) {
    return peer.getDB().checkThresholdAndResponse(DBRecordType.GENERAL, this.getID(), peer.getThreshold());
  }

  /**
   * Checks all of the messages received in a commit round 2.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param validPeerMessages
   *          The output list of valid peer messages.
   * @param fields
   *          The fields required in the message.
   * @throws JSONException
   * @throws MessageJSONException
   * @throws KeyStoreException
   * @throws TVSSignatureException
   */
  public void checkValidPeerMessagesInR2Record(WBBPeer peer, JSONObject record, ArrayList<JSONWBBMessage> validPeerMessages,
      DBFields fields) throws JSONException, MessageJSONException, KeyStoreException, TVSSignatureException {
    JSONArray checkedSigs = record.getJSONArray(fields.getField(DBFieldName.CHECKED_SIGS));

    Map<String, Boolean> peerMsgCheck = new HashMap<String, Boolean>();
    // Add any existing validMessages into the list of accepted message
    for (JSONWBBMessage existMsg : validPeerMessages) {
      if (peerMsgCheck.containsKey(existMsg.getMsg().getString(MessageFields.PeerMessage.PEER_ID))) {
        throw new MessageJSONException("Message from same peer is being counted twice");
      }
      else {
        peerMsgCheck.put(existMsg.getMsg().getString(MessageFields.PeerMessage.PEER_ID), true);
      }

    }
    for (int i = 0; i < checkedSigs.length(); i++) {
      JSONObject peerSigMsg = checkedSigs.getJSONObject(i);

      JSONWBBMessage peerMsg = JSONWBBMessage.parseMessage(peerSigMsg.getString(fields.getField(DBFieldName.QUEUED_MESSAGE)));
      ((PeerMessage) peerMsg).setFromPeer(peerSigMsg.getString(fields.getField(DBFieldName.FROM_PEER)));
      boolean valid = this.checkMessageFromExternalPeer(peer, peerMsg.getMsg().getString(MessageFields.PeerMessage.PEER_ID),
          peerMsg.getMsg().getString(MessageFields.PeerMessage.PEER_SIG));

      if (valid) {
        if (!peerMsgCheck.containsKey(peerMsg.getMsg().getString(MessageFields.PeerMessage.PEER_ID))) {
          validPeerMessages.add(peerMsg);
          peerMsgCheck.put(peerMsg.getMsg().getString(MessageFields.PeerMessage.PEER_ID), true);
        }
        else {
          logger.warn("Duplicate peer message received");
        }
      }
    }
  }

  /**
   * Constructs a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @param signature
   *          The message signature.
   * @return The constructed message.
   * @throws JSONException
   */
  protected JSONObject constructPeerMessage(WBBPeer peer, String signature) throws JSONException {
    // Prepare a message to send to all other peers (SK1) message
    JSONObject peermsg = new JSONObject();
    peermsg.put(MessageFields.PeerMessage.TYPE, this.getPeerTypeFromClientType());
    peermsg.put(MessageFields.PeerMessage.ID, this.getID());
    peermsg.put(MessageFields.PeerMessage.PEER_SIG, signature);
    peermsg.put(MessageFields.PeerMessage.PEER_ID, peer.getID());

    return peermsg;
  }

  /**
   * Constructs a simulated peer message as par tof the commit round 2 so that the data can be stored in the database.
   * 
   * @param id
   *          The message id.
   * @param signature
   *          The signature.
   * @param sendingPeer
   *          The sending peer.
   * @return The constructed message.
   * @throws JSONException
   */
  public JSONObject constructSimulatedPeerMessage(String id, String signature, String sendingPeer) throws JSONException {
    JSONObject peermsg = new JSONObject();
    peermsg.put(MessageFields.TYPE, this.getPeerTypeFromClientType());
    peermsg.put(MessageFields.PeerMessage.ID, id);
    peermsg.put(MessageFields.PeerMessage.PEER_SIG, signature);
    peermsg.put(MessageFields.PeerMessage.PEER_ID, sendingPeer);
    peermsg.put(MessageFields.FROM_PEER, this.msg.getString(MessageFields.FROM_PEER));

    return peermsg;
  }

  /**
   * Processes the commit round 2 message.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sendingPeer
   *          The sending peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @param recordType
   *          The type of commit record.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws MessageJSONException
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   * @throws KeyStoreException
   * @throws TVSSignatureException
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   */
  public void doProcessAsPartOfCommitR2(WBBPeer peer, JSONObject record, String sendingPeer, String safeUploadDir,
      DBRecordType recordType) throws JSONException, NoSuchAlgorithmException, MessageJSONException, MessageVerificationException,
      JSONSchemaValidationException, KeyStoreException, TVSSignatureException, AlreadyReceivedMessageException,
      AlreadySentTimeoutException {
    DBFields fields = DBFields.getInstance(recordType);

    if (!peer.getDB().isAlreadyStored(this.getID(), recordType, peer.getThreshold())) {
      ArrayList<JSONWBBMessage> validPeerMessages = new ArrayList<JSONWBBMessage>();
      JSONObject row = peer.getDB().getRecord(this.getID());

      this.performValidation(peer);
      this.preprocessAsPartofCommitR2(peer, safeUploadDir);

      // Store the data in the database if it is valid.
      if (record.has(fields.getField(DBFieldName.MY_SIGNATURE))) {
        boolean valid = this.checkMessageFromExternalPeer(peer, sendingPeer,
            record.getString(fields.getField(DBFieldName.MY_SIGNATURE)));

        if (valid) {
          // Construct simulated peer message so we can store it in our db
          validPeerMessages.add(JSONWBBMessage.parseMessage(this.constructSimulatedPeerMessage(record.getString(DBFields.ID),
              record.getString(fields.getField(DBFieldName.MY_SIGNATURE)), sendingPeer)));
        }
      }
      else {
        return;
      }

      this.checkValidPeerMessagesInR2Record(peer, record, validPeerMessages, fields);

      if (validPeerMessages.size() < peer.getThreshold()) {
        return; // Sent message is not valid
      }

      if (row == null) {
        peer.getDB().submitIncomingCommitMessage(recordType, this, false);

        for (JSONWBBMessage m : validPeerMessages) {
          m.getMsg().put(MessageFields.CommitR2.FROM_COMMIT, true);
          peer.getDB().submitIncomingPeerMessageChecked(recordType, (PeerMessage) m, true, true, false);
        }

        // No record at all
      }
      else if (row.has(fields.getField(DBFieldName.CLIENT_MESSAGE))) {
        // we have an original message ourselves
        JSONWBBMessage rowmsg = JSONWBBMessage.parseMessage(row.getString(fields.getField(DBFieldName.CLIENT_MESSAGE)));

        if (rowmsg.getInternalSignableContent().equals(this.getInternalSignableContent())) {
          // It is equal to the one in this message
          for (JSONWBBMessage m : validPeerMessages) {
            // We don't want to add our own message again
            m.getMsg().put(MessageFields.CommitR2.FROM_COMMIT, true);
            peer.getDB().submitIncomingPeerMessageChecked(recordType, (PeerMessage) m, true, true, false);
          }
        }
        else {
          // There is a conflict, ours is not supported by a threshold set. Store the valid version to the conflictColl
          try {
            peer.getDB().submitIncomingCommitMessage(recordType, this, true);
          }
          catch (AlreadyReceivedMessageException e1) {
            logger.warn("Already stored message to Commit Conflict");
          }

          for (JSONWBBMessage m : validPeerMessages) {
            // We don't want to add our own message again
            m.getMsg().put(MessageFields.CommitR2.FROM_COMMIT, true);
            peer.getDB().submitIncomingPeerMessageChecked(recordType, (PeerMessage) m, true, true, true);
          }
        }
      }
      else {
        // We don't have an original, but have something. However, the supplied version is valid and backed by a threshold, so
        // just add them as peer messages
        try {
          // Try to submit some data - ensure that it won't be blank
          peer.getDB().submitIncomingCommitMessage(recordType, this, false);
        }
        catch (AlreadyReceivedMessageException e1) {
          logger.warn(
              "Received AlreadyReceivedMessageException when storing R2 commit, despite not having a client message when checked",
              e1);
        }
        for (JSONWBBMessage m : validPeerMessages) {
          m.getMsg().put(MessageFields.CommitR2.FROM_COMMIT, true);
          peer.getDB().submitIncomingPeerMessageChecked(recordType, (PeerMessage) m, true, true, false);
        }
      }
      this.postprocessAsPartofCommitR2(peer, safeUploadDir);
    }
  }

  /**
   * Gets the original client message.
   * 
   * @param peer
   *          The WBB peer.
   * @param id
   *          The message id.
   * @return A String representation of the message.
   */
  protected String getClientMessage(WBBPeer peer, String id) {
    return peer.getDB().getClientMessage(DBRecordType.GENERAL, id);
  }

  /**
   * Abstract method
   * 
   * Defines the internal content that should be signed by message. This allows generic processing of different types of message
   * since the message itself defines what should and should not be signed. For example, an audit message is just the serial number,
   * whilst a vote is the serial number and preferences. Note, it is the contents of the message that is to be signed by the
   * WBBPeer, not the contents of the message that was signed by the EBM or client device.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getInternalSignableContent()
   */
  @Override
  public abstract String getInternalSignableContent() throws JSONException;

  /**
   * Gets an ArrayList of PeerMessages that need to be checked
   * 
   * @param peer
   *          The WBB peer.
   * @param id
   *          The message id.
   * @return ArrayList of PeerMessages to check, or an empty list
   * @throws MessageJSONException
   */
  protected ArrayList<? extends PeerMessage> getSigsToCheck(WBBPeer peer, String id) throws MessageJSONException {
    return peer.getDB().getSigsToCheck(DBRecordType.GENERAL, id);
  }

  /**
   * Abstract method to perform validation
   * 
   * The overridden method should perform the relevant validation for the particular message type
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#performValidation(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public abstract void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException;

  /**
   * Post processes a message as a part of the commit round 2 processing. This is only for submissions that impact on the wider
   * database, for example, ciphers.
   * 
   * @param peer
   *          The WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   * @throws MessageJSONException
   */
  protected void postprocessAsPartofCommitR2(WBBPeer peer, String safeUploadDir) throws NoSuchAlgorithmException, JSONException,
      MessageJSONException {
  }

  /**
   * Processes the message as a part of the commit round 2 processing.
   * 
   * @param peer
   *          The WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   * @throws MessageJSONException
   */
  protected void preprocessAsPartofCommitR2(WBBPeer peer, String safeUploadDir) throws NoSuchAlgorithmException, JSONException,
      MessageJSONException {
  }

  /**
   * Processes the messages as part of commit round 2.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sendingPeer
   *          The sending WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws CommitProcessingException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#processAsPartOfCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      org.json.JSONObject, java.lang.String, java.lang.String)
   */
  @Override
  public void processAsPartOfCommitR2(WBBPeer peer, JSONObject record, String sendingPeer, String safeUploadDir)
      throws CommitProcessingException {
    try {
      this.doProcessAsPartOfCommitR2(peer, record, sendingPeer, safeUploadDir, DBRecordType.GENERAL);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("No such algorithm exception whilst checking and storing cancel message", e);
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing cancel message", e);
    }
    catch (MessageJSONException e) {
      logger.error("Exception whilst processing an R2 record - indicates a malformed message - record discarded", e);
    }
    catch (MessageVerificationException e) {
      logger.error(
          "Message verification exception whilst processing an R2 record - indicates a malformed message - record discarded", e);
    }
    catch (AlreadySentTimeoutException e) {
      logger
          .error(
              "Timeout Exception occured whilst processing an R2 record - this shouldn't happen, timeouts should be overridden during commit",
              e);
    }
    catch (AlreadyReceivedMessageException e) {
      // We don't log this because it is expected. hopefully we have already received all messages!
    }
    catch (JSONException e) {
      logger.error("JSON Exception whilst processing R2 commit record - indicates malformed message - record discarded", e);
    }
    catch (JSONSchemaValidationException e) {
      logger.error(
          "JSON Schema validation failed whilst processing an R2 record - indicates a malformed message - record discarded", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing cancel message", e);
    }
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    boolean messageAccepted = false;
    String signature = this.createInternalSignature(peer);
    messageAccepted = this.storeIncomingMessage(peer, signature);

    if (messageAccepted) {
      telemetry.info("MSG{}#{}",this.type,this);
      // We could have received internal peer messages related to this serial number before we received the original message from
      // the client. Therefore we check if there is anything queued up waiting for us to verify
      ArrayList<? extends PeerMessage> sigsToSign = this.getSigsToCheck(peer, this.getID());
      for (PeerMessage sigToSign : sigsToSign) {
        try {
          // Check the internal peer message and store it in the database
          this.checkAndStoreMessage(peer, this, sigToSign);
        }
        catch (AlreadyReceivedMessageException e) {
          // We've already received that peer message, will ignore it.
          logger.warn("Already received message: {} from {}. Will ignore", this.getID(),
              this.getMsg().getString(MessageFields.FROM_PEER));
        }
      }

      // Prepare a message to send to all other peers (SK1) message
      JSONObject peerMsg = this.constructPeerMessage(peer, signature);

      // Send it to all peers
      peer.sendToAllPeers(peerMsg.toString());
      if (!this.checkThreshold(peer)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Stores the incoming message in the database.
   * 
   * @param peer
   *          The WBB peer.
   * @param signature
   *          The signature of the message.
   * @return True if the message was successfully stored.
   * @throws IOException
   * @throws UnknownDBException
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws JSONException
   */
  protected boolean storeIncomingMessage(WBBPeer peer, String signature) throws IOException, UnknownDBException,
      NoSuchAlgorithmException, InvalidKeyException, SignatureException, JSONException {
    try {
      peer.getDB().submitIncomingClientMessage(DBRecordType.GENERAL, this, signature);

      // If we get here the message has been stored, otherwise an exception would have been thrown
      return true;
    }
    catch (AlreadyReceivedMessageException e) {
      // Serial number already used or message already received - either way the message is rejected.
      logger.warn("Serial No {} already used. Sending error message to Client. msg:{}", this.getID(), this.msg);
      SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.SERIALNO_USED);

      return false;
    }
  }

  /**
   * Stores an incoming checked peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          The message to store.
   * @param isValid
   *          Whether the message is valid.
   * @param ignoreTimeout
   *          Ignore the timeout block?
   * @param isConflict
   *          Store in the conflict collection?
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   */
  public void submitIncomingPeerMessageChecked(WBBPeer peer, PeerMessage msg, boolean isValid, boolean ignoreTimeout,
      boolean isConflict) throws AlreadyReceivedMessageException, AlreadySentTimeoutException {
    peer.getDB().submitIncomingPeerMessageChecked(DBRecordType.GENERAL, msg, isValid, ignoreTimeout, isConflict);
  }
}
