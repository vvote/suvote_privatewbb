/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldsCancel;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldsPOD;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * Abstract class that is extended by all Message classes
 * 
 * Defines basic methods for all message classes allowing them to be treated as generic messages whenever possible. Includes a
 * parsing method to generate the appropriate message type from the underlying JSON.
 * 
 * @author Chris Culnane
 * 
 */
public abstract class JSONWBBMessage {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(JSONWBBMessage.class);

  /**
   * The underlying JSONObject containing the message
   */
  protected JSONObject        msg;

  /**
   * All message must have a type. Generic should never be explicitly constructed
   */
  protected Message           type   = Message.GENERIC;

  /**
   * The id attribute confers the unique ID for this message. It could be serialNo, submissionID or sessionID
   */
  protected String            id     = "";

  /**
   * All messages, except cancel messages, have a commitTime which defines when they will be committed
   */
  protected String            commitTime;

  /**
   * Protected Constructor for JSONWBBMessage takes message as a JSONObject
   * 
   * @param msg
   *          the JSONObject of the message
   * @throws MessageJSONException
   */
  protected JSONWBBMessage(JSONObject msg) throws MessageJSONException {
    super();

    this.msg = msg;

    try {
      // Check and set commitTime
      if (msg.has(MessageFields.JSONWBBMessage.COMMIT_TIME)) {
        this.commitTime = msg.getString(MessageFields.JSONWBBMessage.COMMIT_TIME);
      }
    }
    catch (JSONException e) {
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Protected constructor for a JSONWBBMessage
   * 
   * Should never be called directly
   * 
   * @param msgString
   *          the underlying message string
   * @throws MessageJSONException
   */
  protected JSONWBBMessage(String msgString) throws MessageJSONException {
    super();

    try {
      this.msg = new JSONObject(msgString);

      // Check for commit time and set accordingly - can't guarantee it exists, cancel messages won't have one
      if (this.msg.has(MessageFields.JSONWBBMessage.COMMIT_TIME)) {
        this.commitTime = this.msg.getString(MessageFields.JSONWBBMessage.COMMIT_TIME);
      }
    }
    catch (JSONException e) {
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Adds the commit time to a response.
   * 
   * @param response
   *          The message response.
   * 
   * @throws JSONException
   */
  public void addAdditionalContentToResponse(JSONObject response) throws JSONException {
    response.put(MessageFields.JSONWBBMessage.COMMIT_TIME, this.getCommitTime());
  }

  /**
   * Performs signature verification on the message and stores the result to the database.
   * 
   * The message being checked is from another peer. That message will contain a serial number and a signature, nothing else. As
   * such, the original message it is referring to is passed in to verify that the signature from the peer matches the data we have
   * locally.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          generic message from a client that is used to perform the check
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   */
  public void checkAndStoreMessage(WBBPeer peer, PeerMessage msg) throws JSONException, AlreadyReceivedMessageException {
    try {
      // Get the signable content from the message. As such, the type of message is not important, Vote, Audit, POD File the data
      // being signed is determined by the underlying message type
      boolean valid = this.checkPeerMessageSignature(peer, msg);

      // Irrespective of the whether signature is valid or not we store it. The only exception is if the signature checking throws
      // and exception. In which case we do not store it to the database. Whilst we could in theory rely on the SSL connection to
      // perform authentication, from a protocol point of view we do not. If the signature is well formed but incorrect we know the
      // peer received the wrong data. If the signature is not well formed we cannot tell
      peer.getDB().submitIncomingPeerMessageChecked(DBRecordType.GENERAL, msg, valid, false, false);
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing message", e);
    }
    catch (AlreadySentTimeoutException e) {
      try {
        // Indicates we received a peer message after the timeout. We want to store it, but not include it within the set of valid
        // messages
        peer.getDB().submitPostTimeoutMessage(msg);
      }
      catch (UnknownDBException e1) {
        logger.error("Error trying store message to postTimeout set", e1);
      }
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing message", e);
    }
  }

  /**
   * Verifies the peer message signature.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          The message to verify.
   * @return True if the signature is valid.
   * @throws KeyStoreException
   * @throws TVSSignatureException
   * @throws JSONException
   */
  protected boolean checkPeerMessageSignature(WBBPeer peer, PeerMessage msg) throws KeyStoreException, TVSSignatureException,
      JSONException {
    TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateFromCerts(CertStore.PEER, msg.getMsg()
        .getString("peerID") + WBBConfig.SIGNING_KEY_SK1_SUFFIX));

    tvsSig.update(this.getInternalSignableContent());

    return tvsSig.verify(msg.getMsg().getString(MessageFields.PeerMessage.PEER_SIG), EncodingType.BASE64);
  }

  /**
   * Abstract method.
   * 
   * Checks whether we have reached a consensus on a message and sends the response.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message sent. Consensus may not have been reached in which case an error response was sent.
   * @throws MessageJSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws JSONException
   * @throws InvalidKeyException
   * @throws IOException
   * @throws TVSSignatureException
   */
  public abstract boolean checkThreshold(WBBPeer peer) throws MessageJSONException, NoSuchAlgorithmException, SignatureException,
      JSONException, InvalidKeyException, IOException, TVSSignatureException;

  /**
   * Constructs a response to a message and sends it with a signature.
   * 
   * @param peer
   *          The WBB peer.
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws IOException
   */
  public void constructResponseAndSend(WBBPeer peer) throws TVSSignatureException, JSONException, IOException {
    peer.sendAndClose(this.getMsg().getString(MessageFields.UUID), this.constructResponseAndSign(peer).toString());
  }

  /**
   * Constructs a response to a message with a signature.
   * 
   * @param peer
   *          The WBB peer.
   * @return The response.
   * @throws TVSSignatureException
   * @throws JSONException
   */
  public JSONObject constructResponseAndSign(WBBPeer peer) throws TVSSignatureException, JSONException {
    TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getSK2());

    tvsSig.update(this.getExternalSignableContent());

    JSONObject response = this.constructResponseWithoutSignature(peer);
    response.put(MessageFields.JSONWBBMessage.PEER_SIG, tvsSig.signAndEncode(EncodingType.BASE64));

    return response;
  }

  /**
   * Constructs a response to a message without signing it.
   * 
   * @param peer
   *          The WBB peer.
   * @return The response.
   * @throws JSONException
   */
  public JSONObject constructResponseWithoutSignature(WBBPeer peer) throws JSONException {
    JSONObject response = new JSONObject();
    response.put(MessageFields.JSONWBBMessage.ID, this.getID());
    response.put(MessageFields.JSONWBBMessage.TYPE, this.getTypeString());
    response.put(MessageFields.JSONWBBMessage.PEER_ID, peer.getID());

    return response;
  }

  /**
   * Creates a signature on the internal signable content.
   * 
   * @param peer
   *          The WBB peer.
   * @return The internal signature.
   * @throws TVSSignatureException
   * @throws JSONException
   */
  public String createInternalSignature(WBBPeer peer) throws TVSSignatureException, JSONException {
    TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getSK1());
    tvsSig.update(this.getInternalSignableContent());

    return tvsSig.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Get the commitTime Checks if the instance variable has been set, if not tries to load it
   * 
   * @return a string containing the commitTime or an empty string if it does not exist
   */
  public String getCommitTime() {
    if (this.commitTime == null) {
      if (this.msg.has(MessageFields.JSONWBBMessage.COMMIT_TIME)) {
        try {
          this.commitTime = this.msg.getString(MessageFields.JSONWBBMessage.COMMIT_TIME);
        }
        catch (JSONException e) {
          logger.warn("Tried to get commitTime from message, but value was null", e);
        }
      }
      else {
        return "";
      }
    }

    return this.commitTime;
  }

  /**
   * Abstract method
   * 
   * Defines the external content that should be signed by message. This allows generic processing of different types of message
   * since the message itself defines what should and should not be signed. For example, an audit message is just the serial number,
   * whilst a vote is the serial number and preferences. Note, it is the contents of the message that is to be signed by the
   * WBBPeer, not the contents of the message that was signed by the EBM or client device.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  public abstract String getExternalSignableContent() throws JSONException;

  /**
   * Gets the ID of this message
   * 
   * @return String of the id of this message, could be a serial number or a submission ID, depending on message type
   */
  public String getID() {
    return this.id;
  }

  /**
   * Abstract method
   * 
   * Defines the internal content that should be signed by message. This allows generic processing of different types of message
   * since the message itself defines what should and should not be signed. For example, an audit message is just the serial number,
   * whilst a vote is the serial number and preferences. Note, it is the contents of the message that is to be signed by the
   * WBBPeer, not the contents of the message that was signed by the EBM or client device.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  public abstract String getInternalSignableContent() throws JSONException;

  /**
   * Get the underlying JSONObject of this message
   * 
   * @return JSONObject of the message
   */
  public JSONObject getMsg() {
    return this.msg;
  }

  /**
   * @return The peer type for the current message.
   */
  public String getPeerTypeFromClientType() {
    switch (this.type) {
      case AUDIT_MESSAGE:
      case VOTE_MESSAGE:
      case FILE_MESSAGE:
      case BALLOT_AUDIT_COMMIT:
      case BALLOT_GEN_COMMIT:
      case MIX_RANDOM_COMMIT:
        return "peer";
      case CANCEL_MESSAGE:
        return "peercancel";
      case POD_MESSAGE:
        return "peerpod";
      default:
        return "peer";

    }
  }

  /**
   * Gets the message type
   * 
   * @return Message type
   */
  public Message getType() {
    return this.type;
  }

  /**
   * @return The message type for the current message.
   */
  public String getTypeString() {
    return JSONWBBMessage.getTypeString(this.type);
  }

  /**
   * Abstract method to perform validation
   * 
   * The overridden method should perform the relevant validation for the particular message type
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  public abstract void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException;

  /**
   * Pre-processes an incoming message.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream for the message.
   * @param socket
   *          The socket used for the send.
   * @return True if the message was pre-processed and validated successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   */
  public boolean preProcessMessage(WBBPeer peer, BoundedBufferedInputStream in, Socket socket) throws IOException, JSONException,
      NoSuchAlgorithmException {
    return true;
  }

  /**
   * Dummy method which should be overridden for commit round 2 messages.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sendingPeer
   *          The sending peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws CommitProcessingException
   */
  public void processAsPartOfCommitR2(WBBPeer peer, JSONObject record, String sendingPeer, String safeUploadDir)
      throws CommitProcessingException {
    // Do nothing - should be overridden by messages that are part of CommitR2
    throw new CommitProcessingException("Message class has not implemented Commit R2 processing - FATAL ERROR");
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   */
  public abstract boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException;

  /**
   * Utility method to send an error message and close the socket connection.
   * 
   * @param message
   *          the message to send
   * @param sock
   *          the socket to send the message on
   * @throws IOException
   */
  public void sendMessageAndClose(String message, Socket sock) throws IOException {
    PrintStream ps = new PrintStream(sock.getOutputStream(), true);
    ps.println(message);
    sock.close();
  }

  /**
   * Set the commitTime for this message.
   * 
   * Sets the value both on the underlying message and on the instance variable
   * 
   * @param cid
   *          string of the commitTime
   * @throws JSONException
   */
  public void setCommitTime(String cid) throws JSONException {
    this.commitTime = cid;
    this.msg.put(MessageFields.JSONWBBMessage.COMMIT_TIME, cid);
  }

  /**
   * @return A string version of the object.
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    if (this.msg != null) {
      return this.msg.toString();
    }
    else {
      return super.toString();
    }
  }

  /**
   * Validates a the underlying message against the passed in JSON Schema
   * 
   * The result is true or false, but a full list of errors is logged.
   * 
   * Uses built in JavaScript engine to run tv4 javascript schema validator
   * 
   * 
   * @param schema
   *          the String containing the JSON Schema to use for validation
   * @return true if it is valid, false if not
   */
  protected boolean validateSchema(String schema) {
    return JSONUtils.validateSchema(schema, this.msg.toString());
  }

  /**
   * Generic validation serial signature method
   * 
   * Any type of message that contains a signed ID (serialNo) can call this method to check that it has been signed by the
   * ElectionManager. In most cases this will be the public key of the PrintOnDemand service.
   * 
   * @param peer
   *          the WBBPeer this is running on - needed for access to keys
   * @return the result of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  protected boolean validateSerialSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      String serialNo = this.msg.getString(MessageFields.JSONWBBMessage.ID);
      Object serialSigs = this.msg.get(MessageFields.JSONWBBMessage.SERIAL_SIG);

      if (serialSigs instanceof JSONArray) {
        JSONArray serialSig = (JSONArray) serialSigs;

        try {
          TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateKeyStore(CertStore.PEER));
          tvsSig.update(serialNo);
          tvsSig.update(this.msg.getString(MessageFields.JSONWBBMessage.DISTRICT));
          JSONObject response = tvsSig.verify(serialSig, WBBConfig.SIGNING_KEY_SK2_SUFFIX);

          if (response.getInt(TVSSignature.VALID_COUNT) >= peer.getThreshold()) {
            logger.info("Verified serial signature successfully {}", response);
            return true;
          }
          else {
            logger.warn("Serial number signature failed {}", response);
            return false;
          }
        }
        catch (TVSSignatureException e) {
          throw new MessageSignatureException("Error when verifying signature", e);
        }
      }
      else {
        try {
          TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateKeyStore(CertStore.PEER).getBLSCertificate(
              "WBB"));
          tvsSig.update(serialNo);
          tvsSig.update(this.msg.getString(MessageFields.JSONWBBMessage.DISTRICT));
          if (tvsSig.verify((String) serialSigs, EncodingType.BASE64)) {
            logger.info("Verified serial signature successfully");
            return true;
          }
          else {

            logger.warn("Serial number signature failed");
            return false;
          }
        }
        catch (TVSSignatureException e) {
          throw new MessageSignatureException("Error when verifying signature", e);
        }
        catch (TVSKeyStoreException e) {
          throw new MessageSignatureException("Error when verifying signature", e);
        }
      }
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
  }

  /**
   * @return The message type for the current message.
   */
  public static String getTypeString(Message msgType) {
    switch (msgType) {
      case AUDIT_MESSAGE:
        return "audit";
      case BALLOT_AUDIT_COMMIT:
        return "ballotauditcommit";
      case BALLOT_GEN_COMMIT:
        return "ballotgencommit";
      case CANCEL_MESSAGE:
        return "cancel";
      case COMMIT_R1:
        return "commitr1";
      case FILE_MESSAGE:
        return "file";
      case GENERIC:
        return "generic";
      case MIX_RANDOM_COMMIT:
        return "mixrandomcommit";
      case PEER_CANCEL_MESSAGE:
        return "peercancel";
      case PEER_FILE_MESSAGE:
        return "peerfile";
      case PEER_MESSAGE:
        return "peer";
      case PEER_POD_MESSAGE:
        return "peerpod";
      case POD_MESSAGE:
        return "pod";
      case VOTE_MESSAGE:
        return "vote";
      case START_EVM:
        return "startevm";
      case HEARTBEAT:
        return "heartbeat";
      default:
        return "UNKNOWN";
    }
  }

  /**
   * Loads in the messages sent as part of a commit record.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @return The loaded messages.
   * @throws MessageJSONException
   * @throws JSONException
   */
  public static List<JSONWBBMessage> loadCommitMessages(WBBPeer peer, JSONObject record) throws MessageJSONException, JSONException {
    ArrayList<JSONWBBMessage> msgsInRecord = new ArrayList<JSONWBBMessage>();

    if (record.has(DBFieldsPOD.getInstance().getField(DBFieldName.CLIENT_MESSAGE))) {
      msgsInRecord
          .add(JSONWBBMessage.parseMessage(record.getString(DBFieldsPOD.getInstance().getField(DBFieldName.CLIENT_MESSAGE))));
    }

    if (record.has(DBFields.getInstance().getField(DBFieldName.CLIENT_MESSAGE))) {
      msgsInRecord.add(JSONWBBMessage.parseMessage(record.getString(DBFields.getInstance().getField(DBFieldName.CLIENT_MESSAGE))));
    }

    if (record.has(DBFieldsCancel.getInstance().getField(DBFieldName.CLIENT_MESSAGE))) {
      JSONArray clientCancels = record.getJSONArray(DBFieldsCancel.getInstance().getField(DBFieldName.CLIENT_MESSAGE));

      for (int x = 0; x < clientCancels.length(); x++) {
        JSONWBBMessage message = JSONWBBMessage.parseMessage(clientCancels.getString(x));
        try {
          // stop once we find a message that passes validation
          message.performValidation(peer);
          msgsInRecord.add(message);
          break;
        }
        catch (MessageVerificationException | JSONSchemaValidationException e) {
          logger.warn("Messages sent as part of Commit are invalid, will ignore", e);
        }
      }
    }

    return msgsInRecord;
  }

  /**
   * Parse a JSONObject into the relevant Message Type and returns a generic JSONWBBMessage
   * 
   * @param message
   *          JSONObject of the message
   * @return JSONWBBMessage with the appropriate underlying Message type
   * @throws MessageJSONException
   */
  public static JSONWBBMessage parseMessage(JSONObject message) throws MessageJSONException {
    try {
      if (message.has("type")) {
        if (message.getString("type").equalsIgnoreCase("audit")) {
          return new AuditMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("cancel")) {
          return new CancelMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("vote")) {
          return new VoteMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peer")) {
          return new PeerMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peercancel")) {
          return new PeerCancelMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("file")) {
          return new FileMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("pod")) {
          return new PODMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peerpod")) {
          return new PeerPODMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("commitr1")) {
          return new CommitR1Message(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peerfile")) {
          return new CommitR2Message(message);
        }
        else if (message.getString("type").equalsIgnoreCase("mixrandomcommit")) {
          return new MixRandomCommitMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("ballotgencommit")) {
          return new BallotGenCommitMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("ballotauditcommit")) {
          return new BallotAuditCommitMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("startevm")) {
          return new StartEVMMessage(message);
        }else if (message.getString("type").equalsIgnoreCase("heartbeat")) {
          return new HeartBeatMessage(message);
        }
        else {
          logger.warn("Cannot parse message - unknown type:{}", message);
          throw new MessageJSONException("Cannot parse message - unknown type");
        }
      }
      else {
        logger.warn("Cannot parse message - missing type:{}", message);
        throw new MessageJSONException("Cannot parse message - missing type");
      }
    }
    catch (JSONException e) {
      logger.warn("Cannot parse message -JSON Exception:", e);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Parses the string message into a JSONObject and then constructs the appropriate Message Type
   * 
   * @param message
   *          JSON String representing the message
   * @return a generic JSONWBBMessage with the appropriate underlying type
   * @throws MessageJSONException
   */
  public static JSONWBBMessage parseMessage(String message) throws MessageJSONException {
    try {
      JSONObject messageObj = new JSONObject(message);
      return parseMessage(messageObj);
    }
    catch (JSONException e) {
      logger.warn("Cannot parse message -JSON Exception:", e);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Parses a message from a JSON object and constructs the corresponding message.
   * 
   * @param message
   *          The JSON message object.
   * @return The constructed message.
   * @throws MessageJSONException
   */
  public static PeerMessage parsePeerMessage(JSONObject message) throws MessageJSONException {
    try {
      if (message.has("type")) {
        if (message.getString("type").equalsIgnoreCase("peer")) {
          return new PeerMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peercancel")) {
          return new PeerCancelMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peerpod")) {
          return new PeerPODMessage(message);
        }
        else if (message.getString("type").equalsIgnoreCase("commitr1")) {
          return new CommitR1Message(message);
        }
        else if (message.getString("type").equalsIgnoreCase("peerfile")) {
          return new CommitR2Message(message);
        }
        else {
          logger.warn("Cannot parse message - unknown type:{}", message);
          throw new MessageJSONException("Cannot parse message - unknown type");
        }
      }
      else {
        logger.warn("Cannot parse message - missing type:{}", message);
        throw new MessageJSONException("Cannot parse message - missing type");
      }
    }
    catch (JSONException e) {
      logger.warn("Cannot parse message -JSON Exception:", e);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Parses the string message into a PeerMessage and constructs the corresponding message.
   * 
   * @param message
   *          JSON String representing the message
   * @return a generic PeerMessage with the appropriate underlying type
   * @throws MessageJSONException
   */
  public static PeerMessage parsePeerMessage(String message) throws MessageJSONException {
    try {
      JSONObject messageObj = new JSONObject(message);
      return parsePeerMessage(messageObj);
    }
    catch (JSONException e) {
      logger.warn("Cannot parse message -JSON Exception:", e);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }
}
