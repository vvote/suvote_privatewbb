/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents an internal message between peers that is used during round 1 of the consensus protocol.
 * 
 * It only contains the signature from the sending peer and a serial number
 * 
 * @author Chris Culnane
 * 
 */
public class PeerCancelMessage extends PeerMessage {

  /**
   * Logger
   */
  private static final Logger logger   = LoggerFactory.getLogger(PeerCancelMessage.class);

  /**
   * Instance variable to record which peer sent the message
   */
  private String              fromPeer = null;

  /**
   * Constructor to create PeerCancelMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public PeerCancelMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create PeerCancelMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public PeerCancelMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return null;
  }

  /**
   * Gets the _fromPeer value and caches it in the instance variable if it exists.
   * 
   * @return String of the _fromPeer value
   */
  @Override
  public String getFromPeer() {
    if (this.fromPeer == null) {
      if (this.msg.has(MessageFields.FROM_PEER)) {
        try {
          this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
        }
        catch (JSONException e) {
          logger.error("Trying to get _fromPeer, has thrown error", e);
        }
      }
    }

    return this.fromPeer;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    // This message will never be signed by anything else
    return null;
  }

  /**
   * Init the message object
   * 
   * Extract the id, set the type and check for a _fromPeer value
   * 
   * @throws JSONException
   */
  @Override
  protected void init() throws JSONException {
    this.type = Message.PEER_CANCEL_MESSAGE;
    this.id = this.msg.getString(MessageFields.PeerMessage.ID);
    if (this.msg.has(MessageFields.FROM_PEER)) {
      this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    // Check against PEER CANCEL schema
    if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.PEERCANCEL))) {
      logger.warn("JSON Schema validation failed");
      throw new JSONSchemaValidationException("JSON Schema validation failed");
    }
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    String[] messagesFromClients = peer.getDB().getClientCancelMessages(this.getID());

    if (messagesFromClients.length == 0) {
      // No message from audit machine received
      try {
        peer.getDB().submitIncomingPeerMessageUnchecked(DBRecordType.CANCEL, this);
      }
      catch (AlreadyReceivedMessageException e) {
        logger.warn("Already received message: {} from {}. Will ignore", this.getID(), this.msg.getString(MessageFields.FROM_PEER));
        return false;
      }
      catch (ClientMessageExistsException e) {
        // EBM Message now received perform checks
        try {
          JSONWBBMessage.parseMessage(peer.getDB().getClientCancelMessages(this.getID())[0]).checkAndStoreMessage(peer, this);
          // this.checkAndStoreCancelMessage(JSONWBBMessage.parseMessage(peer.getDB().getClientCancelMessages(getID())[0]), this);
        }
        catch (AlreadyReceivedMessageException e1) {
          logger.warn("Already received message: {} from {}. Will ignore", this.getID(),
              this.msg.getString(MessageFields.FROM_PEER));
          return false;
        }
      }
      catch (AlreadySentTimeoutException e) {
        logger.error("Received already sent timeout exception on Cancel - this should not happen", e);
      }
    }
    else {
      try {
        JSONWBBMessage.parseMessage(messagesFromClients[0]).checkAndStoreMessage(peer, this);
      }
      catch (AlreadyReceivedMessageException e) {
        logger.warn("Already received message: {} from {}. Will ignore", this.getID(), this.msg.getString(MessageFields.FROM_PEER));
        return false;
      }
    }

    // Have we got a threshold of responses?
    if (peer.getDB().checkThresholdAndResponse(DBRecordType.CANCEL, this.getID(), peer.getThreshold())) {
      String[] clientCancelMessage = peer.getDB().getClientCancelMessages(this.getID());

      if (clientCancelMessage.length > 0) {
        CancelMessage clientCancelMsg = new CancelMessage(clientCancelMessage[0]);
        JSONObject response = clientCancelMsg.constructResponseAndSign(peer);
        peer.getDB().submitMyCancellationSignatureSK2(this.getID(), response.getString(MessageFields.JSONWBBMessage.PEER_SIG));

        for (String client : clientCancelMessage) {
          CancelMessage ccMsg = new CancelMessage(client);
          peer.sendAndClose(ccMsg.getMsg().getString(MessageFields.UUID), response.toString());
        }
      }
      else {
        logger.error("Client message is null even though signatures have been validated. Database must be corrupt. msg:{}",
            this.msg);
        return false;
      }
    }
    else {
      if (!peer.getDB().canOrHaveReachedConsensus(DBRecordType.CANCEL, this.getID(), peer.getThreshold(), peer.getPeerCount() + 1)) {
        SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.NO_CONSENSUS_POSSIBLE);
      }
    }

    return true;
  }
}
