/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.CommitProcessor;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents the message sent by each peer during round 1 of the commit procedure. In essence, this is a hash of the entire
 * database and any uploaded files.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitR1Message extends PeerMessage {

  /**
   * The commit round 1 message type.
   */
  public static final String  TYPE_STRING = "commitr1";

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(CommitR1Message.class);

  /**
   * Source WBB peer
   */
  private String              fromPeer    = null;

  /**
   * Signable content string represents the parts of the message to be signed or used in verification
   */
  private String              signableContent;

  /**
   * Constructor to create CommitR1Message from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public CommitR1Message(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create CommitR1Message from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public CommitR1Message(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Checks and stores a commit message.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          The message to check.
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#checkAndStoreMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage)
   */
  @Override
  public void checkAndStoreMessage(WBBPeer peer, PeerMessage msg) throws JSONException, AlreadyReceivedMessageException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateFromCerts(CertStore.PEER, msg.getMsg()
          .getString(MessageFields.PeerMessage.PEER_ID) + WBBConfig.SIGNING_KEY_SK1_SUFFIX), false);

      tvsSig.update(IOUtils.decodeData(EncodingType.BASE64, peer.getDB().getCurrentCommitHash()));
      tvsSig.update(peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_TIME));
      String commitDesc =peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_DESC);
      if(commitDesc!=null){
        tvsSig.update(commitDesc);
      }
      boolean valid = tvsSig.verify(msg.getMsg().getString(MessageFields.PeerMessage.PEER_SIG), EncodingType.BASE64);

      peer.getDB().submitIncomingPeerCommitR1Checked(this, valid);
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing cancel message", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing cancel message", e);
    }
    catch (NoCommitExistsException e) {
      logger.error(
          "Could not find Commit to check message against, this should have been caught earlier - possible database error", e);
    }
  }

  /**
   * Defines the content that should be signed by message.
   * 
   * @return a string of the signable content.
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return this.signableContent;
  }

  /**
   * Returns the _fromPeer property
   * 
   * Checks if the property has already been loaded from the JSON. If it has it returns it, otherwise it attempts to load it.
   * 
   * @return _fromPeer value
   */
  @Override
  public String getFromPeer() {
    if (this.fromPeer == null) {
      if (this.msg.has(MessageFields.FROM_PEER)) {
        try {
          this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
        }
        catch (JSONException e) {
          logger.error("Trying to get _fromPeer, has thrown error", e);
        }
      }
    }
    return this.fromPeer;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.signableContent;
  }

  /**
   * Init method to set type and extract id - in this the Commit ID a generated UUID
   * 
   * @throws JSONException
   */
  @Override
  protected void init() throws JSONException {
    this.type = Message.COMMIT_R1;
    this.id = this.msg.getString(MessageFields.Commit.COMMIT_ID);
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    // We only perform Schema validation on these messages. The signatures/ are checked as part of the wider processing
    if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.COMMITR1))) {
      logger.warn("JSON Schema validation failed");
      throw new JSONSchemaValidationException("JSON Schema validation failed");
    }
  }

  /**
   * Processes an incoming commit message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was processed successfully.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    try {
      // We call this to check we have a current commit hash, throwing an exception if we don't
      peer.getDB().getCurrentCommitHash();

      this.checkAndStoreMessage(peer, this);
    }
    catch (NoCommitExistsException e) {
      logger.warn("Have received commit message before preparing a commit: {} from {}. Will create commit stub",
          this.msg.toString(), this.msg.getString(MessageFields.FROM_PEER));

      try {
        peer.getDB().submitIncomingPeerCommitR1UnChecked(this);
      }
      catch (AlreadyReceivedMessageException e1) {
        logger.warn("Already received message: {} from {}. Will ignore", this.getID(), this.msg.getString(MessageFields.FROM_PEER));
        return false;
      }
      catch (CommitSignatureExistsException e1) {
        try {
          peer.getDB().getCurrentCommitHash();

          this.checkAndStoreMessage(peer, this);
        }
        catch (AlreadyReceivedMessageException e2) {
          logger.warn("Already received message: {} from {}. Will ignore", this.getID(),
              this.msg.getString(MessageFields.FROM_PEER));
          return false;
        }
        catch (NoCommitExistsException e2) {
          logger
              .error(
                  "Commit Signature is null even though it previously existed - indicates the underlying commit has been modified whilst running the commit protocol - message discarded msg:{}",
                  this.msg, e2);
          return false;
        }
      }
    }
    catch (AlreadyReceivedMessageException e) {
      logger.warn("Already received message: {} from {}. Will ignore", this.getID(), this.msg.getString(MessageFields.FROM_PEER));
      return false;
    }

    CommitProcessor.checkCommitThreshold(peer);

    return true;
  }
}
