/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldsCancel;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents CancelMessage received from the cancellation machine.
 * 
 * Cancel messages are handled differently to other messages because the cancellation procedure can be run many times until it
 * succeeds. There is only one acceptable outcome from cancellation and that is a joint signature showing the cancellation has
 * succeeded. If that is not obtained the procedure is run again until it is.
 * 
 * 
 * TODO Look at how we cancel a ballot for which we do not have a serial number. For example when POD fails
 * 
 * @author Chris Culnane
 * 
 */
public class CancelMessage extends ExternalMessage {

  /**
   * Telemetry Logger
   */
  private static final Logger telemetry = LoggerFactory.getLogger("Telemetry");
  /**
   * Logger
   */
  private static final Logger logger    = LoggerFactory.getLogger(CancelMessage.class);

  /**
   * String to hold signature object - this required for CancelMessage because we only store one signature on the client message, no
   * matter how many times a cancel is requested. This is because the contents of the signature cannot vary. Strictly speaking, with
   * BLS the generated signature will always be the same, because it is deterministic. However, to provide compatibility we replace
   * any generated signature with the one from the database if it exists. Note: CommitTime is not include in Cancel signatures
   */
  private String              signature;

  /**
   * Constructor to create CancelMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public CancelMessage(JSONObject msg) throws MessageJSONException {
    super(msg);
    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create CancelMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public CancelMessage(String msgString) throws MessageJSONException {
    super(msgString);
    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Checks and stores a cancellation message.
   * 
   * Unlike other types of messages the cancellation process can be run multiple times until it is successful. As such, it needs to
   * be handled slightly differently by the database.
   * 
   * @param peer
   *          The WBBPeer this is running on
   * @param msg
   *          message from peer that is being checked
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   *           - already received a message from the peer
   */
  protected void checkAndStoreCancelMessage(WBBPeer peer, PeerMessage msg) throws JSONException, AlreadyReceivedMessageException {
    try {
      boolean valid = this.checkPeerMessageSignature(peer, msg);
      peer.getDB().submitIncomingPeerMessageChecked(DBRecordType.CANCEL, msg, valid, true, false);
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing cancel message", e);
    }
    catch (AlreadySentTimeoutException e) {
      logger.error("Received already sent timeout exception on Cancel - this should not happen", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing cancel message", e);
    }
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return this.externalSignableContent;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.internalSignableContent;
  }

  /**
   * Initialise object
   * 
   * Extracts the id and sets the type as well as calculating the signable content
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.CANCEL_MESSAGE;
    this.id = this.msg.getString(MessageFields.CancelMessage.ID);

    // Sign cancel and id
    StringBuffer signableContentSB = new StringBuffer();
    signableContentSB.append(this.getTypeString());
    signableContentSB.append(this.id);
    this.internalSignableContent = signableContentSB.toString();
    this.externalSignableContent = this.internalSignableContent;
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Validate the JSON against the Cancel Schema
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.CANCEL))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Validates the serial number signature
      if (!this.validateSerialSignature(peer)) {
        logger.warn("Serial Number signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Serial Number signature is not valid");
      }

      // Validates the signature came from a valid cancellation machine
      if (!this.validateCancelMachineSignature(peer)) {
        logger.warn("Cancel machine signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Cancel Machine signature is not valid");
      }

      // // Validates the auth signature came from a valid cancellation authorisation device
      if (!this.validateCancelAuthSignature(peer)) {
        logger.warn("Cancel authorisation signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Cancel authorisation signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg, e);
      throw new MessageVerificationException("Message is not well formed", e);
    }
  }

  /**
   * Processes the messages as part of commit round 2.
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sendingPeer
   *          The sending WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#processAsPartOfCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      org.json.JSONObject, java.lang.String, java.lang.String)
   */
  @Override
  public void processAsPartOfCommitR2(WBBPeer peer, JSONObject record, String sendingPeer, String safeUploadDir) {
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);
    try {
      if (!peer.getDB().isAlreadyStored(this.getID(), DBRecordType.CANCEL, peer.getThreshold())) {
        ArrayList<JSONWBBMessage> validPeerMessages = new ArrayList<JSONWBBMessage>();
        JSONObject row = peer.getDB().getRecord(this.getID());

        // We should already have performed this in order to end up with a CancelMessage, however, it would be possible that we
        // haven't if the message had been constructed via a different route.
        this.performValidation(peer);

        // Store the data in the database if it is valid.
        if (record.has(fields.getField(DBFieldName.MY_SIGNATURE))) {
          boolean valid = this.checkMessageFromExternalPeer(peer, sendingPeer,
              record.getString(fields.getField(DBFieldName.MY_SIGNATURE)));
          if (valid) {
            // Construct simulated peer message so we can store it in our db
            validPeerMessages.add(JSONWBBMessage.parseMessage(this.constructSimulatedPeerMessage(record.getString(DBFields.ID),
                record.getString(fields.getField(DBFieldName.MY_SIGNATURE)), sendingPeer)));
          }
        }
        else {
          return;
        }
        this.checkValidPeerMessagesInR2Record(peer, record, validPeerMessages, fields);
        boolean haveExistingClientCancel = false;

        if (row != null && row.has(fields.getField(DBFieldName.CLIENT_MESSAGE))) {
          haveExistingClientCancel = true;
        }

        if (row == null || !haveExistingClientCancel) {
          try {
            peer.getDB().submitIncomingCommitMessage(DBRecordType.CANCEL, this, false);
          }
          catch (AlreadyReceivedMessageException e1) {

          }
          // No record at all
        }
        for (JSONWBBMessage m : validPeerMessages) {
          // We don't want to add our own message again
          m.getMsg().put(MessageFields.CommitR2.FROM_COMMIT, true);
          try {
            peer.getDB().submitIncomingPeerMessageChecked(DBRecordType.CANCEL, (PeerCancelMessage) m, true, true, false);
          }
          catch (AlreadyReceivedMessageException e1) {

          }
        }
      }
    }
    catch (KeyStoreException e) {
      logger.error("Exception with key store whilst checking and storing cancel message", e);
    }
    catch (MessageJSONException e) {
      logger.error("Message Exception when processing R2 Cancel message record - indicates malformed message - will ignore record",
          e);
    }
    catch (MessageVerificationException e) {
      logger
          .error(
              "Message Verification Exception when processing R2 Cancel message record - indicates malformed message - will ignore record",
              e);
    }

    catch (JSONException e) {
      logger.error("JSON Exception whilst processing R2 Cancel record - indicates malformed message - record discarded", e);
    }
    catch (JSONSchemaValidationException e) {
      logger
          .error(
              "JSON Schema Validation failed when processing R2 Cancel message record - indicates malformed message - will ignore record",
              e);
    }
    catch (AlreadySentTimeoutException e) {
      logger.error("Received already sent timeout exception on Cancel - this should not happen", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception whilst checking and storing cancel message", e);
    }
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    this.signature = this.createInternalSignature(peer);

    boolean messageAccepted = this.storeIncomingMessage(peer, signature);

    if (messageAccepted) {
      // Check if there are any message from other peers that need verifying now we have a message from the client
      ArrayList<PeerMessage> cancelSigsToCheck = peer.getDB().getSigsToCheck(DBRecordType.CANCEL, this.getID());
      for (PeerMessage cancelSigToCheck : cancelSigsToCheck) {
        try {
          this.checkAndStoreCancelMessage(peer, cancelSigToCheck);
        }
        catch (AlreadyReceivedMessageException e) {
          logger.warn("Already received message: {} from {}. Will ignore", this.getID(),
              this.getMsg().getString(MessageFields.FROM_PEER));
        }
      }

      JSONObject peermsg = this.constructPeerMessage(peer, signature);

      peer.sendToAllPeers(peermsg.toString());

      // Check for a threshold of cancel signatures from other peers
      if (peer.getDB().checkThresholdAndResponse(DBRecordType.CANCEL, this.getID(), peer.getThreshold())) {
        // Because the cancel procedure can be run as many times as desired there is a set of original message.
        String[] clientCancelMessage = peer.getDB().getClientCancelMessages(this.getID());

        if (clientCancelMessage.length > 0) {
          CancelMessage clientCancelMsg = new CancelMessage(clientCancelMessage[0]);
          JSONObject response = clientCancelMsg.constructResponseAndSign(peer);

          String cancelSigSK2 = response.getString(MessageFields.JSONWBBMessage.PEER_SIG);
          cancelSigSK2 = peer.getDB().submitMyCancellationSignatureSK2(this.getID(), cancelSigSK2);

          // update the signature in the message - incase we already stored one in the DB
          response.put(MessageFields.JSONWBBMessage.PEER_SIG, cancelSigSK2);
          // We send the response to all clients who have sent a cancel message. This is to both clean up any left over response
          // channels (note they do not time out like other messages) but also to give the best chance of successful communication
          for (String client : clientCancelMessage) {
            CancelMessage ccMsg = new CancelMessage(client);
            peer.sendAndClose(ccMsg.getMsg().getString(MessageFields.UUID), response.toString());
          }
        }
        else {
          logger.error("Client message is null even though signatures have been validated. Database must be corrupt. msg:{}",
              this.msg);
          return false;
        }

      }
      else {
        // Check whether we can reach a consensus on the cancellation. If we can't it indicates the threshold assumption has failed
        // and we have many peers acting dishonestly
        if (!peer.getDB()
            .canOrHaveReachedConsensus(DBRecordType.CANCEL, this.getID(), peer.getThreshold(), peer.getPeerCount() + 1)) {
          SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.NO_CONSENSUS_POSSIBLE);
        }
      }
    }
    telemetry.info("MSGCANCEL_MESSAGE#{}", this);
    return true;
  }

  /**
   * Stores the incoming message in the database.
   * 
   * @param peer
   *          The WBB peer.
   * @param signature
   *          The signature of the message.
   * @return True if the message was successfully stored.
   * @throws IOException
   * @throws UnknownDBException
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#storeIncomingMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected boolean storeIncomingMessage(WBBPeer peer, String signature) throws NoSuchAlgorithmException, InvalidKeyException,
      SignatureException, JSONException, IOException, UnknownDBException {
    try {
      // Try to store the message
      peer.getDB().submitIncomingClientCancellationMessage(this);

      // If we get to this point it means the message has been stored

      // We submit the signature we generated, however, if we have already generated a signature it will be returned instead of
      // storing a new one.
      this.signature = peer.getDB().submitMyCancellationSignature(this.getID(), signature); 


      return true;
    }
    catch (AlreadyReceivedMessageException e) {
      // If we have already stored an SK2 signature the above Exception will be thrown, we therefore just need to return out
      // previously generated SK2
      String SK2 = peer.getDB().getField(this.getID(), DBFieldsCancel.getInstance().getField(DBFieldName.SK2_SIGNATURE));

      if (SK2 == null) {
        // It should not be possible for this to happen, because of the query structures, but catch it just in case
        logger.error("Cancellation is recorded as happening, but no SK2 exists - corrupt db? msg:{}", this.msg);
        throw new UnknownDBException("Cancellation is recorded as happening, but no SK2 exists - corrupt db?");
      }
      else {
        // Construct a message to the client using the previously generated SK2
        JSONObject response = this.constructResponseWithoutSignature(peer);
        response.put(MessageFields.JSONWBBMessage.PEER_SIG, SK2);
        peer.sendAndClose(this.getMsg().getString(MessageFields.UUID), response.toString());
      }
    }
    catch (UnknownDBException e) {
      logger.error("Unknown DB Exception has occured during Cancellation", e);
      SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.UNKNOWN_DB_ERROR);
    }

    return false;
  }

  /**
   * /** Stores an incoming checked peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @param msg
   *          The message to store.
   * @param isValid
   *          Whether the message is valid.
   * @param ignoreTimeout
   *          Ignore the timeout block?
   * @param isConflict
   *          Store in the conflict collection?
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#submitIncomingPeerMessageChecked(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage, boolean, boolean, boolean)
   */
  @Override
  public void submitIncomingPeerMessageChecked(WBBPeer peer, PeerMessage msg, boolean isValid, boolean ignoreTimeout,
      boolean isConflict) throws AlreadyReceivedMessageException, AlreadySentTimeoutException {
    peer.getDB().submitIncomingPeerMessageChecked(DBRecordType.CANCEL, msg, isValid, true, false);

  }

  /**
   * Validates the cancel authorisation message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if valid.
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validateCancelAuthSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.CANCEL_AUTH,
          this.msg.getString(MessageFields.CancelMessage.AUTH_ID)));

      String serialNo = this.msg.getString(MessageFields.CancelMessage.ID);
      tvsSig.update(serialNo);
      tvsSig.update(this.getTypeString());

      return tvsSig.verify(this.msg.getString(MessageFields.CancelMessage.AUTH_SIG), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg, e);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }

    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }

    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }

  /**
   * Validates the signature in the message is from a valid audit machine
   * 
   * @param peer
   *          the WBBPeer this running on - needed for access to keys
   * @return boolean result of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validateCancelMachineSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.CANCEL,
          this.msg.getString(MessageFields.CancelMessage.SENDER_ID)));

      String serialNo = this.msg.getString(MessageFields.CancelMessage.ID);
      tvsSig.update(serialNo);
      tvsSig.update(this.getTypeString());

      return tvsSig.verify(this.msg.getString(MessageFields.CancelMessage.SENDER_SIG), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }

    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }

    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }
}
