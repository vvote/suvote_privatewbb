/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyRespondedException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.R2AlreadyRunningException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.messages.CancelMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR2Message;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.StartEVMMessage;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;
import com.mongodb.util.JSON;

/**
 * <p>
 * Core Wrapper for the ConcurrentDB, in this case MongoDB.
 * <p>
 * 
 * <p>
 * Provides an abstraction to the underlying database. If a different datasource were to be used this class would be the only part
 * that should need changing. This should be the only class where MongoDB database objects are used. No MongoDB database objects
 * should ever be directly returned to the code; the messages should be converted into ArrayLists, HashMaps or JSON, before
 * returning to the caller to ensure this stays a true abstraction.
 * </p>
 * 
 * <p>
 * When looking through the rest of the code it has probably become apparent that there is almost no use of "synchronized" methods
 * or blocks. We have made extensive use of Concurrent Collections provided by Java, but outside of that there is only one location
 * where we have synchronized access to an object. In keeping with that, there are no synchronized methods within this class either.
 * Fundamentally we are synchronizing on the data, not the methods. This is important for our setting, because two different runs of
 * the protocol should be able to run entirely concurrently, as such, we only want to synchronize if we are performing actions on
 * the same data at the same time. We therefore defer the locking to the underlying database, in this case, MongoDB. MongoDB
 * provides a locking mechanism as detailed in: <a href="http://docs.mongodb.org/manual/faq/concurrency/">MongoDB Manual</a>
 * </p>
 * 
 * <p>
 * MongoDB does not offer transactions, as such, the queries we have written have been done so in a way to mimic transactions,
 * without the usual overhead of transactions. We have achieved this by using a custom _id value - the serialNo. When we want to
 * simulate a transaction, for example, checking whether we have a threshold of signatures and if we have, check if we have already
 * sent a response and it not mark it that we will send a response (we don't want multiple threads trying to send the response
 * multiple times). Normally this would involve the following transaction: check the value of the signature counter (and that we
 * haven't already sent a response) and if it is above a certain value and we haven't sent a response, run a second update query to
 * mark that we will send the signature.
 * </p>
 * 
 * <p>
 * For example, imagine we are processing NTH:1, we are checking if sigCount>3 and if it is and sentSig!=1, setting sentSig to 1.
 * Otherwise we are do nothing. To achieve this with MongoDB we write a single update query to update the record with _id:"NTH:1"
 * AND sigCount>3 with the new value sigCount:1. We set the Upsert option to True, which states that if the record does not exist
 * create a new record based on that query. As a result, if sigCount is greater than 3 and sigCount=0, we will set sigCount=1 and
 * MongoDB will return stating it has modified 1 document. In any other situation MongoDB will attempt to insert a new record,
 * because it found no records that matched the query. When it attempts to insert a new record it will throw a
 * DuplicateKeyException, because the serialNo is the same. We catch this exception to determine the outcome of the
 * pseudo-transaction.
 * </p>
 * 
 * <p>
 * As such, the queries have been carefully written with the above in mind. Points where a value is queried, returned to the thread
 * processing it and then an action taken based on that, are written to handle changes in the underlying state. In cases where that
 * does occur, for example, when checking for any values that need processing. The action that is taken based on the original query
 * must check the conditions remain the same and handle it if not. Examples of this are shown in External and Internal message
 * processors. A prime example is when the internal message processor asks for the client message so that it can verify an incoming
 * peer message. If that client message does not exist the MessageProcessor should then store the internal message in a queue for it
 * to be processed when we receive the client message. This is a two part query. The first query looks for the original message. If
 * the original message does not exist the second update query, which attempts to write to the queue, checks the client message
 * still doesn't exist, if it does it throws an exception. The message processor handles this exception by getting the client
 * message and performing as it would have done. As such, even if the client message is written to the database between a thread
 * looking for it, not finding it, then trying to write to the waiting queue, it is gracefully handled. Likewise, once a client
 * message is written it should always check if there are any awaiting messages to checked. Whilst this might appear to tie us to
 * MongoDB it is more of a tie to NoSQL, which generally does not offer transaction support. A natural question would be why not use
 * SQL and transaction? The primary reason is speed and concurrency. If you start a transaction it locks the record, or possibly
 * table, until the transaction finishes. This has the potential to be extremely expensive. The other advantage of MongoDB and NOSQL
 * is that is unstructured. We can modify the structure without impacting on existing or future data.
 * </p>
 * 
 * <p>
 * One point to note is that we trust our own database, in that we do not anticipate that it is being modified outside of the
 * modifications we are making. As such, if we check a value that is final, for example, if we check a signature has been sent we
 * don't consider that the value will later change.
 * </p>
 * 
 * @author Chris Culnane
 * 
 */
public class ConcurrentDB {

  /**
   * The actual reference to the MongoClient
   */
  private MongoClient         mongoClient;

  /**
   * Reference to the database object (in our case the database name is the peer address)
   */
  private DB                  db;

  /**
   * Reference to the submission collection
   */
  private DBCollection        coll;

  /**
   * Reference to the CommitCollection - we store commits outside the other collections
   */
  private DBCollection        commitColl;

  /**
   * Reference to the conflictCollection, this where conflicting data from other peers, during a R2 commit, is received.
   */
  private DBCollection        conflictColl;

  /**
   * Reference to the cipherCollection, this where the unzipped ciphers are stored. It does not need commiting, since it is derived
   * from committed data
   */
  private DBCollection        cipherColl;

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ConcurrentDB.class);

  /**
   * Construct a ConcurrentDB instance with this PeerID
   * 
   * @param peerID
   *          The owning peer.
   */
  public ConcurrentDB(String peerID) {
    super();

    try {
      this.mongoClient = new MongoClient();
    }
    catch (UnknownHostException e) {
      logger.error("Could not connect to MongoDB", e);
    }

    this.db = this.mongoClient.getDB(peerID);

    // Get the various collections
    this.coll = this.db.getCollection(DBFields.Collections.SUBMISSIONS);
    this.commitColl = this.db.getCollection(DBFields.Collections.COMMITS);
    this.conflictColl = this.db.getCollection(DBFields.Collections.COMMIT_CONFLICTS);
    this.cipherColl = this.db.getCollection(DBFields.Collections.CIPHERS);
    logger.info("Database initialised");
  }

  /**
   * Adds all of the commit data for a commit record to the message and file.
   * 
   * @param recordType
   *          The commit message.
   * @param threshold
   *          The peer threshold.
   * @param bw
   *          Output commit writer.
   * @param commitTime
   *          The commit session.
   * @param commit
   *          The commit record.
   * @throws IOException
   * @throws MessageJSONException
   */
  private void addCommitData(DBRecordType recordType, int threshold, BufferedWriter bw, String commitTime,
      ArrayList<JSONWBBMessage> commit) throws IOException, MessageJSONException {
    DBFields fields = DBFields.getInstance(recordType);
    BasicDBObject query = new BasicDBObject(fields.getField(DBFieldName.SIGNATURE_COUNT), new BasicDBObject(
        DBFields.GREATER_THAN_EQUAL_TO, threshold));

    if (!recordType.equals(DBRecordType.CANCEL)) {
      query.append(fields.getField(DBFieldName.COMMIT_TIME), commitTime);
    }

    // HashMap to check uniqueness of messages between this collection and the conflict collection
    HashMap<String, String> uniqueCheck = new HashMap<String, String>();

    // Find records
    DBCursor cursor = this.coll.find(query);

    while (cursor.hasNext()) {
      DBObject next = cursor.next();

      if (recordType.equals(DBRecordType.CANCEL)) {
        BasicDBList list = (BasicDBList) next.get(fields.getField(DBFieldName.CLIENT_MESSAGE));
        Iterator<Object> itr = list.iterator();
        while (itr.hasNext()) {
          this.removeInternalFieldsAndAddToCommit((String) itr.next(), commit, uniqueCheck);
        }
      }
      else {
        this.removeInternalFieldsAndAddToCommit((String) next.get(fields.getField(DBFieldName.CLIENT_MESSAGE)), commit, uniqueCheck);
      }
      // We know these values are unique because the ID is the document index

      bw.write(((BasicDBObject) next).toString());
      bw.newLine();
    }

    if (!recordType.equals(DBRecordType.CANCEL)) {
      this.getCommitDataFromConflicts(fields, threshold, bw, commitTime, commit, uniqueCheck);
    }

    // Clear uniqueCheck so we can reuse it below
    uniqueCheck.clear();
  }

  /**
   * Checks if we have reached consensus on a cancellation message and if not, whether it is possible to do so
   * 
   * Currently this assumes the cancellation message is very simple it is Cancel+SerialNo. As such, we expect to only ever receive
   * one message from each peer authorising a cancellation on a particular serial number. As such, it is not important which Client
   * Cancellation Message the peer authorisation relates to. As such, this is a very robust protocol that can be run multiple times
   * to eventually reach consensus. However, it means we cannot commit to any centrally generated authorisation signature. Since we
   * can only commit to data that has been signed via SK1. We can check that the a suitable central authorisation signature was
   * sent, and reject a client cancellation if not. However, this information cannot end up on the Public WBB, since we can't
   * jointly commit to it. If we wish to commit it to the Public WBB we will need to include it in the SK1 signature. As a result,
   * this method, and a number of others, will need modifying to handle matching peer messages to the client messages, since it
   * would be possible multiple central signatures would be generated and we need to ensure we reach consensus on both the
   * cancellation and the central message.
   * 
   * UPDATE: We will not commit the central signature. It is not necessary to commit it, since the threshold of peers will check
   * that validity of the central signature. Since we trust a threshold of peers to behave we can assume that if a cancellation
   * appears on the publicWBB a threshold set of peers received a valid central authorisation signature.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String of serial number to check
   * @param threshold
   *          integer of the threshold we need to reach
   * @param peerCount
   *          integer of total number of peers
   * @return boolean - true if have or can reach consensus, false otherwise
   */
  public boolean canOrHaveReachedConsensus(DBRecordType recordType, String serialNo, int threshold, int peerCount) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.SIGNATURE_COUNT), new BasicDBObject(DBFields.LESS_THAN, threshold));
    query.append(fields.getField(DBFieldName.SENT_SIGNATURE), new BasicDBObject(DBFields.NOT_EQUAL, 1));

    int sigCount = 0;
    int checkedSigsSize = 0;
    DBObject obj = this.coll.findOne(query);

    if (obj != null) {
      if (obj.containsField(fields.getField(DBFieldName.SIGNATURE_COUNT))) {
        sigCount = (Integer) obj.get(fields.getField(DBFieldName.SIGNATURE_COUNT));
      }
      BasicDBList list = null;
      if (obj.containsField(fields.getField(DBFieldName.CHECKED_SIGS))) {
        list = (BasicDBList) obj.get(fields.getField(DBFieldName.CHECKED_SIGS));
      }
      if (list != null) {
        checkedSigsSize = list.size();
      }
      if (obj.get(fields.getField(DBFieldName.MY_SIGNATURE)) != null) {
        checkedSigsSize++;
      }
      return ((threshold - sigCount) <= (peerCount - checkedSigsSize));
    }
    else {
      return true;
    }
  }

  /**
   * Checks whether it is still possible to reach consensus on R1 of the commitment procedure
   * 
   * Counts the number of valid commit signatures received, how many signatures have been checked and calculates if it is still
   * possible, assuming other peers send data or we create our signature, to reach a consensus.
   * 
   * @param threshold
   *          the threshold of peers needed to reach consensus
   * @param peerCount
   *          the number of peers in total
   * @return boolean - true if we can still reach commitment, false if not
   */
  public boolean canReachCommitConsensus(int threshold, int peerCount) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    DBObject obj = this.commitColl.findOne(query);

    int sigCount = 0;
    int checkedSigsSize = 0;

    if (obj != null) {
      obj = (DBObject) obj.get(DBFields.COMMITMENT);
    }

    if (obj != null) {
      // How many valid signatures have we received
      if (obj.containsField(DBFields.COMMITMENT_SIG_COUNT)) {
        sigCount = (Integer) obj.get(DBFields.COMMITMENT_SIG_COUNT);
      }

      // How many signatures have we checked.
      BasicDBList list = null;
      if (obj.containsField(DBFields.COMMITMENT_CHECKED_SIGS)) {
        list = (BasicDBList) obj.get(DBFields.COMMITMENT_CHECKED_SIGS);
      }
      if (list != null) {
        checkedSigsSize = list.size();
      }

      // Include our own signature (already included in commitSigCount
      if (obj.get(DBFields.COMMITMENT_ROUND1_SIG) != null) {
        checkedSigsSize++;
      }
    }

    return ((threshold - sigCount) <= (peerCount - checkedSigsSize));
  }

  /**
   * Checks whether we have already responded to a message and if not marks it as timed out
   * 
   * If we have already responded a DuplicateKey exception is thrown, which is normal
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param msg
   *          JSONWBBMessage from client that the timeout has occurred on
   * @return boolean - true if we have set timeout, false if we already set timeout.
   * @throws AlreadyRespondedException
   */
  public boolean checkAndSetTimeout(DBRecordType recordType, JSONWBBMessage msg) throws AlreadyRespondedException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, msg.getID());
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.SENT_SIGNATURE), new BasicDBObject(DBFields.NOT_EQUAL, 1));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.SENT_TIMEOUT), 1);
    update.append(DBFields.SET, data);

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 1) {
        return true;
      }
      else {
        logger.debug("Document NOT Updated:{}", msg);
        return false;
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.info("Already responded to client, ignore timeout");
      throw new AlreadyRespondedException("Already responded to client, ignore timeout", e);
    }
  }

  /**
   * Checks whether we have received a threshold of R2 messages and have not already sent a signature
   * 
   * @param threshold
   *          integer of the threshold to check
   * @return boolean - true if we have reached consensus, false if not
   */
  public boolean checkCommitR2Threshold(int threshold) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_ROUND2_PROCESSED_COUNT, new BasicDBObject(DBFields.GREATER_THAN_EQUAL_TO, threshold));
    query.append(DBFields.COMMITMENT_COMMIT_SENT_SIG, new BasicDBObject(DBFields.NOT_EQUAL, 1));

    DBObject cur = this.commitColl.findOne(query);

    return (cur != null);
  }

  /**
   * Checks if we have reached the threshold in R1 of the commit signature and have not sent an SK2 signature
   * 
   * @param threshold
   *          integer threshold to check
   * @return boolean if we have reached the threshold, but not sent an Sk2 message, otherwise false.
   */
  public boolean checkCommitThreshold(int threshold) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_SIG_COUNT, new BasicDBObject(DBFields.GREATER_THAN_EQUAL_TO, threshold));
    query.append(DBFields.COMMITMENT_COMMIT_SENT_SIG, new BasicDBObject(DBFields.NOT_EQUAL, 1));

    DBObject cur = this.commitColl.findOne(query);

    return (cur != null);
  }

  /**
   * Checks whether we have reached a consensus on a message and if we have sent a response.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to check
   * @param threshold
   *          integer threshold to check
   * @return boolean - true if we have reached consensus and should send response, otherwise false
   */
  public boolean checkThresholdAndResponse(DBRecordType recordType, String serialNo, int threshold) {
    return this.checkThresholdAndResponse(recordType, serialNo, threshold, 0);
  }

  /**
   * Checks whether we have reached a consensus on a message and if we have sent a response.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to check
   * @param threshold
   *          integer threshold to check
   * @param timeout
   *          integer timeout for Print on Demand - zero otherwise
   * @return boolean - true if we have reached consensus and should send response, otherwise false
   */
  public boolean checkThresholdAndResponse(DBRecordType recordType, String serialNo, int threshold, int timeout) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.SIGNATURE_COUNT), new BasicDBObject(DBFields.GREATER_THAN_EQUAL_TO, threshold));
    query.append(fields.getField(DBFieldName.SENT_SIGNATURE), new BasicDBObject(DBFields.NOT_EQUAL, 1));

    BasicDBObject update = new BasicDBObject();

    update.append(DBFields.SET, new BasicDBObject(fields.getField(DBFieldName.SENT_SIGNATURE), 1));
    if (recordType.equals(DBRecordType.POD) && timeout != 0) {
      update.append(DBFields.SET, new BasicDBObject(DBFieldsPOD.BALLOT_TIMEOUT, System.currentTimeMillis() + timeout));
    }
    try {
      WriteResult wr = this.coll.update(query, update, true, false);
      logger.info("Successfully updated sent signature: {}", serialNo);
      return (wr.getN() == 1);
    }
    catch (MongoException.DuplicateKey e) {
      logger.info("Either threshold not met or response already sent: {}", serialNo);
      return false;
    }
  }

  /**
   * Creates a new currentCommit record.
   * 
   * The currentCommit stores the commit we are currently processing. We need to use this to handle the gap between R1 and R2.
   * 
   * Having constructed our commitment values we then construct the actual CommitRecord. The commitProcessor is run in a
   * singleThreaded executor, so there is no chance of concurrency issues with this.
   * 
   * @param messageToSend
   *          the JSONWBBMessage that we will send to other peers (R1 commit)
   * @param commitFile
   *          the JSON file with the commit data in it
   * @param commitFileFull
   *          the Full database file with everything in it
   * @param commitAttachments
   *          -the location of any attachments - files we will be send with the commitment
   * @param commitTime
   *          - the CommitTime this relates to
   * @param commitDesc
   *          - String description of the commit or null if end of day commit
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   */
  public void createNewCommitRecord(JSONWBBMessage messageToSend, String commitFile, String commitFileFull,
      String commitAttachments, String commitTime, String commitDesc) throws JSONException, AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    // Check we have closed the previous commitment - i.e. there is no currentCommit in operation
    query.append(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG, new BasicDBObject(DBFields.EXISTS, false));
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    // Set the data related to this commitment
    data.append(DBFields.COMMITMENT_COMMIT_COMMIT_ID, messageToSend.getMsg().getString(MessageFields.Commit.COMMIT_ID));
    data.append(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG, messageToSend.getMsg().getString(MessageFields.Commit.SIGNATURE));
    data.append(DBFields.COMMITMENT_COMMIT_HASH, messageToSend.getMsg().getString(MessageFields.Commit.HASH));
    data.append(DBFields.COMMITMENT_COMMIT_FILE, commitFile);
    data.append(DBFields.COMMITMENT_COMMIT_FILE_FULL, commitFileFull);
    data.append(DBFields.COMMITMENT_COMMIT_TIME, commitTime);
    data.append(DBFields.COMMITMENT_COMMIT_ATTACHMENTS, commitAttachments);
    data.append(DBFields.COMMITMENT_COMMIT_CURRENT_COMMIT, true);
    data.append(DBFields.COMMITMENT_COMMIT_DATETIME, messageToSend.getMsg().getLong(MessageFields.Commit.DATETIME));
    if (commitDesc != null) {
      data.append(DBFields.COMMITMENT_COMMIT_DESC, commitDesc);
    }
    update.append(DBFields.INCREMENT, new BasicDBObject(DBFields.COMMITMENT_COMMIT_SIG_COUNT, 1));
    update.append(DBFields.SET, data);

    try {
      this.commitColl.update(query, update, true, false);
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Commit has already been recorded - will reject");
      throw new AlreadyReceivedMessageException("Commit has already been recorded", e);
    }
  }

  /**
   * Gets a Ballot Cipher record as a JSONObject. This returns all data in the document for that serial no
   * 
   * @param serial
   *          String of serial number of look for
   * @return JSONObject containing the document or null if it doesn't exist
   * @throws JSONException
   */
  public JSONObject getBallotRecord(String serial) throws JSONException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serial);
    DBCursor cursor = this.cipherColl.find(query);

    if (cursor.hasNext()) {
      return new JSONObject(cursor.next().toString());
    }
    else {
      return null;
    }
  }

  /**
   * Gets a String array of client cancellation messages
   * 
   * Due to the nature of multiple runs of the cancellation protocol it is possible multiple client cancellation requests are
   * received. All valid messages are recorded. When checking the validity of incoming peer messages this method is called to get
   * the list of client messages to check against.
   * 
   * @param serialNo
   *          String serial number to lookup
   * @return String[] of client messages in JSON String format or array of length 0 if none exist
   */
  public String[] getClientCancelMessages(String serialNo) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);
    DBFieldsCancel cancelFields = DBFieldsCancel.getInstance();
    query.append(cancelFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, true));

    DBObject doc = this.coll.findOne(query);

    if (doc == null) {
      return new String[0];
    }
    else {
      BasicDBList list = (BasicDBList) doc.get(cancelFields.getField(DBFieldName.CLIENT_MESSAGE));
      return list.toArray(new String[0]);
    }
  }

  /**
   * Gets the String JSON representation of the original client message
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to lookup
   * @return String of JSON of the original message or null if the message does not exist
   */
  public String getClientMessage(DBRecordType recordType, String serialNo) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, true));

    DBObject doc = this.coll.findOne(query);

    if (doc == null) {
      return null;
    }
    else {
      return (String) doc.get(fields.getField(DBFieldName.CLIENT_MESSAGE));
    }
  }

  /**
   * Finds all data related to a particular commitTime and prepares the data for commitment
   * 
   * Creates and returns an ArrayList of messages that should be included within the commit. It simultaneously prepares a
   * fullCommitFile containing all the data in our database. It will only include data in the commit which has an appropriate
   * threshold of signatures.
   * 
   * The returned list of messages will list Vote, Audit, POD, Cancel, File etc messages separately, even if the relate to the same
   * serial number.
   * 
   * If we run R1 of the commit and it fails, we run R2 to exchange databases. We then re-run this method to get an updated
   * commitment. As such, this looks for data both in our own database and the conflict collection.
   * 
   * @param threshold
   *          integer of the threshold
   * @param commitFull
   *          File to write full commit data to
   * @param commitTime
   *          String of CommitTime we are making the commitment on
   * @return ArrayList of JSONWBBMessages that should be committed
   * @throws MessageJSONException
   * @throws IOException
   */
  public ArrayList<JSONWBBMessage> getCommitData(int threshold, File commitFull, String commitTime) throws MessageJSONException,
      IOException {
    // Prepare array and output file for full commit
    ArrayList<JSONWBBMessage> commit = new ArrayList<JSONWBBMessage>();
    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(commitFull));
      this.addCommitData(DBRecordType.GENERAL, threshold, bw, commitTime, commit);
      this.addCommitData(DBRecordType.POD, threshold, bw, commitTime, commit);
      this.addCommitData(DBRecordType.CANCEL, threshold, bw, commitTime, commit);
    }
    finally {
      if (bw != null) {
        bw.close();
      }
    }

    return commit;
  }

  /**
   * Gets the conflict commit messages and adds them to the commit record (object and file).
   * 
   * @param fields
   *          Database fields
   * @param threshold
   *          Peer threshold.
   * @param bw
   *          Output commit writer.
   * @param commitTime
   *          The commit session.
   * @param commit
   *          The commit record.
   * @param uniqueCheck
   *          To check if the message id is unique.
   * @throws MessageJSONException
   * @throws IOException
   */
  private void getCommitDataFromConflicts(DBFields fields, int threshold, BufferedWriter bw, String commitTime,
      ArrayList<JSONWBBMessage> commit, HashMap<String, String> uniqueCheck) throws MessageJSONException, IOException {
    // Get data from conflict collection
    BasicDBObject query = new BasicDBObject(fields.getField(DBFieldName.SIGNATURE_COUNT), new BasicDBObject(
        DBFields.GREATER_THAN_EQUAL_TO, threshold));
    query.append(fields.getField(DBFieldName.COMMIT_TIME), commitTime);
    DBCursor cursor = this.conflictColl.find(query);

    while (cursor.hasNext()) {
      // It should be rare for us to have conflicts, it indicates a dishonest EBM, so the expense of checking the uniqueness should
      // be low
      DBObject next = cursor.next();
      JSONWBBMessage msg = JSONWBBMessage.parseMessage((String) next.get(DBFields.CONFLICT_MESSAGE));

      if (uniqueCheck.containsKey(msg.getID())) {
        logger.error("Conflict between conflictColl and standard Coll. This should not happen");
      }
      else {
        msg.getMsg().remove(MessageFields.UUID);
        msg.getMsg().remove(MessageFields.FROM_PEER);
        commit.add(msg);
        bw.write(((BasicDBObject) next).toString());
        bw.newLine();
      }
    }
  }

  /**
   * Returns a JSONObject with the contents of the commit collection
   * 
   * This reads all records in the commitCollection and stores them in a JSONArray. It also sets any currently active commit in the
   * JSONObject itself
   * 
   * @return JSONObject with contents of commit collection
   * @throws JSONException
   */
  public JSONObject getCommitTable() throws JSONException {
    JSONObject result = new JSONObject();
    JSONArray commits = new JSONArray();
    DBCursor cursor = this.commitColl.find();

    while (cursor.hasNext()) {
      DBObject obj = cursor.next();
      if (obj.get(DBFields.ID).equals(DBFields.CURRENT_COMMIT)) {
        result.put(MessageFields.Commit.CURRENT_COMMIT, new JSONObject(obj.toString()));
      }
      else {
        commits.put(new JSONObject(obj.toString()));
      }
    }
    result.put(MessageFields.Commit.COMMITS, commits);

    return result;
  }

  /**
   * Utility method for getting a field from the CurrentCommit document
   * 
   * @param field
   *          String of field name to get
   * @return String of field value of null
   * @throws NoCommitExistsException
   */
  public String getCurrentCommitField(String field) throws NoCommitExistsException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    query.append(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG, new BasicDBObject(DBFields.EXISTS, true));
    DBObject cur = this.commitColl.findOne(query);

    if (cur == null) {
      throw new NoCommitExistsException("No current commit found");
    }
    BasicDBObject obj = (BasicDBObject) cur.get(DBFields.COMMITMENT);
    return (String) obj.get(field);
  }

  /**
   * Gets a String of the Base64 encoding of the hash of our own data for the current commitment
   * 
   * @return Base64 String of the hash of our own data
   * @throws NoCommitExistsException
   */
  public String getCurrentCommitHash() throws NoCommitExistsException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_HASH, new BasicDBObject(DBFields.EXISTS, true));
    query.append(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG, new BasicDBObject(DBFields.EXISTS, true));

    DBObject cur = this.commitColl.findOne(query);

    if (cur != null) {
      return (String) ((DBObject) cur.get(DBFields.COMMITMENT)).get(DBFields.COMMIT_HASH);
    }
    else {
      throw new NoCommitExistsException("No current commit found");
    }
  }

  /**
   * Gets an ArrayList of CommitR1Messages that need checking
   * 
   * This would be called after submitting our own commit data. These messages would have been received prior to us finishing our
   * own commit data
   * 
   * @return ArrayList of CommitR1Messages that need checking
   * @throws MessageJSONException
   */
  public ArrayList<CommitR1Message> getCurrentCommitSigsToCheck() throws MessageJSONException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_SIGS_TO_CHECK, new BasicDBObject(DBFields.EXISTS, true));

    ArrayList<CommitR1Message> sigsToSign = new ArrayList<CommitR1Message>();

    DBObject doc = this.commitColl.findOne(query);

    if (doc == null) {
      // Either not currentCommit exists or there are not sigsToCheck
      return sigsToSign;
    }
    else {
      // The currentCommit exists with a sigsToCheck field
      BasicDBObject comm = (BasicDBObject) doc.get(DBFields.COMMITMENT);
      BasicDBList dblist = (BasicDBList) comm.get(DBFields.COMMITMENT_SIGS_TO_CHECK);

      // Gets the list of sigs
      if (dblist == null) {
        // If the list is null indicates they have all been read and removed
        return sigsToSign;
      }
      else {
        // return list
        Iterator<Object> itr = dblist.iterator();

        while (itr.hasNext()) {
          BasicDBObject obj = (BasicDBObject) itr.next();
          sigsToSign.add(new CommitR1Message(obj.getString(DBFields.COMMITMENT_MESSAGE)));
        }

        return sigsToSign;
      }
    }
  }

  /**
   * Gets a field from the submissions for the specified serial number.
   * 
   * @param serialNo
   *          The target serial number.
   * @param field
   *          The field to retrieve.
   * @return The field or null if it or the record do not exist.
   */
  public String getField(String serialNo, String field) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);

    query.append(field, new BasicDBObject(DBFields.EXISTS, true));

    DBObject doc = this.coll.findOne(query);

    if (doc == null) {
      return null;
    }
    else {
      return (String) doc.get(field);
    }
  }

  /**
   * Gets the R2Queue in the form of an ArrayList of JSONWBBMessages
   * 
   * @return ArrayList of JSONWBBMessages of the R2Queue or an empty ArrayList if no messages are queued
   * @throws MessageJSONException
   */
  public ArrayList<JSONWBBMessage> getR2Queue() throws MessageJSONException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    ArrayList<JSONWBBMessage> queue = new ArrayList<JSONWBBMessage>();
    DBObject doc = this.commitColl.findOne(query);

    if (doc == null) {
      return queue;
    }
    else {
      BasicDBList dblist = (BasicDBList) doc.get(DBFields.COMMITMENT_R2_QUEUE);
      if (dblist == null) {
        return queue;
      }
      else {
        Iterator<Object> itr = dblist.iterator();

        while (itr.hasNext()) {
          BasicDBObject obj = (BasicDBObject) itr.next();
          queue.add(JSONWBBMessage.parseMessage(obj.getString(DBFields.COMMITMENT_MESSAGE)));
        }

        return queue;
      }
    }
  }

  /**
   * Gets a record as a JSONObject. This returns all data in the document for that serial no
   * 
   * @param serial
   *          String of serial number of look for
   * @return JSONObject containing the document or null if it doesn't exist
   * @throws JSONException
   */
  public JSONObject getRecord(String serial) throws JSONException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serial);
    DBCursor cursor = this.coll.find(query);

    if (cursor.hasNext()) {
      return new JSONObject(cursor.next().toString());
    }
    else {
      return null;
    }
  }

  /**
   * Gets an ArrayList of PeerMessages that need to be checked
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to lookup
   * @return ArrayList of PeerMessages to check, or an empty list
   * @throws MessageJSONException
   */
  public ArrayList<PeerMessage> getSigsToCheck(DBRecordType recordType, String serialNo) throws MessageJSONException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);

    DBFields fields = DBFields.getInstance(recordType);

    ArrayList<PeerMessage> sigsToSign = new ArrayList<PeerMessage>();
    DBObject doc = this.coll.findOne(query);

    if (doc == null) {
      return sigsToSign;
    }
    else {
      BasicDBList dblist = (BasicDBList) doc.get(fields.getField(DBFieldName.SIGNATURES_TO_CHECK));

      if (dblist == null) {
        return sigsToSign;
      }
      else {
        Iterator<Object> itr = dblist.iterator();

        while (itr.hasNext()) {
          BasicDBObject obj = (BasicDBObject) itr.next();
          sigsToSign.add(JSONWBBMessage.parsePeerMessage(obj.getString(fields.getField(DBFieldName.QUEUED_MESSAGE))));
        }

        return sigsToSign;
      }
    }
  }

  /**
   * Called during commit to check if the database already contains a record for that serial number, for the appropriate
   * DBRecordType with a threshold of signatures
   * 
   * Cancel, POD and Standard messages related to the same serial and stored in the same document (record), as such the type that is
   * being checked is specified
   * 
   * @param serial
   *          String of serial number to look up
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param threshold
   *          integer threshold it must be >= to be valid
   * @return boolean - true if threshold met, false if not
   */
  public boolean isAlreadyStored(String serial, DBRecordType type, int threshold) {
    // Find serial
    BasicDBObject query = new BasicDBObject(DBFields.ID, serial);
    DBFields fields = DBFields.getInstance(type);

    // Append threshold condition
    query.append(fields.getField(DBFieldName.SIGNATURE_COUNT), new BasicDBObject(DBFields.GREATER_THAN_EQUAL_TO, threshold));

    // If we find a record it means we have a record with that threshold
    DBCursor cursor = this.coll.find(query);

    return cursor.hasNext();
  }

  /**
   * Check if we are currently running R2 of the commit protocol
   * 
   * @return boolean - true if R2 is running, false if not
   */
  public boolean isCommitInR2() {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    query.append(DBFields.COMMITMENT_COMMIT_RUNNING_R2, true);
    DBObject cur = this.commitColl.findOne(query);

    return (cur != null);
  }

  /**
   * Called when a Peer sends an R2 commitment before we have finished creating our own commitment. Stores that message in the queue
   * 
   * @param commitMessage
   *          JSONWBBMessage to store
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   * @throws R2AlreadyRunningException
   */
  public void queueR2Commit(JSONWBBMessage commitMessage) throws JSONException, AlreadyReceivedMessageException,
      R2AlreadyRunningException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    // Ensure we aren't already running R2. If we are then we should check against our commit data
    query.append(DBFields.COMMITMENT_COMMIT_RUNNING_R2, new BasicDBObject(DBFields.NOT_EQUAL, true));

    // Add data to the commitR2Queue
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    data.append(DBFields.FROM_PEER, commitMessage.getMsg().get(MessageFields.FROM_PEER));
    data.append(DBFields.COMMITMENT_MESSAGE, commitMessage.getMsg().toString());
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(DBFields.COMMITMENT_R2_QUEUE, data));

    try {
      WriteResult wr = this.commitColl.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", commitMessage);
      }
    }
    catch (MongoException.DuplicateKey e) {
      // We have received a duplicate key exception. Now check what caused it
      BasicDBObject exquery = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
      exquery.append(DBFields.COMMITMENT_COMMIT_RUNNING_R2, new BasicDBObject(DBFields.NOT_EQUAL, true));
      if (this.commitColl.findOne(exquery) == null) {
        logger.info("Tried adding queuing R2Message, but R2 has been set as running");
        throw new R2AlreadyRunningException("Tried adding queuing R2Message, but R2 has been set as running", e);
      }
      else {
        logger.warn("Message from Peer has already been recorded - will ignore:{}", commitMessage);
        throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
      }
    }
  }

  /**
   * Removes internal fields from a message and commits it.
   * 
   * @param message
   *          The message.
   * @param commit
   *          The commit record.
   * @param uniqueCheck
   *          To check if the message id is unique.
   * @throws MessageJSONException
   */
  private void removeInternalFieldsAndAddToCommit(String message, ArrayList<JSONWBBMessage> commit,
      HashMap<String, String> uniqueCheck) throws MessageJSONException {
    JSONWBBMessage msg = JSONWBBMessage.parseMessage(message);
    msg.getMsg().remove("_uuid");
    msg.getMsg().remove("_fromPeer");
    commit.add(msg);
    uniqueCheck.put(msg.getID(), "");
  }

  /**
   * Marks that we have sent a signature under SK2 and close the currentCommitment
   * 
   * This would be called having reached a consensus in round 1 or after an R2 exchange
   * 
   * It sets the commitSentSig to 1 and closes the currentCommit. Closing the commit copies the commit data to a new record and
   * removes clears the currentCommit record. currentCommit should only store ongoing or stalled commit attempts. All other commits
   * are archived.
   * 
   * 
   * @return boolean returns true if commitSentSig has been updated and the record archived, clearing currentCommit, otherwise
   *         false. - false should not happen, but if it does it indicates the closing of the commit has failed
   * @throws AlreadyRespondedException
   */
  public boolean setCommitSentSig(String mySK2Signature) throws AlreadyRespondedException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_SENT_SIG, new BasicDBObject(DBFields.NOT_EQUAL, 1));
    query.append(DBFields.COMMITMENT_COMMIT_SK2_SIG, new BasicDBObject(DBFields.EXISTS, false));
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(DBFields.COMMITMENT_COMMIT_SENT_SIG, 1);
    data.append(DBFields.COMMITMENT_COMMIT_SK2_SIG, mySK2Signature);
    data.append(DBFields.COMMITMENT_COMMIT_CURRENT_COMMIT, false);
    update.append(DBFields.SET, data);

    WriteResult wr = this.commitColl.update(query, update, false, false);
    if(wr.getN()!=1){
      throw new AlreadyRespondedException("Have already recorded SK2 - which will have initiated response.");
    }
    return (wr.getError() == null);
  }

  /**
   * Gets a Ballot Cipher record as a JSONObject. This returns all data in the document for that serial no
   * 
   * @param serial
   *          String of serial number of look for
   * @throws JSONException
   * @throws UnknownDBException
   */
  public void setFieldInRecord(String serial, String field, String value) throws UnknownDBException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serial);
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    data.append(field, value);
    update.append(DBFields.SET, data);

    try {
      this.coll.update(query, update, false, false);
    }
    catch (MongoException.DuplicateKey e) {
      logger.error("Exception whilst setting {} to {}, the record should exist", e);
      throw new UnknownDBException("Exception whilst setting {} to {}, the record should exist", e);
    }
  }

  /**
   * Force closes a commit. This can be called if a commit has got stuck midway, for example, if some peers go offline during the
   * commit or no consensus can be reached. It shouldn't be required during normal operation, but could be required in certain
   * extreme situations.
   * 
   * @return boolean true if successful
   * @throws NoCommitExistsException
   */
  public boolean forceCloseCurrentCommit() throws NoCommitExistsException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    // The minimum we can have is a commitment, otherwise the record is empty
    query.append(DBFields.COMMITMENT, new BasicDBObject(DBFields.EXISTS, true));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(DBFields.COMMITMENT_COMMIT_RESPONSE, "Force Closed");
    update.append(DBFields.SET, data);

    // Update the record and return the new version.
    DBObject result = this.commitColl.findAndModify(query, null, null, false, update, true, false);
    if (result == null) {
      throw new NoCommitExistsException(
          "Exception occurred when force closing a commit, indicates no Commit to close or invalid commit to close");
    }

    // Save the commit as a new (closed) record for the archive.
    WriteResult wr = this.commitColl.save((DBObject) result.get(DBFields.COMMITMENT));

    // wr.getN() will always return 0 on insert, therefore check for error.
    int counter = 0;

    if (wr.getError() == null) {
      counter++;
    }

    query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    update = new BasicDBObject();
    data = new BasicDBObject();
    data.append(DBFields.COMMITMENT, "");
    update.append(DBFields.UNSET, data);
    wr = this.commitColl.update(query, update, false, false);

    if (wr.getN() == 1) {
      counter++;
    }

    return (counter == 2);
  }

  /**
   * Stores the response to a commit and closes the commit.
   * 
   * @param response
   *          The commit response.
   * @return True if stored and closed successfully.
   * @throws NoCommitExistsException
   */
  public boolean setResponseAndCloseCommit(String response) throws NoCommitExistsException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_SENT_SIG, 1);
    query.append(DBFields.COMMITMENT_COMMIT_SK2_SIG, new BasicDBObject(DBFields.EXISTS, true));
    query.append(DBFields.COMMITMENT, new BasicDBObject(DBFields.EXISTS, true));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(DBFields.COMMITMENT_COMMIT_RESPONSE, response);
    update.append(DBFields.SET, data);

    // Update the record and return the new version.
    DBObject result = this.commitColl.findAndModify(query, null, null, false, update, true, false);
    if (result == null) {
      throw new NoCommitExistsException(
          "Exception occurred when setting Commit response, indicates no Commit to close or invalid commit to close");
    }

    // Save the commit as a new (closed) record for the archive.
    WriteResult wr = this.commitColl.save((DBObject) result.get(DBFields.COMMITMENT));

    // wr.getN() will always return 0 on insert, therefore check for error.
    int counter = 0;

    if (wr.getError() == null) {
      counter++;
    }

    query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    update = new BasicDBObject();
    data = new BasicDBObject();
    data.append(DBFields.COMMITMENT, "");
    update.append(DBFields.UNSET, data);
    wr = this.commitColl.update(query, update, false, false);

    if (wr.getN() == 1) {
      counter++;
    }

    return (counter == 2);
  }

  /**
   * Checks whether R2 of the commit procedure should be run
   * 
   * This does not check whether R1 has succeeded or not, just whether R2 has been started and if not, return true and marks that
   * the caller will start R2.
   * 
   * @return boolean true if R2 should be started, false if R2 has already been started
   */
  public boolean shouldRunR2Commit() {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_RUNNING_R2, new BasicDBObject(DBFields.NOT_EQUAL, true));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(DBFields.COMMITMENT_COMMIT_RUNNING_R2, true);
    update.append(DBFields.SET, data);

    WriteResult wr = this.commitColl.update(query, update, false, false);

    return (wr.getN() == 1);
  }

  /**
   * Submits a cipher to the database.
   * 
   * @param cipher
   *          The cipher to submit.
   * @throws AlreadyReceivedMessageException
   * @throws JSONException
   */
  public void submitCipher(JSONObject cipher) throws AlreadyReceivedMessageException, JSONException {
    cipher.put(DBFields.ID, cipher.getString(MessageFields.Cipher.ID));
    cipher.remove(MessageFields.Cipher.ID);
    DBObject obj = (DBObject) JSON.parse(cipher.toString());

    try {
      this.cipherColl.insert(obj);
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Cipher has already been recorded - will reject");
      throw new AlreadyReceivedMessageException("Cipher has already been recorded", e);
    }
  }

  /**
   * Records an incoming Client Cancellation message
   * 
   * This will add this message to the set of received cancellation messages. This is unlike other message types which can only be
   * received once.
   * 
   * If we have already generated a cancellation SK2 message an exception is thrown. The SK2 message should then be returned. No
   * additional Client Cancellations are accepted once SK2 has been sent
   * 
   * @param msg
   *          CancelMessage to store
   * @throws AlreadyReceivedMessageException
   */
  public void submitIncomingClientCancellationMessage(CancelMessage msg) throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, msg.getID());
    DBFieldsCancel fields = DBFieldsCancel.getInstance();
    query.append(fields.getField(DBFieldName.SK2_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    BasicDBObject update = new BasicDBObject();
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(fields.getField(DBFieldName.CLIENT_MESSAGE), msg.getMsg().toString()));

    try {
      WriteResult wr = this.coll.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", msg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Cancellation has already been generated - will return previous signature");
      throw new AlreadyReceivedMessageException("Cancellation already generated", e);
    }
  }

  /**
   * Stores an incoming client message (generally from an EBM)
   * 
   * Checks that the message has not already been used or cancelled
   * 
   * @param recordType
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param msg
   *          JSONWBBMessage to store
   * @param mySig
   *          signature from this peer of the message
   * @throws AlreadyReceivedMessageException
   */
  public void submitIncomingClientMessage(DBRecordType recordType, JSONWBBMessage msg, String mySig)
      throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, msg.getID());
    DBFields fields = DBFields.getInstance(recordType);

    // Check I haven't already received a vote for this serial no
    query.append(fields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(fields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    // Check no cancellations exist for it
    DBFields cancelFields = DBFieldsCancel.getInstance();
    query.append(cancelFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(cancelFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    if (recordType.equals(DBRecordType.POD)) {
      // Check no audit/vote/general message have already been received for this serial number
      DBFields generalFields = DBFields.getInstance();
      query.append(generalFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
      query.append(generalFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));
    }
    if (recordType.equals(DBRecordType.AUDIT)) {
      query.append(DBFields.START_EVM_MSG, new BasicDBObject(DBFields.EXISTS, false));
    }
    // Update it with my data
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.CLIENT_MESSAGE), msg.getMsg().toString());
    data.append(fields.getField(DBFieldName.MY_SIGNATURE), mySig);
    data.append(fields.getField(DBFieldName.COMMIT_TIME), msg.getCommitTime());
    update.append(DBFields.INCREMENT, new BasicDBObject().append(fields.getField(DBFieldName.SIGNATURE_COUNT), 1));
    update.append(DBFields.SET, data);

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", msg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Message from Client has already been recorded - will reject");
      throw new AlreadyReceivedMessageException("Message from Client has already been recorded", e);
    }
  }

  /**
   * Submits a generic client message during R2 commit.
   * 
   * @param recordType
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param msg
   *          JSONWBBMessage we should submit
   * @throws AlreadyReceivedMessageException
   */
  public void submitIncomingCommitMessage(DBRecordType recordType, JSONWBBMessage msg, boolean isConflict)
      throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, msg.getID());
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(fields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.MY_SIGNATURE), DBFields.COMMITMENT_COMMIT_R2);

    if (!recordType.equals(DBRecordType.CANCEL)) {
      data.append(fields.getField(DBFieldName.CLIENT_MESSAGE), msg.getMsg().toString());
      data.append(fields.getField(DBFieldName.COMMIT_TIME), msg.getCommitTime());
    }
    update.append(DBFields.SET, data);

    if (recordType.equals(DBRecordType.CANCEL)) {
      update.append(DBFields.ADD_TO_SET, new BasicDBObject(fields.getField(DBFieldName.CLIENT_MESSAGE), msg.getMsg().toString()));
    }

    try {
      WriteResult wr;
      if (isConflict) {
        wr = this.conflictColl.update(query, update, true, false);
      }
      else {
        wr = this.coll.update(query, update, true, false);
      }
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", msg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Message from Commit R2 has already been recorded - will reject");
      throw new AlreadyReceivedMessageException("Message from Commit R2 has already been recorded", e);
    }
  }

  /**
   * Records an R1 Commit message received from another peer
   * 
   * If isValid is set to true the commitSigCount will be incremented
   * 
   * @param cMsg
   *          CommitR1Message to store
   * @param isValid
   *          boolean - true if valid, false if invalid
   * @throws AlreadyReceivedMessageException
   */
  public void submitIncomingPeerCommitR1Checked(CommitR1Message cMsg, boolean isValid) throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    // Check we haven't already stored a message from this peer
    query.append(DBFields.COMMITMENT_COMMIT_CHECKED_SIGS, new BasicDBObject(DBFields.NOT, new BasicDBObject(DBFields.ELEMENT_MATCH,
        new BasicDBObject(DBFields.FROM_PEER, cMsg.getFromPeer()))));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(DBFields.FROM_PEER, cMsg.getFromPeer());
    data.append(DBFields.COMMITMENT_MESSAGE, cMsg.getMsg().toString());

    // Add to the set of checkedSigs
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(DBFields.COMMITMENT_COMMIT_CHECKED_SIGS, data));

    // If it is valid increment commitSigCount
    if (isValid) {
      update.append(DBFields.INCREMENT, new BasicDBObject().append(DBFields.COMMITMENT_COMMIT_SIG_COUNT, 1));
    }
    update.append(DBFields.PULL, new BasicDBObject(DBFields.COMMITMENT_COMMIT_SIGS_TO_CHECK, new BasicDBObject(DBFields.FROM_PEER,
        cMsg.getFromPeer())));

    try {
      WriteResult wr = this.commitColl.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", cMsg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Message from Peer has already been recorded - will ignore:{}", cMsg);
      throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
    }
  }

  /**
   * Records an unchecked R1 Commit message received from another peer
   * 
   * This will occur if we receive an R1 commit message from another peer before we have finished constructing our own commitment.
   * In such cases the message is queued for checking later
   * 
   * @param cMsg
   *          CommitR1Message to store
   * @throws AlreadyReceivedMessageException
   * @throws CommitSignatureExistsException
   */
  public void submitIncomingPeerCommitR1UnChecked(CommitR1Message cMsg) throws AlreadyReceivedMessageException,
      CommitSignatureExistsException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    // Check we haven't already stored this message
    query.append(DBFields.COMMITMENT_COMMIT_SIGS_TO_CHECK, new BasicDBObject(DBFields.NOT, new BasicDBObject(
        DBFields.ELEMENT_MATCH, new BasicDBObject(DBFields.FROM_PEER, cMsg.getFromPeer()))));

    // Check we haven't finished constructing our own commitment
    query.append(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG, new BasicDBObject(DBFields.EXISTS, false));
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    data.append(DBFields.FROM_PEER, cMsg.getFromPeer());
    data.append(DBFields.COMMITMENT_MESSAGE, cMsg.getMsg().toString());

    // Add to set of signatures for checking
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(DBFields.COMMITMENT_COMMIT_SIGS_TO_CHECK, data));

    try {
      WriteResult wr = this.commitColl.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", cMsg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      // Duplicate key exception
      BasicDBObject exquery = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

      // Check if the message is duplicate
      exquery.append(DBFields.COMMITMENT_COMMIT_SIGS_TO_CHECK, new BasicDBObject(DBFields.NOT, new BasicDBObject(
          DBFields.ELEMENT_MATCH, new BasicDBObject(DBFields.FROM_PEER, cMsg.getFromPeer()))));
      if (this.commitColl.findOne(exquery) == null) {
        logger.warn("Message from Peer has already been recorded - will ignore:{}", cMsg);
        throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
      }
      else {
        // Or we have tried to add an unchecked message when our signature is now available for checking
        logger.info("Tried adding unchecked CommitR1Message, but My Signature for R1 has now been recorded - will perform check");
        throw new CommitSignatureExistsException("Check Signature using Current Commit", e);
      }
    }
  }

  /**
   * Submits a checked R2 message from another peer.
   * 
   * This not only records a received R2 message, but also removes it from the queue if it was in the queue.
   * 
   * @param cMsg
   *          PeerFileMessage (R2 message) to record
   * @throws AlreadyReceivedMessageException
   */
  public void submitIncomingPeerCommitR2Checked(CommitR2Message cMsg) throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    // Check it hasn't already been stored
    query.append(DBFields.COMMITMENT_COMMIT_R2_COMMITS, new BasicDBObject(DBFields.NOT, new BasicDBObject(DBFields.ELEMENT_MATCH,
        new BasicDBObject(DBFields.FROM_PEER, cMsg.getFromPeer()))));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(DBFields.FROM_PEER, cMsg.getFromPeer());
    data.append(DBFields.COMMITMENT_MESSAGE, cMsg.getMsg().toString());

    // Add it to the set of R2Commits
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(DBFields.COMMITMENT_COMMIT_R2_COMMITS, data));

    // Increment the appropriate counter
    update.append(DBFields.INCREMENT, new BasicDBObject().append(DBFields.COMMITMENT_COMMIT_R2_PROCESSED_COUNT, 1));

    // Remove it from the queue if it exists
    update.append(DBFields.PULL,
        new BasicDBObject(DBFields.COMMITMENT_R2_QUEUE, new BasicDBObject(DBFields.FROM_PEER, cMsg.getFromPeer())));

    try {
      WriteResult wr = this.commitColl.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", cMsg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Message from Peer has already been recorded - will ignore:{}", cMsg);
      throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
    }
  }

  /**
   * Stores an incoming Peer SK1 message
   * 
   * Also handles storing Peer messages during the commit process. If ignoreTimeout is set to true it will override the timeout
   * block, which would stop peer messages being recorded following a timeout. During the commit we need to override this to allow
   * us to jointly commit on the collective set of data. Likewise, isConflict is true if the underlying client message is different
   * to ours. This changes which collection the data is stored in. ignoreTimeout and isConflict should be false, except when running
   * commit R2.
   * 
   * @param recordType
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param pMsg
   *          PeerMessage to store
   * @param isValid
   *          boolean is pMsg valid
   * @param ignoreTimeout
   *          - if true the timeout block will be overridden
   * @param isConflict
   *          - if true the record will be stored in the conflict collection
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   */
  public void submitIncomingPeerMessageChecked(DBRecordType recordType, PeerMessage pMsg, boolean isValid, boolean ignoreTimeout,
      boolean isConflict) throws AlreadyReceivedMessageException, AlreadySentTimeoutException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, pMsg.getID());
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.CHECKED_SIGS), new BasicDBObject(DBFields.NOT, new BasicDBObject(
        DBFields.ELEMENT_MATCH, new BasicDBObject(fields.getField(DBFieldName.FROM_PEER), pMsg.getFromPeer()))));

    if (!ignoreTimeout && !recordType.equals(DBRecordType.CANCEL)) {
      // We don't have timeouts on Cancel messages because the protocol can be run multiple times
      query.append(fields.getField(DBFieldName.SENT_TIMEOUT), new BasicDBObject(DBFields.NOT_EQUAL, 1));
    }

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.FROM_PEER), pMsg.getFromPeer());
    data.append(fields.getField(DBFieldName.QUEUED_MESSAGE), pMsg.getMsg().toString());
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(fields.getField(DBFieldName.CHECKED_SIGS), data));

    if (isValid) {
      update.append(DBFields.INCREMENT, new BasicDBObject().append(fields.getField(DBFieldName.SIGNATURE_COUNT), 1));
    }

    update.append(
        DBFields.PULL,
        new BasicDBObject(fields.getField(DBFieldName.SIGNATURES_TO_CHECK), new BasicDBObject(fields
            .getField(DBFieldName.FROM_PEER), pMsg.getFromPeer())));

    DBCollection collection;
    if (isConflict && !recordType.equals(DBRecordType.CANCEL)) {
      // Cancel messages cannot be in conflict, since the message is serial number and cancel.
      collection = this.conflictColl;
    }
    else {
      collection = this.coll;
    }

    try {
      WriteResult wr = collection.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", pMsg);
      }
    }
    catch (MongoException.DuplicateKey e) {
      if (!recordType.equals(DBRecordType.CANCEL)) {
        // No timeout on cancel
        BasicDBObject exquery = new BasicDBObject(DBFields.ID, pMsg.getID());
        exquery.append(fields.getField(DBFieldName.SENT_TIMEOUT), new BasicDBObject(DBFields.NOT_EQUAL, 1));
        if (collection.findOne(exquery) == null) {
          logger.warn("Have already sent timeout on message:{}", pMsg);
          throw new AlreadySentTimeoutException("Have already sent timeout on message", e);
        }
      }
      logger.warn("Message from Peer has already been recorded - will ignore:{}", pMsg);
      throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
    }
  }

  /**
   * Stores an incoming Peer SK1 signature for later checking
   * 
   * @param recordType
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param pm
   *          PeerMessage to store
   * @throws ClientMessageExistsException
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   */
  public void submitIncomingPeerMessageUnchecked(DBRecordType recordType, PeerMessage pm) throws ClientMessageExistsException,
      AlreadyReceivedMessageException, AlreadySentTimeoutException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, pm.getID());
    DBFields fields = DBFields.getInstance(recordType);

    // Check we haven't already stored a message from this peer
    query.append(fields.getField(DBFieldName.SIGNATURES_TO_CHECK), new BasicDBObject(DBFields.NOT, new BasicDBObject(
        DBFields.ELEMENT_MATCH, new BasicDBObject(fields.getField(DBFieldName.FROM_PEER), pm.getFromPeer()))));

    if (recordType.equals(DBRecordType.CANCEL)) {
      query.append(fields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));
    }
    else {
      query.append(fields.getField(DBFieldName.SENT_TIMEOUT), new BasicDBObject(DBFields.NOT_EQUAL, 1));
    }

    // Check we haven't received an original message, which would allow this message to be checked
    query.append(fields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.FROM_PEER), pm.getFromPeer());
    data.append(fields.getField(DBFieldName.QUEUED_MESSAGE), pm.getMsg().toString());
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(fields.getField(DBFieldName.SIGNATURES_TO_CHECK), data));

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", pm);
      }
    }
    catch (MongoException.DuplicateKey e) {
      // DuplicateKey, we may have sent a timeout, already received the message, or tried queueing a message when the original is
      // now available
      BasicDBObject exquery = new BasicDBObject(DBFields.ID, pm.getID());
      if (!recordType.equals(DBRecordType.CANCEL)) {
        // Cancel message don't timeout
        exquery.append(fields.getField(DBFieldName.SENT_TIMEOUT), new BasicDBObject(DBFields.NOT_EQUAL, 1));

        if (this.coll.findOne(exquery) == null) {
          logger.warn("Have already sent timeout on message:{}", pm);
          throw new AlreadySentTimeoutException("Have already sent timeout on message", e);
        }

        exquery = new BasicDBObject(DBFields.ID, pm.getID());
      }
      exquery.append(fields.getField(DBFieldName.SIGNATURES_TO_CHECK), new BasicDBObject(DBFields.NOT, new BasicDBObject(
          DBFields.ELEMENT_MATCH, new BasicDBObject(fields.getField(DBFieldName.FROM_PEER), pm.getFromPeer()))));

      if (this.coll.findOne(exquery) == null) {
        logger.warn("Message from Peer has already been recorded - will ignore:{}", pm);
        throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
      }
      else {
        logger.info("Tried adding unchecked Peer Message, but message signature now recorded - will perform check");
        throw new ClientMessageExistsException("Check Signature using Client Message", e);
      }
    }
  }

  /**
   * Stores the SK1 cancellation signature generated by this peer
   * 
   * If no signature has been stored it will store the signature passed in as mySig, otherwise it will return the signature already
   * stored in the database.
   * 
   * @param id
   *          String serial number to store cancellation against - the serial number could a UUID in the case of a file submission
   * @param mySig
   *          Base64 String of signature under SK1 of the cancellation
   * @return String of the signature in the database
   * @throws UnknownDBException
   */
  public String submitMyCancellationSignature(String id, String mySig) throws UnknownDBException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, id);
    DBFieldsCancel fields = DBFieldsCancel.getInstance();
    query.append(fields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.MY_SIGNATURE), mySig);
    update.append(DBFields.INCREMENT, new BasicDBObject().append(fields.getField(DBFieldName.SIGNATURE_COUNT), 1));
    update.append(DBFields.SET, data);

    try {
      this.coll.update(query, update, true, false);

      return mySig;
    }
    catch (MongoException.DuplicateKey e) {
      BasicDBObject exquery = new BasicDBObject(DBFields.ID, id);
      DBObject result = this.coll.findOne(exquery);

      if (result == null) {
        logger.error("Cannot store cancellation signature, but also cannot find it - Corrupt DB?");
        throw new UnknownDBException("Cannot store cancellation signature, but also cannot find it - Corrupt DB?", e);
      }
      else {
        String cancelSig = (String) result.get(fields.getField(DBFieldName.MY_SIGNATURE));
        if (cancelSig == null) {
          logger.error("Cancellation signature in database is null - this should not happen - Corrupt DB?");
          throw new UnknownDBException("Store cancellation signature is null - Corrupt DB?", e);
        }
        return cancelSig;
      }
    }
  }

  /**
   * Stores the SK2 cancellation signature generated by the peer
   * 
   * If no SK2 signature has been stored it will store the signature passed in as mySig, otherwise it will return the signature
   * already stored in the database.
   * 
   * @param id
   *          String of serial number
   * @param mySig
   *          Base64 of the SK2 signature of the cancellation
   * @return Base64 String of mySig or if a signature was already stored in the database the stored signature
   * @throws UnknownDBException
   */
  public String submitMyCancellationSignatureSK2(String id, String mySig) throws UnknownDBException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, id);
    DBFieldsCancel fields = DBFieldsCancel.getInstance();
    query.append(fields.getField(DBFieldName.SK2_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(fields.getField(DBFieldName.SK2_SIGNATURE), mySig);
    update.append(DBFields.SET, data);

    try {
      this.coll.update(query, update, true, false);

      return mySig;
    }
    catch (MongoException.DuplicateKey e) {
      BasicDBObject exquery = new BasicDBObject(DBFields.ID, id);
      DBObject result = this.coll.findOne(exquery);

      if (result == null) {
        logger.error("Cannot store cancellation signature (SK2), but also cannot find it - Corrupt DB?");
        throw new UnknownDBException("Cannot store cancellation signature SK2, but also cannot find it - Corrupt DB?", e);
      }
      else {
        String cancelSig = (String) result.get(fields.getField(DBFieldName.SK2_SIGNATURE));
        if (cancelSig == null) {
          logger.error("Cancellation signature (SK2) in database is null - this should not happen - Corrupt DB?");
          throw new UnknownDBException("Store cancellation signature SK2 is null - Corrupt DB?", e);
        }
        return cancelSig;
      }
    }
  }

  /**
   * Records a message we have received following a timeout
   * 
   * Messages received after a timeout are recorded, but not processed. As such, we don't distinguish between POD and Standard
   * messages.
   * 
   * Cancel message don't timeout.
   * 
   * @param message
   *          JSONWBBMessage to store
   * @throws UnknownDBException
   */
  public void submitPostTimeoutMessage(JSONWBBMessage message) throws UnknownDBException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    DBFields fields = DBFields.getInstance();

    data.append(fields.getField(DBFieldName.QUEUED_MESSAGE), message.getMsg().toString());
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(fields.getField(DBFieldName.POST_TIMEOUT), data));

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", message);
      }
    }
    catch (MongoException.DuplicateKey e) {
      // This should never happen
      logger.error("Have tried adding to postTimeout set, should not have caused duplicate key exception");
      throw new UnknownDBException("Have tried adding to postTimeout set, should not have caused duplicate key exception", e);
    }
  }

  /**
   * Stores a StartEVM message in the database, checking we haven't already sent one or received a vote or audit message (note we
   * could have received a valid vote message with joint signature authorising the startEVM even though we haven't seen the startEVM
   * message ourselves.
   * 
   * @param message
   *          StartEVMMessage to store
   * @throws AlreadyReceivedMessageException
   * @throws MaxTimeoutExceeded
   * @throws ClientMessageExistsException
   */
  public void submitStartEVMMessage(StartEVMMessage message) throws AlreadyReceivedMessageException, MaxTimeoutExceeded,
      ClientMessageExistsException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
    DBFields fields = DBFields.getInstance();
    long checkTime = System.currentTimeMillis();

    // Check we haven't already sent a signature authorising StartEVM or received a vote or audit
    DBFields generalFields = DBFields.getInstance();
    query.append(DBFields.START_EVM_MSG, new BasicDBObject(DBFields.EXISTS, false));
    query.append(generalFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(generalFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));
    // Check no cancellations exist for it
    DBFields cancelFields = DBFieldsCancel.getInstance();
    query.append(cancelFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(cancelFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    // Check we haven't timed-out - we use an $or clause to handle a situation where we haven't received the BallotTimeout
    BasicDBList orClause = new BasicDBList();
    orClause.add(new BasicDBObject(DBFieldsPOD.BALLOT_TIMEOUT, new BasicDBObject(DBFields.GREATER_THAN_EQUAL_TO, checkTime)));
    orClause.add(new BasicDBObject(DBFieldsPOD.BALLOT_TIMEOUT, new BasicDBObject(DBFields.EXISTS, false)));

    query.append(DBFields.OR, orClause);

    // Check we haven't already received a vote
    query.append(fields.getField(DBFieldName.SENT_SIGNATURE), new BasicDBObject(DBFields.NOT_EQUAL, 1));

    BasicDBObject update = new BasicDBObject();
    update.append(DBFields.SET, new BasicDBObject(DBFields.START_EVM_MSG, message.getMsg().toString()));

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}", message);
      }
    }
    catch (MongoException.DuplicateKey e) {
      BasicDBObject checkquery = new BasicDBObject(DBFields.ID, message.getID());
      BasicDBObject record = (BasicDBObject) this.coll.findOne(checkquery);
      if(record.containsField(cancelFields.getField(DBFieldName.CLIENT_MESSAGE))){
        throw new ClientMessageExistsException("Have already sent a cancel signature on the ballot");
      }else if (record.containsField(DBFields.START_EVM_MSG)) {
        logger.warn("Message already exists or has timed out {}", message.getID());
        throw new AlreadyReceivedMessageException("Message already received or has timed out", e);
      }
      else if (record.containsField(DBFieldsPOD.BALLOT_TIMEOUT)) {
        long timeout = record.getLong(DBFieldsPOD.BALLOT_TIMEOUT);

        if (!(timeout >= checkTime)) {
          throw new MaxTimeoutExceeded("Timeout on using ballot has been exceeded");
        }
        else {
          throw new ClientMessageExistsException("Have already sent a signature on the ballot");
        }
      }
      else {
        throw new ClientMessageExistsException("Have already sent a signature on the ballot");
      }
    }
  }

  /**
   * Checks if the timeout for this message has been sent
   * 
   * @param recordType
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param serialNo
   *          of message to check
   * @return boolean true if it has been sent, false if not
   */
  public boolean timeoutSent(DBRecordType recordType, String serialNo) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);
    DBFields fields = DBFields.getInstance(recordType);

    query.append(fields.getField(DBFieldName.SENT_TIMEOUT), 1);
    DBObject doc = this.coll.findOne(query);

    return (doc != null);
  }

  /**
   * Updates the commit record with the specified information.
   * 
   * @param commitFile
   *          The commit file.
   * @param commitFileFull
   *          Whether the commit file is full.
   * @param commitAttachments
   *          The commit attachments.
   * @param hash
   *          Hash of the data.
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   */
  public void updateCurrentCommitRecord(String commitFile, String commitFileFull, String commitAttachments, String hash)
      throws JSONException, AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    query.append(DBFields.COMMITMENT_COMMIT_FILE, new BasicDBObject(DBFields.EXISTS, true));
    query.append(DBFields.COMMITMENT_COMMIT_HASH, new BasicDBObject(DBFields.EXISTS, true));
    query.append(DBFields.COMMITMENT_COMMIT_FILE_FULL, new BasicDBObject(DBFields.EXISTS, true));
    query.append(DBFields.COMMITMENT_COMMIT_ATTACHMENTS, new BasicDBObject(DBFields.EXISTS, true));

    DBObject cur = this.commitColl.findOne(query);

    BasicDBObject updatequery = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    // Calling cur.get(DBFields.COMMITMENT_COMMIT_FILE) returns null for each field. Therefore we need to extract the commitment
    // first to retrieve the fields. It's probably something to do with the "."s in the field names which are not working.
    DBObject commitment = (DBObject) cur.get(DBFields.COMMITMENT);
    String commitFileR1 = (String) commitment.get(DBFields.COMMITMENT_FILE);
    String hashR1 = (String) commitment.get(DBFields.COMMIT_HASH);
    String commitFileFullR1 = (String) commitment.get(DBFields.COMMITMENT_FILE_FULL);
    String commitAttachmentsR1 = (String) commitment.get(DBFields.COMMITMENT_ATTACHMENTS);

    data.append(DBFields.COMMITMENT_COMMIT_FILE_R1, commitFileR1);
    data.append(DBFields.COMMITMENT_COMMIT_HASH_R1, hashR1);
    data.append(DBFields.COMMITMENT_COMMIT_FILE_FULL_R1, commitFileFullR1);
    data.append(DBFields.COMMITMENT_COMMIT_ATTACHMENTS_R1, commitAttachmentsR1);

    data.append(DBFields.COMMITMENT_COMMIT_FILE, commitFile);
    data.append(DBFields.COMMITMENT_COMMIT_HASH, hash);
    data.append(DBFields.COMMITMENT_COMMIT_FILE_FULL, commitFileFull);
    data.append(DBFields.COMMITMENT_COMMIT_ATTACHMENTS, commitAttachments);

    update.append(DBFields.SET, data);

    try {
      WriteResult wr = this.commitColl.update(updatequery, update, false, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}");
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Exception whilst updating commit record during Round 2");
      throw new AlreadyReceivedMessageException("Exception whilst updating commit record during Round 2", e);
    }
  }

  /**
   * Marks a serial number as having been used in all different data types. This is called when a ballot has been audited during the
   * ballot generation audit. In such circumstances the ballot cannot be used for PoD, Audit,Cancel or Voting. The ballot itself
   * should never have been printed, since the Client should never have issued it because it had already been opened as part of the
   * ballot generation audit.
   * 
   * The database update checks that no messages already exist for the serial number and then marks the fields for all the possible
   * message fields as "BallotGenAudit" as a placeholder. This ensure the checks elsewhere that look for the existence of that field
   * will behave correctly. Additionally, the number of signatures is set to 0 to prevent this entry being included in any of the
   * commits - it should not be included since the ballot generation audit message covers the underlying data, this is just a
   * database guard.
   * 
   * @param serialNo
   *          String serial number of ballot to mark as audited
   * @throws AlreadyReceivedMessageException
   */
  public void recordBallotGenAudit(String serialNo) throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, serialNo);
    DBFields podfields = DBFields.getInstance(DBRecordType.POD);
    // Check no POD messages
    query.append(podfields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(podfields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    // Check no cancellations exist for it
    DBFields cancelFields = DBFieldsCancel.getInstance();
    query.append(cancelFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(cancelFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    // Check no general messages
    DBFields generalFields = DBFields.getInstance();
    query.append(generalFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(generalFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    // Check no audit messages
    DBFields auditFields = DBFields.getInstance(DBRecordType.AUDIT);
    query.append(auditFields.getField(DBFieldName.CLIENT_MESSAGE), new BasicDBObject(DBFields.EXISTS, false));
    query.append(auditFields.getField(DBFieldName.MY_SIGNATURE), new BasicDBObject(DBFields.EXISTS, false));

    // Check no StartEVM messages
    query.append(DBFields.START_EVM_MSG, new BasicDBObject(DBFields.EXISTS, false));
    // Update it with my data
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    // Mark the fields as BallotGenAudit
    data.append(generalFields.getField(DBFieldName.CLIENT_MESSAGE), "BallotGenAudit");
    data.append(generalFields.getField(DBFieldName.MY_SIGNATURE), "BallotGenAudit");
    data.append(generalFields.getField(DBFieldName.SIGNATURE_COUNT), 0);

    data.append(cancelFields.getField(DBFieldName.CLIENT_MESSAGE), "BallotGenAudit");
    data.append(cancelFields.getField(DBFieldName.MY_SIGNATURE), "BallotGenAudit");
    data.append(cancelFields.getField(DBFieldName.SIGNATURE_COUNT), 0);

    data.append(auditFields.getField(DBFieldName.CLIENT_MESSAGE), "BallotGenAudit");
    data.append(auditFields.getField(DBFieldName.MY_SIGNATURE), "BallotGenAudit");
    data.append(auditFields.getField(DBFieldName.SIGNATURE_COUNT), 0);

    data.append(podfields.getField(DBFieldName.CLIENT_MESSAGE), "BallotGenAudit");
    data.append(podfields.getField(DBFieldName.MY_SIGNATURE), "BallotGenAudit");
    data.append(podfields.getField(DBFieldName.SIGNATURE_COUNT), 0);

    update.append(DBFields.SET, data);

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Ballot gen audit record not updated:{}", serialNo);
      }
    }
    catch (MongoException.DuplicateKey e) {
      logger.warn("Message from Client has already been recorded - will reject");
      throw new AlreadyReceivedMessageException("Message from Client has already been recorded", e);
    }
  }
}