/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

/**
 * @author Chris Culnane
 * 
 */
public class DBFieldsPOD extends DBFields {

  /**
   * Singleton instance.
   */
  private static final DBFieldsPOD instance       = new DBFieldsPOD();
  public static final String       BALLOT_TIMEOUT = "ballotTimeout";

  /**
   * Private constructor to enforce singleton.
   */
  private DBFieldsPOD() {
    super();
  }

  /**
   * Converts an enum field into a name.
   * 
   * @param fieldName
   *          The enum field.
   * @return The string version of the field name.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields#getField(uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName)
   */
  @Override
  public String getField(DBFieldName fieldName) {
    switch (fieldName) {
      case CHECKED_SIGS:
        return "checkedSigsPOD";
      case MY_SIGNATURE:
        return "podsig";
      case SENT_SIGNATURE:
        return "PODsentSig";
      case SIGNATURE_COUNT:
        return "PODsigCount";
      case SENT_TIMEOUT:
        return "sentTimeoutPOD";
      case CLIENT_MESSAGE:
        return "podmsg";
      case SIGNATURES_TO_CHECK:
        return "sigsToCheckPOD";
      case QUEUED_MESSAGE:
        return "msg";
      case FROM_PEER:
        return "fromPeer";
      case COMMIT_TIME:
        return "PODCommitTime";
      case POST_TIMEOUT:
        return "postTimeout";
      default:
        return null;
    }
  }

  /**
   * @return The singleton instance.
   */
  public static DBFieldsPOD getInstance() {
    return instance;
  }
}
