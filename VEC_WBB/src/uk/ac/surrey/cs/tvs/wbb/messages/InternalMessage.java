/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;

/**
 * /** Represents an internal message to the peer.
 * 
 * @author Chris Culnane
 * 
 */
public abstract class InternalMessage extends JSONWBBMessage {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ExternalMessage.class);

  /**
   * Constructor for JSONWBBMessage takes message as a JSONObject.
   * 
   * @param msg
   *          the JSONObject of the message
   * @throws MessageJSONException
   */
  public InternalMessage(JSONObject msg) throws MessageJSONException {
    super(msg);
  }

  /**
   * Constructor for a JSONWBBMessage.
   * 
   * Should never be called directly
   * 
   * @param msgString
   *          the underlying message string
   * @throws MessageJSONException
   */
  public InternalMessage(String msgString) throws MessageJSONException {
    super(msgString);
  }

  /**
   * Checks whether we have reached a consensus on a message and sends the response.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message sent. Consensus may not have been reached in which case an error response was sent.
   * @throws MessageJSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws JSONException
   * @throws InvalidKeyException
   * @throws IOException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#checkThreshold(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean checkThreshold(WBBPeer peer) throws MessageJSONException, NoSuchAlgorithmException, SignatureException,
      JSONException, InvalidKeyException, IOException, TVSSignatureException {
    // Check if we have reached the required threshold, if we have set the record to show we are going to send an SK2 signature
    if (peer.getDB().checkThresholdAndResponse(DBRecordType.GENERAL, this.getID(), peer.getThreshold())) {
      // Get the original message from the client - strictly speaking we already have it above.
      // method.
      String clientMessage = peer.getDB().getClientMessage(DBRecordType.GENERAL, this.getID());

      if (clientMessage != null) {
        JSONWBBMessage clientMsg = JSONWBBMessage.parseMessage(clientMessage);

        // Create SK2 signature
        JSONObject response = clientMsg.constructResponseAndSign(peer);
        clientMsg.addAdditionalContentToResponse(response);

        // Construct response and send back to client
        peer.sendAndClose(clientMsg.getMsg().getString(MessageFields.UUID), response.toString());

        return true;
      }
      else {
        logger.error("Client message is null even though signatures have been validated. Database must be corrupt. msg:{}",
            this.msg);
        return false;
      }
    }
    else {
      // Check whether it is still possible to reach a successful consensus, if not send an error to the client
      if (!peer.getDB().canOrHaveReachedConsensus(DBRecordType.GENERAL, this.getID(), peer.getThreshold(), peer.getPeerCount() + 1)) {
        String clientMessage = peer.getDB().getClientMessage(DBRecordType.GENERAL, this.getID());
        if (clientMessage != null) {
          JSONWBBMessage clientMsg = JSONWBBMessage.parseMessage(clientMessage);        
          SendErrorMessage.sendErrorMessage(peer, clientMsg, ClientErrorMessage.NO_CONSENSUS_POSSIBLE);
        }else{
          logger.error("Client message is null even though signatures have been validated. Database must be corrupt. msg:{}",
              this.msg);
          return false;
        }
      }
      return true;
    }
  }

  /**
   * Abstract method
   * 
   * Defines the internal content that should be signed by message. This allows generic processing of different types of message
   * since the message itself defines what should and should not be signed. For example, an audit message is just the serial number,
   * whilst a vote is the serial number and preferences. Note, it is the contents of the message that is to be signed by the
   * WBBPeer, not the contents of the message that was signed by the EBM or client device.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getInternalSignableContent()
   */
  @Override
  public abstract String getInternalSignableContent() throws JSONException;

  /**
   * Abstract method to perform validation
   * 
   * The overridden method should perform the relevant validation for the particular message type
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#performValidation(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public abstract void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException;
}
