/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a generic message sent between peers during the consensus protocol.
 * 
 * This covers covers audit, vote and file messages
 * 
 * @author Chris Culnane
 * 
 */
public class PeerMessage extends InternalMessage {

  /**
   * The peer message type.
   */
  public static final String  TYPE_STRING = "Peer";

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(PeerMessage.class);

  /**
   * Instance variable to store where this message has come from
   */
  private String              fromPeer    = null;

  /**
   * Constructor to create PeerMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public PeerMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create PeerMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public PeerMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return null;
  }

  /**
   * Gets the _fromPeer value loading it from the JSON file if necessary
   * 
   * @return String of _fromPeer
   */
  public String getFromPeer() {
    if (this.fromPeer == null) {
      if (this.msg.has(MessageFields.FROM_PEER)) {
        try {
          this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
        }
        catch (JSONException e) {
          logger.error("Trying to get _fromPeer, has thrown error", e);
        }
      }
    }
    return this.fromPeer;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    // This message is never signed by anyone else, so return no signable content
    return null;
  }

  /**
   * Init method to set the type, extract serialNo and lookup _fromPeer
   * 
   * @throws JSONException
   */
  protected void init() throws JSONException {
    this.type = Message.PEER_MESSAGE;
    this.id = this.msg.getString(MessageFields.PeerMessage.ID);
    if (this.msg.has(MessageFields.FROM_PEER)) {
      this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    // Check JSONSchema for PEER message
    if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.PEERMSG))) {
      logger.warn("JSON Schema validation failed");
      throw new JSONSchemaValidationException("JSON Schema validation failed");
    }
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    String serial = this.getID();
    String messageFromClient = peer.getDB().getClientMessage(DBRecordType.GENERAL, serial);

    // Check the message source.
    if (messageFromClient == null) {
      // No EBM message received
      try {
        peer.getDB().submitIncomingPeerMessageUnchecked(DBRecordType.GENERAL, this);
      }
      catch (AlreadyReceivedMessageException e) {
        logger.warn("Already received message: {} from {}. Will ignore", serial, this.msg.getString(MessageFields.FROM_PEER));
        return false;
      }
      catch (ClientMessageExistsException e) {
        // EBM Message now received perform checks
        try {
          JSONWBBMessage.parseMessage(peer.getDB().getClientMessage(DBRecordType.GENERAL, serial)).checkAndStoreMessage(peer, this);
        }
        catch (AlreadyReceivedMessageException e1) {
          logger.warn("Already received message: {} from {}. Will ignore", serial, this.msg.getString(MessageFields.FROM_PEER));
          return false;
        }
      }
      catch (AlreadySentTimeoutException e) {
        try {
          peer.getDB().submitPostTimeoutMessage(this);
          return false;
        }
        catch (UnknownDBException e1) {
          logger.error("Error trying store message to postTimeout set", e1);
          return false;
        }
      }
    }
    else {
      try {
        JSONWBBMessage.parseMessage(messageFromClient).checkAndStoreMessage(peer, this);
      }
      catch (AlreadyReceivedMessageException e) {
        logger.warn("Already received message: {} from {}. Will ignore", serial, this.msg.getString(MessageFields.FROM_PEER));
        return false;
      }
    }

    // Have we got a threshold of responses?
    if (peer.getDB().checkThresholdAndResponse(DBRecordType.GENERAL, serial, peer.getThreshold())) {
      String clientMessage = peer.getDB().getClientMessage(DBRecordType.GENERAL, serial);
      if (clientMessage != null) {
        JSONWBBMessage clientMsg = JSONWBBMessage.parseMessage(clientMessage);
        JSONObject response = clientMsg.constructResponseAndSign(peer);
        clientMsg.addAdditionalContentToResponse(response);

        peer.sendAndClose(clientMsg.getMsg().getString(MessageFields.UUID), response.toString());
      }
      else {
        logger.error("Client message is null even though signatures have been validated. Database must be corrupt. msg:{}",
            this.msg);
        return false;
      }
    }
    else {
      if (!peer.getDB().canOrHaveReachedConsensus(DBRecordType.GENERAL, serial, peer.getThreshold(), peer.getPeerCount() + 1)) {
        SendErrorMessage.sendErrorMessage(peer,
            JSONWBBMessage.parseMessage(peer.getDB().getClientMessage(DBRecordType.GENERAL, serial)),
            ClientErrorMessage.NO_CONSENSUS_POSSIBLE);
      }
    }

    return true;
  }

  /**
   * Set the _fromPeer value both in the instance variable and the underlying JSON msg
   * 
   * @param fromPeer
   *          String of who sent this message
   * @throws JSONException
   */
  public void setFromPeer(String fromPeer) throws JSONException {
    this.fromPeer = fromPeer;
    this.msg.put(MessageFields.FROM_PEER, fromPeer);
  }
}
