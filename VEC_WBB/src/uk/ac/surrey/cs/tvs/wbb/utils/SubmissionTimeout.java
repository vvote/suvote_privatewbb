/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyRespondedException;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * Used by the TimeoutManager to check for timeouts and construct any error messages.
 * 
 * This will get called for every single message received. However, the process checks whether message has been responded to, if
 * not, it responds with a timeout and removes the UUID from the responseChannels, thus blocking any further responses from being
 * sent.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class SubmissionTimeout implements Runnable {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(SubmissionTimeout.class);

  /**
   * UUID of the response channel, needed to send the message to the client
   */
  private UUID                uuid;

  /**
   * Instance variable of the WBBPeer this is running, needed for access to channels and keys
   */
  private WBBPeer             peer;

  /**
   * The message that the timeout has been called on
   */
  private JSONWBBMessage      msg;

  /**
   * Construct a SubmissionTimeout Runnable
   * 
   * @param peer
   *          WBBPeer we are running on
   * @param uuid
   *          UUID for the message
   * @param msg
   *          Original message the timeout was called on
   */
  public SubmissionTimeout(WBBPeer peer, UUID uuid, JSONWBBMessage msg) {
    super();

    this.uuid = uuid;
    this.peer = peer;
    this.msg = msg;
  }

  /**
   * Checks if the incoming message has been processed and responded to within the timeout period. If not, an error message is
   * created.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    // Checks if the session still exists, if not it means it has been responded to and we can ignore the timeout. Most cases will
    // be just such a situation.
    if (this.peer.sessionIDExists(this.uuid)) {
      try {
        // The session still exists, so we need to timeout on it
        if (this.msg.getType() == Message.POD_MESSAGE) {
          // Check and set the database to see if we have recorded a final message. If we have ignore the timeout and just remove
          // the channel, otherwise send a timeout
          if (this.peer.getDB().checkAndSetTimeout(DBRecordType.POD, this.msg)) {
            SendErrorMessage.sendTimeoutMessage(this.peer, this.msg);
          }
          else {
            this.peer.getRemoveResponseChannel(this.uuid.toString());
          }
        }
        else if (this.msg.getType() == Message.AUDIT_MESSAGE || this.msg.getType() == Message.VOTE_MESSAGE) {
          // Check and set the database to see if we have recorded a final message. If we have ignore the timeout and just remove
          // the channel, otherwise send a timeout
          if (this.peer.getDB().checkAndSetTimeout(DBRecordType.GENERAL, this.msg)) {
            SendErrorMessage.sendTimeoutMessage(this.peer, this.msg);
          }
          else {
            this.peer.getRemoveResponseChannel(this.uuid.toString());
          }
        }
        else if (this.msg.getType() == Message.START_EVM || this.msg.getType() == Message.CANCEL_MESSAGE) {
          SendErrorMessage.sendTimeoutMessage(this.peer, this.msg);
        }
        // Note FILE_MESSAGES do not timeout because they could take a long time to read. Cancel messages
        // now timeout in that a response is closed, although unlike other types of messages the protocol can be re-run for cancel.
        // The timeout will still fire for a file message, but it will be ignored. The danger is the session UUID will remain in the
        // list of responses forever. This assumes that something has failed in such a way as to not try and return an error
        // message. Another possibility is that the peer receives a submission but no other peers do. This peer will then keep that
        // response in the list because the protocol is unfinished. During normal running there shouldn't be many/if any file
        // submissions, so this is something that should be monitored, but there is no easy way of automatically handling it - short
        // of a timeout.
      }
      catch (AlreadyRespondedException e) {
        logger.info("Have already responded to Client - will ignore timeout");
      }
    }
  }
}
