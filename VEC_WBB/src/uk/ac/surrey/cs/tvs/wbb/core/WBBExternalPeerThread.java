/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.utils.SubmissionTimeout;

/**
 * Processes incoming connections from the ServerExternalListener
 * 
 * Reads the actual message from the stream, performs some additional verification and validation of the message before creating a
 * MessageProcessor to process the actual contents of the message.
 * 
 * @author Chris Culnane
 * 
 */
public class WBBExternalPeerThread implements Runnable {

  /**
   * Socket from the accepted connection
   */
  private Socket                                socket;

  /**
   * The BoundedBufferInputStream we will use to read from the socket
   */
  private BoundedBufferedInputStream            in;

  /**
   * A ConcurrentMap to track serial number processing - it should be a weakhashmap
   */
  private ConcurrentMap<String, SerialExecutor> serialMap;
  /**
   * The underlying WBBPeer
   */
  private WBBPeer                               peer;

  /**
   * Logger
   */
  private static final Logger                   logger = LoggerFactory.getLogger(WBBExternalPeerThread.class);

  /**
   * Constructor for the WBBExternalPeerThread
   * 
   * @param peer
   *          that this running on
   * @param socket
   *          to read from
   * @param serialQueue
   *          ConcurrentHashMap that enforces messages for same serial number are not processed concurrently
   * 
   * @throws IOException
   */
  public WBBExternalPeerThread(WBBPeer peer, Socket socket, ConcurrentMap<String, SerialExecutor> serialMap) throws IOException {
    super();

    this.socket = socket;
    this.serialMap = serialMap;

    // Setup new BoundedBufferInputStream with bufferCount from config file
    this.in = new BoundedBufferedInputStream(socket.getInputStream(), peer.getConfig().getBufferBound());
    this.peer = peer;
  }

  /**
   * Process a single external message.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    logger.debug("Connection from:{}", this.socket.getRemoteSocketAddress());

    String line;

    try {
      // Wait until line is ready to be read
      line = this.in.readLine();
      if (line == null) {
        // Catch if the connection has dropped
        logger.info("readLine returned Null - likely connection drop");
        this.socket.close();
        return;
      }

      // Prepare variable to store the incoming message
      JSONWBBMessage msg = null;
      logger.info("Received: {} from {}", line, this.socket.getRemoteSocketAddress());

      try {
        // Try to parse the incoming message
        msg = JSONWBBMessage.parseMessage(line);
      }
      catch (MessageJSONException e) {
        // Failure here indicates the message could be parse. Might be malformed, corrupted or malicious. Either way, we send an
        // error and close the connection
        logger.warn("Could not parse JSON message:{}, error:{}", line, e.getMessage());
        this.sendMessageAndClose(
            SendErrorMessage.constructErrorMessage(this.peer, e.getMessage(), ClientErrorMessage.COULD_NOT_PARSE_JSON), this.socket);
        return;
      }

      try {
        msg.performValidation(this.peer);
      }
      catch (MessageVerificationException | JSONSchemaValidationException e) {
        // The failure could be based on a signature exception
        logger.warn("Message failed validation:{} - rejected:{}", e.getMessage(), msg);
        this.sendMessageAndClose(
            SendErrorMessage.constructErrorMessage(this.peer, e.getMessage(), ClientErrorMessage.MESSAGE_FAILED_VALIDATION),
            this.socket);
        return;
      }

      // We set the current CommitTime - this indicates which commit this message will go into
      msg.setCommitTime(this.peer.getCommitTime());

      // Create a sessionID - this is different to the protocol sessionID. This is for tracking this response socket through other
      // threads.
      UUID sessionID = UUID.randomUUID();
      msg.getMsg().put(MessageFields.UUID, sessionID.toString());

      // Place this socket in the central list of response channels, indexed by the sessionID so we can find it later
      this.peer.addResponseChannel(sessionID, this.socket);

      // Add a timeout for this message
      this.peer.getTimeoutManager().addDefaultTimeout(new SubmissionTimeout(this.peer, sessionID, msg));

      // Add details of where this connection has come from. It uses the notation _fromPeer even though the communication is from a
      // client. This is to allow easy reuse of other code later on.
      msg.getMsg().put(MessageFields.FROM_PEER, this.socket.getRemoteSocketAddress().toString());

      if (!msg.preProcessMessage(this.peer, this.in, this.socket)) {
        logger.warn("Preprocessing failed, will ignore message {}", line);
        return;
      }

      String internID = msg.getID().intern();
      SerialExecutor se = new SerialExecutor(this.peer.getExternalMessageExecutor());
      SerialExecutor ret = serialMap.putIfAbsent(internID, se);
      if (ret != null) {
        ret.execute(new ExternalMessageProcessor(this.peer, msg, internID));
      }
      else {
        se.execute(new ExternalMessageProcessor(this.peer, msg, internID));
      }
      logger.info("Added message to Queue");

    }
    catch (SocketException e) {
      // This will occur when a socket is closed. It is normal - hence we ignore it.
      return;
    }
    catch (IOException e) {
      logger.error("IO exception occured, will try to continue", e);
    }
    catch (JSONException e) {
      logger.error("JSON exception occured, will try to continue", e);

    }
    catch (StreamBoundExceededException e) {
      logger.warn("Input has exceeded bound on server socket - no newline character found. The data has been discarded.",
          this.socket.getRemoteSocketAddress(), e);
      try {
        this.sendMessageAndClose(SendErrorMessage.constructErrorMessage(this.peer, "", ClientErrorMessage.INPUT_EXCEEDED_BOUND),
            this.socket);
      }
      catch (IOException e1) {
        logger.warn("Exception whilst trying to send error message to client", e1);
      }
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Crypto exception", e);
    }
  }

  /**
   * Utility method to send an error message and close the socket connection.
   * 
   * @param message
   *          the message to send
   * @param sock
   *          the socket to send the message on
   * @throws IOException
   */
  private void sendMessageAndClose(String message, Socket sock) throws IOException {
    PrintStream ps = new PrintStream(sock.getOutputStream(), true);
    ps.println(message);
    sock.close();
  }
}
