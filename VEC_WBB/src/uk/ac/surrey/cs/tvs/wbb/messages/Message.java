/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

/**
 * Enum of the different message types that the WBB can process.
 * 
 * The order of declaration is important - be careful if ever changing this. The order these are declared determines the natural
 * ordering and therefore the order of messages in the Commit files (since we order by type first and then serial number). As such,
 * the ordering of these types has been set to ensure a sensible and consistent order of the messages. For example,
 * BallotFileMessages are put first, so that if a peer is recovering from a total loss of data it will received the ballot
 * information first and therefore be able to correctly handle the subsequent POD messages. Without this, the POD messages would be
 * rejected because they fail the ballot reduction verification.
 * 
 * @author Chris Culnane
 * 
 */
public enum Message {
  MIX_RANDOM_COMMIT, BALLOT_GEN_COMMIT, BALLOT_AUDIT_COMMIT, POD_MESSAGE,START_EVM, VOTE_MESSAGE, AUDIT_MESSAGE, FILE_MESSAGE, CANCEL_MESSAGE, PEER_CANCEL_MESSAGE, PEER_MESSAGE, PEER_POD_MESSAGE, COMMIT_R1, PEER_FILE_MESSAGE, GENERIC, HEARTBEAT  
}
