/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.Principal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.SSLSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Internal ServerListener - listens for connections from other peers.
 * 
 * Is similar to the ServerExternalListener except we expect there to be bounded number of these connections. Connections between
 * peers are kept open, they are not opened and closed for each message or run of the protocol.
 * 
 * As such, we create a separate thread for each connection, since there should be at most 2 per peer - one for message transfer and
 * one for file transfer. We use two so that an ongoing commit does not inhibit the transmission of further protocols messages,
 * which would occur if the we only had one connection.
 * 
 * 
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class ServerInternalListener implements Runnable {

  /**
   * Telemetry Logger
   */
  private static final Logger         telemetry             = LoggerFactory.getLogger("Telemetry");
  /**
   * The name of the internal thread.
   */
  private static final String                         THREAD_NAME       = "InternalPeerListener";

  /**
   * Logger
   */
  private static final Logger                         logger            = LoggerFactory.getLogger(ServerInternalListener.class);

  /**
   * Set to true to gracefully shutdown the listener
   */
  private boolean                                     shutdown          = false;

  /**
   * Underlying WBBPeer this listener is associated with
   */
  private WBBPeer                                     peer;

  /**
   * Connection monitor to ensure that each connecting peer only opens two connections
   */
  private ConcurrentHashMap<Principal, AtomicInteger> connectionMonitor = new ConcurrentHashMap<Principal, AtomicInteger>();

  /**
   * Constructor for internal listener
   * 
   * @param peer
   *          this listener is associated to
   */
  public ServerInternalListener(WBBPeer peer) {
    super();

    this.peer = peer;
  }

  /**
   * Continuously listen for messages and hand them off to the internal processor.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    while (!this.shutdown) {
      try {
        SSLSocket socket = (SSLSocket) this.peer.getPeerServerSocket().accept();

        // Because we anticipate a limited number of connections we conduct the SSL verification on the listening thread. A small
        // delay in initially setting up the communication between the peers is acceptable, since once they are setup they will not
        // be closed - unless an error occurs.
        if (this.peer.verifyConnection((InetSocketAddress) socket.getRemoteSocketAddress(), socket.getSession().getPeerPrincipal(),
            false)) {
          AtomicInteger connectionCount = new AtomicInteger(0);

          // Put a new AtomicInteger of 1 if no counter exists, if it does return it we limit the number of connection from each
          // peer to 2, one for messages, one for files
          AtomicInteger connectionCountRet = this.connectionMonitor.putIfAbsent(socket.getSession().getPeerPrincipal(),
              connectionCount);
          if (connectionCountRet != null) {
            connectionCount = connectionCountRet;
          }
          if (connectionCount != null && connectionCount.incrementAndGet() > 2) {
            // This indicates that the counter is greater than 2 we will therefore close this connection, we also decrement the
            // counter to reflect us closing the counter
            connectionCount.decrementAndGet();
            logger.warn("Too many connection attempts from {}. Closing connection.", socket.getRemoteSocketAddress());
            telemetry.info("TOOMANYCONNS#{}", socket.getRemoteSocketAddress());
            socket.getSession().invalidate();
            socket.close();
          }
          else {
            WBBPeerThread wbbPeerThread = new WBBPeerThread(this.peer, socket, connectionCount);
            (new Thread(wbbPeerThread, ServerInternalListener.THREAD_NAME)).start();
          }
        }
        else {
          // If the connection is invalid we close it and invalidate the SSL connection. We are using ClientAuthentication and an
          // independent verification step.
          logger.warn("Connection to {} failed verification. Closing connection.", socket.getRemoteSocketAddress());
          telemetry.info("CONNVALFAIL#{}", socket.getRemoteSocketAddress());
          socket.getSession().invalidate();
          socket.close();
        }
      }
      catch (IOException e) {
        telemetry.info("CONNFAIL#Unavailable,Incoming Connection failed");
        logger.error("IOException occurred during internal Server socket accept. Will retry in {} seconds", (this.peer.getConfig()
            .getSocketFailTimeoutInitial() / 1000), e);
        try {
          Thread.sleep(this.peer.getConfig().getSocketFailTimeoutInitial());
        }
        catch (InterruptedException e1) {
          logger.error("Interrupted whilst waiting after socket failure", e1);
        }
      }
    }
  }

  /**
   * Shuts down the listener.
   */
  public void shutdown() {
    this.shutdown = true;
  }
}
