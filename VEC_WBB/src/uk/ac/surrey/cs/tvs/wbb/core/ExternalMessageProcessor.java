/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;

/**
 * Performs the actual processing of messages received from EBMs, PODClients, MixPeers etc.
 * 
 * @author Chris Culnane
 * 
 */
public class ExternalMessageProcessor extends MessageProcessor implements Runnable {

  /**
   * Holds a reference to the underlying message to be processed
   */
  protected JSONWBBMessage    msg;
  
  /**
   * Reference to serial no
   */
  private String              serialNo;

  

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ExternalMessageProcessor.class);


  /**
   * Constructor for ExternalMessageProcessor.
   * 
   * @param peer
   *          WBBPeer this is running on - needed to access central resources
   * @param queue
   *          message queue related to the serial number of the message being processed
   * @param serialQueue
   *          global set of all message queues, needed for tidying up at the end of processing
   * @param serialNo
   *          String containing the serialNo that this ExternalMessageProcessor will work on
   */
  public ExternalMessageProcessor(WBBPeer peer, JSONWBBMessage msg, String serialNo) {
    super(peer);
    this.msg = msg;
    this.serialNo = serialNo;
  }

  /**
   * Processes the next message as a runnable action.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      if (!this.msg.processMessage(this.peer)) {
        logger.info("Processing message failed on {} with msg {}", this.serialNo,this.msg);
        return;
      }

    }
    catch (Exception e) {
      logger.error("Error has occured whilst processing external message. Will try to continue", e);
    }
  }

}
