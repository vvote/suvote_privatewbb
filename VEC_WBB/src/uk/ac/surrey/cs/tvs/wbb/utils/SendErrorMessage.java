/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.ErrorMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;

/**
 * Utility method to construct an appropriate Error message to send it to the client.
 * 
 * @author Chris Culnane
 * 
 */
public class SendErrorMessage {

  /**
   * Logger
   */
  private static final Logger logger    = LoggerFactory.getLogger(SendErrorMessage.class);

  /**
   * Telemetry Logger
   */
  private static final Logger telemetry = LoggerFactory.getLogger("Telemetry");

  /**
   * Static utility method to construct a valid Error Message for when we don't have a valid original message. This ensures that we
   * are still using the same set of Error Message codes even if we are not sending it directly from this class.
   * 
   * @param peer
   *          WBBPeer this is running on - needed for access to keys
   * @param message
   *          String of the message we want to send in addition to the predefined ClientErrorMessage
   * @param errMsg
   *          ClientErrorMessage representing type of error message
   * @return JSON String of error message or JSON with unknown error
   */
  public static String constructErrorMessage(WBBPeer peer, String message, ClientErrorMessage errMsg) {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getSK2());
      JSONObject response = new JSONObject();

      response.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
      tvsSig.update(ErrorMessage.TYPE_ERROR);
      response.put(MessageFields.ErrorMessage.PEER_ID, peer.getID());

      switch (errMsg) {
        case COULD_NOT_PARSE_JSON:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Could not parse JSON message:" + message);
          break;
        case UNKNOWN_MESSAGE_TYPE:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Unknown message type received:" + message);
          break;
        case MESSAGE_FAILED_VALIDATION:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Message failed validation - rejected:" + message);
          break;
        case SIGNATURE_CHECK_FAILED:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Signature check on received file failed - message rejected:" + message);
          break;
        case INPUT_EXCEEDED_BOUND:
          response.put(MessageFields.ErrorMessage.MESSAGE,
              "Input has exceeded bound on server socket - no newline character found. The data has been discarded:" + message);
          break;
        default:
          // we don't cover all types of errors because this method only handles errors when a message is rejected, i.e. it will not
          // be processed or could be invalid
          logger.error("Request to send an unknown Error Type to the client");
          response.put(MessageFields.ErrorMessage.MESSAGE, "An unknown error has occured");
          break;
      }

      tvsSig.update(response.getString(MessageFields.ErrorMessage.MESSAGE));
      response.put(MessageFields.ErrorMessage.PEER_SIG, tvsSig.signAndEncode(EncodingType.BASE64));
      telemetry.info("ERRMSG#{}", response);
      return response.toString();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return "{type:\"ERROR\",msg:\"Unknown\"}";
  }

  /**
   * Static method to send an error message
   * 
   * The original method is needed to get the UUID to obtain the response channel that this message should be sent on.
   * 
   * Note this is for messages which have a readable original message. If the message is not readable the UUID cannot be extracted.
   * In such circumstances a message is constructed from ClientErrorMessage and sent directly.
   * 
   * @param peer
   *          WBBPeer this is running on - needed for access to keys
   * @param original
   *          original message that caused the error
   * @param errMsg
   *          the error message to send
   */
  public static void sendErrorMessage(WBBPeer peer, JSONWBBMessage original, ClientErrorMessage errMsg) {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getSK2());
      JSONObject response = new JSONObject();
      response.put(MessageFields.ErrorMessage.ID, original.getID());
      tvsSig.update(original.getID());
      response.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
      tvsSig.update(ErrorMessage.TYPE_ERROR);
      response.put(MessageFields.ErrorMessage.PEER_ID, peer.getID());

      switch (errMsg) {
        case SERIALNO_USED:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Serial No has already been used");
          break;
        case TIME_OUT_ON_CONSENSUS:
          response.put(MessageFields.ErrorMessage.MESSAGE,
              "Did not get a threshold of valid responses from Peers. No consensus reached");
          break;
        case NO_CONSENSUS_POSSIBLE:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Received conflicting messages from Peers. No consensus possible");
          break;
        case UNKNOWN_DB_ERROR:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Received an Unknown DB Error");
          break;
        case SESSION_TIMEOUT:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Consensus was not reached within the timeout");
          break;
        case COMMITMENT_FAILURE:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Commitment did not verify");
          break;
        case UNKNOWN_SERIAL_NO:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Cannot find a record of the serial number");
          break;
        case BALLOT_TIMEOUT:
          response.put(MessageFields.ErrorMessage.MESSAGE, "Ballot has already timedout");
          break;
        case STARTEVM_ALREADY_SENT:
          response.put(MessageFields.ErrorMessage.MESSAGE, "StartEVM message previously sent");
          break;
        default:
          // we don't cover all types of errors because this method only handle errors for which a valid message was received, but
          // we reject of another reason (no concensus, already used etc).
          logger.error("Request to send an unknown Error Type to the client");
          response.put(MessageFields.ErrorMessage.MESSAGE, "An unknown error has occured");
          break;
      }

      tvsSig.update(response.getString(MessageFields.ErrorMessage.MESSAGE));
      response.put(MessageFields.ErrorMessage.PEER_SIG, tvsSig.signAndEncode(EncodingType.BASE64));
      telemetry.info("ERRMSG#{}", response);
      peer.sendAndClose(original.getMsg().getString(MessageFields.UUID), response.toString());
    }
    catch (RuntimeException e) {
      logger.error("RuntimeException whilst sending error message to the client.", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception whilst creating signature to send error message to the client.", e);
    }
    catch (JSONException e) {
      logger.error("JSON Exception whilst creating error message to send to the client.", e);
    }

    catch (IOException e) {
      logger.error("IOException whilst trying to send error message to the client.", e);
    }
  }

  /**
   * Static utility method to send a timeout message to the client.
   * 
   * @param peer
   *          WBBPeer this is running - needed for access to keys
   * @param original
   *          JSONWBBMessage the timeout has been called on
   */
  public static void sendTimeoutMessage(WBBPeer peer, JSONWBBMessage original) {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getSK2());

      JSONObject response = new JSONObject();

      response.put(MessageFields.ErrorMessage.ID, original.getID());
      tvsSig.update(original.getID());
      response.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
      tvsSig.update(ErrorMessage.TYPE_ERROR);
      response.put(MessageFields.ErrorMessage.PEER_ID, peer.getID());
      response.put(MessageFields.ErrorMessage.MESSAGE, "Consensus was not reached within the timeout");

      tvsSig.update(response.getString(MessageFields.ErrorMessage.MESSAGE));
      response.put(MessageFields.ErrorMessage.PEER_SIG, tvsSig.signAndEncode(EncodingType.BASE64));
      peer.sendAndClose(original.getMsg().getString(MessageFields.UUID), response.toString(), false);
      telemetry.info("MSGTIMEOUT#{}", response);
    }
    catch (Exception e) {
      logger.error("Error whilst sending timeout message to the client.", e);
    }
  }
}