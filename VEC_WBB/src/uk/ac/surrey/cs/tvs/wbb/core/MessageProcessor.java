/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

/**
 * An abstract MessageProcessor class that provides the generic methods used by both the internal and external message processor.
 * The reason both subclasses share methods is because the internal processor is simulating an external processor when processing
 * the commit data
 * 
 * @author Chris Culnane
 * 
 */
public abstract class MessageProcessor implements Runnable {

  /**
   * Peer this is running on
   */
  protected WBBPeer peer;

  /**
   * Constructor for the Message processor
   * 
   * @param peer
   *          reference to peer this is running on
   */
  public MessageProcessor(WBBPeer peer) {
    super();

    this.peer = peer;
  }
}
