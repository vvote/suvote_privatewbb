/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation

 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;

/**
 * Utility class for importing all JKS certificates into a specified BKS KeyStore
 * 
 * @author Chris Culnane
 * 
 */
public class ImportAllCertsInFolder {

  /**
   * Constant specifying the type of certificate build
   */
  protected static final String CERTIFICATE_TYPE = "X.509";

  /**
   * Simple Utility for importing all JKS Public Certificates into the specified BKS KeyStore. This is needed when setting up peers
   * and the SK1 certificates need combining into a single keystore. It is designed to expand the peers.bks to add an underlying JKS
   * and then populate it.
   * 
   * NOTE: This is very similar to the KeyGeneration code in the vVoteLibrary. However, that code incorrectly includes a slash at
   * the front of the entity name. That has no impact on the usage within the client, which is importing an SSL CA certificate, but
   * would have an impact here, where we are explicitly looking for entity names. As such, the code has been separated due to not
   * wanting to change the library running on the client, but in time the vVoteLibrary code should be modified and this refactored
   * to use it.
   * 
   * @param args
   *          String[] of three arguments, the first is the BKS keystore to open, the second is the output filename (without
   *          extension) and the third is the folder containing the certificates to import
   * @throws KeyStoreException
   * @throws IOException
   */
  public static void main(String[] args) throws KeyStoreException, IOException {
    if (args.length != 3) {
      printUsage();
    }
    else {
      try {
        System.out.println("Loading KeyStore:" + args[0]);
        TVSKeyStore inputKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        inputKeyStore.load(args[0], "".toCharArray());
        inputKeyStore.initKeyStore();
        CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE);

        File certs = new File(args[2]);
        if (certs.isDirectory()) {
          FileInputStream in = null;
          for (File f : certs.listFiles()) {
            try {
              if (f.isFile() && f.getName().endsWith(".pem")) {
                System.out.println("Importing:" + f.getName());
                try {
                  in = new FileInputStream(f);
                  inputKeyStore.getKeyStore().setCertificateEntry(f.getName().substring(0, f.getName().lastIndexOf(".")),
                      certFactory.generateCertificate(in));
                }
                finally {
                  if (in != null) {
                    in.close();
                  }
                }
              }
              inputKeyStore.store(args[1] + ".bks", args[1] + ".jks", "".toCharArray());

            }
            catch (IOException e) {
              throw new CertificateStoringException("Exception whilst importing certificates", e);
            }
            catch (KeyStoreException e) {
              throw new CertificateStoringException("Exception whilst importing certificates", e);
            }
            catch (CertificateException e) {
              throw new CertificateStoringException("Exception whilst importing certificates", e);
            }
          }
          System.out.println("KeyStore Saved to: " + args[1] + ".bks" + " and " + args[1] + ".jks");
        }

        // System.out.println(Arrays.toString(filesToProcess.toArray(new String[0])));
        // KeyGeneration.importCertificateFiles(inputKeyStore.getJavaKeystorePath(), filesToProcess.toArray(new String[0]));
        // TVSKeyStore testKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        // testKeyStore.load(args[1]+".bks", "".toCharArray());
        // System.out.println(testKeyStore.getCertificate("peer1_signingSK1").getCertificate().getType());

        else {
          System.err.println("Path is not a directory");
        }
      }
      catch (IOException e) {
        e.printStackTrace();
      }
      catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
      catch (CertificateException e) {
        e.printStackTrace();
      }
      catch (CertificateStoringException e) {
        e.printStackTrace();
      }
    }

  }

  public static void printUsage() {
    System.out.println("ImportAllCertsInFolder imports all the JKS certificates found in a folder");
    System.out.println("into a BKS KeyStore. It is assumed that the BKS KeyStore does not have an");
    System.out.println("underlying JKS KeyStore. The output will be saved as two files (linked) the BKS");
    System.out.println("and the JKS - with appropriate extensions");
    System.out.println("Usage:");
    System.out.println("ImportAllCertsInFolder KeyStorePath KeyStoreDestination CertsFolderToImport");
    System.out.println("\t\tKeyStorePath: the path to the BKS KeyStore, or an empty JSON File");
    System.out.println("\t\tKeyStoreDestination: the output filename WITHOUT extension.");
    System.out.println("\t\t\ti.e ./mypathto/mykeystore. The actual output will");
    System.out.println("\t\t\tbe ./mypathto/mykeystore.bks and ./mypathto/mykeystore.jks");
  }

}
