/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;

/**
 * Represents a FileMessage sent from a client device to the WBB
 * 
 * This will be utilised by PODPeers to periodically submit a zip file of proofs covering key transformation. It will also be
 * utilised by the MixNetPeer to bulk upload its proofs, mixes, decryptions etc.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public abstract class BallotFileMessage extends FileMessage {

  /**
   * Logger
   */
  private static final Logger logger               = LoggerFactory.getLogger(BallotFileMessage.class);

  /**
   * Static int setting the default number of files to expect
   */
  private static final int    DEFAULT_NUMBER_FILES = 1;
  /**
   * Temporary directory to hold message file data.
   */
  protected File              outputDir            = null;

  /**
   * Unzip file prefix.
   */
  private static final String COMMIT_UNZIP_PREFIX  = "CommitUnzip";

  /**
   * Constructor to create FileMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public BallotFileMessage(JSONObject msg) throws MessageJSONException {
    super(msg);
  }

  /**
   * Constructor to create FileMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public BallotFileMessage(String msgString) throws MessageJSONException {
    super(msgString);
  }

  /**
   * Processes the file associated with the message using the DEFAULT_NUMBER_FILES bound on the number of files to accept in a zip -
   * Default is 1.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream.
   * @param socket
   *          The socket used for the send.
   * @param unzipDirPrefix
   *          The directory prefix for placing temporary files.
   * @return True if the file was processed successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   */
  protected boolean doFileProcessing(WBBPeer peer, BoundedBufferedInputStream in, Socket socket, String unzipDirPrefix) throws IOException, JSONException, NoSuchAlgorithmException {
    return doFileProcessing(peer, in, socket, unzipDirPrefix, BallotFileMessage.DEFAULT_NUMBER_FILES);
  }

  /**
   * Processes the file associated with the message.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream.
   * @param socket
   *          The socket used for the send.
   * @param unzipDirPrefix
   *          The directory prefix for placing temporary files.
   * @param numberFiles
   *          The number of files to allow in the zip - default is 1
   * @return True if the file was processed successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   */
  protected boolean doFileProcessing(WBBPeer peer, BoundedBufferedInputStream in, Socket socket, String unzipDirPrefix,
      int numberFiles, String ...fileOrder) throws IOException, JSONException, NoSuchAlgorithmException {
    // If it is a file message we need to download the file from the stream
    logger.info("Message is Ballot Audit Commit upload");

    // Uploads should always be zip files to allow multiple files to be uploaded in a single file. We always create an
    // independent filename, we do not trust any sent filenames. All uploads go in a single directory. createTempFile guarantees
    // uniqueness of filenames in the directory, however, the file itself is not a temporary file.
    File dest = File.createTempFile(FileMessage.UPLOAD_FILE_PREFIX, FileMessage.UPLOAD_FILE_EXT, peer.getUploadDir());
    logger.info("Message is file upload, will store in: {}, msg{}:", dest.getName(), this.msg);

    // Set the filename in the FileMessage so we can access the file during later processing
    this.setFilename(dest.getName());
    String newDigest = "";

    try {
      // Set the socket timeout based on config option - this handles cases where a large file is specified in the FileMessage
      // but the client doesn't send all the data
      socket.setSoTimeout(peer.getUploadTimeout());

      // Read the file into the destination.
      in.readFile(this.getFileSize(), dest);

      ZipUtil zu = new ZipUtil(peer.getConfig().getInt(WBBConfig.MAX_ZIP_ENTRY));
      Path commitUploadDir = Paths.get(peer.getUploadDir().getAbsolutePath());

      this.outputDir = Files.createTempDirectory(commitUploadDir, unzipDirPrefix).toFile();

      try {
        zu.unzip(dest.getAbsolutePath(), this.outputDir);
        if (this.outputDir.listFiles().length != numberFiles) {
          logger.warn("In correct number of files in submitted BallotFileMessage. Expected {} but got {}",
              numberFiles,this.outputDir.listFiles().length);
          return false;
        }
        else {
          MessageDigest md = peer.getConfig().getMessageDigest();
          if(fileOrder!=null && fileOrder.length>0){
            for(String entryName:fileOrder){
              IOUtils.addFileIntoDigest(new File(this.outputDir,entryName), md);
            }
            newDigest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
          }else{
            IOUtils.addFileIntoDigest(this.outputDir.listFiles()[0], md);
            newDigest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
          }
        }
      }
      catch (UploadedZipFileEntryTooBigException | UploadedZipFileNameException e) {
        logger.warn("Exception when reading zip file, either corrupt of malicious", e);
        this.sendMessageAndClose(SendErrorMessage.constructErrorMessage(peer, "", ClientErrorMessage.INVALID_ZIP_FILE), socket);
        return false;
      }
      finally {
        socket.setSoTimeout(0);
      }

      // The readFile message returns the digest of the read file. We update the underlying FileMessage to now perform
      // validation on the digest of the File we have actually received
      if (!this.updateDigestAndVerify(peer, newDigest)) {
        logger.info("Signature check on received file failed - message rejected. msg:{}, file:{}", this.msg, dest.getName());
        this.sendMessageAndClose(SendErrorMessage.constructErrorMessage(peer, "", ClientErrorMessage.SIGNATURE_CHECK_FAILED),
            socket);
        return false;
      }

      return true;
    }
    catch (MessageSignatureException e) {
      // Message has failed verification following the update of the local digest
      logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg,
          dest.getName());
      this.sendMessageAndClose(
          SendErrorMessage.constructErrorMessage(peer, e.getMessage(), ClientErrorMessage.SIGNATURE_CHECK_FAILED), socket);
      return false;
    }
    catch (MessageJSONException e) {
      logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg,
          dest.getName());
      this.sendMessageAndClose(
          SendErrorMessage.constructErrorMessage(peer, e.getMessage(), ClientErrorMessage.SIGNATURE_CHECK_FAILED), socket);
      return false;
    }
    finally {
      // Reset timeout to infinity, assuming the socket isn't closed.
      if (!socket.isClosed()) {
        socket.setSoTimeout(0);
      }
    }
  }

  /**
   * Gets the filename of the underlying file
   * 
   * @return string of the fileName
   */
  @Override
  public String getFilename() {
    return this.fileName;
  }

  /**
   * Gets the fileSize of the underlying file
   * 
   * @return long value of the file size
   */
  @Override
  public long getFileSize() {
    return this.fileSize;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.internalSignableContent;
  }

  /**
   * Processes the message as a part of the commit round 2 processing.
   * 
   * @param peer
   *          The WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.FileMessage#preprocessAsPartofCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected void preprocessAsPartofCommitR2(WBBPeer peer, String safeUploadDir) throws NoSuchAlgorithmException, JSONException,
      MessageJSONException {

    try {
      File uploadFile = new File(safeUploadDir + "/" + this.getFilename());

      // If it is a file message we need to download the file from the stream
      logger.info("Message is Ballot Audit Commit upload in Commit R2");
      this.msg.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, safeUploadDir);

      // Set the filename in the FileMessage so we can access the file during later processing
      this.setFilename(uploadFile.getName());
      String newDigest = "";

      try {
        ZipUtil zu = new ZipUtil(peer.getConfig().getInt(WBBConfig.MAX_ZIP_ENTRY));
        Path commitUploadDir = Paths.get(safeUploadDir);

        this.outputDir = Files.createTempDirectory(commitUploadDir, BallotFileMessage.COMMIT_UNZIP_PREFIX).toFile();

        try {
          zu.unzip(uploadFile.getAbsolutePath(), this.outputDir);
          if (this.outputDir.listFiles().length != 1) {
            logger.warn("In correct number of files in submitted BallotFileMessage. Expected 1 but got {}",
                this.outputDir.listFiles().length);
            return;
          }
          else {
            MessageDigest md = peer.getConfig().getMessageDigest();
            IOUtils.addFileIntoDigest(this.outputDir.listFiles()[0], md);
            newDigest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
          }
        }
        catch (UploadedZipFileEntryTooBigException | UploadedZipFileNameException e) {
          logger.warn("Exception when reading zip file, either corrupt of malicious", e);

          return;
        }

        // The readFile message returns the digest of the read file. We update the underlying FileMessage to now perform
        // validation on the digest of the File we have actually received
        if (!this.updateDigestAndVerify(peer, newDigest)) {
          logger
              .info("Signature check on received file failed - message rejected. msg:{}, file:{}", this.msg, uploadFile.getName());
          return;
        }
        return;
      }
      catch (MessageSignatureException e) {
        // Message has failed verification following the update of the local digest
        logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg,
            uploadFile.getName());

        return;
      }
      catch (MessageJSONException e) {
        logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg,
            uploadFile.getName());

        return;
      }

    }
    catch (IOException e) {
      logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg, e);
    }
  }
}
