/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Party object represents parties in ATL races and their corresponding candidates
 * 
 * @author Chris Culnane
 * 
 */
public class Party {

  /**
   * String containing the preferred name of party
   */
  private String               preferredName;

  /**
   * Logger
   */
  private static final Logger  logger       = LoggerFactory.getLogger(Party.class);

  /**
   * ArrayList<Candidate> candidates associated with this party
   */
  private ArrayList<Candidate> candidates   = new ArrayList<Candidate>();

  /**
   * Boolean of whether this party has a ballot box or not
   */
  private boolean              hasBallotBox = false;

  /**
   * Boolean of whether this party is ungrouped
   */
  private boolean              isUngrouped  = false;

  /**
   * Region reference to parent region
   */
  private Region               parentRegion;

  /**
   * Constructs a party with the relevant JSONObject representing the party, including loading the relevant candidates
   * 
   * @param partyObj
   *          JSONObject containing the party information
   * @param region
   *          Region that this party is in
   * @throws JSONException
   */
  public Party(JSONObject partyObj, Region region) throws JSONException {
    super();

    this.preferredName = partyObj.getString("preferredName");
    this.parentRegion = region;
    this.hasBallotBox = partyObj.getBoolean("hasBallotBox");
    this.isUngrouped = partyObj.getBoolean("ungrouped");

    JSONArray cands = null;

    try {
      cands = partyObj.getJSONArray("candidates");
    }
    catch (JSONException e) {
      logger.error("Empty candidate list in {} for {}", this.parentRegion.getName(), this.preferredName, e);
    }

    if (cands != null) {
      for (int i = 0; i < cands.length(); i++) {
        this.candidates.add(new Candidate(cands.getJSONObject(i)));
      }
    }
  }

  /**
   * Gets all the candidates associated with this party
   * 
   * @return ArrayList<Candidate> containing all this parties candidates
   */
  public ArrayList<Candidate> getAllCandidates() {
    return this.candidates;
  }

  /**
   * @return the parentRegion.
   */
  public Region getRegion() {
    return this.parentRegion;
  }

  /**
   * Checks if this party has a ballot box
   * 
   * @return true if it does have a ballot box, false if not
   */
  public boolean hasBallotBox() {
    return this.hasBallotBox;
  }

  /**
   * Checks if this party is ungrouped
   * 
   * @return true if ungrouped and false it not
   */
  public boolean isUngrouped() {
    return this.isUngrouped;
  }

  /**
   * Returns a string representation of this party and its candidates
   */
  @Override
  public String toString() {
    return "Party: " + this.preferredName + ", " + this.candidates;
  }
}
