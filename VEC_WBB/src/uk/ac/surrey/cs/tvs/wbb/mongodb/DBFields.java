/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

/**
 * Singleton for database field names.
 * 
 * @author Chris Culnane
 * 
 */
public class DBFields {

  /**
   * Collections within the database.
   */
  public class Collections {

    public static final String SUBMISSIONS      = "submissions";
    public static final String COMMITS          = "commits";
    public static final String COMMIT_CONFLICTS = "commitConflicts";
    public static final String CIPHERS          = "ciphers";
  }

  /**
   * Singleton instance.
   */
  private static final DBFields instance                             = new DBFields();

  // Useful database strings.

  public static final String    ID                                   = "_id";
  public static final String    LESS_THAN                            = "$lt";
  public static final String    NOT_EQUAL                            = "$ne";
  public static final String    GREATER_THAN_EQUAL_TO                = "$gte";
  public static final String    SET                                  = "$set";
  public static final String    EXISTS                               = "$exists";
  public static final String    OR                                   = "$or";
  public static final String    INCREMENT                            = "$inc";
  public static final String    ADD_TO_SET                           = "$addToSet";
  public static final String    UNSET                                = "$unset";
  public static final String    NOT                                  = "$not";
  public static final String    ELEMENT_MATCH                        = "$elemMatch";
  public static final String    PULL                                 = "$pull";

  // Database fields.

  public static final String    COMMIT_HASH                          = "hash";
  public static final String    CONFLICT_MESSAGE                     = "message";
  public static final String    CURRENT_COMMIT                       = "currentCommit";
  public static final String    COMMITMENT                           = "commitment";
  public static final String    COMMITMENT_SIG_COUNT                 = "commitSigCount";
  public static final String    COMMITMENT_CHECKED_SIGS              = "checkedSigs";
  public static final String    COMMITMENT_ROUND1_SIG                = "mysigR1";
  public static final String    COMMITMENT_COMMIT_SK2_SIG            = "commitment.mySK2Sig";
  public static final String    COMMITMENT_ROUND2_PROCESSED_COUNT    = "commitment.R2ProcessedCount";
  public static final String    COMMITMENT_COMMIT_SENT_SIG           = "commitment.commitSentSig";
  public static final String    COMMITMENT_COMMIT_SIG_COUNT          = "commitment.commitSigCount";
  public static final String    COMMITMENT_COMMIT_MY_ROUND1_SIG      = "commitment.mysigR1";
  public static final String    COMMITMENT_COMMIT_RESPONSE           = "commitment.response";

  public static final String    COMMITMENT_COMMIT_COMMIT_ID          = "commitment.commitID";
  public static final String    COMMITMENT_COMMIT_HASH               = "commitment.hash";
  public static final String    COMMITMENT_COMMIT_DESC               = "commitment.commitDesc";
  public static final String    COMMITMENT_COMMIT_FILE               = "commitment.commitFile";
  public static final String    COMMITMENT_COMMIT_FILE_FULL          = "commitment.commitFileFull";
  public static final String    COMMITMENT_COMMIT_TIME               = "commitment.commitTime";
  public static final String    COMMITMENT_COMMIT_ATTACHMENTS        = "commitment.attachments";
  public static final String    COMMITMENT_COMMIT_DATETIME           = "commitment.dateTime";
  public static final String    COMMITMENT_COMMIT_CURRENT_COMMIT     = "commitment.currentCommit";
  public static final String    COMMITMENT_COMMIT_SIGS_TO_CHECK      = "commitment.sigsToCheck";
  public static final String    COMMITMENT_SIGS_TO_CHECK             = "sigsToCheck";
  public static final String    COMMITMENT_MESSAGE                   = "msg";
  public static final String    COMMITMENT_R2_QUEUE                  = "commitR2Queue";
  public static final String    COMMITMENT_COMMIT_CHECKED_SIGS       = "commitment.checkedSigs";
  public static final String    COMMITMENT_COMMIT_RUNNING_R2         = "commitment.runningR2";
  public static final String    COMMITMENT_COMMIT_R2                 = "CommitR2";
  public static final String    COMMITMENT_COMMIT_R2_COMMITS         = "commitment.R2Commits";
  public static final String    COMMITMENT_COMMIT_R2_PROCESSED_COUNT = "commitment.R2ProcessedCount";
  public static final String    COMMITMENT_TIME                      = "commitTime";
  public static final String    COMMITMENT_DESC                      = "commitDesc";
  public static final String    COMMITMENT_FILE                      = "commitFile";
  public static final String    COMMITMENT_FILE_FULL                 = "commitFileFull";
  public static final String    COMMITMENT_ATTACHMENTS               = "attachments";
  public static final String    COMMITMENT_ID                        = "commitID";
  public static final String    FROM_PEER                            = "fromPeer";

  public static final String    COMMITMENT_COMMIT_HASH_R1            = "commitment.hashR1";
  public static final String    COMMITMENT_COMMIT_FILE_R1            = "commitment.commitFileR1";
  public static final String    COMMITMENT_COMMIT_FILE_FULL_R1       = "commitment.commitFileFullR1";
  public static final String    COMMITMENT_COMMIT_ATTACHMENTS_R1     = "commitment.attachmentsR1";

  public static final String    AUDIT_FAILURE_STATUS                 = "auditFailure";
  public static final String    START_EVM_MSG                        = "startEVMMsg";

  /**
   * Protected constructor to enforce singleton.
   */
  protected DBFields() {
    super();
  }

  /**
   * Converts an enum field into a name.
   * 
   * @param fieldName
   *          The enum field.
   * @return The string version of the field name.
   */
  public String getField(DBFieldName fieldName) {
    switch (fieldName) {
      case CHECKED_SIGS:
        return "checkedSigs";
      case MY_SIGNATURE:
        return "mysig";
      case SENT_SIGNATURE:
        return "sentSig";
      case SIGNATURE_COUNT:
        return "sigCount";
      case SENT_TIMEOUT:
        return "sentTimeout";
      case CLIENT_MESSAGE:
        return "message";
      case SIGNATURES_TO_CHECK:
        return "sigsToCheck";
      case QUEUED_MESSAGE:
        return "msg";
      case FROM_PEER:
        return "fromPeer";
      case COMMIT_TIME:
        return "commitTime";
      case POST_TIMEOUT:
        return "postTimeout";
      default:
        return null;
    }
  }

  /**
   * @return The singleton instance.
   */
  public static DBFields getInstance() {
    return instance;
  }

  /**
   * Gets the correct database fields instance for the record type.
   * 
   * @param type
   *          The record type.
   * @return The singleton instance.
   */
  public static DBFields getInstance(DBRecordType type) {
    switch (type) {
      case POD:
        return DBFieldsPOD.getInstance();
      case CANCEL:
        return DBFieldsCancel.getInstance();
      default:
      case GENERAL:
        return DBFields.getInstance();
      case AUDIT:
        return DBFields.getInstance();
    }
  }
}
