/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

/**
 * Class that holds UI related constants
 * 
 * @author Chris Culnane
 * 
 */
public class UIConstants {

  public class CLICommand {

    public static final String CLI_COMMIT             = "commit";
    public static final String CLI_SHUTDOWN           = "shutdown";
    public static final String CLI_FORCE_CLOSE_COMMIT = "forceCloseCommit";
  }

  public class UIMessage {

    public static final String UI_TYPE = "type";

  }

  public class UIRequest extends UIMessage {

    public static final String CMD_START_LOGGING      = "startLog";
    public static final String CMD_STOP_LOGGING       = "stopLog";
    public static final String VALUE_LEVEL            = "level";
    public static final String CMD_START_MEMORY       = "startmemory";
    public static final String CMD_STOP_MEMORY        = "stopmemory";
    public static final String VALUE_DELAY            = "delay";

    public static final String CMD_START_PEER_STAT    = "startpeerstat";
    public static final String CMD_STOP_PEER_STAT     = "stoppeerstat";
    public static final String CMD_GET_COMMIT_DATA    = "getcommitdata";
    public static final String CMD_GET_NEXT_COMMIT    = "getnextcommit";
    public static final String CMD_DO_EXTRA_COMMIT    = "doextracommit";
    public static final String CMD_FORCE_CLOSE_COMMIT = "forceclosecommit";
    
    public static final String VALUE_COMMIT_TIME            = "commitTime";
    public static final String VALUE_COMMIT_DESC            = "commitDesc";
  }

  public class UIResponse extends UIMessage {

    public static final String UI_TYPE_MEMORY       = "memory";
    public static final String UI_TYPE_COMMIT       = "commit";
    public static final String UI_TYPE_COMMIT_TIME  = "commitTime";
    public static final String UI_TYPE_PEER_STAT    = "peerstat";
    public static final String UI_TYPE_LOGGING      = "logging";
    public static final String UI_VALUE_MEMORY      = "memory";
    public static final String UI_VALUE_MAX_MEMORY  = "maxmemory";
    public static final String UI_VALUE_CHANNELS    = "channels";

    public static final String UI_VALUE_MESSAGE     = "message";
    public static final String UI_VALUE_LEVEL       = "level";
    public static final String UI_VALUE_THREAD      = "thread";
    public static final String UI_VALUE_DATETIME    = "datetime";
    public static final String UI_VALUE_COMMIT_TIME = "commitTime";

  }
}
