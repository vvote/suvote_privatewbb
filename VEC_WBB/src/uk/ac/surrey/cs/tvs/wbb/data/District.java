/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * District class that represents a District in the data structure
 * 
 * @author Chris Culnane
 * 
 */
public class District {

  /**
   * Logger
   */
  private static final Logger  logger     = LoggerFactory.getLogger(District.class);

  /**
   * String of the district name
   */
  private String               districtName;

  /**
   * ArrayList<Candidate> of the candidates in this district
   */
  private ArrayList<Candidate> candidates = new ArrayList<Candidate>();

  /**
   * Reference back to the Region this district is in
   */
  private Region               parentRegion;

  /**
   * int of the number candidates in this district configuration file used in ballot generation
   */
  private int                  candidateCount;

  /**
   * Constructs a new District object from the JSONObject and constructs the relevant candidates in this district
   * 
   * @param districtObj
   *          JSONObject with district data
   * @param region
   *          Region that this district is in
   * @param districtConf
   *          JSONObject containing the district config data (candidate count used in ballot generation)
   * @throws JSONException
   */
  public District(JSONObject districtObj, Region region, JSONObject districtConf) throws JSONException {
    super();

    this.districtName = districtObj.getString("district");
    this.candidateCount = districtConf.getJSONObject(this.districtName).getInt("la");
    this.parentRegion = region;

    JSONArray cands = null;

    try {
      cands = districtObj.getJSONArray("candidates");
    }
    catch (JSONException e) {
      logger.error("Empty candidate list in " + this.districtName);
    }

    if (cands != null) {
      for (int i = 0; i < cands.length(); i++) {
        this.candidates.add(new Candidate(cands.getJSONObject(i)));
      }
    }
  }

  /**
   * Gets the district name
   * 
   * @return String of the district name
   */
  public String getName() {
    return this.districtName;
  }

  /**
   * Gets the region this district is in
   * 
   * @return Region this district is in
   */
  public Region getRegion() {
    return this.parentRegion;
  }

  /**
   * Returns the district name and a list of the candidates
   */
  @Override
  public String toString() {
    return "District: " + this.districtName + ", " + this.candidates;
  }

  /**
   * Validates that the number of candidates listed in the bundle is the same as the number of candidates given in the district
   * config file
   * 
   * @return boolean true if they are the same, false if not
   */
  public boolean validateConsistency() {
    if (this.candidateCount == this.candidates.size()) {
      return true;
    }
    else {
      logger.error("Inconsistent district candidate count:" + this.districtName);
      return false;
    }
  }
}
