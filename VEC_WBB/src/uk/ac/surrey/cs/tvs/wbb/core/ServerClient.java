/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * Used by the WBBPeer to connect to other peers.
 * 
 * This needs to be robust and resilient as possible. The connections set-up by it should be long term connections that if they are
 * closed or fail are automatically re-opened. We also have to handle what happens when another peer goes offline and how that
 * impacts on the performance of other peers who are still trying to send to it.
 * 
 * The timeouts throughout this class use a logarithmic timing scheme. If a connection fails the client waits an initial amount of
 * time. Each time it subsequently fails the wait is doubled up to the maximum wait time. If at any point a connection is successful
 * and a message is sent the wait timeouts are reset to the initial values. This provides a nice way to handle socket failures. If
 * it is an innocuous dropped connection the connection is recreated immediately. However, if it is more serious the time it will
 * wait will gradually get longer, up to the maximum, so resources are not diverted to constantly trying to recreate a failing
 * connection.
 * 
 * The two different wait times are to provide better handling of failures. Socket exceptions/closures happen quite regularly. They
 * generally are rectified after a reconnect. However, an SSL failure indicates something quite different. Either the certificate is
 * no longer valid or the messages during the handshake failed. This is unlikely to be fixed quickly. As such, the max wait time on
 * a SSL failure should be quite large.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class ServerClient {

  /**
   * Telemetry Logger
   */
  private static final Logger         telemetry             = LoggerFactory.getLogger("Telemetry");
  /**
   * Logger
   */
  private static final Logger         logger                = LoggerFactory.getLogger(ServerClient.class);

  /**
   * The address to connect to (IP Address)
   */
  private String                      address;

  /**
   * The port number to connect to
   */
  private int                         port;

  /**
   * Set to true to gracefully shutdown
   */
  private boolean                     shutdown              = false;

  /**
   * A blockingQueue that holds messages that need to be sent. This allows other processes to add a message for sending without
   * waiting for it to be sent.
   */
  private LinkedBlockingQueue<String> writeQueue            = new LinkedBlockingQueue<String>();

  /**
   * Reference to the SSLSocket factory to create a new sockets
   */
  private SSLSocketFactory            factory;

  /**
   * The peer this is running on
   */
  private WBBPeer                     peer;

  /**
   * Local instance of the socket failure timeout (wait)
   */
  private int                         socketFailTimeout;

  /**
   * Local instance of the maximum wait for a socket failure
   */
  private int                         socketFailTimeoutMax;

  /**
   * Set to true to gracefully close after sending the message. Used during FileTransfers in round 2 of the commit
   */
  private boolean                     closeAfterSend        = false;

  /**
   * Do we want to wait for a timeout?
   */
  private int                         waitForMessageTimeout = -1;

  /**
   * How many retries should be executed?
   */
  private int                         maxRetries            = -1;

  /**
   * Size of buffer used in copying data between streams.
   */
  private static final int            BUFFER_SIZE           = 1024;

  /**
   * Boolean value that when set to true tells the server client to discard messages if we have reached the maximum timeout. This
   * prevents the write queue from constantly growing when a peer has gone offline. We have this as an option so that a ServerClient
   * created for Commit can try forever, since the frequency of those messages is lower and it is important they are received
   */
  private boolean                     discardOnTimeout      = true;

  /**
   * Constructor to create a ServerClient to connect to the passed address
   * 
   * @param address
   *          address of the target server
   * @param port
   *          port number of the target server
   * @param factory
   *          the SSL factory to use to create the SSL Socket
   * @param peer
   *          the WBBPeer this is being run by
   */
  public ServerClient(String address, int port, SSLSocketFactory factory, WBBPeer peer) {
    super();

    this.factory = factory;
    this.address = address;
    this.port = port;
    this.peer = peer;
    // Initialise the local timeout/wait variables
    this.socketFailTimeout = peer.getConfig().getSocketFailTimeoutInitial();
    this.socketFailTimeoutMax = peer.getConfig().getSocketFailTimeoutMax();
  }

  /**
   * Connect to the target server and start processing the WriteQueue
   */
  public void connect() {
    SSLSocket sock = null;
    InetSocketAddress isa = new InetSocketAddress(this.address, this.port);

    logger.info("About to connect to {}", isa);

    String tempMessage = "";

    boolean shouldRetry = false;
    int retryCount = 0;

    while (!this.shutdown) {
      try {
        // Create new SSLSocket
        sock = (SSLSocket) this.factory.createSocket();

        sock.setEnabledCipherSuites(new String[] { this.peer.getConfig().getString(WBBConfig.SSL_CONFIG) });

        // We want to use ClientAuthentication (mutual authentication)
        sock.setUseClientMode(true);

        // Connect to the address
        sock.connect(isa);
        logger.info("Connected to {}", sock);
        
        // Verify the target of the connection, close socket if verification fails
        if (this.peer.verifyConnection(isa, sock.getSession().getPeerPrincipal())) {
          logger.info("Connection verified");
          telemetry.info("PEER2PEER#{}", sock);
          // We now loop through the message queue trying to send the messages
          while (!this.shutdown) {
            // Check if the last write was successful. If it was then we can get the next message. If not we try again. Note that
            // because a WriteQueue is unique to an instance of ServerClient it doesn't matter that all other messages are blocked
            // until this has been sent. If the message is too long and would cause the receiver to close the connection, thus
            // causing an error at this end, eventually the message will timeout and will be abandoned.
            boolean cancelSend = false;
            if (!shouldRetry) {
              shouldRetry = true;
              retryCount = 0;

              // Remove the next message from the queue or wait until a message is available
              // if Timeoutset only wait for that long
              if (this.waitForMessageTimeout >= 0) {
                tempMessage = this.writeQueue.poll(this.waitForMessageTimeout, TimeUnit.MILLISECONDS);
                if (tempMessage == null) {
                  cancelSend = true;
                  shouldRetry = false;
                }
              }
              else {
                tempMessage = this.writeQueue.take();
              }

              // this.writeQueue.
              logger.info("Taken \"{}\" from the write queue", tempMessage);
            }
            else {
              retryCount++;
            }

            JSONWBBMessage jwm = null;
            if (tempMessage != null) {
              try {
                // Parse the message (we need to find out what type of message it is so we can check if we have already timed out on
                // a particular message. If it has cancel the sending of the message and move onto the next message
                jwm = JSONWBBMessage.parseMessage(tempMessage);
              }
              catch (MessageJSONException e) {
                logger.warn("Exception whilst checking for response channels for {}, message will not be sent", tempMessage, e);
                // If we can't parse the message ourselves the other server definitely won't be able to, so cancel the send.
                // SuccessfulWrite is set to true to force the next message to be read
                cancelSend = true;
                shouldRetry = false;
              }

              // If we have a JSONWBBMessage check if we should cancel
              if (!cancelSend && this.shouldCancelSend(jwm)) {
                shouldRetry = false;
                cancelSend = true;
              }
            }

            // If we still want to send the message
            if (!cancelSend) {
              logger.info("About to write \"{}\" to outputstream", tempMessage);

              // Check if it is not null and if it Peer File Message, which needs different handling
              if (jwm != null && jwm.getType() == Message.PEER_FILE_MESSAGE) {
                this.sendFiles(jwm, sock);
              }
              else {
                // This is standard message, write to the stream/ along with a newline character
                sock.getOutputStream().write(tempMessage.getBytes());
                sock.getOutputStream().write("\n".getBytes());
              }

              // We have successfully written the file. Reset any timeouts and prepare to send the next message
              shouldRetry = false;
              logger.info("Successfully written \"{}\" to outputstream", tempMessage);
              this.socketFailTimeout = this.peer.getConfig().getSocketFailTimeoutInitial();

            }

            // Check if we have exceed our max retries
            if (this.maxRetries >= 0 && shouldRetry && retryCount > this.maxRetries) {
              shouldRetry = false;
            }

            if (!shouldRetry && this.closeAfterSend) {
              this.shutdown = true;
            }
          }
        }
        else {
          // SSL verification failed - this is our own verification as opposed to an SSL exception
          logger.warn("SSL Connection Failed, will retry in {} seconds", this.socketFailTimeout / 1000);
          telemetry.info("CONNFAIL#SSL Connection failed:{}", sock);
          sock.close();
          this.logarithmicTimeout();
        }
      }
      catch (IOException e) {
        logger.warn("Exception when connection to host {}. Will try and reconnect in {} seconds",
            this.getSocketRemoteAddress(sock), this.socketFailTimeout / 1000, e);
        this.logarithmicTimeout();
        telemetry.info("CONNFAIL#{},IOException:{}", sock,e.getMessage());
      }
      catch (InterruptedException e) {
        logger.error("Interrupted whilst waiting for data - source is writeQueue", e);
      }
    }

    // Close the socket after shutdown
    try {
      if (sock != null) {
        sock.close();
      }
    }
    catch (IOException e) {
      logger.warn("IOException thrown whilst closing socket, can probably ignore", e);
    }
  }

  /**
   * @return The IP address to connect to.
   */
  public String getAddress() {
    return this.address;
  }

  /**
   * @return The port to connect to.
   */
  public int getPort() {
    return this.port;
  }

  /**
   * Utility method to get socket address as a String for Logging.
   * 
   * This will only be called if the logger is called - that only happens if the logging message is at a level greater than the
   * logger. By having it as a separate method we can perform the null check only when required, otherwise we would perform it every
   * time, even if the logger level exceeded the log call.
   * 
   * @param sock
   *          The socket address.
   * @return The string version of the socket.
   */
  private String getSocketRemoteAddress(SSLSocket sock) {
    if (sock == null) {
      return "Socket Is Null";
    }
    else {
      if (sock.getRemoteSocketAddress() != null) {
        return sock.getRemoteSocketAddress().toString();
      }
      else {
        return "Remote Socket Address Is Null";
      }
    }
  }

  /**
   * Waits for a response using a logarithmic timeout.
   */
  private void logarithmicTimeout() {
    try {
      Thread.sleep(this.socketFailTimeout);
      // Wait the required amount of time, then calculate the new logarithmic wait in case it fails again
      this.socketFailTimeout = 2 * this.socketFailTimeout;
      if (this.socketFailTimeout > this.socketFailTimeoutMax) {
        this.socketFailTimeout = this.socketFailTimeoutMax;
        // Check if we should start discarding messages
        if (this.discardOnTimeout) {
          this.writeQueue.clear();
          logger.warn("Have reached maximum timeout, have emptied the writeQueue");
        }
      }
    }
    catch (InterruptedException e1) {
      logger.warn("Interrupted whilst waiting to retry connection", e1);
    }
  }

  /**
   * Copies the data from the input message stream to the output.
   * 
   * @param bis
   *          The input stream.
   * @param bos
   *          The output stream.
   * @param jwm
   *          The message being sent.
   */
  private void sendDataFromInToOut(BufferedInputStream bis, BufferedOutputStream bos, JSONWBBMessage jwm) {
    byte[] buf = new byte[ServerClient.BUFFER_SIZE];
    int read = 0;

    try {
      // Send the first file - all PEER FILE MESSAGES send at least one file
      while ((read = bis.read(buf)) != -1) {
        bos.write(buf, 0, read);
      }
    }
    catch (IOException eio) {
      logger.warn("Exception whilst reading file to send to another peer. Message was {}", jwm.getMsg().toString(), eio);
    }
    finally {
      try {
        bis.close();
      }
      catch (IOException e) {
        logger
            .warn("Exception whilst closing file that was being sent to another peer. Message was {}", jwm.getMsg().toString(), e);
      }
    }
  }

  /**
   * Sends the files associated with a message.
   * 
   * @param jwm
   *          The message.
   * @param sock
   *          The socket to send the data on.
   * @throws IOException
   */
  private void sendFiles(JSONWBBMessage jwm, SSLSocket sock) throws IOException {
    try {
      // We are sending a file so need to find the file to send
      String fileName = jwm.getMsg().getString(MessageFields.PeerFile.FILENAME);
      String attachmentFileName = null;

      // If this is a peer send message we may have a second file to send, if we do get the file name
      if (jwm.getMsg().has(MessageFields.PeerFile.ATTACHMENT_FILENAME)) {
        attachmentFileName = jwm.getMsg().getString(MessageFields.PeerFile.ATTACHMENT_FILENAME);
      }

      // Remove the file names from the JSON - not essential but no point sending them, they won't be used by the
      // receiver
      jwm.getMsg().remove(MessageFields.PeerFile.FILENAME);
      jwm.getMsg().remove(MessageFields.PeerFile.ATTACHMENT_FILENAME);

      // Write the message and a new line to the file
      sock.getOutputStream().write(jwm.getMsg().toString().getBytes());
      sock.getOutputStream().write("\n".getBytes());

      // Prepare some buffered streams for now writing the files we have mentioned in the previously sent message
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName));
      BufferedOutputStream bos = new BufferedOutputStream(sock.getOutputStream());
      this.sendDataFromInToOut(bis, bos, jwm);

      // If there is an additional attachment send that as well
      if (attachmentFileName != null) {
        bis = new BufferedInputStream(new FileInputStream(attachmentFileName));
        this.sendDataFromInToOut(bis, bos, jwm);
      }
      bos.flush();

    }
    catch (JSONException e) {
      logger.warn("Exception whilst processing JSON", e);
      // We catch this exception here because it is not a network exception, as such we don't want to retry
    }
  }

  /**
   * Set to true to close after sending a message.
   * 
   * Call this after creating a ServerClient for sending a file, but before writeMessage
   * 
   * @param closeAfterSend
   *          true to close after send, false to remain open. default is false
   */
  public void setCloseAfterSend(boolean closeAfterSend) {
    this.closeAfterSend = closeAfterSend;
  }

  /**
   * Sets the number of retries.
   * 
   * @param maxRetries
   *          The new number of retries.
   */
  public void setMaxRetries(int maxRetries) {
    this.maxRetries = maxRetries;
  }

  /**
   * Sets the message timeout.
   * 
   * @param timeout
   *          The new timeout.
   */
  public void setWaitForMessageTimeout(int timeout) {
    this.waitForMessageTimeout = timeout;
  }

  /**
   * Determines if the send should be cancelled.
   * 
   * @param jwm
   *          The message being sent.
   * @return True if the send should be cancelled.
   */
  private boolean shouldCancelSend(JSONWBBMessage jwm) {
    boolean timeoutSent;

    if (jwm.getType() == Message.PEER_POD_MESSAGE) {
      timeoutSent = this.peer.getDB().timeoutSent(DBRecordType.POD, jwm.getID());
    }
    else {
      timeoutSent = this.peer.getDB().timeoutSent(DBRecordType.GENERAL, jwm.getID());
    }
    if (timeoutSent) {
      logger.warn("Not sending message {}, have sent timeout", jwm);
      return true;
    }

    return false;
  }

  /**
   * Sets the shutdown flag to true. This does not force a shutdown (it doesn't interrupt the thread), however it will cause it to
   * shutdown the next time it runs.
   */
  public void shutdown() {
    this.shutdown = true;
  }

  /**
   * Write a message to the target server
   * 
   * The message will be queue until the socket is available for sending. Messages will be sent out in FIFO order
   * 
   * @param msg
   *          the message to send (JSON String, no newlines)
   */
  public void writeMessage(String msg) {
    this.writeQueue.offer(msg);
  }

  /**
   * Sets the discardOnTimeout property of this ServerClient. If set to true when a maxTimeout occurs the writeQueue is emptied to
   * prevent it growing forever
   * 
   * @param discardOnTimeout
   *          boolean set to true (default) to discard when connection has not been successful for max timeout, false to retry until
   *          shutdown
   */
  public void setDiscardOnTimeout(boolean discardOnTimeout) {
    logger.info("Set discardOnTimeout on ServerClient to {}", discardOnTimeout);
    this.discardOnTimeout = discardOnTimeout;
  }
}
