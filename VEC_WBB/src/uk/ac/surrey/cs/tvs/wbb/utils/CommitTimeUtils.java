/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import java.util.concurrent.TimeUnit;

/**
 * TimerUtils provides some utility methods for calculating the CommitTime.
 * 
 * The commitID/commitTime is a string that represents the time that the commit period started. This time will always be the number
 * of milliseconds past midnight, plus the milliseconds for a day since the epoch. As such, it is guaranteed to be unique across
 * days.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class CommitTimeUtils {

  /**
   * Variable to hold the calculated number of milliseconds, from midnight, that a time occurs at
   */
  private final long totalMinutesMS;

  /**
   * Constructor for TimeUtils takes hours and minutes of the desired commitTime as arguments and calculates the totalNumber of
   * milliseconds past midnight that the time occurs
   * 
   * @param hours
   *          past midnight
   * @param minutes
   *          minutes past the hour
   */
  public CommitTimeUtils(int hours, int minutes) {
    super();

    this.totalMinutesMS = TimeUnit.MINUTES.toMillis((hours * 60) + minutes);
  }

  /**
   * Calculates the CommitID given the current time and based on hours and minutes of the desired commit time in the constructor.
   * 
   * @return a String that represents which commit a message received at this moment in time should be included in
   */
  public String getCommitTime() {
    // Get current time
    long time = System.currentTimeMillis();

    // Calculate the number of days that time represents
    long days = TimeUnit.MILLISECONDS.toDays(time);

    // Find how many milliseconds are left having removed those from complete days. This gives us the milliseconds since midnight
    // today
    long msLeft = time - TimeUnit.DAYS.toMillis(days);

    // Check if the milliseconds since midnight today is greater than the set value in the constructor.
    if (msLeft > this.totalMinutesMS) {
      // If it is greater it should go into tomorrow's commit - which is represented by starting time that commit period covers
      return String.valueOf(TimeUnit.DAYS.toMillis(days) + this.totalMinutesMS);
    }
    else {
      // If it is less it means it can go in the active commit, which must have started yesterday.
      return String.valueOf(TimeUnit.DAYS.toMillis(days - 1) + this.totalMinutesMS);
    }
  }

  /**
   * Gets the previousCommitID.
   * 
   * This is useful for when we want to perform a commit, we know the current commit is still active, so we get the previous one by
   * calculating the value minus one day.
   * 
   * Note: it would be possible to calculate the commitTime for anytime in history and thus the commitTime is reversible to
   * calculate the actual day it is related to.
   * 
   * @return a string representing the previous commitTime
   */
  public String getPreviousCommitID() {
    long time = System.currentTimeMillis();
    long days = TimeUnit.MILLISECONDS.toDays(time);
    long msLeft = time - TimeUnit.DAYS.toMillis(days);

    // It is either yesterday's, if we have passed the commit point today, or two days ago if we are still active
    if (msLeft > this.totalMinutesMS) {
      return String.valueOf(TimeUnit.DAYS.toMillis(days - 1) + this.totalMinutesMS);
    }
    else {
      return String.valueOf(TimeUnit.DAYS.toMillis(days - 2) + this.totalMinutesMS);
    }
  }

  /**
   * Calculates the number of milliseconds until a given hour and minute past midnight
   * 
   * If the time has already passed today it will give the number of milliseconds until that time tomorrow
   * 
   * @param hours
   *          hours past midnight
   * @param minutes
   *          minutes past the hour
   * @return long milliseconds until the specified time
   */
  public static long getMillisecondsTillTime(int hours, int minutes) {
    long time = System.currentTimeMillis();
    long days = TimeUnit.MILLISECONDS.toDays(time);
    long delay = (TimeUnit.DAYS.toMillis(days) + TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(minutes)) - time;

    // If it is below zero it indicates we have already passed that time today, so return the time for tomorrow
    if (delay < 0) {
      return (TimeUnit.DAYS.toMillis(days + 1) + TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(minutes)) - time;
    }
    else {
      return delay;
    }
  }
}
