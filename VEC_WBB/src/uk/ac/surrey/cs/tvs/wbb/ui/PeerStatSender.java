/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIMessage;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIResponse;

/**
 * Similar to the StatSender, except it demonstrates sending Peer specific information.
 * 
 * This example sends the current number of responseChannels that are being held by the Peer. If this number starts to increase it
 * indicates client messages are not being responded to. This could be because of no consensus being reached or this peer failing to
 * process the messages.
 * 
 * TODO Review - we should record all statistics available to help with integration and trouble shooting when we go live.
 * 
 * @author Chris Culnane
 * 
 */
public class PeerStatSender extends MsgSender {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(PeerStatSender.class);

  /**
   * WBBPeer this is running on and related to
   */
  private WBBPeer             peer;

  /**
   * Construct PeerStatSender with WebSocket and delay
   * 
   * @param ws
   *          WebSocket to write messages to
   * @param delay
   *          milliseconds between updates
   * @param peer
   *          WBBPeer this relates to
   */
  public PeerStatSender(WebSocket ws, long delay, WBBPeer peer) {
    super(ws, delay);

    this.peer = peer;
    logger.info("Created new Peer Stat Sender");
  }

  /**
   * Continuously send statistics to the required web socket.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      while (!this.shutdown) {
        JSONObject msg = new JSONObject();
        msg.put(UIMessage.UI_TYPE, UIResponse.UI_TYPE_PEER_STAT);
        msg.put(UIResponse.UI_VALUE_CHANNELS, this.peer.getResponseChannelCount());
        this.ws.send(msg.toString());
        Thread.sleep(this.delay);
      }
    }
    catch (JSONException | InterruptedException e) {
      logger.error("Exception whilst sending peer stats, will shutdown peerStatSender", e);
    }
  }
}
