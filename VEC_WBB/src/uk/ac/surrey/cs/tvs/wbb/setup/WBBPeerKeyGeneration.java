/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.setup;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.KeyGeneration;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Utility class for generating keys for use with the WBB
 * 
 * @author Chris Culnane
 * 
 */
public class WBBPeerKeyGeneration extends KeyGeneration {

  /**
   * Constant for the location of the default base keystore - this is the keystore with the root certificates in
   */
  private static final String BASE_KEYSTORE_PATH = "./defaults/basekeystore.jks";

  /**
   * Logger
   */
  private static final Logger logger             = LoggerFactory.getLogger(WBBPeerKeyGeneration.class);

  /**
   * Suffix for SK1 keys
   */
  private static final String SK1_SUFFIX         = "_SigningSK1";



  /**
   * Suffix for SSL keys
   */
  private static final String SSL_SUFFIX         = "_SSL";

  /**
   * Construct a new WBBPeerKeyGeneration object with the specified ID.
   * 
   * @param peerID
   *          id of this peer
   */
  public WBBPeerKeyGeneration(String peerID) {
    super(peerID);
  }

  /**
   * Generates SK1, and SSL keys and the corresponding CSRs for a peer - SK2 (BLS) keys are generated via Ximix
   * 
   * @param csrFolder
   *          String path to folder to save CSRs to
   * @param keyStorePath
   *          String path to KeyStore location to store keys
   * @throws CertificateRequestGenException
   * @throws IOException 
   */
  public void generateKeyAndCSR(String csrFolder, String keyStorePath) throws CertificateRequestGenException, IOException {
    try {
      logger.info("Key Generation called, output to {} and {}", csrFolder, keyStorePath);

      KeyStore ks = CryptoUtils.loadKeyStore(BASE_KEYSTORE_PATH);
      File csrOutput = new File(csrFolder);
      IOUtils.checkAndMakeDirs(csrOutput);
      

      logger.info("Generation SK1 key");
      this.generateKey(SK1_SUFFIX, new File(csrOutput, this.id + SK1_SUFFIX + ".csr"), ks, SignatureType.DEFAULT);
      //logger.info("Generation SK2 key");
      //this.generateKey(SK2_SUFFIX, new File(csrOutput, this.id + SK2_SUFFIX + ".csr"), ks, SignatureType.DEFAULT);
      logger.info("Generation SSL key");
      this.generateKey(SSL_SUFFIX, new File(csrOutput, this.id + SSL_SUFFIX + ".csr"), ks, SignatureType.RSA);

      File keyStoreFile = new File(keyStorePath);
      IOUtils.checkAndMakeDirs(keyStoreFile.getParentFile());
      
      CryptoUtils.storeKeyStore(ks, keyStorePath);
      logger.info("Finished key generation");
    }
    catch (CryptoIOException e) {
      throw new CertificateRequestGenException("Exception whilst generating key and CSR", e);
    }
    catch (KeyGenerationException e) {
      throw new CertificateRequestGenException("Exception whilst generating key and CSR", e);
    }
    catch (TVSSignatureException e) {
      throw new CertificateRequestGenException("Exception whilst generating key and CSR", e);
    }
  }

  /**
   * Main method for running key generation and importing certificates.
   * 
   * @param args
   *          String array of program arguments
   */
  public static void main(String[] args) {
    CryptoUtils.initProvider();
    if (args.length < 4) {
      printUsage();
      return;
    }

    try {
      if (args[0].equals("generate") && args.length == 4) {
        WBBPeerKeyGeneration kg = new WBBPeerKeyGeneration(args[1]);
        kg.generateKeyAndCSR(args[2], args[3]);
      }
      else if (args[0].equals("import") && args.length == 5) {
        WBBPeerKeyGeneration kg = new WBBPeerKeyGeneration(args[1]);
        kg.importRelevantCertsFromDir(args[2], new File(args[3]), args[4]);
      }
      else {
        printUsage();
      }
    }
    catch (CertificateRequestGenException | CertificateStoringException | IOException e) {
      logger.error("Exception whilst running key generation", e);
    }
  }

  /**
   * Prints the usage instructions to the System.out
   */
  public static void printUsage() {
    System.out.println("Usage of WBBPeerKeyGeneration:");
    System.out.println("WBBPeerKeyGeneration is used to generate keys, CSRs and");
    System.out.println("import the relevant certificates.");
    System.out.println("Usage:");
    System.out.println("WBBPeerKeyGeneration action [options]");
    System.out.println("WBBPeerKeyGeneration generate entity csrFolder keyStorepath");
    System.out.println("WBBPeerKeyGeneration import entity keyStorepath CertFolder caFilePath");
  }
}
