/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Region object containing the region and district data for an election bundle
 * 
 * @author Chris Culnane
 * 
 */
public class Region {

  /**
   * Logger
   */
  private static final Logger       logger           = LoggerFactory.getLogger(Region.class);

  /**
   * HashMap of districts indexed by district name
   */
  private HashMap<String, District> districts        = new HashMap<String, District>();

  /**
   * ArrayList<Party> containing the ATL parties
   */
  private ArrayList<Party>          atlParties       = new ArrayList<Party>();

  /**
   * ArrayList<Party> of ungrouped parties that exist but aren't in the ATL - currently we don't divide the two
   */
  private ArrayList<Party>          ungroupedParties = new ArrayList<Party>();

  /**
   * ArrayList<Candidate> in this region
   */
  private ArrayList<Candidate>      regionCandidates = new ArrayList<Candidate>();

  /**
   * String of the region name
   */
  private String                    regionName;

  /**
   * int counter of ATL candidate counts
   */
  private int                       atlCandidateCount;

  /**
   * int counter of BTL candidates
   */
  private int                       btlCandidateCount;

  /**
   * Constructs a new Region object, constructing necessary district, parties and candidates
   * 
   * @param region
   *          JSONObject containing the region based data
   * @param districtMap
   *          HashMap<String,District> to add newly constructed districts to
   * @param districtConf
   *          JSONObject with district configuration that was used during ballot generation
   * @throws CandidateCountException
   */
  public Region(JSONObject region, HashMap<String, District> districtMap, JSONObject districtConf) throws CandidateCountException {
    super();

    try {
      this.regionName = region.getString("region");

      JSONArray partiesArr = region.getJSONArray("parties");

      for (int i = 0; i < partiesArr.length(); i++) {
        Party party = new Party(partiesArr.getJSONObject(i), this);

        this.atlParties.add(party);
        this.regionCandidates.addAll(party.getAllCandidates());
      }

      JSONArray districtsArr = region.getJSONArray("districts");
      int atlCount = -1;
      int btlCount = -1;

      for (int i = 0; i < districtsArr.length(); i++) {
        District district = new District(districtsArr.getJSONObject(i), this, districtConf);

        this.districts.put(district.getName(), district);
        districtMap.put(district.getName(), district);

        int tempATLCount = districtConf.getJSONObject(district.getName()).getInt("lc_atl");
        int tempBTLCount = districtConf.getJSONObject(district.getName()).getInt("lc_btl");

        if (atlCount == -1) {
          atlCount = tempATLCount;
        }
        else if (atlCount != tempATLCount) {
          throw new CandidateCountException("Inconsistent ATL candidate counts for region:" + this.regionName);
        }

        if (btlCount == -1) {
          btlCount = tempBTLCount;
        }
        else if (btlCount != tempBTLCount) {
          throw new CandidateCountException("Inconsistent BTL candidate counts for region:" + this.regionName);
        }
      }

      this.atlCandidateCount = atlCount;
      this.btlCandidateCount = btlCount;
    }
    catch (JSONException e) {
      e.printStackTrace();
    }
  }

  /**
   * String get the name of this region
   * 
   * @return String containing the region name
   */
  public String getName() {
    return this.regionName;
  }

  /**
   * Returns a string containing the region name, parties and districts
   */
  @Override
  public String toString() {
    return "Region: " + this.regionName + " Grouped Parties: " + this.atlParties + " Ungrouped Parties: " + this.ungroupedParties
        + " Districts: " + this.districts;
  }

  /**
   * Validates the consistency of the bundle data and the district configuration data used during ballot generation
   * 
   * @return boolean true if consistent, false if not
   */
  public boolean validateConsistency() {
    boolean districtCheck = true;
    if (this.atlCandidateCount == this.atlParties.size() && this.btlCandidateCount == this.regionCandidates.size()) {
      for (District d : this.districts.values()) {
        if (!d.validateConsistency()) {
          districtCheck = false;
        }
      }

      return districtCheck;
    }
    else {
      logger.error("Inconsistent region party count: {} != {}", this.atlCandidateCount, this.atlParties.size());
      return false;
    }
  }
}
