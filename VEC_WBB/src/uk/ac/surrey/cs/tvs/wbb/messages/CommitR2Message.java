/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.CommitR2;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.CommitProcessor;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.R2AlreadyRunningException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.messages.types.CommitFiles;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a File message sent between peers.
 * 
 * This covers the message sent during round 2 of the commit protocol. Namely, that exchanging of the complete databases from each
 * peer.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class CommitR2Message extends PeerMessage {

  /**
   * Logger
   */
  private static final Logger logger          = LoggerFactory.getLogger(CommitR2Message.class);

  /**
   * Filename of the underlying file this message covers
   */
  private String              fileName;

  /**
   * Signable content of this message
   */
  private String              signableContent = null;

  /**
   * locally calculated digest of the data in the file represented by fileName
   */
  private String              digest;

  /**
   * Size of the file, used for sending and receiving
   */
  private long                fileSize;

  /**
   * Instance variable for storing who this message is from
   */
  private String              fromPeer        = null;

  /**
   * Constructor to create PeerFileMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public CommitR2Message(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create PeerFileMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public CommitR2Message(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return null;
  }

  /**
   * Gets the filename of the underlying file
   * 
   * @return string of the fileName
   */
  public String getFilename() {
    return this.fileName;
  }

  /**
   * Gets the fileSize of the underlying file
   * 
   * @return filesize
   */
  public long getFileSize() {
    return this.fileSize;
  }

  /**
   * Returns the _fromPeer property
   * 
   * Checks if the property has already been loaded from the JSON. If it has it returns it, otherwise it attempts to load it.
   * 
   * @return _fromPeer value
   */
  @Override
  public String getFromPeer() {
    if (this.fromPeer == null) {
      if (this.msg.has(MessageFields.FROM_PEER)) {
        try {
          this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
        }
        catch (JSONException e) {
          logger.error("Trying to get _fromPeer, has thrown error", e);
        }
      }
    }

    return this.fromPeer;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.signableContent;
  }

  /**
   * This process the Commit R2 messages in a record. It is worth noting that this could be POD, StartEVM, Audit and Cancel all in one record
   * 
   * @param peer
   *          The WBB peer.
   * @param record
   *          The commit record.
   * @param sentFromPeer
   *          The sending peer ID.
   * @throws MessageJSONException
   * @throws JSONException
   * @throws CommitProcessingException
   */
  private void importCommitR2Messages(WBBPeer peer, JSONObject record, String sentFromPeer) throws MessageJSONException,
      JSONException, CommitProcessingException {
    if (!this.msg.has(MessageFields.PeerFile.SAFE_UPLOAD_DIR)) {
      throw new MessageJSONException("Missing safeUploadDir, indicates failed upload");
    }

    List<JSONWBBMessage> msgs = JSONWBBMessage.loadCommitMessages(peer, record);

    for (JSONWBBMessage msgRecord : msgs) {
      msgRecord.processAsPartOfCommitR2(peer, record, sentFromPeer, this.msg.getString(MessageFields.PeerFile.SAFE_UPLOAD_DIR));
    }
  }

  /**
   * Init the message
   * 
   * Sets the type, extracts the id from commitID, extracts the fileSize and checks whether the underlying message has a _fileName
   * attribute. Having mentioned above that a message does not have a _fileName it may seem odd to load one from the JSON during
   * initialisation. However, this handles loading a message from our own local database when we perform the commit. Note that a
   * message will have been rejected if it was received with a _fileName attribute
   * 
   * Also loads any locally calculated digests and _fromPeer values
   * 
   * @throws JSONException
   */
  @Override
  protected void init() throws JSONException {
    this.type = Message.PEER_FILE_MESSAGE;
    this.id = this.msg.getString(MessageFields.CommitR2.COMMIT_ID);
    this.fileSize = this.msg.getInt(MessageFields.CommitR2.FILESIZE);

    if (this.msg.has(MessageFields.CommitR2.INTERNAL_FILENAME)) {
      this.fileName = this.msg.getString(MessageFields.CommitR2.INTERNAL_FILENAME);
    }

    if (this.msg.has(MessageFields.CommitR2.INTERNAL_DIGEST)) {
      this.updateDigest(this.msg.getString(MessageFields.CommitR2.INTERNAL_DIGEST));
    }

    if (this.msg.has(MessageFields.FROM_PEER)) {
      this.fromPeer = this.msg.getString(MessageFields.FROM_PEER);
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Perform JSON Schema validation
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.PEERFILE))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Validate the sender signature with the submitted digest
      if (!this.validateSenderSignature(peer)) {
        logger.warn("Sender signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Sender signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }
  }

  /**
   * Processes a peer message.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {

    // Check to see if we have started round 2 ourselves.
    if (!peer.getDB().isCommitInR2()) {
      logger.info("Have received an R2 commit before creating a commit ourselves");
      try {
        peer.getDB().queueR2Commit(this);
        return false;
      }
      catch (R2AlreadyRunningException re) {
        logger.info("Tried to queue R2 message, but R2 now running");
      }
      catch (AlreadyReceivedMessageException e) {
        logger.info("R2 message has already been queued, will ignore.");
        return false;
      }
    }

    // Read in the data from the file.
    BoundedBufferedInputStream bbis = null;

    try {
      // we make two passes of the file. This is to extract Ballot Gen message in the first pass. We need to add these first to
      // allow the verification checks for POD to complete successfully. Longer term we will look at sorting the full data file in
      // the same way we sort the R1 commit, that would remove this problem, but will require a file based sort. That in itself will
      // be something we will want to add in the longer term, to be able to handle huge commits that are larger than the available
      // memory
      bbis = new BoundedBufferedInputStream(new FileInputStream(peer.getCommitUploadDir().getAbsolutePath() + "/"
          + this.getFilename()), peer.getConfig().getInt(WBBConfig.BUFFER_BOUND));
      String line;

      while ((line = bbis.readLine()) != null) {
        JSONObject record = new JSONObject(line);
//        if (record.has(MessageFields.JSONWBBMessage.TYPE)) {
//          String recordType = record.getString(MessageFields.JSONWBBMessage.TYPE);
//          if (recordType.equals(JSONWBBMessage.getTypeString(Message.BALLOT_GEN_COMMIT))
//              || recordType.equals(JSONWBBMessage.getTypeString(Message.BALLOT_AUDIT_COMMIT))) {
            record.put(CommitR2.FROM_COMMIT, true);
            this.importCommitR2Messages(peer, record, this.msg.getString(CommitR2.PEER_ID));
//          }
//        }
//      }
//      // Close and reopen the file
//      bbis.close();
//
//      bbis = new BoundedBufferedInputStream(new FileInputStream(peer.getCommitUploadDir().getAbsolutePath() + "/"
//          + this.getFilename()), peer.getConfig().getInt(WBBConfig.BUFFER_BOUND));
//      
//      while ((line = bbis.readLine()) != null) {
//        JSONObject record = new JSONObject(line);
//        if (record.has(MessageFields.JSONWBBMessage.TYPE)) {
//          String recordType = record.getString(MessageFields.JSONWBBMessage.TYPE);
//          if (!recordType.equals(JSONWBBMessage.getTypeString(Message.BALLOT_GEN_COMMIT))
//              && !recordType.equals(JSONWBBMessage.getTypeString(Message.BALLOT_AUDIT_COMMIT))) {
//            record.put(CommitR2.FROM_COMMIT, true);
//            this.importCommitR2Messages(peer, record, this.msg.getString(CommitR2.PEER_ID));
//          }
//        }
      }

      try {
        peer.getDB().submitIncomingPeerCommitR2Checked(this);
      }
      catch (AlreadyReceivedMessageException e) {
        logger.warn("Already received Commit R2 message, will ignore");
        return false;
      }

      // Do we have a threshold of responses?
      if (peer.getDB().checkCommitR2Threshold(peer.getPeerCount())) {
        HashMap<CommitFiles, File> files = CommitProcessor.createCommitFiles(peer);

        byte[] digest = CommitProcessor.prepareCommitFiles(peer, peer.getDB().getCurrentCommitField(DBFields.COMMITMENT_TIME),
            files);

        peer.getDB().updateCurrentCommitRecord(files.get(CommitFiles.COMMIT_FILE).getName(),
            files.get(CommitFiles.COMMIT_FILE_FULL).getName(), files.get(CommitFiles.COMMIT_FILE_FULL_ZIP).getName(),
            IOUtils.encodeData(EncodingType.BASE64, digest));

        CommitProcessor.sendAndStoreCompletedCommitSK2(peer);
      }
    }
    catch (StreamBoundExceededException e) {
      logger
          .warn(
              "Whilst trying to read submitted R2 commit file we exceeded the maximum line length - possible malicious file. The rest of the file will be ignored.",
              e);
    }
    catch (AlreadyReceivedMessageException | CommitProcessingException | NoCommitExistsException e) {
      logger.error("Exception whilst trying to store Commit R2 message", e);
    }
    finally {
      if (bbis != null) {
        bbis.close();
      }
    }

    return true;
  }

  /**
   * Sets the fileName of the underlying file
   * 
   * Set the value in both the local instance and the underlying msg. This may seem an unusual method, but when a PeerFileMessage
   * comes in it has no filename. The fileName is generated locally to both guarantee uniqueness and prevent any attempts at
   * accessing/overwriting other files via maliciously constructed filenames or paths
   * 
   * @param filename
   *          string filename to set
   * @throws JSONException
   */
  public void setFilename(String filename) throws JSONException {
    this.fileName = filename;
    this.msg.put(MessageFields.CommitR2.INTERNAL_FILENAME, this.fileName);
  }

  /**
   * Updates the digest of the message.
   * 
   * The message will already have a digest set when it is received. We are able to perform a validation of the signature and origin
   * based on that digest. However, that is performed before we have even read the file from the stream. Having read the file we
   * want to check that the signature covers the actual file, not just the digest sent in the message
   * 
   * As such, a message could have two digest. One will be "digest" the one sent by the client and the locally calculated value
   * "_digest"
   * 
   * 
   * @param digest
   *          base64 string of the digest
   * @throws JSONException
   */
  private void updateDigest(String digest) throws JSONException {
    this.digest = digest;
    this.msg.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
  }

  /**
   * Both updates the digest and then performs a validation of the new digest.
   * 
   * @param peer
   *          the WBBPeer this is running on - needed for acccess to keys
   * @param digest
   *          Base64 string of the digest of the file
   * 
   * 
   * @return boolean of the outcome of the validation
   * @throws JSONException
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  public boolean updateDigestAndVerify(WBBPeer peer, String digest) throws JSONException, MessageSignatureException,
      MessageJSONException {
    this.updateDigest(digest);
    return this.validateSenderSignatureWithLocalDigest(peer);
  }

  /**
   * Validates the signature based on the digest included in the message
   * 
   * This is only used during initial message verification. After we have calculated our own digest we re-checks
   * 
   * @param peer
   *          WBBPeer this is running on - needed for keys
   * @return boolean outcome of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validateSenderSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateFromCerts(CertStore.PEER,
          this.msg.getString(MessageFields.CommitR2.PEER_ID) + WBBConfig.SIGNING_KEY_SK1_SUFFIX));
      tvsSig.update(IOUtils.decodeData(EncodingType.BASE64, this.msg.getString(MessageFields.CommitR2.DIGEST)));
      return tvsSig.verify(this.msg.getString(MessageFields.CommitR2.PEER_SIGNATURE), EncodingType.BASE64);

    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }

    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }

  /**
   * Validates the message with the locally calculated digest value
   * 
   * @param peer
   *          WBBPeer this is running on - needed for access to keys
   * @return boolean result of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */

  private boolean validateSenderSignatureWithLocalDigest(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateFromCerts(CertStore.PEER,
          this.msg.getString(MessageFields.CommitR2.PEER_ID) + WBBConfig.SIGNING_KEY_SK1_SUFFIX));
      tvsSig.update(IOUtils.decodeData(EncodingType.BASE64, this.digest));

      return tvsSig.verify(this.msg.getString(MessageFields.CommitR2.PEER_SIGNATURE), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }
}
