/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.config;

/**
 * Certificate store enumeration.
 * 
 * @author Chris Culnane
 * 
 */
public enum CertStore {
  VPS, EVM, PEER, MIXSERVER, FILEMESSAGE, CANCEL, CANCEL_AUTH;
  
  public static final String CERT_ID="id";
  public static final String CERT_KEYSTORE="keystore";
  
}
