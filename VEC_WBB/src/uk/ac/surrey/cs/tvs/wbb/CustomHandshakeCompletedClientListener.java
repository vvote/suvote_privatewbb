/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.io.IOException;
import java.net.InetSocketAddress;

import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;

import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;

/**
 * 
 * Overrides the handshake completion for client SSL connections.
 * 
 * Used to close the connection if the wrong principal is connected.
 * 
 * @author Chris Culnane
 * 
 */
public class CustomHandshakeCompletedClientListener implements HandshakeCompletedListener {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CustomHandshakeCompletedClientListener.class);
  /**
   * The peer this is running on
   */
  private WBBPeer             peer;

  /**
   * Creates the handshake for the current peer.
   * 
   * @param peer
   *          the WBBPeer this is being run by
   */
  public CustomHandshakeCompletedClientListener(WBBPeer peer) {
    super();

    this.peer = peer;
  }

  /**
   * Called when the handshake is complete. Validates the principal and closes the connection if invalid.
   * 
   * @param evt
   *          The source SSL socket.
   * 
   * @see javax.net.ssl.HandshakeCompletedListener#handshakeCompleted(javax.net.ssl.HandshakeCompletedEvent)
   */
  @Override
  public void handshakeCompleted(HandshakeCompletedEvent evt) {
    InetSocketAddress remoteAddress = (InetSocketAddress) evt.getSocket().getRemoteSocketAddress();
    remoteAddress.getHostName();

    try {
      X500Name certName = new X500Name(evt.getPeerPrincipal().getName());
      RDN cn = certName.getRDNs(BCStyle.CN)[0];
      String id = IETFUtils.valueToString(cn.getFirst().getValue());
      JSONObject host = this.peer.getConfig().getHostDetails(id);

      // If the remote is not the valid principal, close the socket.
      if ((host == null) || (!host.getString(WBBConfig.ADDRESS).equals(remoteAddress.getAddress().getHostAddress()))
          || (host.getInt(WBBConfig.PORT) != remoteAddress.getPort())) {
        logger.warn("Incorrect SSL, closing connection from {}", remoteAddress.toString());
        evt.getSocket().close();
      }
    }
    catch (JSONException | IOException e) {
      logger.error("Exception whilst checking handshake, will close socket", e);
      try {
        evt.getSocket().close();
      }
      catch (IOException e1) {
        logger.error("Exception whilst closing socket", e1);
      }
    }
  }
}
