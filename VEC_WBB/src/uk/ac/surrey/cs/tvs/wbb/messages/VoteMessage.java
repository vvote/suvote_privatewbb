/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.security.KeyStoreException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.verify.PrefsVerification;
import uk.ac.surrey.cs.tvs.utils.verify.VerifyPreferences;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a VoteMessage sent from an EBM
 * 
 * @author Chris Culnane
 * 
 */
public class VoteMessage extends ExternalMessage {

  /**
   * Static variable for accessing the ATL race size in the district conf
   */
  private static final String DISTRICT_CONF_LC_ATL = "lc_atl";
  /**
   * Static variable for accessing the BTL race size in the district conf
   */
  private static final String DISTRICT_CONF_LC_BTL = "lc_btl";
  /**
   * Static variable for accessing the LA race size in the district conf
   */
  private static final String DISTRICT_CONF_LA     = "la";
  /**
   * Logger
   */
  private static final Logger logger               = LoggerFactory.getLogger(VoteMessage.class);

  /**
   * Cached construction of the vote preferences
   */
  private String              vPrefs;

  /**
   * Constructor to create VoteMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public VoteMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create VoteMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public VoteMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    if (this.externalSignableContent == null) {
      // Create signable content from id, serial sig, prefs, boothsig, boothid and commitTime
      StringBuffer signableContentSB = new StringBuffer();
      signableContentSB.append(this.id);
      signableContentSB.append(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.DISTRICT));
      signableContentSB.append(this.vPrefs);
      signableContentSB.append(this.commitTime);

      this.externalSignableContent = signableContentSB.toString();
    }

    return this.externalSignableContent;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() throws JSONException {
    if (this.internalSignableContent == null) {
      // Create signable content from id, serial sig, prefs, boothsig, boothid and commitTime
      StringBuffer signableContentSB = new StringBuffer();

      signableContentSB.append(this.id);
      signableContentSB.append(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.DISTRICT));
      signableContentSB.append(this.vPrefs);
      signableContentSB.append(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.SENDER_SIG));
      signableContentSB.append(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.SENDER_ID));
      signableContentSB.append(this.commitTime);

      this.internalSignableContent = signableContentSB.toString();
    }

    return this.internalSignableContent;
  }

  /**
   * Initialisation of the VoteMessage object
   * 
   * This is the most complicated message initialisation of all the messages because of extracting the preferences.
   * 
   * The preferences are stored in a JSONArray - note JSONArrays guarantee to maintain their order, JSONObjects do not.
   * 
   * In order to sign the content we need to create a single string to sign, in the same way the EBM did. As such, we iterate
   * through the various races and reconstruct a combined signature, which we then cache in the vPrefs instance variable and also in
   * the underlying message. The reason we cache it in the underlying message is that it saves us recalculating when performing
   * verification of incoming messages, which are checked against our received preference list.
   * 
   * Preferences are concatenated without any additional formating characters.
   * 
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.VOTE_MESSAGE;
    this.id = this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.ID);

    // If vPrefs already exists in the underlying message reload it
    if (this.msg.has(MessageFields.VoteMessage.INTERNAL_PREFS)) {
      this.vPrefs = this.msg.getString(MessageFields.VoteMessage.INTERNAL_PREFS);
    }
    else {
      // Otherwise calculate
      StringBuffer vPrefsSB = new StringBuffer();
      JSONArray races = this.msg.getJSONArray(MessageFields.VoteMessage.RACES);
      String lc_atl = "";
      String lc_btl = "";
      String la = "";
      for (int i = 0; i < races.length(); i++) {
        JSONObject race = races.getJSONObject(i);
        if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LC_ATL)) {
          lc_atl = race.getJSONArray(MessageFields.VoteMessage.PREFERENCES).join(MessageFields.PREFERENCE_SEPARATOR)
              .replaceAll(MessageFields.VoteMessage.QUOTE_CHAR, "");
        }
        else if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LC_BTL)) {
          lc_btl = race.getJSONArray(MessageFields.VoteMessage.PREFERENCES).join(MessageFields.PREFERENCE_SEPARATOR)
              .replaceAll(MessageFields.VoteMessage.QUOTE_CHAR, "");
        }
        else if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LA)) {
          la = race.getJSONArray(MessageFields.VoteMessage.PREFERENCES).join(MessageFields.PREFERENCE_SEPARATOR)
              .replaceAll(MessageFields.VoteMessage.QUOTE_CHAR, "");
        }
      }

      // Having create joined strings for each race we combined the different races
      vPrefsSB.append(la);
      vPrefsSB.append(MessageFields.RACE_SEPARATOR);
      vPrefsSB.append(lc_atl);
      vPrefsSB.append(MessageFields.RACE_SEPARATOR);
      vPrefsSB.append(lc_btl);
      vPrefsSB.append(MessageFields.RACE_SEPARATOR);

      // Store vprefs in instance variable and message
      this.vPrefs = vPrefsSB.toString();
      this.msg.put(MessageFields.VoteMessage.INTERNAL_PREFS, this.vPrefs);
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Validates against JSON Schema
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.VOTE))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Checks serial signature
      if (!this.validateSerialSignature(peer)) {
        logger.warn("Serial Number signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Serial Number signature is not valid");
      }

      // Checks EBM signature
      if (!this.validateEBMSignature(peer)) {
        logger.warn("EBM signature is not valid:{}", this.msg);
        throw new MessageVerificationException("EBM signature is not valid");
      }

      // Checks it contains a StartEVM signature
      if (!this.validateStartEVMSignature(peer)) {
        logger.warn("StartEVM signature is not valid:{}", this.msg);
        throw new MessageVerificationException("StartEVM signature is not valid");
      }

      // Check preferences are consistent
      if (VerifyPreferences.checkAllRacePreferences(this.msg.getJSONArray(MessageFields.VoteMessage.RACES)) != PrefsVerification.ALL_VALID) {
        logger.warn("Preferences are not valid:{}", this.msg);
        throw new MessageVerificationException("Preferences are not valid");
      }

      // Checks the right size of preference array has been submitted
      if (!this.validatePreferenceLength(peer, this.msg.getJSONArray(MessageFields.VoteMessage.RACES))) {
        logger.warn("Incorrent number of preferences submitted:{}", this.msg);
        throw new MessageVerificationException("Incorrect number of preferences submitted");
      }

    }
    catch (MessageSignatureException e) {
      logger.warn("Signature could not be verified on :{}", this.msg, e);
      throw new MessageVerificationException("Signature could not be verified - either invalid or no certificate could be found", e);
    }
    catch (MessageJSONException | JSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }

  }

  /**
   * Validates that the correct size of preference array has been submitted. It enforces that the client has submitted an array of
   * values equal in size to the race sizes defined in the district config. This does not enforce that a vote is in those
   * preferences, it could be an array of empty preferences, it just verifies that the length is consistent with what it is
   * expecting.
   * 
   * @param peer
   *          WBBPeer that we are doing the check on
   * @param races
   *          JSONArray of the races that have been submitted by the client
   * @return true if the length is consistent with the district conf, false if not
   * @throws MessageJSONException
   */
  private boolean validatePreferenceLength(WBBPeer peer, JSONArray races) throws MessageJSONException {
    try {
      JSONObject districtData = peer.getConfig().getDistrictConf()
          .getJSONObject(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.DISTRICT));
      for (int i = 0; i < races.length(); i++) {
        JSONObject race = races.getJSONObject(i);
        if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LC_ATL)
            && race.getJSONArray(MessageFields.VoteMessage.PREFERENCES).length() != districtData.getInt(DISTRICT_CONF_LC_ATL)) {
          return false;
        }
        else if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LC_BTL)
            && race.getJSONArray(MessageFields.VoteMessage.PREFERENCES).length() != districtData.getInt(DISTRICT_CONF_LC_BTL)) {
          return false;
        }
        else if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LA)
            && race.getJSONArray(MessageFields.VoteMessage.PREFERENCES).length() != districtData.getInt(DISTRICT_CONF_LA)) {
          return false;
        }
      }

      return true;
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object (could be districtconf):{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
  }

  /**
   * Validates the EBM signature
   * 
   * @param peer
   *          WBBPeer this is running on - needed for access to keys
   * @return boolean of output from validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validateEBMSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.EVM,
          this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.SENDER_ID)));
      tvsSig.update(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.ID));
      tvsSig.update(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.DISTRICT));
      tvsSig.update(this.vPrefs.getBytes());

      return tvsSig.verify(this.msg.getString(uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage.SENDER_SIG),
          EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }

  /**
   * Validates the vote request has a signed StartEVM signature indicating a threshold of appears accept the session as valid. As
   * such, it should be accepted.
   * 
   * @param peer
   *          the WBBPeer this is running on - needed for access to keys
   * @return the result of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  protected boolean validateStartEVMSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      String serialNo = this.msg.getString(MessageFields.JSONWBBMessage.ID);
      Object startEVMSigs = this.msg.get(MessageFields.VoteMessage.START_EVM_SIG);
      String district = this.msg.getString(MessageFields.JSONWBBMessage.DISTRICT);

      if (startEVMSigs instanceof JSONArray) {
        JSONArray serialSig = (JSONArray) startEVMSigs;
        try {
          TVSSignature tvsSig = new TVSSignature(SignatureType.DEFAULT, peer.getCertificateKeyStore(CertStore.PEER));
          tvsSig.update(JSONWBBMessage.getTypeString(Message.START_EVM));
          tvsSig.update(serialNo);
          tvsSig.update(district);

          JSONObject response = tvsSig.verify(serialSig, WBBConfig.SIGNING_KEY_SK2_SUFFIX);

          if (response.getInt(TVSSignature.VALID_COUNT) >= peer.getThreshold()) {
            logger.info("Verified serial signature successfully {}", response);
            return true;
          }
          else {
            logger.warn("Serial number signature failed {}", response);
            return false;
          }

        }
        catch (TVSSignatureException e) {
          throw new MessageSignatureException("Error when verifying signature", e);
        }
      }
      else {
        try {

          TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateKeyStore(CertStore.PEER).getBLSCertificate(
              WBBPeer.JOINT_SIGNATURE_ENTITY));
          tvsSig.update(JSONWBBMessage.getTypeString(Message.START_EVM));
          tvsSig.update(serialNo);
          tvsSig.update(this.msg.getString(MessageFields.JSONWBBMessage.DISTRICT));

          if (tvsSig.verify((String) startEVMSigs, EncodingType.BASE64)) {
            logger.info("Verified serial signature successfully");
            return true;
          }
          else {
            logger.warn("Serial number signature failed");
            return false;
          }
        }
        catch (TVSSignatureException e) {
          throw new MessageSignatureException("Error when verifying signature", e);
        }
        catch (TVSKeyStoreException e) {
          throw new MessageSignatureException("Error when verifying signature", e);
        }
      }
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
  }

}
