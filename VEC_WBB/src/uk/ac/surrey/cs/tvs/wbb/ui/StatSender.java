/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIMessage;
import uk.ac.surrey.cs.tvs.wbb.ui.UIConstants.UIResponse;

/**
 * Example class to send runtime statistics to an HTML5 client
 * 
 * Gets the maximum memory and the free memory and sends them back in a JSON message. This allows the client to calculate the
 * percentage of the available memory that has been used.
 * 
 * @author Chris Culnane
 * 
 */
public class StatSender extends MsgSender {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(StatSender.class);

  /**
   * Constructs a StatSender with the WebSocket to send data to and the delay between messages
   * 
   * @param ws
   *          WebSocket to output data on
   * @param delay
   *          the delay between updates
   */
  public StatSender(WebSocket ws, long delay) {
    super(ws, delay);
    logger.info("Created new StatSender");
  }

  /**
   * Continuously send statistics to the required web socket.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      while (!this.shutdown) {
        JSONObject msg = new JSONObject();
        msg.put(UIMessage.UI_TYPE, UIResponse.UI_TYPE_MEMORY);
        msg.put(UIResponse.UI_VALUE_MEMORY, Runtime.getRuntime().freeMemory());
        msg.put(UIResponse.UI_VALUE_MAX_MEMORY, Runtime.getRuntime().maxMemory());
        this.ws.send(msg.toString());
        Thread.sleep(this.delay);
      }
    }
    catch (JSONException | InterruptedException e) {
      logger.error("Exception whilst sending stats, will shutdown statSender", e);
    }
  }
}
