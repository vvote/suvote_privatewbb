/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;

/**
 * Performs the actual processing of messages received from other Private WBB Peers
 * 
 * @author Chris Culnane
 * 
 */
public class InternalMessageProcessor extends MessageProcessor implements Runnable {

  /**
   * Holds a reference to the underlying message to be processed
   */
  private JSONWBBMessage      msg;

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(InternalMessageProcessor.class);

  /**
   * Constructor for InternalMessageProcessor.
   * 
   * @param msg
   *          The message to be processed.
   * @param peer
   *          WBBPeer this is running on - needed to access central resources
   */
  public InternalMessageProcessor(JSONWBBMessage msg, WBBPeer peer) {
    super(peer);

    this.msg = msg;
  }

  /**
   * Processes the message as a runnable action.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      if (!this.msg.processMessage(this.peer)) {
        return;
      }
    }
    catch (Exception e) {
      logger.error("Error has occurred whilst processing internal message. Will try to continue", e);
    }
  }
}
