/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509CertSelector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.net.ServerSocketFactory;
import javax.net.ssl.CertPathTrustManagerParameters;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.SendAndWaitForResponse;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocket;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.CommitR2;
import uk.ac.surrey.cs.tvs.utils.TimeoutManager;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CertificateManagerException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.ui.CommandListener;
import uk.ac.surrey.cs.tvs.wbb.utils.CertificateManager;
import uk.ac.surrey.cs.tvs.wbb.utils.CommitTimeUtils;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONValidator;
import uk.ac.surrey.tvs.cs.utils.crls.CachedCRLManager;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileWatcherException;

/**
 * WBBPeer represents the actual WBB Peer we are operating on.
 * 
 * Very little processing is performed in this class, however, a reference to it is passed to almost all the processing class
 * running on the various different threads. It provides a central point for accessing executors, key files, configuration values,
 * communication channels etc.
 * 
 * It currently needs refactoring to meet good coding style guidelines. The problem shown in the code audit is that it references
 * too many other different class types. This is somewhat to be expected given the nature of what it is doing. That said, there is
 * scope for improvement without changing the underlying ideology of having a central point for resources. For example, we could
 * wrap the networking components into a NetworkManager, the crypto/Signature stuff into a SecurityManager etc.
 * 
 * There could be an argument for creating a singleton pattern version of this, which would save having to pass a reference to it
 * all the time. However, the scope of the Singleton is the class loader, as such, there is a danger code we have not written could
 * access the singleton if it was loaded by the same class loader. This is clearly not desirable when we are providing access to
 * Private Keys!
 * 
 * @author Chris Culnane
 * 
 */
public class WBBPeer {

  /**
   * Logger
   */
  private static final Logger              logger                      = LoggerFactory.getLogger(WBBPeer.class);
  /**
   * Telemetry Logger
   */
  private static final Logger              telemetry                   = LoggerFactory.getLogger("Telemetry");

  /**
   * The name of the thread.
   */
  private static final String              TIMEOUT_MANAGER_THREAD_NAME = "TimeoutManager";

  /**
   * Number of retries of messages for commit round 2.
   */
  private static final int                 MAX_R2_SEND_RETRIES         = 4;

  /**
   * Length of time in milliseconds to wait for a commit round 2 response.
   */
  private static final int                 MAX_R2_WAIT_FOR_MESSAGE     = 10000;

  /**
   * Address we are listening on
   */
  private String                           address;

  /**
   * Port we are listening on
   */
  private int                              internalPort;

  /**
   * SSLServerSocket used for internal communications between peers
   */
  private SSLServerSocket                  ss;

  /**
   * SSLServerSocket for external communication
   */
  private SSLServerSocket                  extSS;

  /**
   * ConcurrantHashMap of UUID and response channels
   * 
   * When a connection is received a UUID is generated and stored in this with the socket we need to use to send a response on
   */
  private ConcurrentHashMap<UUID, Socket>  responseChannels            = new ConcurrentHashMap<UUID, Socket>();

  /**
   * Internal ServerListener
   * 
   * Listens for connections from other peers
   */
  private ServerInternalListener           sl;

  /**
   * External ServerListener
   * 
   * Listens for external communications from EBMs, MixNetPeers, PODPeers
   */
  private ServerExternalListener           extSL;

  /**
   * Holds a reference to the ConcurrentDB class which is a wrapper to the underlying data store
   */
  private ConcurrentDB                     cdb;

  /**
   * ExecutorService used for processing internal messages from other peers
   */
  private ExecutorService                  internalMessageExecutor     = Executors.newFixedThreadPool(64);

  /**
   * ExecutorService use for processing external db messages
   */
  private ExecutorService                  externalMessageExecutor     = Executors.newFixedThreadPool(64);

  /**
   * ExecutorService use for processing external messages
   */
  private ExecutorService                  externalListenerExecutor    = Executors.newFixedThreadPool(64);

  /**
   * We create a single threaded Executor for the commit processor because we only want one commit to run at any one time.
   */
  private ExecutorService                  commitExecutor              = Executors.newSingleThreadExecutor();

  /**
   * We utilise a scheduledExecutorService to handle the running of the daily commit - hence we only require a single thread again
   */
  private ScheduledExecutorService         scheduledExecutor           = Executors.newScheduledThreadPool(1);

  /**
   * ArrayList of all the ServerClient objects which are used for communication with other peers
   */
  private ArrayList<ServerClient>          peers                       = new ArrayList<ServerClient>();

  /**
   * SSL context for internal communication
   */
  private SSLContext                       ctx;

  /**
   * TrustManager for internal communication
   */
  private TrustManagerFactory              tmf;

  /**
   * KeyManager factory for internal communication
   */
  private KeyManagerFactory                kmf;

  /**
   * Single reference to the WBBConfig
   */
  private WBBConfig                        wbbConfig;

  /**
   * TimerUtils for handling commitTime calculations
   */
  private CommitTimeUtils                  timerID;

  /**
   * The PrivateKey for SK1 signatures
   */
  private PrivateKey                       sk1;

  /**
   * PrivateKey for SK2 signatures
   */
  private BLSPrivateKey                    sk2;

  /**
   * The id of this peer, it should match the details in the certificate
   */
  private String                           id;

  /**
   * SSLServerFactory for creating SSLServerSockets for receiving connections from other peers
   */
  private ServerSocketFactory              sslSocketFactory;

  /**
   * UploadDir is the location we should store FileUploads (typically from MixNetPeers or PODPeers - these are not commit file
   * uploads
   */
  private File                             uploadDir;

  /**
   * Stores the location we should upload and unzip commit uploads, which take place when exchanging databases
   */
  private File                             commitUploadDir;

  /**
   * The timeout for file uploads
   */
  private int                              fileUploadTimeout;

  /**
   * SSLSocket Factory for creating SSL connections other peers
   */
  private SSLSocketFactory                 factory;

  /**
   * TimeoutManager to handle the timeouts on messages
   */
  private TimeoutManager                   timeoutManager;

  /**
   * Central link the JSONValidator that provides access to JSON Schemas and validators
   */
  private JSONValidator                    jsonValidator;

  /**
   * The threads used to execute messages.
   */
  private HashMap<String, ExecutorService> peerExecutors               = new HashMap<String, ExecutorService>();

  /**
   * VECWebSocket that is used for the UI
   */
  private VECWebSocket                     vws;

  /**
   * CertificateManager that will be initialised to provide access, and monitor for changes, the underlying certificate stores
   */
  private CertificateManager               certManager                 = null;

  /**
   * Thread that runs the internal listener
   */
  private Thread                           internalListenerThread      = null;

  /**
   * Thread that runs the external listener
   */
  private Thread                           externalListenerThread      = null;

  /**
   * How many minutes we wait during a shutdown to finish processing existing tasks before forcing a shutdown
   */
  public static final int                  SHUTDOWN_WAIT               = 5;

  /**
   * CachedCRLManager maintains a cache of current CRLs and dynamically reloads changed files
   */
  private CachedCRLManager                 crlManager                  = null;
  /**
   * String that holds the entity name for the joint public key for the threshold signature
   */
  public static final String               JOINT_SIGNATURE_ENTITY      = "WBB";

  /**
   * ScheduledFuture keeps a record of the last scheduled commit - it is used to prevent multiple commits being scheduled
   */
  private ScheduledFuture<?>               scheduledCommit             = null;

  /**
   * CommandListener listens to System.in for incoming commands
   */
  private CommandListener                  commandListener             = null;

  /**
   * Constructor for a WBBPeer
   * 
   * Performs all the necessary bootstrapping for a Peer. Loads the config file and starts the listeners. Also creates the
   * connection between us and other peers.
   * 
   * @param config
   *          String of the file path to the WBBConfig JSON file
   * @throws PeerInitException
   */
  public WBBPeer(String config) throws PeerInitException {
    // Load the config
    try {
      this.wbbConfig = new WBBConfig(config);
      CryptoUtils.initProvider();

      // Store the internal address and port
      this.address = this.wbbConfig.getString(WBBConfig.ADDRESS);
      this.internalPort = this.wbbConfig.getInt(WBBConfig.PORT);
      this.fileUploadTimeout = this.wbbConfig.getInt(WBBConfig.FILE_UPLOAD_TIMEOUT);

      this.crlManager = new CachedCRLManager(this.wbbConfig.getString(WBBConfig.CRL_FOLDER));
      this.crlManager.startWatching();
      logger.info("Starting WBBPeer on {}:{}", this.address, this.internalPort);

      this.id = this.wbbConfig.getString(WBBConfig.PEER_ID);

      // Initialise the truststore
      this.initTrustStore(this.wbbConfig.getString(WBBConfig.PATH_TO_KEYSTORE),
          this.wbbConfig.getString(WBBConfig.KEYSTORE_PASSWORD).toCharArray());

      // Setup the timeout manager and create a new thread for it
      this.timeoutManager = new TimeoutManager(1000, this.wbbConfig.getSubmissionTimeout(), this.wbbConfig.getSubmissionTimeout());

      // Check the various upload and commitUpload directories exist - note, at the moment we inject the PeerID into the filepath,
      // that is due to testing on a single machine
      this.uploadDir = new File(this.wbbConfig.getString(WBBConfig.UPLOAD_DIR));
      IOUtils.checkAndMakeDirs(this.uploadDir);

      this.commitUploadDir = new File(this.wbbConfig.getString(WBBConfig.COMMIT_UPLOAD_DIR));
      IOUtils.checkAndMakeDirs(this.commitUploadDir);

      // Prepare a single threaded executor for each peer - this is used for sending files during R2. It guarantees we only have a
      // single file connection to each peer
      this.setupPeerExecutors();

      TVSKeyStore blsKey = TVSKeyStore.getInstance("JKS");
      blsKey.load(this.wbbConfig.getString(WBBConfig.SIGNING_KEY_KEYSTORE),
          this.wbbConfig.getString(WBBConfig.SIGNING_KEY_KEYSTORE_PASSWORD).toCharArray());
      this.sk1 = (PrivateKey) blsKey.getKeyStore().getKey(
          this.wbbConfig.getString(WBBConfig.SIGNING_KEY_ALIAS) + WBBConfig.SIGNING_KEY_SK1_SUFFIX,
          this.wbbConfig.getString(WBBConfig.SIGNING_KEY_SK1_PASSWORD).toCharArray());
      this.sk2 = blsKey.getBLSPrivateKey(this.wbbConfig.getString(WBBConfig.SIGNING_KEY_ALIAS) + WBBConfig.SIGNING_KEY_SK2_SUFFIX);
      this.ctx = SSLContext.getInstance("TLS");

      this.ctx.init(this.kmf.getKeyManagers(), this.tmf.getTrustManagers(), null);
      this.sslSocketFactory = this.ctx.getServerSocketFactory();

      this.ss = (SSLServerSocket) this.sslSocketFactory.createServerSocket();
      this.initServerSocket(this.ss, this.internalPort);

      // Load up the internal and external socket listeners.
      this.sl = new ServerInternalListener(this);
      this.extSS = (SSLServerSocket) this.sslSocketFactory.createServerSocket();
      this.initServerSocket(this.extSS, this.wbbConfig.getInt(WBBConfig.EXTERNAL_PORT));

      // this.extSS = new ServerSocket(this.wbbConfig.getInt(WBBConfig.EXTERNAL_PORT));
      this.extSL = new ServerExternalListener(this);

      // Start-up the database.
      this.cdb = new ConcurrentDB(this.id);
      this.factory = this.ctx.getSocketFactory();

      this.peers.addAll(this.createServerClientsToOtherPeers());

      this.timerID = new CommitTimeUtils(this.wbbConfig.getTime(WBBConfig.COMMIT_TIME, TimeUnit.HOURS), this.wbbConfig.getTime(
          WBBConfig.COMMIT_TIME, TimeUnit.MINUTES));
      this.scheduleNextCommit();

      this.jsonValidator = new JSONValidator(this);
      this.loadCertStores();
      telemetry.info("PEERINIT#Peer has started");
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Could not initialise peer - SSL Algorithm", e);
      throw new PeerInitException("Could not initialise peer - SSL Algorithm", e);
    }
    catch (KeyManagementException e) {
      logger.error("Could not initialise peer - Key management", e);
      throw new PeerInitException("Could not initialise peer - Key management", e);
    }
    catch (IOException e) {
      logger.error("Could not initialise peer - IO, Check file paths", e);
      throw new PeerInitException("Could not initialise peer - IO, Check file paths", e);
    }
    catch (PeerSSLInitException e) {
      logger.error("Could not initialise peer - Truststore initialisation", e);
      throw new PeerInitException("Could not initialise peer - Truststore initialisation", e);
    }
    catch (KeyStoreException e) {
      logger.error("Could not initialise peer - KeyStore initialisation", e);
      throw new PeerInitException("Could not initialise peer - KeyStore initialisation", e);
    }
    catch (UnrecoverableKeyException e) {
      logger.error("Could not initialise peer - Loading private key", e);
      throw new PeerInitException("Could not initialise peer - Loading private key", e);
    }
    catch (MaxTimeoutExceeded e) {
      logger.error("Could not initialise peer - Timeout Manager exception", e);
      throw new PeerInitException("Could not initialise peer - Timeout Manager exception", e);
    }
    catch (JSONException e) {
      logger.error("Could not initialise peer - Server Client exception", e);
      throw new PeerInitException("Could not initialise peer - Server Client exception", e);
    }
    catch (JSONSchemaValidationException e) {
      logger.error("WBBConfig failed validation", e);
      throw new PeerInitException("WBBConfig failed validation", e);
    }
    catch (CertificateException e) {
      logger.error("Exception loading CRLManager", e);
      throw new PeerInitException("Exception loading CRLManager", e);
    }
    catch (CRLException e) {
      logger.error("Exception loading CRL", e);
      throw new PeerInitException("Exception loading CRL", e);
    }
    catch (FileWatcherException e) {
      logger.error("Exception creating FileSystemWatcher", e);
      throw new PeerInitException("Exception creating FileSystemWatcher", e);
    }
  }

  /**
   * Adds a response channel.
   * 
   * @param session
   *          Unique id.
   * @param sock
   *          The channel's socket.
   */
  public void addResponseChannel(UUID session, Socket sock) {
    this.responseChannels.putIfAbsent(session, sock);
  }

  /**
   * Creates the internal server sockets for peer communication.
   * 
   * @return A list of the server clients.
   * @throws JSONException
   */
  private ArrayList<ServerClient> createServerClientsToOtherPeers() throws JSONException {
    ArrayList<ServerClient> serverClients = new ArrayList<ServerClient>();
    HashMap<String, JSONObject> wbbHosts = this.wbbConfig.getPeers();
    Iterator<String> itr = wbbHosts.keySet().iterator();

    while (itr.hasNext()) {
      String key = itr.next();
      JSONObject host = wbbHosts.get(key);

      if (!host.getString(WBBConfig.HOST_ADDRESS).equals(this.address) || host.getInt(WBBConfig.HOST_PORT) != this.internalPort) {
        logger.info("Creating ServerClient to {} on {}:{}", key, host.getString(WBBConfig.HOST_ADDRESS),
            host.getInt(WBBConfig.HOST_PORT));
        ServerClient sc = new ServerClient(host.getString(WBBConfig.HOST_ADDRESS), host.getInt(WBBConfig.HOST_PORT), this.factory,
            this);
        serverClients.add(sc);
      }
    }

    return serverClients;
  }

  /**
   * @return The peer's listening address.
   */
  public String getAddress() {
    return this.address;
  }

  /**
   * Gets a certificate given an alias.
   * 
   * @param store
   *          which CertStore the certificate is in
   * @param alias
   *          The alias used to obtain the certificate.
   * @return The retrieved certificate, if one exists.
   * @throws KeyStoreException
   */
  public TVSCertificate getCertificateFromCerts(CertStore store, String alias) throws KeyStoreException {
    return this.certManager.getCertificateStore(store).getCertificate(alias);
  }

  /**
   * @return The certificate key store.
   */
  public TVSKeyStore getCertificateKeyStore(CertStore store) {
    return this.certManager.getCertificateStore(store);
  }

  /**
   * Gets the name of a certificate for an X500 principal.
   * 
   * @param principal
   *          The X500 principal.
   * @return The certificate name.
   */
  public String getCertificateName(Principal principal) {
    X500Name certName = new X500Name(principal.getName());
    RDN cn = certName.getRDNs(BCStyle.CN)[0];

    return IETFUtils.valueToString(cn.getFirst().getValue());
  }

  /**
   * @return The commit processor.
   */
  public ExecutorService getCommitExecutor() {
    return this.commitExecutor;
  }

  /**
   * @return The commit time based upon the current time.
   */
  public String getCommitTime() {
    return this.timerID.getCommitTime();
  }

  /**
   * @return Directory to store commit uploads.
   */
  public File getCommitUploadDir() {
    return this.commitUploadDir;
  }

  /**
   * @return The peer config.
   */
  public WBBConfig getConfig() {
    return this.wbbConfig;
  }

  /**
   * @return The concurrent database wrapper.
   */
  public ConcurrentDB getDB() {
    return this.cdb;
  }

  /**
   * @return The processor used to process external messages.
   */
  public ExecutorService getExternalListenerExecutor() {
    return this.externalListenerExecutor;
  }

  /**
   * @return The processor used to process external db messages from other peers.
   */
  public ExecutorService getExternalMessageExecutor() {
    return this.externalMessageExecutor;
  }

  /**
   * @return The peer id.
   */
  public String getID() {
    return this.id;
  }

  /**
   * @return The processor used to process internal db messages from other peers.
   */
  public ExecutorService getInternalMessageExecutor() {
    return this.internalMessageExecutor;
  }

  /**
   * @return The listening port.
   */
  public int getInternalPort() {
    return this.internalPort;
  }

  /**
   * @return The JSON validator.
   */
  public JSONValidator getJSONValidator() {
    return this.jsonValidator;
  }

  /**
   * @return The number of peers.
   */
  public int getPeerCount() {
    return this.peers.size();
  }

  /**
   * @return The socket used for external messages.
   */
  public SSLServerSocket getPeerExternalServerSocket() {
    return this.extSS;
  }

  /**
   * @return The socket used to listen for internal messages.
   */
  public SSLServerSocket getPeerServerSocket() {
    return this.ss;
  }

  /**
   * @return The previous commit id.
   */
  public String getPreviousCommitTime() {
    return this.timerID.getPreviousCommitID();
  }

  /**
   * Removes a response channel given the unique id's session.
   * 
   * @param sessionID
   *          The session id for the channel.
   * @return The channel which has been removed.
   */
  public Socket getRemoveResponseChannel(String sessionID) {
    return this.responseChannels.remove(UUID.fromString(sessionID));
  }

  /**
   * @return The number of response channels in use.
   */
  public int getResponseChannelCount() {
    return this.responseChannels.size();
  }

  /**
   * @return The processor used to run the daily commit.
   */
  public ScheduledExecutorService getScheduledExecutor() {
    return this.scheduledExecutor;
  }

  /**
   * @return The private key for SK1 signatures.
   */
  public PrivateKey getSK1() {
    return this.sk1;
  }

  /**
   * @return The private key for SK2 signatures.
   */
  public BLSPrivateKey getSK2() {
    return this.sk2;
  }

  /**
   * @return The threshold number of peers.
   */
  public int getThreshold() {
    return this.wbbConfig.getThreshold();
  }

  /**
   * @return The manager processing timeouts on messages.
   */
  public TimeoutManager getTimeoutManager() {
    return this.timeoutManager;
  }

  /**
   * @return Directory to store file (not commit) uploads.
   */
  public File getUploadDir() {
    return this.uploadDir;
  }

  /**
   * @return The timeout used for file uploads.
   */
  public int getUploadTimeout() {
    return this.fileUploadTimeout;
  }

  /**
   * Initialises the socket used for internal communication.
   * 
   * @throws IOException
   */
  private void initServerSocket(SSLServerSocket sock, int port) throws IOException {
    sock.setEnabledCipherSuites(new String[] { this.getConfig().getString(WBBConfig.SSL_CONFIG) });
    sock.setNeedClientAuth(true);
    sock.setUseClientMode(false);
    sock.setReuseAddress(true);

    InetSocketAddress sa = new InetSocketAddress(port);
    sock.bind(sa);
    logger.info("Successfully bound socket");
    logger.info("Initialised and bound SSLServerSocket on {}", sa);
  }

  /**
   * Initialises the key store given the store's password.
   * 
   * @param pathToKS
   *          The key store location.
   * @param pwd
   *          The key store password.
   * @throws PeerSSLInitException
   */
  private void initTrustStore(String pathToKS, char[] pwd) throws PeerSSLInitException {
    try {
      KeyStore ks = CryptoUtils.loadKeyStore(pathToKS, pwd);

      this.tmf = TrustManagerFactory.getInstance("PKIX");
      PKIXBuilderParameters pkixParams = new PKIXBuilderParameters(ks, new X509CertSelector());
      java.security.cert.CertStore cs = java.security.cert.CertStore.getInstance("Collection", new CollectionCertStoreParameters(
          crlManager.getCRLCollection()));
      pkixParams.addCertStore(cs);
      tmf.init(new CertPathTrustManagerParameters(pkixParams));

      this.kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
      this.kmf.init(ks, pwd);
    }
    catch (CryptoIOException | NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException
        | InvalidAlgorithmParameterException e) {
      logger.error("Error whilst initialising SSL Key and Trust Stores", e);
      throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores", e);
    }

  }

  /**
   * Initialises the CertificateManager that loads the certificates stores listed in the CERT_STORE_SPEC field in the config file.
   * 
   * @throws PeerInitException
   */
  private void loadCertStores() throws PeerInitException {
    try {
      this.certManager = new CertificateManager(this.getConfig());
      this.certManager.startMonitoring();
      logger.info("CertificateManager loaded and started monitoring");
    }
    catch (CertificateManagerException | FileWatcherException e) {
      throw new PeerInitException("Exception whilst loading keystores");
    }
  }

  /**
   * Runs the daily commit for the current commit time.
   */
  public void runCommit() {
    CommitProcessor cp = new CommitProcessor(this);
    Thread t = new Thread(cp, "CommitProcessor");
    t.start();
    telemetry.info("DAILYCOMMIT#Started daily commit");
  }

  /**
   * Run the daily commit for the specified commit time.
   */
  public void runCommit(String commitTime) {
    CommitProcessor cp = new CommitProcessor(this, commitTime);
    Thread t = new Thread(cp, "CommitProcessor");
    t.start();
    telemetry.info("EXTRACOMMIT#{}", commitTime);
  }

  /**
   * Run the daily commit for the specified commit time.
   */
  public void runCommit(String commitTime, String commitDesc) {
    CommitProcessor cp = new CommitProcessor(this, commitTime, commitDesc);
    Thread t = new Thread(cp, "CommitProcessor");
    t.start();
    telemetry.info("EXTRACOMMIT#{}", commitTime);
  }

  /**
   * Runs the server clients on separate executors.
   * 
   * @param serverClients
   *          The clients to be run.
   */
  private void runServerClientsInPeerExecutors(ArrayList<ServerClient> serverClients) {
    for (ServerClient sc : serverClients) {
      ExecutorService es = this.peerExecutors.get(sc.getAddress() + ":" + sc.getPort());
      es.execute(new ServerClientThread(sc));
    }
  }

  /**
   * Runs the server clients on separate threads.
   * 
   * @param serverClients
   *          The clients to be run.
   */
  private void runServerClientsOnThreads(ArrayList<ServerClient> serverClients) {
    for (ServerClient sc : serverClients) {
      ServerClientThread scThread = new ServerClientThread(sc);
      (new Thread(scThread, "ServerClient" + sc.getAddress() + ":" + sc.getPort())).start();
    }
  }

  /**
   * Schedule a job to run the next daily commit.
   */
  public void scheduleNextCommit() {

    if (this.scheduledCommit == null || this.scheduledCommit.isDone() || this.scheduledCommit.getDelay(TimeUnit.MILLISECONDS) <= 0) {
      logger.info("Scheduling next commit");
      this.scheduledCommit = this.scheduledExecutor.schedule(
          new CommitProcessor(this),
          CommitTimeUtils.getMillisecondsTillTime(this.wbbConfig.getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.HOURS),
              this.wbbConfig.getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.MINUTES)), TimeUnit.MILLISECONDS);
    }
    else {
      logger.warn("A commit is already scheduled in {} minutes, cannot schedule another one",
          this.scheduledCommit.getDelay(TimeUnit.MINUTES));
    }

  }

  /**
   * Sends the specified message using the session's response channel. The channel is then closed. A warning is written if the
   * response channel does not exist.
   * 
   * @param sessionID
   *          The session for the response channel.
   * @param msg
   *          The message to send.
   * @throws IOException
   */
  public void sendAndClose(String sessionID, String msg) throws IOException {
    this.sendAndClose(sessionID, msg, true);
  }

  /**
   * Sends the specified message using the session's response channel. The channel is then closed. A warning is written if the
   * response channel does not exist and warnIfNoResponseChannel is set to true.
   * 
   * @param sessionID
   *          The session for the response channel.
   * @param msg
   *          The message to send.
   * @param warnIfNoResponseChannel
   *          if trues logs a warning if there is no response channel
   * @throws IOException
   */
  public void sendAndClose(String sessionID, String msg, boolean warnIfNoResponseChannel) throws IOException {
    Socket socket = this.responseChannels.remove(UUID.fromString(sessionID));

    if (socket != null) {
      PrintStream ps = new PrintStream(socket.getOutputStream(), true);
      ps.println(msg);
      logger.info("Sent Response");
      socket.close();
    }
    else {
      if (warnIfNoResponseChannel) {
        logger.warn("Couldn't find session: {} can't send: {}", sessionID, msg);
      }
    }
  }

  /**
   * Sends the round 2 commit data file to all peers for consolidation.
   * 
   * @param file
   *          The file to send.
   * @param attachments
   *          The file attachments.
   * @param commitID
   *          The commit id for the commit session.
   * @param commitTime
   *          The commit time for the commit session.
   */
  public void sendR2CommitFileToAllPeers(String file, String attachments, String commitID, String commitTime) {
    try {
      // Read the file into the message.
      boolean addAttachment = false;

      MessageDigest md = this.wbbConfig.getMessageDigest();
      int filesize = IOUtils.addFileIntoDigest(new File(this.wbbConfig.getString(WBBConfig.COMMIT_DIRECTORY) + file), md);

      // Read in the attachments.
      File attachmentFile = new File(this.wbbConfig.getString(WBBConfig.COMMIT_DIRECTORY) + attachments);
      int attachmentSize = 0;

      if (attachmentFile.exists() && attachmentFile.length() > 0) {
        attachmentSize = attachmentSize
            + IOUtils.addFileIntoDigest(new File(this.wbbConfig.getString(WBBConfig.COMMIT_DIRECTORY) + attachments), md);

        addAttachment = true;
      }

      TVSSignature peerSig = new TVSSignature(SignatureType.DEFAULT, this.getSK1());
      byte[] digestBytes = md.digest();
      peerSig.update(digestBytes);

      // Sign the message.
      String digest = IOUtils.encodeData(EncodingType.BASE64, digestBytes);

      // Construct the JSON to be sent.
      JSONObject messageToSend = new JSONObject();
      messageToSend.put(CommitR2.MSG_TYPE, CommitR2.TYPE_STRING);
      messageToSend.put(CommitR2.COMMIT_ID, commitID);
      messageToSend.put(CommitR2.COMMIT_TIME, commitTime);
      messageToSend.put(CommitR2.FILESIZE, filesize);
      messageToSend.put(CommitR2.FILENAME, this.wbbConfig.getString(WBBConfig.COMMIT_DIRECTORY) + file);

      if (addAttachment) {
        messageToSend.put(CommitR2.ATTACHMENT_SIZE, attachmentSize);
        messageToSend.put(CommitR2.ATTACHMENT_FILENAME, this.wbbConfig.getString(WBBConfig.COMMIT_DIRECTORY) + attachments);
      }

      messageToSend.put(CommitR2.DIGEST, digest);
      messageToSend.put(CommitR2.PEER_ID, this.id);
      messageToSend.put(CommitR2.PEER_SIGNATURE, peerSig.signAndEncode(EncodingType.BASE64));

      ArrayList<ServerClient> serverClients = this.createServerClientsToOtherPeers();

      // Set some custom parameters for the ServerClients
      for (ServerClient sc : serverClients) {
        sc.setDiscardOnTimeout(false);
        sc.setCloseAfterSend(true);
        sc.setMaxRetries(MAX_R2_SEND_RETRIES);
        sc.setWaitForMessageTimeout(WBBPeer.MAX_R2_WAIT_FOR_MESSAGE);
      }

      // Run the server clients - single thread per peer
      this.runServerClientsInPeerExecutors(serverClients);

      // Write the messages
      String messageString = messageToSend.toString();
      for (ServerClient sc : serverClients) {
        sc.writeMessage(messageString);
      }
      telemetry.info("R2COMMITSENT#Sent R2 Commit (database) to other peers");
    }
    catch (JSONException e) {
      logger.error("JSONException when trying to construct and send R2 Commit to other peers", e);
    }
    catch (FileNotFoundException e) {
      logger.error(
          "File Not Found Exception when trying to construct and send R2 Commit to other peers - possible missing files on HDD", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Crypto Exception when trying to construct and send R2 Commit to other peers", e);
    }
    catch (IOException e) {
      logger.error(
          "General IO Exception when trying to construct and send R2 Commit to other peers - possible missing files on HDD", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Crypto Exception when trying to construct and send R2 Commit to other peers", e);
    }
  }

  /**
   * Sends a message to all peers.
   * 
   * @param msg
   *          The message to send.
   */
  public void sendToAllPeers(String msg) {
    for (ServerClient s : this.peers) {
      s.writeMessage(msg.toString());
    }
  }

  /**
   * Sends a message to the Public WBB.
   * 
   * @param msg
   *          The message to send.
   * @throws NoCommitExistsException
   */
  public void sendToPublicWBB(String msg) throws NoCommitExistsException {
    StringBuffer sb = new StringBuffer();
    sb.append(msg);
    sb.append(MBB.MESSAGE_SEPARATOR);
    sb.append(MBB.FILE_SUB_TOKEN);

    File commitsFolder = new File(this.getConfig().getString(WBBConfig.COMMIT_DIRECTORY));
    sb.append(new File(commitsFolder, this.getDB().getCurrentCommitField(DBFields.COMMITMENT_FILE)).getAbsolutePath());
    sb.append(MBB.MESSAGE_SEPARATOR);
    sb.append(MBB.FILE_SUB_TOKEN);
    sb.append(new File(commitsFolder, this.getDB().getCurrentCommitField(DBFields.COMMITMENT_ATTACHMENTS)).getAbsolutePath());

    SendAndWaitForResponse sendMsg = new SendAndWaitForResponse(this.getConfig().getString(WBBConfig.PUBLIC_WBB_ADDRESS), this
        .getConfig().getInt(WBBConfig.PUBLIC_WBB_PORT), sb.toString());
    sendMsg.setSSLSocketFactory(this.factory);

    if (!this.getConfig().getString(WBBConfig.SSL_CONFIG).equalsIgnoreCase(WBBConfig.SSL_CIPHER_DEFAULT)) {
      sendMsg.setCipherSuite(this.getConfig().getString(WBBConfig.SSL_CONFIG));
    }
    sendMsg.addResponseListener(new CommitResponse(this));
    ExecutorService es = Executors.newSingleThreadExecutor();
    es.execute(new FutureTask<String>(sendMsg));
    telemetry.info("PUBLICWBBMSGSENT#Sending message to PublicWBB");
  }

  /**
   * Determines if the specified session has a response channel.
   * 
   * @param sessionID
   *          The session id.
   * @return True if the session has a channel.
   */
  public boolean sessionIDExists(UUID sessionID) {
    return this.responseChannels.containsKey(sessionID);
  }

  /**
   * Sets up the executor threads.
   * 
   * @throws JSONException
   */
  private void setupPeerExecutors() throws JSONException {
    HashMap<String, JSONObject> wbbHosts = this.wbbConfig.getPeers();
    Iterator<String> itr = wbbHosts.keySet().iterator();
    while (itr.hasNext()) {
      String key = itr.next();
      JSONObject host = wbbHosts.get(key);
      if (!host.getString(WBBConfig.HOST_ADDRESS).equals(this.address) || host.getInt(WBBConfig.HOST_PORT) != this.internalPort) {
        this.peerExecutors.put(host.getString(WBBConfig.HOST_ADDRESS) + ":" + host.getInt(WBBConfig.HOST_PORT),
            Executors.newSingleThreadExecutor());
      }
    }
  }

  /**
   * Performs an orderly shutdown of the system. If waitForFinish is set to false it will force a shutdown without waiting for tasks
   * to finish.
   * 
   * If waitForFinish is set to true the maximum wait time is set in minutes by SHUTDOWN_WAIT constant. Note that the wait is
   * performed when shutting down each executor service, therefore it is possible that the actual wait time is a multiple of
   * SHUTDOWN_WAIT. However, it is likely there will be almost no wait and if a wait is performed the other executors will also have
   * finished.
   * 
   * @param waitForFinish
   *          boolean true if it should wait for current tasks to finish, false to shutdown immediately
   */
  public void shutdown(boolean waitForFinish) {
    logger.info("Shutdown called on WBBPeer");
    logger.info("Shutting down External Listener");
    telemetry.info("SHUTDOWN#Shutdown called on Peer");
    this.extSL.shutdown();
    if (this.externalListenerThread != null) {
      this.externalListenerThread.interrupt();
    }

    logger.info("Shutting down external socket");
    try {
      if (this.extSS != null) {
        this.extSS.close();
      }
    }
    catch (IOException e) {
      logger.error("Exception whilst closing external socket", e);
    }
    logger.info("Shutting down Internal Listener");
    this.sl.shutdown();
    if (this.internalListenerThread != null) {
      this.internalListenerThread.interrupt();
    }
    logger.info("Shutting down internal socket");
    try {

      if (this.ss != null) {
        this.ss.close();
      }
    }
    catch (IOException e) {
      logger.error("Exception whilst closing internal socket", e);
    }
    for (ServerClient sc : this.peers) {
      logger.info("Shutting down client to {}:{}", sc.getAddress(), sc.getPort());
      sc.shutdown();
    }
    this.shutdownExecutorService(this.commitExecutor, "CommitExecutor", waitForFinish);
    this.shutdownExecutorService(this.externalListenerExecutor, "ExternalListenerExecutor", waitForFinish);
    this.shutdownExecutorService(this.externalMessageExecutor, "ExternalMessageExecutor", waitForFinish);
    this.shutdownExecutorService(this.internalMessageExecutor, "InternalMessageExecutor", waitForFinish);
    this.shutdownExecutorService(this.scheduledExecutor, "ScheduledExecutor", false); // have to force the shutdown - otherwise it
                                                                                      // will wait for the scheduled task to run
    Set<String> peerExecutorKeys = this.peerExecutors.keySet();
    for (String peerKey : peerExecutorKeys) {
      this.shutdownExecutorService(this.peerExecutors.get(peerKey), "PeerExecutor:" + peerKey, waitForFinish);
    }

    logger.info("Shutting CertificateManager");
    if (this.certManager != null) {
      try {
        this.certManager.stopMonitoringAndShutdown();
      }
      catch (FileWatcherException e) {
        logger.error("Exception whilst shuting down CertManager", e);
      }
    }
    try {
      logger.info("Shutting down WebSocket Server");
      this.vws.stop();
    }
    catch (IOException | InterruptedException e) {
      logger.error("Exception whislt Shutting down WebSocket Server", e);

    }

  }

  /**
   * Utility method for actually shutting down an executor service. If waitForFinish is set to true it will awaitTermination of
   * existing tasks for upto SHUTDOWN_WAIT minutes. If waitForFinish is set to false the executor is immediately shutdown. If there
   * were tasks remaining a warning will be logged.
   * 
   * @param service
   *          ExecutorService to shutdown
   * @param name
   *          String name of executor service - used for logging
   * @param waitForFinish
   *          boolean to determine if it should wait for existing tasks to finish, true to wait, false to not
   */
  private void shutdownExecutorService(ExecutorService service, String name, boolean waitForFinish) {
    logger.info("Shutting down {}, waitToFinish:{}", name, waitForFinish);
    if (waitForFinish) {
      service.shutdown();
      try {
        if (!service.awaitTermination(SHUTDOWN_WAIT, TimeUnit.MINUTES)) {
          logger.warn("Timeout expired when shutting down {} executor", name);
        }
      }
      catch (InterruptedException e) {
        logger.error("Interrupted whilst waiting to shutdown {}", name, e);
      }
    }
    else {
      List<Runnable> waiting = service.shutdownNow();
      if (waiting.size() > 0) {
        logger.warn("Peer shutdown with task waiting in {}", name);
      }
    }
    logger.info("{} Shutdown:{}", name, service.isTerminated());
  }

  /**
   * Starts the peer listening on the internal and external message ports and starts the timeout manager
   */
  public void start() {
    Thread timeoutThread = new Thread(this.timeoutManager, WBBPeer.TIMEOUT_MANAGER_THREAD_NAME);
    timeoutThread.setDaemon(true);
    timeoutThread.start();
    this.commandListener = new CommandListener(this);
    this.commandListener.startCL();
    this.vws = new VECWebSocket(new InetSocketAddress("localhost", this.wbbConfig.getInt("uiPort")));
    this.vws.addWebSocketListener(commandListener);
    this.vws.start();
    this.runServerClientsOnThreads(this.peers);
    this.internalListenerThread = new Thread(this.sl, this.id + "-InternalListener");
    this.internalListenerThread.start();
    this.externalListenerThread = new Thread(this.extSL, this.id + "-ExternalListener");
    this.externalListenerThread.start();
  }

  /**
   * Verifies that the sender's address and port matches to the required X500 principal.
   * 
   * @param remoteAddress
   *          The sender's address.
   * @param principal
   *          The required principal.
   * @return True if the sender's address and port match the required principal.
   */
  public boolean verifyConnection(InetSocketAddress remoteAddress, Principal principal) {
    return this.verifyConnection(remoteAddress, principal, true);
  }

  /**
   * Verifies that the sender's address and optionally the port matches to the required X500 principal.
   * 
   * @param remoteAddress
   *          The sender's address.
   * @param principal
   *          The required principal.
   * @param checkPort
   *          True to check the port as well.
   * @return True if the sender's address and optionally the port match the required principal.
   */
  public boolean verifyConnection(InetSocketAddress remoteAddress, Principal principal, boolean checkPort) {
    try {
      X500Name certName = new X500Name(principal.getName());

      RDN cn = certName.getRDNs(BCStyle.CN)[0];
      String id = IETFUtils.valueToString(cn.getFirst().getValue());

      JSONObject host = this.getConfig().getHostDetails(id);
      if (remoteAddress == null || host == null
          || !host.getString(WBBConfig.HOST_ADDRESS).equals(remoteAddress.getAddress().getHostAddress())) {
        telemetry.info("SSLFAIL#{},{},Failed SSLConnection, RemoteAddress does not match", remoteAddress, host);
        return false;
      }

      if (checkPort && host.getInt(WBBConfig.HOST_PORT) != remoteAddress.getPort()) {
        telemetry.info("SSLFAIL#{},{},Failed SSLConnection, port number does not match", remoteAddress, host);
        return false;
      }
    }
    catch (JSONException e) {
      logger.error("JSONException whilst trying to verify connection:{}, error:", remoteAddress, e.getMessage());
    }

    return true;
  }

  /**
   * Main entry point for the peer. Starts everything running given the configuration file passed as the first command line
   * argument.
   * 
   * @param args
   *          The command line arguments: one expected for the configuration file.
   */
  public static void main(String[] args) {
    try {
      if (args.length == 1) {
        WBBPeer peer = new WBBPeer(args[0]);
        peer.start();
      }
      else {
        printUsage();
      }
    }
    catch (PeerInitException e) {
      logger.error("FATAL Exception when trying to initialise WBB Peer. System will exit", e);
      System.exit(1);
    }
  }

  /**
   * Prints the usage instructions for starting the UI
   */
  private static final void printUsage() {
    System.out.println("Usage of WBBPeer:");
    System.out.println("WBBPeer starts a WBBPeer. This listens for connections");
    System.out.println("from clients and maintains connections to other peers.");
    System.out.println("Usage:\n");
    System.out.println("WBBPeer pathToConfigFile \n");
    System.out.println("\tpathToConfigFile: path to configuration file to use.");
  }
}
