/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a FileMessage sent from a client device to the WBB
 * 
 * This will be utilised by PODPeers to periodically submit a zip file of proofs covering key transformation. It will also be
 * utlised by the MixNetPeer to bulk upload its proofs, mixes, decryptions etc.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class BallotAuditCommitMessage extends BallotFileMessage {

  /**
   * Post processes a message as a part of the commit round 2 processing. This is only for submissions that impact on the wider
   * database, for example, ciphers or ballot gen audit.
   * 
   * @param peer
   *          The WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#postprocessAsPartofCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#postprocessAsPartofCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   * java.lang.String)
   */
  @Override
  protected void postprocessAsPartofCommitR2(WBBPeer peer, String safeUploadDir) throws NoSuchAlgorithmException, JSONException,
      MessageJSONException {
    super.postprocessAsPartofCommitR2(peer, safeUploadDir);
    try {
      this.markAuditedBallots(peer);
    }
    catch (IOException e) {
      throw new MessageJSONException(e);
    }
  }

  /**
   * Performs the standard file processing as defined in BallotFileMessage, with the addition of opening the ballot gen audit data
   * file and marking all the audited ballots as used in the database.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream.
   * @param socket
   *          The socket used for the send.
   * @param unzipDirPrefix
   *          The directory prefix for placing temporary files.
   * @return True if the file was processed successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   */
  @Override
  protected boolean doFileProcessing(WBBPeer peer, BoundedBufferedInputStream in, Socket socket, String unzipDirPrefix)
      throws IOException, JSONException, NoSuchAlgorithmException {
    // Do standard processing, if it fails return false immediately
    // We expect two files in the audit - the second file is the response used to generate the fiat-shamir
    if (super.doFileProcessing(peer, in, socket, unzipDirPrefix,2,new String[]{uk.ac.surrey.cs.tvs.fields.messages.MessageFields.BallotAuditCommitMessage.AUDIT_DATA_ZIP_ENTRY,uk.ac.surrey.cs.tvs.fields.messages.MessageFields.BallotAuditCommitMessage.SUBMISSION_RESPONSE_ZIP_ENTRY})) {
      // Prepare a reader object to reader the file
      return this.markAuditedBallots(peer);
    }
    else {
      return false;
    }

  }

  /**
   * Reads the submitted audit file and marks the ballots as used to prevent them being used for voting, auditing, pod etc.
   * 
   * @param peer
   *          WBBPeer that this method is running on
   * @return boolean true if all marked successfully, false if it could not be completed
   * @throws IOException
   * @throws JSONException
   */
  private boolean markAuditedBallots(WBBPeer peer) throws IOException, JSONException {
    BufferedReader br = null;
    try {
      // we know that there is only one file because that is checked as part of super.doFileProcessing
      br = new BufferedReader(new FileReader(new File(this.outputDir,uk.ac.surrey.cs.tvs.fields.messages.MessageFields.BallotAuditCommitMessage.AUDIT_DATA_ZIP_ENTRY)));
      String line;
      // Read each line
      while ((line = br.readLine()) != null) {
        // Gets the randomness array
        JSONArray randomnessArray = new JSONArray(line);
        String serialNo = this.checkForConsistentSerialNo(randomnessArray);
        logger.info("Serial No {} has been audited, will remove", serialNo);
        if (serialNo == null) {
          logger.warn("Inconsistent serial numbers in Ballot Gen Audit Commit: {}", randomnessArray.toString());
          return false;
        }
        try {
          logger.info("Marking {} as used in the database", serialNo);
          peer.getDB().recordBallotGenAudit(serialNo);
        }
        catch (AlreadyReceivedMessageException e) {
          logger.warn("Attempted to audit a ballot which already has a record against it", e);
          return false;
        }
      }
      return true;
    }
    finally {
      if (br != null) {
        br.close();
      }
    }
  }

  /**
   * Iterates through the JSONArray of ballot generation audit data to find and check that there is a consistent serial number for
   * the randomness.
   * 
   * @param randomnessArray
   *          JSONArray of randomness data as sent by the Print On Demand device during a ballot gen audit
   * @return String serial no if found and consistent, otherwise null
   * @throws JSONException
   */
  private String checkForConsistentSerialNo(JSONArray randomnessArray) throws JSONException {
    String serialNo = null;
    for (int i = 0; i < randomnessArray.length(); i++) {
      JSONObject randSubmission = randomnessArray.getJSONObject(i);
      if (randSubmission.has(MessageFields.Ballot.SERIAL_NO) && serialNo == null) {
        serialNo = randSubmission.getString(MessageFields.Ballot.SERIAL_NO);
      }
      else if (randSubmission.has(MessageFields.Ballot.SERIAL_NO)) {
        if (!serialNo.equals(randSubmission.getString(MessageFields.Ballot.SERIAL_NO))) {
          return null;
        }
      }
      else {
        return null;
      }
    }
    return serialNo;
  }

  /**
   * Directory prefix used for ballot upload.
   */
  private static final String UPLOAD_DIR_PREFIX = "BallotAuditCommitAttachments";

  /**
   * Logger
   */
  private static final Logger logger            = LoggerFactory.getLogger(BallotAuditCommitMessage.class);

  /**
   * Constructor to create FileMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public BallotAuditCommitMessage(JSONObject msg) throws MessageJSONException {
    super(msg);
    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create FileMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public BallotAuditCommitMessage(String msgString) throws MessageJSONException {
    super(msgString);
    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Gets the filename of the underlying file
   * 
   * @return string of the fileName
   */
  @Override
  public String getFilename() {
    return this.fileName;
  }

  /**
   * Gets the fileSize of the underlying file
   * 
   * @return long value representing the size of the file
   */
  @Override
  public long getFileSize() {
    return this.fileSize;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.internalSignableContent;
  }

  /**
   * Init the message
   * 
   * Sets the type, extracts the id from submissionID, extracts the fileSize and checks whether the underlying message has a
   * _fileName attribute. Having mentioned above that a message does not have a _fileName it may seem odd to load one from the JSON
   * during initialisation. However, this handles loading a message from our own local database when we perform the commit. Note
   * that a message will have been rejected if it was received with a _fileName attribute
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.BALLOT_AUDIT_COMMIT;
    this.id = this.msg.getString(MessageFields.FileMessage.ID);
    this.fileSize = this.msg.getInt(MessageFields.FileMessage.FILESIZE);

    if (this.msg.has(MessageFields.FileMessage.INTERNAL_FILENAME)) {
      this.fileName = this.msg.getString(MessageFields.FileMessage.INTERNAL_FILENAME);
    }

    if (this.msg.has(MessageFields.FileMessage.INTERNAL_DIGEST)) {
      this.updateDigest(this.msg.getString(MessageFields.FileMessage.INTERNAL_DIGEST));
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Perform JSONSchema validation on the underlying message
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.BALLOTAUDITCOMMIT))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Check the sender signature is valid with the digest sent in the message
      if (!this.validateSenderSignature(peer)) {
        logger.warn("Sender signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Sender signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }

  }

  /**
   * Pre-processes an incoming message by saving the file data to a file and by validating the message and verifying the message
   * signature.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream for the message.
   * @param socket
   *          The socket used for the send.
   * @return True if the message was pre-processed and validated successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.FileMessage#preProcessMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream, java.net.Socket)
   */
  @Override
  public boolean preProcessMessage(WBBPeer peer, BoundedBufferedInputStream in, Socket socket) throws IOException, JSONException,
      NoSuchAlgorithmException {
    // If it is a file message we need to download the file from the stream
    logger.info("Message is Ballot Audit Commit upload");

    return this.doFileProcessing(peer, in, socket, BallotAuditCommitMessage.UPLOAD_DIR_PREFIX);
  }
}
