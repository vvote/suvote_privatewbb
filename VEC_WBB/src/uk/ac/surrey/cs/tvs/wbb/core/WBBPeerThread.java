/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.SSLSocket;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR2Message;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;

/**
 * Provides connection processing for internal communications amongst peers.
 * 
 * Due to the stable nature of the connections between peers this runs in its own thread and waits for data to arrive, in contrast
 * to the external socket processor which only processes a single incoming message.
 * 
 * @author Chris Culnane
 * 
 */
public class WBBPeerThread implements Runnable {

  /**
   * SSLSocket that was accepted on the internal connection
   */
  private SSLSocket                  socket;

  /**
   * Set to true to gracefully shutdown
   */
  private boolean                    shutdown = false;

  /**
   * BoundedBufferedInputStream used to read from the stream
   */
  private BoundedBufferedInputStream in;

  /**
   * The peer this is running on
   */
  private WBBPeer                    peer;

  /**
   * Logger
   */
  private static final Logger        logger   = LoggerFactory.getLogger(WBBPeerThread.class);

  /**
   * Counter to keep track of connections from a peer
   */
  private AtomicInteger              connectionCounter;

  /**
   * Constructor for internal socket processor
   * 
   * Reads messages from the stream, verifies them and packages them for processing
   * 
   * @param peer
   *          the peer this is running on
   * @param socket
   *          underlying socket
   * @param connectionCounter
   *          Counter for the number of connections from this peer - should decrement this when the socket is closed
   * @throws IOException
   */
  public WBBPeerThread(WBBPeer peer, SSLSocket socket, AtomicInteger connectionCounter) throws IOException {
    super();

    this.socket = socket;
    this.in = new BoundedBufferedInputStream(socket.getInputStream(), peer.getConfig().getBufferBound());
    this.peer = peer;
    this.connectionCounter = connectionCounter;
  }

  /**
   * Force shutdown of the internal communications.
   */
  public void forceShutdown() {
    this.shutdown();

    try {
      this.in.close();
    }
    catch (IOException e) {
      logger.warn("IOException whilst closing socket in forceShutdown", e);
    }
  }

  /**
   * Process a peer file message.
   * 
   * @param jobj
   *          The WBB message.
   * @throws IOException
   * @throws JSONException
   */
  private void processPeerFileMessage(JSONWBBMessage jobj) throws IOException, JSONException {
    // If it is a FileMessage we need to process is differently./ Currently the only time peers exchange files is during a round 2
    // commit.
    logger.info("Message is peer file upload");
    CommitR2Message fm = (CommitR2Message) jobj;

    // Even though this comes from a peer we don't trust filenames, as such we create a unique filename using createTempFile,
    // however, the file is not a temporary file
    File dest = File.createTempFile("CommitUpload", ".json", this.peer.getCommitUploadDir());

    logger.info("Message is peer file upload, will store in: {}, msg{}:", dest.getName(), jobj);

    // Set the filename in the underlying message
    fm.setFilename(dest.getName());
    try {
      // Set the upload timeout to catch a socket not sending data
      this.socket.setSoTimeout(this.peer.getUploadTimeout());

      if (fm.getMsg().has(MessageFields.PeerFile.ATTACHMENT_SIZE)) {
        // If the message has an attachmentSize parameter it means two files are going to be sent. The first will be the
        // database from the other peer, the second will be a zip file containing all the file uploads that the peer has
        // processed
        File destAttachment = File.createTempFile(WBBConfig.COMMIT_UPLOAD_ATTACHMENT_PREFIX,
            WBBConfig.COMMIT_UPLOAD_ATTACHMENT_EXTENSION, this.peer.getCommitUploadDir());

        // We will need to Hash both files together, hence using dedicated DigestInputStream
        // Gets a Message digest of a suitable type
        MessageDigest md = this.peer.getConfig().getMessageDigest();
        DigestInputStream dis = new DigestInputStream(this.in, md);

        // Read the first file
        IOUtils.readFile((fm).getFileSize(), dest, dis);

        // Read the second file
        IOUtils.readFile(fm.getMsg().getLong(MessageFields.PeerFile.ATTACHMENT_SIZE), destAttachment, dis);

        // We now update the underlying message and reverify the contents
        if (!fm.updateDigestAndVerify(this.peer, IOUtils.encodeData(EncodingType.BASE64, md.digest()))) {
          logger.info("Signature check on received file failed - message rejected. msg:{}, file:{}", fm, dest.getName());
          this.socket.setSoTimeout(0);
          return;
        }

        // Since we have uploaded a zip file containing new uploads we need to unzip them. We don't yet know if they are
        // duplicates or not, since that will only be known when processing the underlying messages. As such, we need to unzip
        // the files and make them available
        Path commitUploadDir = Paths.get(this.peer.getCommitUploadDir().getAbsolutePath());

        // We again need to construct a location to unzip the files to
        Path safeUploadDir = Files.createTempDirectory(commitUploadDir, WBBConfig.COMMIT_UPLOAD_ATTACHMENT_DIRECTORY_PREFIX);
        ZipUtil zipUtil = new ZipUtil(this.peer.getConfig().getInt(WBBConfig.MAX_ZIP_ENTRY));
        try {
          // This performs a safe unzipping of the file. See comments in ZipUtil for more details
          zipUtil.unzip(destAttachment.getAbsolutePath(), safeUploadDir.toFile());
        }
        catch (UploadedZipFileEntryTooBigException e) {
          logger.error("The Zip File {} entry has exceeded the limit - unzipping abandoned", destAttachment.getAbsolutePath(), e);
          this.socket.setSoTimeout(0);
          return;
        }
        catch (UploadedZipFileNameException e) {
          logger.error("The Zip File {} entry has an invalid filename - unzipping abandoned", destAttachment.getAbsolutePath(), e);
          this.socket.setSoTimeout(0);
          return;
        }

        // Add the location of the safeUploadDir to the underlying message so the messages in the attached database can be
        // correctly processed
        fm.getMsg().put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, safeUploadDir.toString());
      }
      else {
        // There were no upload files included in the database from the other peer. Only download the database
        if (!fm.updateDigestAndVerify(this.peer, this.in.readFile((fm).getFileSize(), dest))) {
          logger.info("Signature check on received file failed - message rejected. msg:{}, file:{}", fm, dest.getName());
          this.socket.setSoTimeout(0);
          return;
        }
      }

      // Reset the socket timeout to infinity
      this.socket.setSoTimeout(0);
    }
    catch (MessageSignatureException | MessageJSONException | NoSuchAlgorithmException e) {
      logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), fm,
          dest.getName());
      return;
    }

    // File transfer take place on a separate connection, as such we can now close this connection. There shouldn't be another
    // file transfer until the next commit
    this.shutdown = true;

    // We run commits in a single thread to avoid concurrency issues with the database updates
    this.peer.getCommitExecutor().execute(new InternalMessageProcessor(jobj, this.peer));
  }

  /**
   * Continuously process internal messages.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    logger.debug("Connection from:{}", this.socket.getRemoteSocketAddress());

    String line = "";

    // Loop forever or until shutdown
    while (!this.shutdown) {
      try {
        // Blocks until a line is available
        line = this.in.readLine();

        // Check if the socket has been closed
        if (line == null) {
          logger.info("readLine returned Null - likely connection drop");
          this.socket.close();
          break;
        }
        logger.info("ReceivedInternal:{}", line);

        // Parse the message
        JSONWBBMessage jobj = JSONWBBMessage.parseMessage(line);

        jobj.performValidation(this.peer);

        // Add details to the message about where it has come from. The ID is extracted from the SSL session.
        jobj.getMsg().put(MessageFields.FROM_PEER, this.peer.getCertificateName(this.socket.getSession().getPeerPrincipal()));

        // Check if this is a FileMessage
        if (jobj.getType() == Message.PEER_FILE_MESSAGE) {
          this.processPeerFileMessage(jobj);
        }
        else {
          // Create an InternalMessageProcessor for the message and pass it to the executor for processing
          this.peer.getInternalMessageExecutor().execute(new InternalMessageProcessor(jobj, this.peer));
        }
      }
      catch (EOFException e) {
        logger.warn("EOF received, likely connection dropped:{}", this.socket.getRemoteSocketAddress());
        break;
      }
      catch (IOException e) {
        logger.error("IO exception occured, likely connection dropped:{}", e);
        break;
      }
      catch (JSONException e) {
        logger.error("JSON exception occured, will try to continue:{}", line, e);
      }
      catch (MessageJSONException e) {
        logger.error("Message is malformed {}", line, e);
      }
      catch (StreamBoundExceededException e) {
        logger.warn("Input has exceeded bound on server socket - no newline character found. The data has been discarded.",
            this.socket.getRemoteSocketAddress());
      }
      catch (MessageVerificationException e) {
        logger.warn("Message verification failed on message from Peer, this should not happen. Possibly corrupt peer {}",
            this.socket.getRemoteSocketAddress(), e);
      }
      catch (JSONSchemaValidationException e) {
        logger.warn("Schema validation failed on message from Peer, this should not happen. Possibly corrupt peer {}",
            this.socket.getRemoteSocketAddress(), e);
      }
    }

    // We now try and close the socket
    try {
      this.in.close();

    }
    catch (IOException e) {
      logger.warn("IOException whilst closing input stream", e);
    }
    finally {
      try {
        this.connectionCounter.decrementAndGet();
        this.socket.close();
      }
      catch (IOException e) {
        logger.warn("IOException whilst closing socket", e);
      }
    }
  }

  /**
   * Shutdown the internal communications.
   */
  public void shutdown() {
    this.shutdown = true;
  }
}
