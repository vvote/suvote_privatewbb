/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.ResponseListener;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;

/**
 * CommitResponse handles the response from the PublicWBB and records it in the database and closes the commit.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitResponse implements ResponseListener {

  /**
   * Telemetry Logger
   */
  private static final Logger              telemetry                      = LoggerFactory.getLogger("Telemetry");
  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CommitResponse.class);

  /**
   * WBBPeer - reference to the underlying peer
   */
  private WBBPeer             peer;

  /**
   * Constructs a new CommitResponse with the related peer. This will listen for a response from the PublicWBB to signify the commit
   * has been successfully received.
   * 
   * @param peer
   *          WBBPeer that this is running on
   */
  public CommitResponse(WBBPeer peer) {
    super();

    this.peer = peer;
  }

  /**
   * Called when a response is received. Records the response and closes the commit. It then schedules the next commit.
   * 
   * @param response
   *          String of the JSONObject that holds the response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.ResponseListener#responseFromWait(java.lang.String)
   */
  @Override
  public void responseFromWait(String response) {
    logger.info("Received response from Commit {}", response);
    telemetry.info("COMMITRESP#{}", response);
    if (response != null) {
      try {
        this.peer.getDB().setResponseAndCloseCommit(response);
      }
      catch (NoCommitExistsException e) {
        logger.error("Exception whilst setting the response and closing the commit - commit will need manually closing", e);
      }

      this.peer.scheduleNextCommit();
    }
    else {
      logger.error("Null response received from commit to PublicWBB - indicates a possible problem");
    }
  }
}
