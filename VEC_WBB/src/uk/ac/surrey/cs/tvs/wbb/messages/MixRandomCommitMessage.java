/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.net.Socket;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a FileMessage sent from a client device to the WBB
 * 
 * This will be utilised by PODPeers to periodically submit a zip file of proofs covering key transformation. It will also be
 * utlised by the MixNetPeer to bulk upload its proofs, mixes, decryptions etc.
 * 
 * @author Chris Culnane
 * 
 */
public class MixRandomCommitMessage extends BallotFileMessage {

  /**
   * Directory prefix used for mixnet commit upload.
   */
  private static final String UPLOAD_DIR_PREFIX = "MixCommitAttachments";

  /**
   * Logger
   */
  private static final Logger logger            = LoggerFactory.getLogger(MixRandomCommitMessage.class);

  /**
   * Constructor to create FileMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public MixRandomCommitMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create FileMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public MixRandomCommitMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Gets the filename of the underlying file
   * 
   * @return string of the fileName
   */
  @Override
  public String getFilename() {
    return this.fileName;
  }

  /**
   * Gets the fileSize of the underlying file
   * 
   * @return long value of the file size
   */
  @Override
  public long getFileSize() {
    return this.fileSize;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.internalSignableContent;
  }

  /**
   * Init the message
   * 
   * Sets the type, extracts the id from submissionID, extracts the fileSize and checks whether the underlying message has a
   * _fileName attribute. Having mentioned above that a message does not have a _fileName it may seem odd to load one from the JSON
   * during initialisation. However, this handles loading a message from our own local database when we perform the commit. Note
   * that a message will have been rejected if it was received with a _fileName attribute
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.MIX_RANDOM_COMMIT;
    this.id = this.msg.getString(FileMessage.ID);
    this.fileSize = this.msg.getInt(FileMessage.FILESIZE);

    if (this.msg.has(FileMessage.INTERNAL_FILENAME)) {
      this.fileName = this.msg.getString(FileMessage.INTERNAL_FILENAME);
    }

    if (this.msg.has(FileMessage.INTERNAL_DIGEST)) {
      this.updateDigest(this.msg.getString(FileMessage.INTERNAL_DIGEST));
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Perform JSONSchema validation on the underlying message
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.MIXRANDOMCOMMIT))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Check the sender signature is valid with the digest sent in the message
      if (!this.validateSenderSignature(peer)) {
        logger.warn("Sender signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Sender signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }
  }

  /**
   * Pre-processes an incoming message by saving the file data to a file and by validating the message and verifying the message
   * signature.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream for the message.
   * @param socket
   *          The socket used for the send.
   * @return True if the message was pre-processed and validated successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.FileMessage#preProcessMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream, java.net.Socket)
   */
  @Override
  public boolean preProcessMessage(WBBPeer peer, BoundedBufferedInputStream in, Socket socket) throws IOException, JSONException,
      NoSuchAlgorithmException {
    // If it is a file message we need to download the file from the stream
    logger.info("Message is Commit upload");
    return this.doFileProcessing(peer, in, socket, MixRandomCommitMessage.UPLOAD_DIR_PREFIX);
  }

  /**
   * Updates the digest of the message.
   * 
   * The message will already have a digest set when it is received. We are able to perform a validation of the signature and origin
   * based on that digest. However, that is performed before we have even read the file from the stream. Having read the file we
   * want to check that the signature covers the actual file, not just the digest sent in the message
   * 
   * As such, a message could have two digest. One will be "digest" the one sent by the client and the locally calculated value
   * "_digest"
   * 
   * Having updated the digest the signable content is calculated
   * 
   * @param digest
   *          base64 string of the digest
   * @throws JSONException
   */
  @Override
  protected void updateDigest(String digest) throws JSONException {
    this.digest = digest;
    this.msg.put(FileMessage.INTERNAL_DIGEST, digest);

    StringBuffer signableContentSB = new StringBuffer();
    signableContentSB.append(this.id);
    signableContentSB.append(this.msg.getString(FileMessage.SENDER_ID));
    signableContentSB.append(this.msg.getString(MessageFields.MixRandomCommit.PRINTER_ID));
    signableContentSB.append(this.digest);
    signableContentSB.append(this.commitTime);
    this.internalSignableContent = signableContentSB.toString();
  }

  /**
   * Validates the signature based on the digest included in the message
   * 
   * This is only used during initial message verification. After we have calculated our own digest we re-check
   * 
   * @param peer
   *          WBBPeer this is running on - needed for keys
   * @return boolean of the outcome of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  @Override
  protected boolean validateSenderSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.MIXSERVER,
          this.msg.getString(MessageFields.FileMessage.SENDER_ID)));
      tvsSig.update(this.id);
      tvsSig.update(this.msg.getString(MessageFields.MixRandomCommit.PRINTER_ID).getBytes());
      tvsSig.update(this.msg.getString(FileMessage.DIGEST).getBytes());

      return tvsSig.verify(this.msg.getString(MessageFields.FileMessage.SENDER_SIG), EncodingType.BASE64);

    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }

  /**
   * Validates the message with the locally calculated digest value
   * 
   * @param peer
   *          WBBPeer this running on - needed for keys
   * @return boolean of the outcome of the signature validation
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.FileMessage#validateSenderSignatureWithLocalDigest(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  protected boolean validateSenderSignatureWithLocalDigest(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.FILEMESSAGE,
          this.msg.getString(MessageFields.FileMessage.SENDER_ID)));
      tvsSig.update(this.id);
      tvsSig.update(this.msg.getString(MessageFields.MixRandomCommit.PRINTER_ID).getBytes());
      tvsSig.update(this.digest);

      return tvsSig.verify(this.msg.getString(MessageFields.FileMessage.SENDER_SIG), EncodingType.BASE64);

    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }

    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }
}
