/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.config;

/**
 * Enum of the different types of ClientErrorMessages that can be sent
 * 
 * @author Chris Culnane
 * 
 */
public enum ClientErrorMessage {
  SERIALNO_USED, TIME_OUT_ON_CONSENSUS, NO_CONSENSUS_POSSIBLE, UNKNOWN_DB_ERROR, SESSION_TIMEOUT, COULD_NOT_PARSE_JSON, UNKNOWN_MESSAGE_TYPE, MESSAGE_FAILED_VALIDATION, SIGNATURE_CHECK_FAILED, INPUT_EXCEEDED_BOUND, COMMITMENT_FAILURE, UNKNOWN_SERIAL_NO, INVALID_ZIP_FILE, INVALID_MESSAGE, BALLOT_TIMEOUT, STARTEVM_ALREADY_SENT
}
