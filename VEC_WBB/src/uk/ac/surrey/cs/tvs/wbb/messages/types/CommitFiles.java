/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages.types;

/**
 * Enum for commit files.
 * 
 * @author Chris Culnane
 * 
 */
public enum CommitFiles {
  COMMIT_FOLDER, COMMIT_FILE_FULL, COMMIT_FILE_FULL_ZIP, COMMIT_FILE
}
