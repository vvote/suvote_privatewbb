/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import java.util.Comparator;

import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;

/**
 * Comparator to order the messages that are to be hashes during round 1 of the commit protocol. It is important all peers perform
 * the same ordering otherwise the hashes will differ
 * 
 * @author Chris Culnane
 * 
 */
public class CommitComparator implements Comparator<JSONWBBMessage> {

  /**
   * Separator character for serial numbers.
   */
  private static final char SERIAL_NO_SEPARATOR = ':';

  /**
   * Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first argument is less
   * than, equal to, or greater than the second.
   * 
   * @param o1
   *          the first object to be compared.
   * @param o2
   *          the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the
   *         second.
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(JSONWBBMessage o1, JSONWBBMessage o2) {
    // Messages are ordered initially by type and then by ID
    if (o1.getType() == o2.getType()) {
      String lhs = o1.getID();
      String rhs = o2.getID();

      if (lhs.indexOf(CommitComparator.SERIAL_NO_SEPARATOR) >= 0 && rhs.indexOf(CommitComparator.SERIAL_NO_SEPARATOR) >= 0) {
        //If it is a serial number in form devicename:number we split to get the natural ordering
        int comp = lhs.substring(0, lhs.indexOf(CommitComparator.SERIAL_NO_SEPARATOR)).compareTo(
            rhs.substring(0, rhs.indexOf(CommitComparator.SERIAL_NO_SEPARATOR)));

        if (comp == 0) {
          int one = Integer.parseInt(lhs.substring(lhs.indexOf(CommitComparator.SERIAL_NO_SEPARATOR) + 1));
          int two = Integer.parseInt(rhs.substring(rhs.indexOf(CommitComparator.SERIAL_NO_SEPARATOR) + 1));

          return one - two;
        }
        else {
          return comp;
        }
      }
      else {
        //Otherwise we just compare the strings
        return lhs.compareTo(rhs);
      }
    }
    else {
      return o1.getType().compareTo(o2.getType());
    }
  }
}
