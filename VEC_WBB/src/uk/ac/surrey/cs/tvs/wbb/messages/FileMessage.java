/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents a FileMessage sent from a client device to the WBB
 * 
 * This will be utilised by PODPeers to periodically submit a zip file of proofs covering key transformation. It will also be
 * utlised by the MixNetPeer to bulk upload its proofs, mixes, decryptions etc.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class FileMessage extends ExternalMessage {

  /**
   * Filename prefix used to upload files from a message.
   */
  protected final static String UPLOAD_FILE_PREFIX = "WBBUpload";

  /**
   * Filename suffix used to upload files from a message.
   */
  protected final static String UPLOAD_FILE_EXT    = ".zip";

  /**
   * Logger
   */
  private static final Logger   logger             = LoggerFactory.getLogger(FileMessage.class);

  /**
   * Filename of the file connected to this message. We do not care what the file actually is, it could be anything. This is only a
   * fileName, the path is fixed as either an upload directory or commitUpload
   */
  protected String              fileName;

  /**
   * A local instance variable for the digest of the file referred to by fileName
   */
  protected String              digest;

  /**
   * Size of the file, used to determine how much data is read
   */
  protected long                fileSize;

  /**
   * Constructor to create FileMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public FileMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create FileMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public FileMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Defines the external content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    return this.getInternalSignableContent();
  }

  /**
   * Gets the filename of the underlying file
   * 
   * @return string of the fileName
   */
  public String getFilename() {
    return this.fileName;
  }

  /**
   * Gets the fileSize of the underlying file
   * 
   * @return long value of the file size
   */
  public long getFileSize() {
    return this.fileSize;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    return this.internalSignableContent;
  }

  /**
   * Init the message.getMsg()
   * 
   * Sets the type, extracts the id from submissionID, extracts the fileSize and checks whether the underlying message has a
   * _fileName attribute. Having mentioned above that a message does not have a _fileName it may seem odd to load one from the JSON
   * during initialisation. However, this handles loading a message from our own local database when we perform the commit. Note
   * that a message will have been rejected if it was received with a _fileName attribute
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.FILE_MESSAGE;
    this.id = this.msg.getString(MessageFields.FileMessage.ID);
    this.fileSize = this.msg.getInt(MessageFields.FileMessage.FILESIZE);

    if (this.msg.has(MessageFields.FileMessage.INTERNAL_FILENAME)) {
      this.fileName = this.msg.getString(MessageFields.FileMessage.INTERNAL_FILENAME);
    }

    if (this.msg.has(MessageFields.FileMessage.INTERNAL_DIGEST)) {
      this.updateDigest(this.msg.getString(MessageFields.FileMessage.INTERNAL_DIGEST));
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Perform JSONSchema validation on the underlying message
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.FILE))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Check the sender signature is valid with the digest sent in the message
      if (!this.validateSenderSignature(peer)) {
        logger.warn("Sender signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Sender signature is not valid");
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }
  }

  /**
   * Processes the message as a part of the commit round 2 processing.
   * 
   * @param peer
   *          The WBB peer.
   * @param safeUploadDir
   *          The source upload directory.
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#preprocessAsPartofCommitR2(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected void preprocessAsPartofCommitR2(WBBPeer peer, String safeUploadDir) throws NoSuchAlgorithmException, JSONException,
      MessageJSONException {
    MessageDigest md = peer.getConfig().getMessageDigest();

    try {
      IOUtils.addFileIntoDigest(new File(safeUploadDir + "/" + this.getFilename()), md);

      if (!this.updateDigestAndVerify(peer, IOUtils.encodeData(EncodingType.BASE64, md.digest()))) {
        logger.warn("FileMessage in Commit R2 failed message verification");
        return;
      }

      this.msg.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, safeUploadDir);
    }
    catch (IOException e) {
      logger
          .warn(
              "IO Exception  when importing R2 commit File_Message record. Indicates an incomplete submission from peer. Will ignore this message.",
              e);
      return;
    }
    catch (MessageSignatureException e) {
      logger
          .warn(
              "Signature Exception when verifying R2 commit File_Message record. Indicates an incomplete submission from peer. Will ignore this message.",
              e);
      return;
    }
  }

  /**
   * Pre-processes an incoming message by saving the file data to a file and by validating the message and verifying the message
   * signature.
   * 
   * @param peer
   *          The WBB peer.
   * @param in
   *          The input stream for the message.
   * @param socket
   *          The socket used for the send.
   * @return True if the message was pre-processed and validated successfully.
   * @throws IOException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#preProcessMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream, java.net.Socket)
   */
  @Override
  public boolean preProcessMessage(WBBPeer peer, BoundedBufferedInputStream in, Socket socket) throws IOException, JSONException,
      NoSuchAlgorithmException {
    // If it is a file message we need to download the file from the stream
    logger.info("Message is file upload");

    // Uploads should always be zip files to allow multiple files to be uploaded in a single file. We always create an
    // independent filename, we do not trust any sent filenames. All uploads go in a single directory. createTempFile guarantees
    // uniqueness of filenames in the directory, however, the file itself is not a temporary file.
    File dest = File.createTempFile(FileMessage.UPLOAD_FILE_PREFIX, FileMessage.UPLOAD_FILE_EXT, peer.getUploadDir());
    logger.info("Message is file upload, will store in: {}, msg{}:", dest.getName(), this.msg);

    // Set the filename in the FileMessage so we can access the file during later processing
    this.setFilename(dest.getName());

    try {
      // Set the socket timeout based on config option - this handles cases where a large file is specified in the FileMessage
      // but the client doesn't send all the data
      socket.setSoTimeout(peer.getUploadTimeout());

      // The readFile message returns the digest of the read file. We update the underlying FileMessage to now perform
      // validation on the digest of the File we have actually received
      if (!this.updateDigestAndVerify(peer, in.readFile((this).getFileSize(), dest))) {
        logger.info("Signature check on received file failed - message rejected. msg:{}, file:{}", this.msg, dest.getName());
        this.sendMessageAndClose(SendErrorMessage.constructErrorMessage(peer, "", ClientErrorMessage.SIGNATURE_CHECK_FAILED),
            socket);

        return false;
      }

      return true;
    }
    catch (MessageSignatureException e) {
      // Message has failed verification following the update of the local digest
      logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg,
          dest.getName());
      this.sendMessageAndClose(
          SendErrorMessage.constructErrorMessage(peer, e.getMessage(), ClientErrorMessage.SIGNATURE_CHECK_FAILED), socket);
      return false;
    }
    catch (MessageJSONException e) {
      logger.info("Signature check on received file failed - message rejected: {}. msg:{}, file:{}", e.getMessage(), this.msg,
          dest.getName());
      this.sendMessageAndClose(
          SendErrorMessage.constructErrorMessage(peer, e.getMessage(), ClientErrorMessage.SIGNATURE_CHECK_FAILED), socket);
      return false;
    }
    finally {
      // Reset timeout to infinity, assuming the socket isn't closed.
      if (!socket.isClosed()) {
        socket.setSoTimeout(0);
      }
    }
  }

  /**
   * Sets the fileName of the underlying file
   * 
   * Set the value in both the local instance and the underlying msg. This may seem an unusual method, but when a FileMessage comes
   * in it has no filename. The fileName is generated locally to both guarantee uniqueness and prevent any attempts at
   * accessing/overwriting other files via maliciously constructed filenames or paths
   * 
   * @param filename
   *          string filename to set
   * @throws JSONException
   */
  public void setFilename(String filename) throws JSONException {
    this.fileName = filename;
    this.msg.put(MessageFields.FileMessage.INTERNAL_FILENAME, this.fileName);
  }

  /**
   * Updates the digest of the message.
   * 
   * The message will already have a digest set when it is received. We are able to perform a validation of the signature and origin
   * based on that digest. However, that is performed before we have even read the file from the stream. Having read the file we
   * want to check that the signature covers the actual file, not just the digest sent in the message
   * 
   * As such, a message could have two digest. One will be "digest" the one sent by the client and the locally calculated value
   * "_digest"
   * 
   * Having updated the digest the signable content is calculated
   * 
   * @param digest
   *          base64 string of the digest
   * @throws JSONException
   */
  protected void updateDigest(String digest) throws JSONException {
    this.digest = digest;
    this.msg.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    StringBuffer signableContentSB = new StringBuffer();
    signableContentSB.append(this.id);
    signableContentSB.append(this.digest);
    signableContentSB.append(this.msg.getString(MessageFields.FileMessage.SENDER_ID));
    signableContentSB.append(this.commitTime);
    this.internalSignableContent = signableContentSB.toString();
  }

  /**
   * Both updates the digest and then performs a validation of the new digest.
   * 
   * @param peer
   *          the WBBPeer this is running on - needed for access to keys
   * @param digest
   *          Base64 string of the digest of the file
   * 
   * @return boolean of the outcome of the validation
   * @throws JSONException
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  public boolean updateDigestAndVerify(WBBPeer peer, String digest) throws JSONException, MessageSignatureException,
      MessageJSONException {
    this.updateDigest(digest);

    return this.validateSenderSignatureWithLocalDigest(peer);
  }

  /**
   * Validates the signature based on the digest included in the message
   * 
   * This is only used during initial message verification. After we have calculated our own digest we re-checks
   * 
   * @param peer
   *          WBBPeer this is running on - needed for keys
   * @return boolean of the outcome of the validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  protected boolean validateSenderSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.FILEMESSAGE,
          this.msg.getString(MessageFields.FileMessage.SENDER_ID)));
      tvsSig.update(this.id);
      tvsSig.update(this.msg.getString(MessageFields.FileMessage.DIGEST));

      return tvsSig.verify(this.msg.getString(MessageFields.FileMessage.SENDER_SIG), EncodingType.BASE64);

    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }

  /**
   * Validates the message with the locally calculated digest value
   * 
   * @param peer
   *          WBBPeer this running on - needed for keys
   * @return boolean of the outcome of the signature validation
   * @throws MessageSignatureException
   * @throws MessageJSONException
   * @throws TVSSignatureException
   */
  protected boolean validateSenderSignatureWithLocalDigest(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.FILEMESSAGE,
          this.msg.getString(MessageFields.FileMessage.SENDER_ID)));
      tvsSig.update(this.id);
      tvsSig.update(this.digest);

      return tvsSig.verify(this.msg.getString(MessageFields.FileMessage.SENDER_SIG), EncodingType.BASE64);

    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("Error when reading value from JSON Object", e);
    }

    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }

    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when updating signature", e);
    }
  }
}
