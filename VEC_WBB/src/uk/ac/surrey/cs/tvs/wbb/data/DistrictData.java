/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Meta class to hold references to the bundle data for regions and districts. Holds references to both regions and districts to
 * optimise access data either via the region or directly via the distr.ict
 * 
 * @author Chris Culnane
 * 
 */
public class DistrictData {

  /**
   * Logger
   */
  private static final Logger       logger    = LoggerFactory.getLogger(DistrictData.class);

  /**
   * HashMap of regions indexed by region name
   */
  private HashMap<String, Region>   regions   = new HashMap<String, Region>();

  /**
   * HashMap of districts indexed by district name
   */
  private HashMap<String, District> districts = new HashMap<String, District>();

  /**
   * Constructs the district and region data using both the bundle data file and the district configuration file used during ballot
   * generation. It also checks the consistency of the data between the two files.
   * 
   * @param regionData
   *          String path to the region/district bundle data
   * @param districtConf
   *          String path to the district configuration file used during ballot generation
   */
  public DistrictData(String regionData, String districtConf) {
    super();

    try {
      logger.info("Loading district data from {} and {}", regionData, districtConf);
      JSONObject dataFile = IOUtils.readJSONObjectFromFile(regionData);
      JSONObject districtConfFile = IOUtils.readJSONObjectFromFile(districtConf);
      JSONArray regionsArr = dataFile.names();

      if (regionsArr != null) {
        for (int i = 0; i < regionsArr.length(); i++) {
          Region region = new Region(dataFile.getJSONObject(regionsArr.getString(i)), this.districts, districtConfFile);
          this.regions.put(region.getName(), region);
          logger.info("{} is valid: {}", region.getName(), region.validateConsistency());
        }

        logger.info("Regions loaded: {}", this.regions);
        logger.info("Districts Loaded: {}", this.districts);
      }
    }
    catch (JSONIOException e) {
      logger.error("Exception loading district and region data", e);
    }
    catch (JSONException e) {
      logger.error("Exception loading district and region data", e);
    }
    catch (CandidateCountException e) {
      logger.error("Exception loading district and region data", e);
    }
  }
}
