/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CommitException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageSignatureException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;
import uk.ac.surrey.cs.tvs.wbb.utils.SendErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * Represents an Audit message being sent from the Audit machine in the poll station. Extends the abstract JSONWBBMessage with
 * appropriate modifications to handle Audit messages.
 * 
 * @author Chris Culnane
 * 
 */
public class AuditMessage extends ExternalMessage {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(AuditMessage.class);

  /**
   * The reduced permutations of the ballot.
   */
  private String              adjustedPerms;

  /**
   * Constructor to create AuditMessage from JSONObject
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msg
   *          JSONObject of the message
   * @throws MessageJSONException
   */
  public AuditMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msg);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Constructor to create AuditMessage from string of JSON
   * 
   * Should avoid directly constructing messages, since the type will not be checked. Should use JSONWBBMessage.parseMessage
   * instead. Direct instantiations are OK if the message type is known.
   * 
   * @param msgString
   *          string of JSON of the message
   * @throws MessageJSONException
   */
  public AuditMessage(String msgString) throws MessageJSONException {
    super(msgString);

    try {
      this.init();
    }
    catch (JSONException e) {
      logger.warn("Cannot initialise message, likely malformed message:{}", msgString);
      throw new MessageJSONException("Cannot parse message", e);
    }
  }

  /**
   * Performs ballot reduction to audit the ballot and stores the reduced ballot in the msg field.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the ballot can be reduced.
   * @throws JSONException
   */
  private boolean extractReducedPerms(WBBPeer peer) throws JSONException {
    JSONObject cipherRecord = peer.getDB().getBallotRecord(this.id);
    String perm = this.msg.getString(MessageFields.AuditMessage.PERMUTATION);
    String podMessage = peer.getDB().getClientMessage(DBRecordType.POD, this.getID());

    if (!(cipherRecord == null) && podMessage != null) {
      JSONObject clientMsg = new JSONObject(podMessage);
      JSONArray ballotReductions = clientMsg.getJSONArray(MessageFields.PODMessage.BALLOT_REDUCTIONS);

      int raceCounter = 0;
      StringTokenizer racePerms = new StringTokenizer(perm, MessageFields.RACE_SEPARATOR);
      StringBuffer reducedPerms = new StringBuffer();

      while (racePerms.hasMoreTokens()) {
        String[] fullPerms = racePerms.nextToken().split(MessageFields.PREFERENCE_SEPARATOR);

        for (int i = 0; i < ballotReductions.getJSONArray(raceCounter).length(); i++) {
          fullPerms[ballotReductions.getJSONArray(raceCounter).getJSONObject(i)
              .getInt(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX)] = null;
        }
        boolean isFirst = true;
        for (String s : fullPerms) {
          if (s != null) {
            if (!isFirst) {
              reducedPerms.append(MessageFields.PREFERENCE_SEPARATOR);
            }
            else {
              isFirst = false;
            }
            reducedPerms.append(s);

          }
        }
        reducedPerms.append(MessageFields.RACE_SEPARATOR);
        raceCounter++;
      }

      this.adjustedPerms = reducedPerms.toString();
      this.msg.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, this.adjustedPerms);

      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Defines the content that should be signed by message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    // If we have already calculated the signable content we return that, otherwise calculate it
    if (this.externalSignableContent == null) {
      StringBuffer signableContentSB = new StringBuffer();

      // for an audit we sign audit, id
      signableContentSB.append(this.getTypeString());
      signableContentSB.append(this.id);

      if (this.adjustedPerms == null) {
        logger.error("Getting signable content with null adjusted perms");
      }
      signableContentSB.append(this.adjustedPerms);
      this.externalSignableContent = signableContentSB.toString();
    }

    return this.externalSignableContent;
  }

  /**
   * Defines the content that should be signed in the message.
   * 
   * @return a string of the signable content
   * @throws JSONException
   */
  @Override
  public String getInternalSignableContent() {
    // If we have already calculated the signable content we return that, otherwise calculate it
    if (this.internalSignableContent == null) {
      StringBuffer signableContentSB = new StringBuffer();

      // for an audit we sign audit, id, and commitTime
      signableContentSB.append(this.getTypeString());
      signableContentSB.append(this.id);
      signableContentSB.append(this.commitTime);

      if (this.adjustedPerms == null) {
        logger.error("Getting signable content with null adjusted perms");
      }
      signableContentSB.append(this.adjustedPerms);

      this.internalSignableContent = signableContentSB.toString();
    }

    return this.internalSignableContent;
  }

  /**
   * Initialise the object by setting the message type and extracting the SerialNo to the id attribute.
   * 
   * @throws JSONException
   */
  private void init() throws JSONException {
    this.type = Message.AUDIT_MESSAGE;
    this.id = this.msg.getString(JSONWBBMessage.ID);
    if (this.msg.has(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION)) {
      this.adjustedPerms = this.msg.getString(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION);
    }
  }

  /**
   * Validate the message.
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
    try {
      // Verify the message against the AUDIT schema
      if (!this.validateSchema(peer.getJSONValidator().getSchema(JSONSchema.AUDIT))) {
        logger.warn("JSON Schema validation failed");
        throw new JSONSchemaValidationException("JSON Schema validation failed");
      }

      // Verify the serial signature
      if (!this.validateSerialSignature(peer)) {
        logger.warn("Serial Number signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Serial Number signature is not valid");
      }

      // Verify the signature from the Audit machine
      if (!this.validateAuditMachineSignature(peer)) {
        logger.warn("Audit machine signature is not valid:{}", this.msg);
        throw new MessageVerificationException("Audit Machine signature is not valid");
      }

      // Check the adjusted permutations.
      if (this.adjustedPerms == null) {
        if (!this.extractReducedPerms(peer)) {
          logger.warn("Cannot perform ballot reduction, either missing ciphers or POD data :{}", this.msg);
          throw new MessageVerificationException("Cannot perform ballot reduction, either missing ciphers or POD data");
        }
      }
    }
    catch (MessageSignatureException e) {
      logger.warn("Serial Number signature was not well formed:{}", this.msg);
      throw new MessageVerificationException("Serial Number signature was not well formed", e);
    }
    catch (MessageJSONException e) {
      logger.warn("Message is not well formed:{}", this.msg);
      throw new MessageVerificationException("Message is not well formed", e);
    }
    catch (JSONException e) {
      logger.warn("Error whilst trying to calculated Ballot Reduction prior to audit. Possible missing data. {}", this.msg);
      throw new MessageVerificationException("Error calculating Ballot Reduction on Audit", e);
    }
  }

  /**
   * Stores the incoming message in the database.
   * 
   * @param peer
   *          The WBB peer.
   * @param signature
   *          The signature of the message.
   * @return True if the message was successfully stored.
   * @throws IOException
   * @throws UnknownDBException
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.ExternalMessage#storeIncomingMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer,
   *      java.lang.String)
   */
  @Override
  protected boolean storeIncomingMessage(WBBPeer peer, String signature) throws IOException, UnknownDBException,
      NoSuchAlgorithmException, InvalidKeyException, SignatureException, JSONException {
    boolean messageStored;
    try {
      peer.getDB().submitIncomingClientMessage(DBRecordType.AUDIT, this, signature);
      messageStored = true;
      // If we get here the message has been stored, otherwise an exception would have been thrown
    }
    catch (AlreadyReceivedMessageException e) {
      logger.warn("Serial No {} already used. Sending error message to Client. msg:{}", this.getID(), this.msg);
      SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.SERIALNO_USED);
      messageStored = false;
    }

    if (messageStored) {
      byte[] witness = IOUtils.decodeData(EncodingType.BASE64, this.msg.getString(MessageFields.AuditMessage.COMMIT_WITNESS));
      String perm = this.msg.getString(MessageFields.AuditMessage.PERMUTATION);

      JSONObject cipherRecord = peer.getDB().getBallotRecord(this.id);

      if (!(cipherRecord == null)) {
        // TODO The cipherRecord Permutation is in fact a Permutation Commitment, not the actual Permutation. Create a new
        // MessageField for the cipher record to get the Permutation_Commitment - the underlying field name will be the same, this
        // is just for clarity
        String permCommit = cipherRecord.getString(MessageFields.AuditMessage.PERMUTATION);
        byte[] permutationCommit = IOUtils.decodeData(EncodingType.BASE64, permCommit);

        try {
          if (CryptoUtils.checkCommitment(permutationCommit, witness, perm.getBytes())) {
            return messageStored;
          }
          else {
            peer.getDB().setFieldInRecord(this.id, DBFields.AUDIT_FAILURE_STATUS, "Permutation Check Failed");
            logger.warn("Pemutation commitment is not the same. Sending error message to Client. msg:{}", this.msg);
            SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.COMMITMENT_FAILURE);

            return false;
          }
        }
        catch (CommitException e) {
          logger.warn("Error calculating commitment. msg:{}", this.msg, e);
          SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.COMMITMENT_FAILURE);

          return false;
        }
      }
      else {
        logger.warn("Serial No is invalid, no ballot ciphers found. msg:{}", this.msg);
        SendErrorMessage.sendErrorMessage(peer, this, ClientErrorMessage.UNKNOWN_SERIAL_NO);

        return false;
      }
    }
    else {
      return messageStored;
    }
  }

  /**
   * Verifies the signature on the received message came from a valid audit machine
   * 
   * @param peer
   *          the WBBPeer this is running - needed for access to keys
   * @return boolean of the outcome of signature verification
   * @throws MessageSignatureException
   * @throws MessageJSONException
   */
  private boolean validateAuditMachineSignature(WBBPeer peer) throws MessageSignatureException, MessageJSONException {
    try {

      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, peer.getCertificateFromCerts(CertStore.VPS,
          this.msg.getString(JSONWBBMessage.SENDER_ID)));

      String serialNo = this.msg.getString(JSONWBBMessage.ID);
      tvsSig.update(serialNo);
      tvsSig.update(this.getTypeString());
      tvsSig.update(this.msg.getString(MessageFields.AuditMessage.PERMUTATION));
      tvsSig.update(this.msg.getString(MessageFields.AuditMessage.COMMIT_WITNESS));

      return tvsSig.verify(this.msg.getString(JSONWBBMessage.SENDER_SIG), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageJSONException("ErrorCommitException when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageSignatureException("Error when initiating signature", e);
    }
  }
}
