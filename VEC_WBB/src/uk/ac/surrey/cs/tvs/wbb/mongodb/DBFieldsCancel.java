/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

/**
 * Singleton for cancel database field names.
 * 
 * @author Chris Culnane
 * 
 */
public class DBFieldsCancel extends DBFields {

  /**
   * Singleton instance.
   */
  private static final DBFieldsCancel instance = new DBFieldsCancel();

  /**
   * Private constructor to enforce singleton.
   */
  private DBFieldsCancel() {
    super();
  }

  /**
   * Converts an enum field into a name.
   * 
   * @param fieldName
   *          The enum field.
   * @return The string version of the field name.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields#getField(uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName)
   */
  @Override
  public String getField(DBFieldName fieldName) {
    switch (fieldName) {
      case CHECKED_SIGS:
        return "checkedCancellationSigs";
      case MY_SIGNATURE:
        return "mycancellationsig";
      case SENT_SIGNATURE:
        return "sentCancelSig";
      case SIGNATURE_COUNT:
        return "cancelSigCount";
      case SIGNATURES_TO_CHECK:
        return "cancellationsToCheck";
      case QUEUED_MESSAGE:
        return "msg";
      case FROM_PEER:
        return "fromPeer";
      case CLIENT_MESSAGE:
        return "clientCancellations";
      case COMMIT_TIME:
        return "commitTime";
      case POST_TIMEOUT:
        return "postTimeout";
      case SK2_SIGNATURE:
        return "mycancellationsigSK2";
      default:
        return null;
    }
  }

  /**
   * @return The singleton instance.
   */
  public static DBFieldsCancel getInstance() {
    return instance;
  }
}
