/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.x500.X500Principal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.VoteMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSCombiner;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * This class defines common test parameters and static initialisers within a singleton.
 */
public class TestParameters {

  public static final String CLIENT_POD_DEVICE_ID="TestDeviceOne";
  public static final String CLIENT_EVM_DEVICE_ID="TestEVMOne";
  public static final String CLIENT_RANDSERV_DEVICE_ID="MixServer1";
  /** Output log folder. */
  public static final String        LOG_FOLDER                              = "logs";

  /** Output data file. */
  public static final String        DATA_FILE                               = LOG_FOLDER + "/data.txt";

  /** Test ZIP file. */
  public static final String        TEST_GOOD_ZIP_FILE                      = "./testdata/test_good.zip";

  /** Test bad ZIP file. */
  public static final String        TEST_BAD_ZIP_FILE                       = "./testdata/test_bad.zip";
  

  /** Commit folder. */
  public static final String        COMMIT_FOLDER                           = "commits";

  /** Uploads folder. */
  public static final String        UPLOADS_FOLDER                          = "uploads";

  /** Output ZIP folder. */
  public static final String        ZIP_FOLDER                              = "zip";

  /** Commit file full prefix. */
  public static final String        COMMIT_FILE_FULL_PREFIX                 = "commitFull";

  /** Commit file full extension. */
  public static final String        COMMIT_FILE_FULL_EXT                    = ".json";

  /** Commit file full. */
  public static final String        COMMIT_FILE_FULL                        = COMMIT_FILE_FULL_PREFIX + COMMIT_FILE_FULL_EXT;

  /** Commit file prefix. */
  public static final String        COMMIT_FILE_PREFIX                      = "commit";

  /** Commit file extension. */
  public static final String        COMMIT_FILE_EXT                         = ".json";

  /** Commit file. */
  public static final String        COMMIT_FILE                             = COMMIT_FILE_PREFIX + COMMIT_FILE_EXT;

  /** Commit file ZIP prefix. */
  public static final String        COMMIT_FILE_ZIP_PREFIX                  = "commitFiles";

  /** Commit file ZIP extension. */
  public static final String        COMMIT_FILE_ZIP_EXT                     = ".zip";

  /** Commit file ZIP. */
  public static final String        COMMIT_FILE_ZIP                         = COMMIT_FILE_ZIP_PREFIX + COMMIT_FILE_ZIP_EXT;

  /** Test peer configuration files. */
  public static final String[]      PEER_CONFIGURATIONS                     = new String[] { "./testdata/wbbconfigPeer1.json",
      "./testdata/wbbconfigPeer2.json", "./testdata/wbbconfigPeer2.json"   };

  /** Test peer threshold. */
  public static final int           THRESHOLD                               = 3;

  /** Test message submission timeout in seconds. */
  public static final int           SUBMISSION_TIMEOUT                      = 30;

  /** Test message short submission timeout in seconds. */
  public static final int           SHORT_SUBMISSION_TIMEOUT                = 2;

  /** Test sleep interval. */
  public static final int           SLEEP_INTERVAL                          = 1000;

  /** Test upper buffer bound. */
  public static final int           BUFFER_BOUND                            = 10240;

  /** Test district configuration file. */
  public static final String        DISTRICT_CONFIG_FILE                    = "./testdata/districtconf.json";

  /** Test initial socket timeout. */
  public static final int           SOCKET_FAIL_TIMEOUT_INITIAL             = 2000;

  /** Test maximum socket timeout. */
  public static final int           SOCKET_FAIL_TIMEOUT_MAX                 = 30000;

  /** File upload timeout. */
  public static final int           FILE_UPLOAD_TIMEOUT                     = 30000;

  /** Test commit time hours. */
  public static final int           COMMIT_TIME_HOURS                       = 18;

  /** Test commit time minutes. */
  public static final int           COMMIT_TIME_MINUTES                     = 0;

  /** Test commit time. */
  public static final String        COMMIT_TIME                             = COMMIT_TIME_HOURS + ":" + COMMIT_TIME_MINUTES;
  /** Test commit time. */
  public static final String        COMMIT_DESC                            = null;

  /** Test run commit at hours. */
  public static final int           RUN_COMMIT_AT_HOURS                     = 19;

  /** Test run commit at minutes. */
  public static final int           RUN_COMMIT_AT_MINUTES                   = 0;

  /** Test message digest algorithm. */
  public static final String        MESSAGE_DIGEST_ALGORITHM                = "SHA1";

  /** Test information about peers as a parsable JSONArray string. */
  public static final String        WBB_PEER_DETAILS                        = "[{\"address\":\"127.0.0.1\",\"port\":9081,\"id\":\"Peer1_SSL\"},{\"address\":\"127.0.0.1\",\"port\":9082,\"id\":\"Peer2_SSL\"},{\"address\":\"127.0.0.1\",\"port\":9083,\"id\":\"Peer3_SSL\"}]";

  /** The listener address. */
  public static final String        LISTENER_ADDRESS                        = "127.0.0.1";

  /** The external listening port. */
  public static final int           EXTERNAL_PORT                           = 9091;

  /** The internal listening port. */
  public static final int           INTERNAL_PORT                           = 9081;

  /** An port not used by the WBB for SSL testing. */
  public static final int           TEST_PORT                               = 9095;

  /** Test POD to peer 1 signing certificate name. */
  public static final String        SIGNING_CERTIFICATE                     = "Peer1_SigningSK2";

  public static final String CLIENT_POD_KEYSTORE="./testdata/TestDeviceOneSigningKey.json";
  public static final String CLIENT_EVM_KEYSTORE="./testdata/TestEVMOneSigningKey.json";
  public static final String RAND_SERVER_KEYSTORE="./testdata/rand_serv_keys.bks";
  /** Test certificate. */
  public static final String        SENDER_CERTIFICATE                      = "Peer1_SigningSK1";

  /** Test peer ids. */
  public static final String[]      PEERS                                   = new String[] { "Peer1", "Peer2", "Peer3" };

  /** Test peer SSL ids. */
  public static final String[]      PEERS_SSL                               = new String[] { "Peer1_SSL", "Peer2_SSL", "Peer3_SSL" };

  /** The client key store. */
  public static final String        CLIENT_KEY_STORE                        = "testdata/keys/Peer1keys.jks";

  /** The client key store password. */
  public static final String        CLIENT_KEY_STORE_PASSWORD               = "";

  /** The server key store. */
  public static final String        SERVER_KEY_STORE                        = "testdata/keys/Peer1keys.jks";

  public static final String[]      PEER_SIGNING_KEYS                       = new String[] { "testdata/Peer1_private.bks",
      "testdata/Peer2_private.bks", "testdata/Peer3_private.bks"           };
  /** The server key store password. */
  public static final String        SERVER_KEY_STORE_PASSWORD               = "";

  /** Test UUID/session ID. */
  public static final UUID          SESSION_ID                              = UUID.randomUUID();

  /** Test principal. */
  public static final X500Principal PRINCIPAL                               = new X500Principal(
                                                                                "CN="
                                                                                    + PEERS_SSL[0]
                                                                                    + ", OU=Dev, O=SurreyPAV, L=Guildford, S=Surrey, C=GB");

  /** Timeout message string. */
  public static final String        TIMEOUT_MESSAGE                         = "Consensus was not reached within the timeout";

  /** Serial number used message string. */
  public static final String        SERIALNO_USED_MESSAGE                   = "Serial No has already been used";

  /** Time out on consensus message string. */
  public static final String        TIME_OUT_ON_CONSENSUS_MESSAGE           = "Did not get a threshold of valid responses from Peers. No consensus reached";

  /** No consensus possible message string. */
  public static final String        NO_CONSENSUS_POSSIBLE_MESSAGE           = "Received conflicting messages from Peers. No consensus possible";

  /** Unknown DB error message string. */
  public static final String        UNKNOWN_DB_ERROR_MESSAGE                = "Received an Unknown DB Error";

  /** Session timeout message string. */
  public static final String        SESSION_TIMEOUT_MESSAGE                 = "Consensus was not reached within the timeout";

  /** Commitment failure message string. */
  public static final String        COMMITMENT_FAILURE_MESSAGE              = "Commitment did not verify";

  /** Unknown serial number message string. */
  public static final String        UNKNOWN_SERIAL_NO_MESSAGE               = "Cannot find a record of the serial number";

  /** Could not parse JSON message string. */
  public static final String        COULD_NOT_PARSE_JSON_MESSAGE            = "Could not parse JSON message:";

  /** Unknown message type message string. */
  public static final String        UNKNOWN_MESSAGE_TYPE_MESSAGE            = "Unknown message type received:";

  /** Message failed validation message string. */
  public static final String        MESSAGE_FAILED_VALIDATION_MESSAGE       = "Message failed validation - rejected:";

  /** Signature check failed message string. */
  public static final String        SIGNATURE_CHECK_FAILED_MESSAGE          = "Signature check on received file failed - message rejected:";

  /** Index exceeded bound message string. */
  public static final String        INPUT_EXCEEDED_BOUND_MESSAGE            = "Input has exceeded bound on server socket - no newline character found. The data has been discarded:";

  /** Unknown error message string. */
  public static final String        UNKNOWN_ERROR_MESSAGE                   = "An unknown error has occured";

  /** Error message type. */
  public static final String        ERROR_MESSAGE                           = "ERROR";

  /** Default error message as a string. */
  public static final String        DEFAULT_ERROR_MESSAGE                   = "{type:\"ERROR\",msg:\"Unknown\"}";

  /** Test device. */
  public static final String        DEVICE                                  = "TestDeviceOne";

  /** Test serial number. */
  public static final String        SERIAL_NO                               = DEVICE + ":1";

  /** Test permutation. */
  public static final String        PERMUTATION                             = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15:1:1:";

  /** Test permutation padded to 32 bytes. */
  public static final String        PADDED_PERMUTATION                      = PERMUTATION + "                        ";

  /** Test district. */
  public static final String        DISTRICT                                = "Northcote";

  /** Test ballot reductions. */
  public static final String        BALLOT_REDUCTIONS                       = "[\"1\",\"2\",\"3\"]";

  /** Test message as string. */
  public static final String        MESSAGE                                 = "{\""
                                                                                + MessageFields.JSONWBBMessage.ID
                                                                                + "\":\""
                                                                                + SERIAL_NO
                                                                                + "\",\""
                                                                                + MessageFields.UUID
                                                                                + "\":\""
                                                                                + SESSION_ID
                                                                                + "\",\""
                                                                                + MessageFields.VoteMessage.RACES
                                                                                + "\":[{\"id\":\"LA\",\"preferences\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\"]},{\"id\":\"LC_ATL\",\"preferences\":[\"1\"]},{\"id\":\"LC_BTL\",\"preferences\":[\" \"]}]}";

  /** Test preferences. */
  public static final String        PREFERENCES                             = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15:1: :";

  /** Peer ID message field. */
  public static final String        PEER_ID_FIELD                           = "peerID";

  /** Serial signature message field. */
  public static final String        SERIAL_SIG_FIELD                        = "serialSig";

  /** Description message field. */
  public static final String        DESC_FIELD                              = "desc";

  /** Randomness message field. */
  public static final String        RANDOMNESS_FIELD                        = "randomness";

  /** Audit message type string. */
  public static final String        MESSAGE_TYPE_AUDIT_MESSAGE_STRING       = "audit";

  /** Ballot audit message type string. */
  public static final String        MESSAGE_TYPE_BALLOT_AUDIT_COMMIT_STRING = "ballotauditcommit";

  /** Ballot generation message type string. */
  public static final String        MESSAGE_TYPE_BALLOT_GEN_COMMIT_STRING   = "ballotgencommit";

  /** Cancel message type string. */
  public static final String        MESSAGE_TYPE_CANCEL_MESSAGE_STRING      = "cancel";

  /** Commit round 1 message type string. */
  public static final String        MESSAGE_TYPE_COMMIT_R1_STRING           = "commitr1";

  /** File message type string. */
  public static final String        MESSAGE_TYPE_FILE_MESSAGE_STRING        = "file";

  /** Generic message type string. */
  public static final String        MESSAGE_TYPE_GENERIC_STRING             = "generic";

  /** Mix random commit message type string. */
  public static final String        MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING   = "mixrandomcommit";

  /** Peer cancel message type string. */
  public static final String        MESSAGE_TYPE_PEER_CANCEL_MESSAGE_STRING = "peercancel";

  /** Peer file message type string. */
  public static final String        MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING   = "peerfile";

  /** Peer message type string. */
  public static final String        MESSAGE_TYPE_PEER_MESSAGE_STRING        = "peer";

  /** Peer POD message type string. */
  public static final String        MESSAGE_TYPE_PEER_POD_MESSAGE_STRING    = "peerpod";

  /** POD message type string. */
  public static final String        MESSAGE_TYPE_POD_MESSAGE_STRING         = "pod";

  /** Vote message type string. */
  public static final String        MESSAGE_TYPE_VOTE_MESSAGE_STRING        = "vote";

  /** Start EVM message type string. */
  public static final String        MESSAGE_TYPE_START_EVM_STRING           = "startevm";

  public static final String        MESSAGE_TYPE_HEARTBEAT_STRING           = "heartbeat";
  /** Client peer cancel type string. */
  public static final String        CLIENT_PEER_CANCEL_STRING               = "peercancel";

  /** Client peer POD type string. */
  public static final String        CLIENT_PEER_POD_STRING                  = "peerpod";

  /** Client peer type string. */
  public static final String        CLIENT_PEER_STRING                      = "peer";

  /** Default message type string. */
  public static final String        MESSAGE_TYPE_DEFAULT_STRING             = "UNKNOWN";

  /** Dummy exception message used for testing. */
  public static final String        EXCEPTION_MESSAGE                       = "Exception Message";

  /** Internal signable content. */
  public static final String        INTERNAL_CONTENT                        = "internalcontent";

  /** External signable content. */
  public static final String        EXTERNAL_CONTENT                        = "externalcontent";

  /** Test internal permutation string (not really a permutation, but something that can be identified). */
  public static final String        INTERNAL_PERMUTATION                    = "findmeunique";

  /** Statistic message type. */
  public static final String        TYPE                                    = "type";

  /** Memory message field. */
  public static final String        MEMORY                                  = "memory";

  /** Maximum memory message field. */
  public static final String        MAX_MEMORY                              = "maxmemory";

  /** Channels message field. */
  public static final String        CHANNELS                                = "channels";

  /** Peer statistic message. */
  public static final Object        PEER_STAT                               = "peerstat";

  /** Log message field. */
  public static final String        LOG_MESSAGE                             = "message";

  /** Log level field. */
  public static final String        LOG_LEVEL                               = "level";

  /** Log thread field. */
  public static final String        THREAD                                  = "thread";

  /** Log time stamp field. */
  public static final String        DATETIME                                = "datetime";

  /** Logging message type. */
  public static final Object        LOGGING                                 = "logging";

  /** Logging warning level. */
  public static final Object        WARN                                    = "WARN";

  /** Logging debug level. */
  public static final Object        DEBUG                                   = "DEBUG";

  /** Logging thread name. */
  public static final Object        MAIN                                    = "main";

  /** Command listener start log command. */
  public static final String        START_LOG                               = "startLog";

  /** Command listener stop log command. */
  public static final String        STOP_LOG                                = "stopLog";

  /** Command listener start memory command. */
  public static final String        START_MEMORY                            = "startmemory";

  /** Command listener stop memory command. */
  public static final String        STOP_MEMORY                             = "stopmemory";

  /** Command listener start peer statistics command. */
  public static final String        START_PEER                              = "startpeerstat";

  /** Command listener stop peer statistics command. */
  public static final String        STOP_PEER                               = "stoppeerstat";

  /** Command listener get commit data command. */
  public static final String        GET_COMMIT                              = "getcommitdata";

  /** Command listener level command. */
  public static final String        LEVEL                                   = "level";

  /** Command listener delay command. */
  public static final String        DELAY                                   = "delay";

  /** Command listener commit command. */
  public static final String        COMMIT                                  = "commit";

  /** Database field for post timeout. */
  public static final String        DB_POST_TIMEOUT                         = "postTimeout";

  /** Database field for commit time. */
  public static final String        DB_COMMIT_TIME                          = "commitTime";

  /** Database field for from peer. */
  public static final String        DB_FROM_PEER                            = "fromPeer";

  /** Database field for msg. */
  public static final String        DB_MSG                                  = "msg";

  /** Database field for signatures to check. */
  public static final String        DB_SIGS_TO_CHECK                        = "sigsToCheck";

  /** Database field for message. */
  public static final String        DB_MESSAGE                              = "message";

  /** Database field for sent timeout. */
  public static final String        DB_SENT_TIMEOUT                         = "sentTimeout";

  /** Database field for signature count. */
  public static final String        DB_SIG_COUNT                            = "sigCount";

  /** Database field for sent signature. */
  public static final String        DB_SENT_SIG                             = "sentSig";

  /** Database field for my signature. */
  public static final String        DB_MYSIG                                = "mysig";

  /** Database field for checked signatures. */
  public static final String        DB_CHECKED_SIGS                         = "checkedSigs";

  /** Database field for POD checked signatures. */
  public static final String        DB_CHECKED_SIGS_POD                     = "checkedSigsPOD";

  /** Database field for POD signature. */
  public static final String        DB_MYSIG_POD                            = "podsig";

  /** Database field for POD sent signature. */
  public static final String        DB_SENT_SIG_POD                         = "PODsentSig";

  /** Database field for POD signature count. */
  public static final String        DB_SIG_COUNT_POD                        = "PODsigCount";

  /** Database field for POD sent timeout. */
  public static final String        DB_SENT_TIMEOUT_POD                     = "sentTimeoutPOD";

  /** Database field for POD message. */
  public static final String        DB_MESSAGE_POD                          = "podmsg";

  /** Database field for POD signatures to check. */
  public static final String        DB_SIGS_TO_CHECK_POD                    = "sigsToCheckPOD";

  /** Database field for POD commit time. */
  public static final String        DB_COMMIT_TIME_POD                      = "PODCommitTime";

  /** Database field for checked cancellation signatures. */
  public static final String        DB_CHECKED_SIGS_CANCEL                  = "checkedCancellationSigs";

  /** Database field for cancellation signature. */
  public static final String        DB_MYSIG_CANCEL                         = "mycancellationsig";

  /** Database field for sent cancellation signature. */
  public static final String        DB_SIG_COUNT_CANCEL                     = "cancelSigCount";

  /** Database field for cancellation signature count. */
  public static final String        DB_SENT_SIG_CANCEL                      = "sentCancelSig";

  /** Database field for cancellation messages. */
  public static final String        DB_MESSAGE_CANCEL                       = "clientCancellations";

  /** Database field for cancellation signatures to check. */
  public static final String        DB_SIGS_TO_CHECK_CANCEL                 = "cancellationsToCheck";

  /** Database field for cancellation SK2 signature. */
  public static final String        DB_SK2_SIGNATURE_CANCEL                 = "mycancellationsigSK2";

  /** Database field for commit round 2 processed count. */
  public static final String        DB_ROUND2_PROCESSED_COUNT               = "R2ProcessedCount";

  /** Database field for commit sent signature. */
  public static final String        DB_COMMIT_SENT_SIG                      = "commitSentSig";

  /** Database field for commit SK2 signature. */
  public static final String        DB_COMMIT_SK2_SIG                       = "mySK2Sig";

  /** Database field for commit current commit. */
  public static final String        DB_COMMIT_CURRENT_COMMIT                = "currentCommit";

  /** Database field for response. */
  public static final String        DB_RESPONSE                             = "response";

  /** Database field for round 2 commit running. */
  public static final String        DB_COMMIT_RUNNING_R2                    = "runningR2";

  /** Database field for round 2 commits. */
  public static final String        DB_R2_COMMITS                           = "R2Commits";

  /** Database field for round 1 commit hash. */
  public static final String        DB_COMMIT_HASH_R1                       = "hashR1";

  /** Database field for round 1 commit file. */
  public static final String        DB_COMMIT_FILE_R1                       = "commitFileR1";

  /** Database field for round 1 commit file full. */
  public static final String        DB_COMMIT_FILE_FULL_R1                  = "commitFileFullR1";

  /** Database field for round 1 commit attachments. */
  public static final String        DB_COMMIT_ATTACHMENTS_R1                = "attachmentsR1";

  /** Test upload file name. */
  public static final String        UPLOAD_FILE                             = "upload.txt";

  /** The data bound for a bounded buffered input stream. */
  public static final int           DATA_BOUND                              = 10240;

  /** Test ZIP content JSON object. */
  public static final String        ZIP_CIPHER                              = "{\"test\":\"hello\"}";

  /** Test string data. */
  public static final String        LINE                                    = "123456789\n";

  /** Upload directory. */
  public static final String        UPLOAD_DIRECTORY                        = "./testdata/temp/uploads/Peer1/uploads/";

  /** Safe upload directory. */
  public static final String        SAFE_UPLOAD_DIRECTORY                   = "./testdata/temp/uploads/Peer1/commit_uploads/";

  /** Test database name. */
  public static final String        TEST_DATABASE                           = "test_wbb_database";

  /** SSL cipher suite. */
  public static final String        CIPHERS                                 = "SSL_RSA_WITH_NULL_SHA";

  /** Output region file. */
  public static final String        OUTPUT_REGION_FILE                      = LOG_FOLDER + "/region.txt";                                                                                                                                                                       ;

  /** Output district file. */
  public static final String        OUTPUT_DISTRICT_FILE                    = LOG_FOLDER + "/district.txt";                                                                                                                                                                     ;

  /** Output certificate/key folder. */
  public static final String        CSR_FOLDER                              = "new_certs";

  /** Output key store. */
  public static final String        OUTPUT_KEYSTORE                         = CSR_FOLDER + "/" + "keystore.jks";

  /** Default buffer size to use when copying files. */
  private static final int          BUFFER_SIZE                             = 8192;

  /** The singleton instance of these parameters. */
  private static TestParameters     instance                                = null;

  /** Test WBB peer. */
  private DummyWBBPeer              wbbPeer                                 = null;

  /** The peer information. */
  private JSONArray                 wbbPeers                                = null;

  /** The district information. */
  private JSONObject                district                                = null;

  /** The SSL socket factory to make client connections. */
  private SSLSocketFactory          clientFactory                           = null;

  /** The SSL socket factory to make server connections. */
  private ServerSocketFactory       serverFactory                           = null;

  /** The test MongoDB. */
  private MongoClient               mongoClient                             = null;

  /** The test MongoDB database. */
  private DB                        db                                      = null;

  /** The commit witness. */
  private byte[]                    witness                                 = null;

  /**
   * Private default constructor to prevent instantiation.
   */
  private TestParameters() {
    super();

    // Initialise BouncyCastle.
    CryptoUtils.initProvider();
  }

  /**
   * Closes the MongoDB.
   */
  public void closeDB() {
    if (this.mongoClient != null) {
      this.mongoClient.close();
      this.mongoClient = null;
      this.db = null;
    }
  }

  /**
   * Initialises and returns the SSL socket factory needed to create client SSL connections.
   * 
   * @return The SSL socket factory.
   * @throws PeerSSLInitException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   */
  public SSLSocketFactory getClientSSLSocketFactory() throws PeerSSLInitException, KeyManagementException, NoSuchAlgorithmException {
    // Lazy creation.
    if (this.clientFactory == null) {
      String pathToKS = TestParameters.CLIENT_KEY_STORE;
      char[] pwd = TestParameters.CLIENT_KEY_STORE_PASSWORD.toCharArray();

      // Initialise the trust store for the client key.
      FileInputStream keyStoreStream = null;
      TrustManagerFactory tmf = null;
      KeyManagerFactory kmf = null;

      try {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStoreStream = new FileInputStream(pathToKS);
        keyStore.load(keyStoreStream, pwd);

        tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore);

        kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, pwd);
      }
      catch (Exception e) {
        throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores", e);
      }
      finally {
        if (keyStoreStream != null) {
          try {
            keyStoreStream.close();
          }
          catch (IOException ie) {
            throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores (Closing File)", ie);
          }
        }
      }

      // Create the client socket factory using the trust store.
      SSLContext context = SSLContext.getInstance("TLS");
      context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
      this.clientFactory = context.getSocketFactory();
    }

    return this.clientFactory;
  }

  /**
   * @return the district.
   * @throws JSONIOException
   */
  public JSONObject getDistrict() throws JSONIOException {
    // Lazy creation.
    if (this.district == null) {
      this.district = IOUtils.readJSONObjectFromFile(DISTRICT_CONFIG_FILE);
    }

    return this.district;
  }

  /**
   * @return The encoded commitment witness.
   */
  public String getEncodedWitness() {
    return IOUtils.encodeData(EncodingType.BASE64, this.getWitness());
  }

  /**
   * Initialises and returns the SSL socket factory needed to create server SSL connections.
   * 
   * @return The SSL socket factory.
   * @throws PeerSSLInitException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   */
  public ServerSocketFactory getServerSSLSocketFactory() throws PeerSSLInitException, KeyManagementException,
      NoSuchAlgorithmException {
    // Lazy creation.
    if (this.serverFactory == null) {
      String pathToKS = TestParameters.SERVER_KEY_STORE;
      char[] pwd = TestParameters.SERVER_KEY_STORE_PASSWORD.toCharArray();

      // Initialise the trust store for the client key.
      FileInputStream keyStoreStream = null;
      TrustManagerFactory tmf = null;
      KeyManagerFactory kmf = null;

      try {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStoreStream = new FileInputStream(pathToKS);
        keyStore.load(keyStoreStream, pwd);

        tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore);

        kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, pwd);
      }
      catch (Exception e) {
        throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores", e);
      }
      finally {
        if (keyStoreStream != null) {
          try {
            keyStoreStream.close();
          }
          catch (IOException ie) {
            throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores (Closing File)", ie);
          }
        }
      }

      // Create the client socket factory using the trust store.
      SSLContext context = SSLContext.getInstance("TLS");
      context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
      this.serverFactory = context.getServerSocketFactory();
    }

    return this.serverFactory;
  }

  /**
   * @return the test WBB peer.
   * @throws PeerInitException
   */
  public DummyWBBPeer getWbbPeer() throws PeerInitException {
    // Lazy creation.
    if (this.wbbPeer == null) {
      this.wbbPeer = new DummyWBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);
    }

    return this.wbbPeer;
  }

  /**
   * @return the WBB peer information
   * @throws JSONException
   */
  public JSONArray getWbbPeers() throws JSONException {
    // Lazy creation.
    if (this.wbbPeers == null) {
      this.wbbPeers = new JSONArray(WBB_PEER_DETAILS);
    }

    return this.wbbPeers;
  }

  /**
   * @return The commitment witness.
   */
  public byte[] getWitness() {
    // Lazy creation.
    if (this.witness == null) {
      SecureRandom random = new SecureRandom();
      this.witness = new byte[256 / 8];
      random.nextBytes(this.witness);
    }

    return this.witness;
  }

  /**
   * @return The test MongoDB database.
   * @throws UnknownHostException
   */
  public DB openDB() throws UnknownHostException {
    // Lazy creation.
    if (this.db == null) {
      this.mongoClient = new MongoClient();
      this.db = this.mongoClient.getDB(TEST_DATABASE);
    }

    return this.db;
  }

  /**
   * Resets the WBB instance so that we can re-create the sockets.
   * 
   * @throws PeerInitException
   * @throws IOException
   */
  public void resetWBB() throws IOException, PeerInitException {
    TestParameters.tidySockets();
    this.wbbPeer.reset();
    this.wbbPeer = null;
  }

  /**
   * Compares the content of two files.
   * 
   * @param file1
   *          Input file 1.
   * @param file2
   *          Input file 2.
   * @return True if the files are identical, false otherwise.
   * @throws IOException
   *           on failure to read.
   */
  public static boolean compareFile(File file1, File file2) throws IOException {
    boolean result = true;

    byte[] buffer1 = new byte[BUFFER_SIZE];
    byte[] buffer2 = new byte[BUFFER_SIZE];

    InputStream in1 = null;
    InputStream in2 = null;

    try {
      in1 = new FileInputStream(file1);
      in2 = new FileInputStream(file2);

      int numBytes1 = in1.read(buffer1);
      int numBytes2 = in2.read(buffer2);

      while ((numBytes1 != -1) && (numBytes2 != -1) && result) {
        if (numBytes1 != numBytes2) {
          result = false;
        }
        else {
          for (int i = 0; i < numBytes1; i++) {
            if (buffer1[i] != buffer2[i]) {
              result = false;
            }
          }
        }

        numBytes1 = in1.read(buffer1);
        numBytes2 = in2.read(buffer2);
      }
    }
    // Throw exceptions up.
    finally {
      if (in1 != null) {
        in1.close();
      }
      if (in2 != null) {
        in2.close();
      }
    }

    return result;
  }

  /**
   * Compares the content of the file with the specified JSON objects. Does not do a deep compare and only compares key values as
   * strings. If the target key is an array, only the first element is extracted for comparison.
   * 
   * @param file
   *          The file to check.
   * @param objects
   *          The JSON objects expected.
   * @param key
   *          The optional key to use to extract a sub-object from the file's line.
   * @return True if the objects are in the file in the correct order and no more, false otherwise.
   * @throws IOException
   */
  public static boolean compareFile(File file, JSONObject[] objects, String key) throws IOException {
    boolean result = true;

    BufferedReader in = null;

    try {
      in = new BufferedReader(new FileReader(file));

      int count = 0;
      String line = in.readLine();

      while ((line != null) && result) {
        if (count >= objects.length) {
          result = false;
        }
        else {
          try {
            JSONObject object = new JSONObject(line);

            if (key != null) {
              String value = object.get(key).toString();

              if (value.startsWith("[")) {
                JSONArray array = new JSONArray(value);

                // Just get the first element.
                object = new JSONObject(array.get(0).toString());
              }
              else {
                object = new JSONObject(value);
              }
            }

            for (String field : JSONObject.getNames(objects[count])) {
              if (object.has(field)) {
                if (!object.get(field).toString().equals(objects[count].get(field).toString())) {
                  result = false;
                }
              }
              else {
                result = false;
              }
            }
          }
          catch (JSONException e) {
            result = false;
          }
        }

        count++;
        line = in.readLine();
      }
    }
    // Throw exceptions up.
    finally {
      if (in != null) {
        in.close();
      }
    }

    return result;
  }

  /**
   * Compares the content of the file with the specified lines.
   * 
   * @param file
   *          The file to check.
   * @param lines
   *          The lines expected in the file in the correct order.
   * @return True if the lines are in the file in the correct order and no more, false otherwise.
   * @throws IOException
   */
  public static boolean compareFile(File file, String[] lines) throws IOException {
    boolean result = true;

    BufferedReader in = null;

    try {
      in = new BufferedReader(new FileReader(file));

      int count = 0;
      String line = in.readLine();

      while ((line != null) && result) {
        if (count >= lines.length) {
          result = false;
        }
        else if (!line.equals(lines[count])) {
          result = false;
        }

        count++;
        line = in.readLine();
      }
    }
    // Throw exceptions up.
    finally {
      if (in != null) {
        in.close();
      }
    }

    return result;
  }

  /**
   * Copies the source file to the destination.
   * 
   * @param source
   *          The source file.
   * @param destination
   *          The destination file.
   * @throws IOException
   * @throws FileNotFoundException
   */
  public static void copyFile(File source, File destination) throws FileNotFoundException, IOException {
    copyStream(new FileInputStream(source), new FileOutputStream(destination));
  }

  /**
   * Copies an input stream to an output stream. Both streams are left at the end of their read/write.
   * 
   * @param in
   *          The destination stream.
   * @param out
   *          The source stream.
   * 
   * @throws IOException
   *           on failure to read or write.
   */
  public static void copyStream(InputStream in, OutputStream out) throws IOException {
    byte[] buffer = new byte[BUFFER_SIZE];

    int numBytes = in.read(buffer);

    while (numBytes != -1) {
      out.write(buffer, 0, numBytes);
      numBytes = in.read(buffer);
    }
  }

  /**
   * Creates and populates a commit file.
   * 
   * @param file
   *          The file to create and populate.
   * @throws IOException
   * @throws MessageJSONException
   * @throws JSONException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   */
  public static void createCommitFile(File file) throws IOException, MessageJSONException, JSONException, TVSSignatureException,
      CryptoIOException {
    // Create commit messages for testing.
    JSONObject record = new JSONObject();

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };
    String signature = TestParameters.signData(data);

    for (int i = 0; i < 10; i++) { // Arbitrary number.
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.setType(Message.VOTE_MESSAGE);

      JSONObject message = new JSONObject(TestParameters.MESSAGE);
      message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
      message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
      message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
      message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
      message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
      message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
      message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
      message.put(VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

      record.put(DBFields.getInstance().getField(DBFieldName.CLIENT_MESSAGE), message.toString());
    }

    // Write the record to the file.
    new File(file.getParent()).mkdirs();

    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
    bw.write(record.toString());
    bw.write("\n");
    bw.close();
  }

  /**
   * Creates and populates a test file.
   * 
   * @param file
   *          The file to create and populate.
   * @throws IOException
   */
  public static void createTestFile(File file) throws IOException {
    new File(file.getParent()).mkdirs();

    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
    bw.write(LINE);
    bw.close();
  }

  /**
   * Recursive method to delete a folder and its content. If something goes wrong, this method remains silent.
   * 
   * @param file
   *          The file/folder to delete.
   */
  public static void deleteRecursive(File file) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive(child);
      }
    }

    file.delete();
  }

  /**
   * Extracts a zip file into the specified directory.
   * 
   * @param source
   *          The ZIP file.
   * @param target
   *          The target directory.
   * @return True if the extraction was successful, false otherwise.
   * @throws IOException
   */
  public static boolean extractZip(File source, File target) throws IOException {
    // Open the ZIP archive.
    ZipInputStream zipInputStream = null;
    OutputStream outputStream = null;
    boolean success = false;

    try {
      zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(source)));

      ZipEntry zipEntry = zipInputStream.getNextEntry();

      while (zipEntry != null) {
        // Extract the file name, including any sub-directory names. Sub-directories are created.
        File outputFile = new File(target, zipEntry.getName());
        outputFile.getParentFile().mkdirs();

        // Extract the file.
        outputStream = new FileOutputStream(outputFile);
        copyStream(zipInputStream, outputStream);
        outputStream.close();
        outputStream = null;

        // Get the next entry.
        zipEntry = zipInputStream.getNextEntry();
      }

      success = true;
    }
    // Throw exceptions up.
    finally {
      // Close any open output stream.
      if (outputStream != null) {
        outputStream.close();
      }

      // Close the input stream.
      if (zipInputStream != null) {
        zipInputStream.close();
      }
    }

    return success;
  }

  /**
   * Creates a buffer from a string with the specified number of lines each of 10 characters long (including newline).
   * 
   * @param lines
   *          The number of lines required.
   * @return The created string.
   */
  public static String getData(int lines) {
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < lines; i++) {
      buffer.append(LINE);
    }

    return buffer.toString();
  }

  /**
   * Singleton access method.
   * 
   * @return The test parameters singleton instance.
   */
  public static TestParameters getInstance() {
    // Lazy creation.
    if (instance == null) {
      instance = new TestParameters();
    }

    return instance;
  }

  /**
   * Generates a set of serial signatures for the specified sending peer.
   * 
   * @param sender
   *          The sending peer.
   * @param data
   *          The data to sign in the signature.
   * @return The serial signatures.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   * @throws JSONException
   */
  public static String getSerialSignatures(String sender, String[] data) throws TVSSignatureException, CryptoIOException,
      JSONException {
    BLSCombiner blsCombiner = new BLSCombiner(3, 3);
    try {
      for (int i = 0; i < TestParameters.PEERS.length; i++) {
        TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        tvsKeyStore.load(TestParameters.PEER_SIGNING_KEYS[i], "".toCharArray());

        blsCombiner.addShare(
            IOUtils.decodeData(EncodingType.BASE64,
                signData(data, tvsKeyStore.getBLSPrivateKey(TestParameters.PEERS[i] + "_SigningSK2"))), i);
        // String signature = "{\"WBBID\":\"" + TestParameters.PEERS[i] + "\"" + ",\"WBBSig\":\"" + + "\"}";
        // String signatures = "";

      }
      //TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      //tvsKeyStore.load(TestParameters.PEER_SIGNING_KEYS[0], "".toCharArray());
      //TVSSignature tvsSig = new TVSSignature(tvsKeyStore.getBLSPublicKey("Peer1_SigningSK2"), false);
      //for (String s : data) {
      //  System.out.println("Adding:" + s);
      //  tvsSig.update(s);
      //}
      //System.out.println(tvsSig.verify(blsCombiner.combined().toBytes()));

    }
    catch (KeyStoreException e) {
      throw new CryptoIOException(e);
    }
    // TVSSignature tvsSig = new TVSSignature();
    return IOUtils.encodeData(EncodingType.BASE64, blsCombiner.combined().toBytes());
    // PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer1keys.jks", "", "Peer1_SigningSK2", "");
    // TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    // tvsKeyStore.load("./testdata/Peer1_private.bks", "".toCharArray());
    // tvsKeyStore.getBLSPrivateKey("Peer1_SigningSK2");
    // BLSPrivateKey blsKey = ,
    // String signature = "{\"WBBID\":\"" + sender + "\"" + ",\"WBBSig\":\"" + signData(data, key) + "\"}";
    // String signatures = "";

    // for (int i = 0; i < 3; i++) {
    // signatures += signature + ",";
    // }

    // return new JSONArray("[" + signatures + "]");
  }

  /**
   * Determines if the specified file is empty or not.
   * 
   * @param file
   *          The file to check.
   * @return True if the file is empty and false otherwise.
   * @throws IOException
   */
  public static boolean isEmpty(File file) throws IOException {
    boolean result = true;

    InputStream in = null;

    try {
      in = new FileInputStream(file);
      result = (in.read() == -1);
    }
    // Throw exceptions up.
    finally {
      if (in != null) {
        in.close();
      }
    }

    return result;
  }

  /**
   * Signs the specified content using the POD to Peer1 signing key.
   * 
   * @param data1
   *          Byte data to sign.
   * @param data2
   *          String data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(byte[] data1, String data2) throws TVSSignatureException, CryptoIOException {
    return signData(data1, data2, "Peer1");
  }

  /**
   * Signs the specified content using the specified peer.
   * 
   * @param data1
   *          Byte data to sign.
   * @param data2
   *          String data to sign.
   * @param The
   *          peer name.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(byte[] data1, String data2, String peer) throws TVSSignatureException, CryptoIOException {
    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/" + peer + "keys.jks", "", peer + "_SigningSK1", "");
    TVSSignature signature = new TVSSignature(SignatureType.DSA, key);

    signature.update(data1);
    signature.update(data2);

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Signs the specified content using the POD to Peer1 signing key.
   * 
   * @param data
   *          The data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String data) throws TVSSignatureException, CryptoIOException {
    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer1keys.jks", "", "Peer1_SigningSK1", "");
    TVSSignature signature = new TVSSignature(SignatureType.DSA, key);
    signature.update(data);

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Signs the specified content using the specified signing key.
   * 
   * @param data
   *          The data to sign.
   * @param key
   *          The signing private key.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String data, PrivateKey key) throws TVSSignatureException, CryptoIOException {
    TVSSignature signature = new TVSSignature(SignatureType.DEFAULT, key);
    signature.update(data);

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Signs the specified content using specified BLS signing key
   * 
   * @param data
   *          The data to sign.
   * @param key
   *          The signing private key.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String data, BLSPrivateKey key) throws TVSSignatureException, CryptoIOException {
    TVSSignature signature = new TVSSignature(SignatureType.BLS, key);
    signature.update(data);

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Signs the specified content using the POD to Peer1 signing key.
   * 
   * @param data
   *          Array of data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String[] data) throws TVSSignatureException, CryptoIOException {
    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer1keys.jks", "", "Peer1_SigningSK1", "");
    TVSSignature signature = new TVSSignature(SignatureType.DSA, key);

    for (int i = 0; i < data.length; i++) {
      signature.update(data[i]);
    }

    return signature.signAndEncode(EncodingType.BASE64);
  }
  /**
   * Signs the specified content using the POD to Peer1 signing key.
   * 
   * @param data
   *          Array of data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   * @throws KeyStoreException 
   */
  public static String signBLSData(String[] data,String clientKeyStore) throws TVSSignatureException, CryptoIOException, KeyStoreException {
   
   return signBLSData(data,clientKeyStore,"clientSigningKey");
  }
  /**
   * Signs the specified content using the POD to Peer1 signing key.
   * 
   * @param data
   *          Array of data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   * @throws KeyStoreException 
   */
  public static String signBLSData(String[] data,String clientKeyStore,String entity) throws TVSSignatureException, CryptoIOException, KeyStoreException {
   
    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.load(clientKeyStore, "".toCharArray());
    
    TVSSignature signature = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(entity));

    for (int i = 0; i < data.length; i++) {
      signature.update(data[i]);
    }

    return signature.signAndEncode(EncodingType.BASE64);
  }
    

  /**
   * Signs the specified content using the key.
   * 
   * @param data
   *          Array of data to sign.
   * @param key
   *          The private key.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String[] data, PrivateKey key) throws TVSSignatureException, CryptoIOException {
    TVSSignature signature = new TVSSignature(SignatureType.DSA, key);

    for (int i = 0; i < data.length; i++) {
      signature.update(data[i]);
    }

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Signs the specified content using the key.
   * 
   * @param data
   *          Array of data to sign.
   * @param key
   *          The bls private key.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String[] data, BLSPrivateKey key) throws TVSSignatureException, CryptoIOException {
    TVSSignature signature = new TVSSignature(SignatureType.BLS, key);

    for (int i = 0; i < data.length; i++) {
      signature.update(data[i]);
    }

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Deletes the test MongoDB database.
   * 
   * @throws UnknownHostException
   */
  public static void tidyDatabase() throws UnknownHostException {
    MongoClient mongoClient = new MongoClient();
    mongoClient.dropDatabase(TEST_DATABASE);
    mongoClient.close();
  }

  /**
   * Deletes all of the test output files that are generated.
   */
  public static void tidyFiles() {
    deleteRecursive(new File(CSR_FOLDER));
    deleteRecursive(new File(OUTPUT_REGION_FILE));
    deleteRecursive(new File(OUTPUT_DISTRICT_FILE));
    deleteRecursive(new File(DATA_FILE));
    deleteRecursive(new File(COMMIT_FOLDER));
    deleteRecursive(new File(UPLOADS_FOLDER));
    deleteRecursive(new File(ZIP_FOLDER));
  }

  /**
   * Closes all WBB sockets created during testing.
   * 
   * @throws PeerInitException
   * @throws IOException
   */
  public static void tidySockets() throws IOException, PeerInitException {
    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    getInstance().getWbbPeer().getPeerServerSocket().close();
    getInstance().getWbbPeer().getPeerExternalServerSocket().close();

    // Reset the peer.
    getInstance().getWbbPeer().reset();
  }

  /**
   * Signs the specified content using the specified signing key.
   * 
   * @param data
   *          The signature to verify.
   * @param key
   *          The signing private key.
   * @return True if the signature is valid.
   * @throws TVSSignatureException
   */
  public static boolean verifySignature(String data, PrivateKey key) throws TVSSignatureException {
    TVSSignature signature = new TVSSignature(SignatureType.DEFAULT, key);

    return signature.verify(data, EncodingType.BASE64);
  }

  /**
   * Wait for the specified number of loops.
   * 
   * @param count
   *          The number of loops.
   * @return
   */
  public static void wait(int count) {
    for (int i = 0; i < count; i++) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }
    }
  }
}
