/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;

/**
 * The class <code>CommitR1MessageTest</code> contains tests for the class <code>{@link CommitR1Message}</code>.
 */
public class CommitR1MessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);

    CommitR1Message fixture = new CommitR1Message(object);

    // Create a peer message.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    // Test invalid signature.
    db.setHash(TestParameters.signData(TestParameters.PEERS[0]).getBytes());

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertEquals(fixture, db.getMessage());
    assertFalse(db.isValid());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);

    CommitR1Message fixture = new CommitR1Message(object);

    // Create a peer message.
    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), DBFields.COMMITMENT_TIME);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, signature);
    PeerMessage peerMessage = new PeerMessage(message);

    // Test valid signature.
    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertEquals(fixture, db.getMessage());
    // TODO This fails because the DB returns a CommitDescription, despite the commitDescription never having been set. If a
    // commitDescription is included it should be in the signature
    assertTrue(db.isValid());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);

    CommitR1Message fixture = new CommitR1Message(object);

    // Create a peer message.
    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, signature);
    PeerMessage peerMessage = new PeerMessage(message);

    // Test valid signature with exception.
    db.addException("getCurrentCommitHash", 0);
    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertFalse(db.isSubmitIncomingPeerCommitR1CheckedCalled());
  }

  /**
   * Run the CommitR1Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCommitR1Message_1() throws Exception {
    CommitR1Message result = new CommitR1Message("");
    assertNull(result);
  }

  /**
   * Run the CommitR1Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitR1Message_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);

    CommitR1Message result = new CommitR1Message(object.toString());
    assertNotNull(result);
  }

  /**
   * Run the CommitR1Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCommitR1Message_3() throws Exception {
    CommitR1Message result = new CommitR1Message(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the CommitR1Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitR1Message_4() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);

    CommitR1Message result = new CommitR1Message(object);
    assertNotNull(result);
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    // Test with a from peer already defined.
    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    // Test with a from peer null.
    JSONObject internal = fixture.getMsg();
    internal.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, TestParameters.signData(TestParameters.PEERS[0]));

    CommitR1Message fixture = new CommitR1Message(object.toString());

    // Test valid.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), DBFields.COMMITMENT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Test valid.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertEquals(fixture, db.getMessage());
    assertTrue(db.isValid());
    // TODO This fails because the DB returns a CommitDescription, despite the commitDescription never having been set. If a
    // commitDescription is included it should be in the signature
    assertTrue(db.isCheckCommitThresholdCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), DBFields.COMMITMENT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Test already received.
    db.addException("submitIncomingPeerCommitR1Checked", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertEquals(fixture, db.getMessage());
    assertTrue(db.isValid());
    // TODO This fails because the DB returns a CommitDescription, despite the commitDescription never having been set. If a
    // commitDescription is included it should be in the signature
    assertFalse(db.isCheckCommitThresholdCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Test no commit.
    db.addException("getCurrentCommitHash", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerCommitR1UnCheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckCommitThresholdCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Test no commit and unchecked exception.
    db.addException("getCurrentCommitHash", 0);
    db.addException("submitIncomingPeerCommitR1UnChecked", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerCommitR1UnCheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertFalse(db.isCheckCommitThresholdCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message fixture = new CommitR1Message(object.toString());

    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Test no commit and unchecked exception.
    db.addException("getCurrentCommitHash", 0);
    db.addException("submitIncomingPeerCommitR1UnChecked", 1);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerCommitR1UnCheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertFalse(db.isCheckCommitThresholdCalled());
  }
}