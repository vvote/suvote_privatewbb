/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;

/**
 * The class <code>MixRandomCommitMessageTest</code> contains tests for the class <code>{@link MixRandomCommitMessage}</code>.
 */
public class MixRandomCommitMessageTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    MixRandomCommitMessage fixture = new MixRandomCommitMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + TestParameters.SENDER_CERTIFICATE + TestParameters.DEVICE + digest
        + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the MixRandomCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testMixRandomCommitMessage_1() throws Exception {
    // Test invalid source message.
    MixRandomCommitMessage result = new MixRandomCommitMessage("");
    assertNull(result);
  }

  /**
   * Run the MixRandomCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMixRandomCommitMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid source message.
    MixRandomCommitMessage result = new MixRandomCommitMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.MIX_RANDOM_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the MixRandomCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMixRandomCommitMessage_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    // Test valid source message with filename and digest.
    MixRandomCommitMessage result = new MixRandomCommitMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.MIX_RANDOM_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.fileName);
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(digest, result.digest);
  }

  /**
   * Run the MixRandomCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testMixRandomCommitMessage_4() throws Exception {
    // Test invalid source message.
    MixRandomCommitMessage result = new MixRandomCommitMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the MixRandomCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMixRandomCommitMessage_5() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid source message.
    MixRandomCommitMessage result = new MixRandomCommitMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.MIX_RANDOM_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the MixRandomCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMixRandomCommitMessage_6() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    // Test valid source message with filename and digest.
    MixRandomCommitMessage result = new MixRandomCommitMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.MIX_RANDOM_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.fileName);
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(digest, result.digest);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    MixRandomCommitMessage fixture = new MixRandomCommitMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    MixRandomCommitMessage fixture = new MixRandomCommitMessage(object);

    // Test missing digest for signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    MixRandomCommitMessage fixture = new MixRandomCommitMessage(object);

    // Test signature validation failure.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_4() throws Exception {
    
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_RANDSERV_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.RAND_SERVER_KEYSTORE,TestParameters.CLIENT_RANDSERV_DEVICE_ID));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    MixRandomCommitMessage fixture = new MixRandomCommitMessage(object);

    // Test with valid signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean preProcessMessage(WBBPeer,BoundedBufferedInputStream,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreProcessMessage_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    MixRandomCommitMessage fixture = new MixRandomCommitMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.DATA_FILE);

    TestParameters.createTestFile(file);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with an empty ZIP file. Just make sure the super method is called.
    DummySocket socket = new DummySocket();

    boolean result = fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket);
    assertFalse(result);
  }
}