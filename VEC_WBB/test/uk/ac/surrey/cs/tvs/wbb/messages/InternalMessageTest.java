/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;

/**
 * The class <code>InternalMessageTest</code> contains tests for the class <code>{@link InternalMessage}</code>.
 */
public class InternalMessageTest {

  /**
   * Concrete implementation of abstract JSONWBBMessage class for testing.
   */
  private class DummyInternalMessage extends InternalMessage {

    public DummyInternalMessage(JSONObject msg) throws MessageJSONException {
      super(msg);
    }

    public DummyInternalMessage(String msgString) throws MessageJSONException {
      super(msgString);
    }

    @Override
    public String getExternalSignableContent() throws JSONException {
      return TestParameters.EXTERNAL_CONTENT;
    }

    @Override
    public String getInternalSignableContent() throws JSONException {
      return TestParameters.INTERNAL_CONTENT;
    }

    @Override
    public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
      throw new MessageVerificationException("invalid message");
    }

    @Override
    public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
        InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
      return false;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(TestParameters.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.ID, "");
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    DummyInternalMessage fixture = new DummyInternalMessage(message);

    // Test the threshold when consensus has been reached.
    db.setReachedConsensus(true);
    db.setJsonObject(fixture.getMsg());

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isGetClientMessageCalled());
    assertFalse(db.isCanOrHaveReachedConsensusCalled());

    assertEquals(fixture.getID(), db.getSerialNo());

    // Test that the message was sent with the correct content.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();
    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);

    assertTrue(sent.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(fixture.getID(), sent.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertNotNull(sent.getString(MessageFields.JSONWBBMessage.PEER_SIG));
    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, sent.getString(MessageFields.JSONWBBMessage.TYPE));
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(TestParameters.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.ID, "");
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    DummyInternalMessage fixture = new DummyInternalMessage(message);

    // Test the threshold when consensus has been reached but without a client message.
    db.setReachedConsensus(true);
    db.setJsonObject(null);

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isGetClientMessageCalled());
    assertFalse(db.isCanOrHaveReachedConsensusCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(TestParameters.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.ID, "");
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    DummyInternalMessage fixture = new DummyInternalMessage(message);

    // Test the threshold when consensus has not been reached and cannot be.
    db.setReachedConsensus(false);
    db.setCanReachConsensus(false);
    db.setJsonObject(message);
    // TODO This returns false because it cannot find the message when it tries to get it, even though consensus cannot be reached
    // which implies a client message must have been received. This currently logs an error and returns false - a client message
    // needs storing for it to pass
    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isCanOrHaveReachedConsensusCalled());

    // Test that the error message was sent with the correct content.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();

    assertEquals(fixture.getID(), sent.getString(MessageFields.ErrorMessage.ID));
    assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.ErrorMessage.PEER_ID));
    assertNotNull(sent.get(MessageFields.ErrorMessage.PEER_SIG));
    assertEquals(TestParameters.NO_CONSENSUS_POSSIBLE_MESSAGE, sent.getString(MessageFields.ErrorMessage.MESSAGE));

    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(TestParameters.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.ID, "");
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    DummyInternalMessage fixture = new DummyInternalMessage(message);

    // Test the threshold when consensus has not been reached but can be.
    db.setReachedConsensus(false);
    db.setCanReachConsensus(true);

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertFalse(db.isGetClientMessageCalled());
    assertTrue(db.isCanOrHaveReachedConsensusCalled());
  }

  /**
   * Run the void InternalMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testInternalMessage_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    DummyInternalMessage result = new DummyInternalMessage(message);
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
    assertEquals(message.toString(), result.getMsg().toString());
  }

  /**
   * Run the void InternalMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testInternalMessage_2() throws Exception {
    DummyInternalMessage result = new DummyInternalMessage(new JSONObject());
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
  }

  /**
   * Run the void InternalMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testInternalMessage_3() throws Exception {
    JSONObject message = new JSONObject();
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, -1);

    DummyInternalMessage result = new DummyInternalMessage(message);
    assertNull(result);
  }

  /**
   * Run the void InternalMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testInternalMessage_4() throws Exception {
    DummyInternalMessage result = new DummyInternalMessage(TestParameters.MESSAGE);
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
  }

  /**
   * Run the void InternalMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testInternalMessage_5() throws Exception {
    DummyInternalMessage result = new DummyInternalMessage("{}");
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
  }

  /**
   * Run the void InternalMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testInternalMessage_6() throws Exception {
    JSONObject message = new JSONObject();
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, -1);

    DummyInternalMessage result = new DummyInternalMessage(message.toString());
    assertNull(result);
  }

  /**
   * Run the void InternalMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testInternalMessage_7() throws Exception {
    JSONObject message = new JSONObject();
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyInternalMessage result = new DummyInternalMessage(message.toString());
    assertNotNull(result);
  }
}