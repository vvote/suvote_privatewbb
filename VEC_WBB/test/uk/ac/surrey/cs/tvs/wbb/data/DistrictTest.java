/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/**
 * The class <code>DistrictTest</code> contains tests for the class <code>{@link District}</code>.
 */
public class DistrictTest {

  /**
   * Run the District(JSONObject,Region,JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testDistrict_1() throws Exception {
    JSONArray candidates = new JSONArray("[{\"name\":\"Bob\"},{\"name\":\"Sid\"}]");

    JSONObject district1 = new JSONObject();
    district1.put("district", "district1");
    district1.put("candidates", candidates);

    JSONObject candidateCount = new JSONObject();
    candidateCount.put("la", candidates.length());
    candidateCount.put("lc_atl", candidates.length());
    candidateCount.put("lc_btl", candidates.length());

    JSONObject districtConf = new JSONObject();
    districtConf.put("district1", candidateCount);

    District result = new District(district1, null, districtConf);
    assertNotNull(result);

    String candidateString = "[";

    for (int i = 0; i < candidates.length(); i++) {
      Candidate candidate = new Candidate(candidates.getJSONObject(i));
      candidateString += candidate.toString();

      if (i < candidates.length() - 1) {
        candidateString += ", ";
      }
    }
    candidateString += "]";

    assertEquals("district1", result.getName());
    assertNull(result.getRegion());
    assertEquals("District: district1, " + candidateString, result.toString());

    assertTrue(result.validateConsistency());
  }

  /**
   * Run the boolean validateConsistency() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateConsistency_1() throws Exception {
    JSONArray candidates = new JSONArray("[{\"name\":\"Bob\"},{\"name\":\"Sid\"}]");

    JSONObject district1 = new JSONObject();
    district1.put("district", "district1");
    district1.put("candidates", candidates);

    JSONObject candidateCount = new JSONObject();
    candidateCount.put("la", -1); // Wrong.
    candidateCount.put("lc_atl", candidates.length());
    candidateCount.put("lc_btl", candidates.length());

    JSONObject districtConf = new JSONObject();
    districtConf.put("district1", candidateCount);

    District result = new District(district1, null, districtConf);
    assertNotNull(result);

    String candidateString = "[";

    for (int i = 0; i < candidates.length(); i++) {
      Candidate candidate = new Candidate(candidates.getJSONObject(i));
      candidateString += candidate.toString();

      if (i < candidates.length() - 1) {
        candidateString += ", ";
      }
    }
    candidateString += "]";

    assertEquals("district1", result.getName());
    assertNull(result.getRegion());
    assertEquals("District: district1, " + candidateString, result.toString());

    assertFalse(result.validateConsistency());
  }
}