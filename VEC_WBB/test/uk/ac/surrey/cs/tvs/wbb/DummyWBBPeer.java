/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyStoreException;
import java.security.Principal;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB;

/**
 * This class defines a dummy WBBPeer which can play around with configuration information.
 */
public class DummyWBBPeer extends WBBPeer {

  /** Replacement configuration information once initialised. */
  private WBBConfig    replacementConfig        = null;

  /** Replacement DB. */
  private ConcurrentDB replacementDB            = null;

  /** Is the client connection valid? */
  private boolean      validConnection          = false;

  /** Last used session ID. */
  private String       sessionID                = null;

  /** Last sent message. */
  private String       message                  = null;

  /** The last received file name. */
  private String       file                     = null;

  /** The last received attachments. */
  private String       attachments              = null;

  /** The last received commit ID. */
  private String       commitID                 = null;

  /** The last received commit time. */
  private String       commitTime               = null;

  /** Has the sendToPublicWBB method been called? */
  private boolean      sendToPublicWBBCalled    = false;

  /** Has the scheduleNextCommit method been called? */
  private boolean      scheduleNextCommitCalled = false;

  /** Has the sendToAllPeersCalled method been called? */
  private boolean      sendToAllPeersCalled     = false;

  /** Has the verifyConnectionCalled method been called? */
  private boolean      verifyConnectionCalled   = false;

  /** Has the runCommit method been called? */
  private boolean      runCommitCalled          = false;

  /**
   * Constructor which allows a WBBPeer to be created using a valid configuration file.
   * 
   * @param initialConfig
   *          Valid configuration file name.
   * @throws PeerInitException
   */
  public DummyWBBPeer(String initialConfig) throws PeerInitException {
    super(initialConfig);
  }

  /**
   * @return the attachments.
   */
  public String getAttachments() {
    return this.attachments;
  }

  /**
   * @return The certificate from a key store. Dummy used to return only the peer keystore.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#getCertificateFromCerts(uk.ac.surrey.cs.tvs.wbb.config.CertStore, java.lang.String)
   */
  @Override
  public TVSCertificate getCertificateFromCerts(CertStore store, String alias) throws KeyStoreException {
    return super.getCertificateFromCerts(store, alias);
  }

  /**
   * @return The certificate key store. Dummy used to return only the peer keystore.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#getCertificateKeyStore(uk.ac.surrey.cs.tvs.wbb.config.CertStore)
   */
  @Override
  public TVSKeyStore getCertificateKeyStore(CertStore store) {
    return super.getCertificateKeyStore(store);
  }

  /**
   * @return the commitID.
   */
  public String getCommitID() {
    return this.commitID;
  }

  /**
   * CJC- added call to getCommitTime if null, otherwise causes a null pointer exception
   * @return the commitTime.
   */
  @Override
  public String getCommitTime() {
    if(this.commitTime==null){
      return super.getCommitTime();
    }
    return this.commitTime;
  }

  /**
   * @return The replacement configuration information if it has been defined.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#getConfig()
   */
  @Override
  public WBBConfig getConfig() {
    WBBConfig config = super.getConfig();

    if (this.replacementConfig != null) {
      config = this.replacementConfig;
    }

    return config;
  }

  /**
   * @return
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#getDB()
   */
  @Override
  public ConcurrentDB getDB() {
    ConcurrentDB db = super.getDB();

    if (this.replacementDB != null) {
      db = this.replacementDB;
    }

    return db;
  }

  /**
   * @return the file.
   */
  public String getFile() {
    return this.file;
  }

  /**
   * @return the last sent message.
   */
  public String getMessage() {
    return this.message;
  }

  /**
   * @return the last used session ID.
   */
  public String getSessionID() {
    return this.sessionID;
  }

  /**
   * @return whether the runCommitCalled method has been called.
   */
  public boolean isRunCommitCalled() {
    return this.runCommitCalled;
  }

  /**
   * @return whether the scheduleNextCommit method has been called.
   */
  public boolean isScheduleNextCommitCalled() {
    return this.scheduleNextCommitCalled;
  }

  /**
   * @return whether the sendToAllPeers method has been called.
   */
  public boolean isSendToAllPeersCalled() {
    return this.sendToAllPeersCalled;
  }

  /**
   * @return whether the sendToPublicWBB method has been called.
   */
  public boolean isSendToPublicWBBCalled() {
    return this.sendToPublicWBBCalled;
  }

  /**
   * @return whether the verifyConnection method has been called.
   */
  public boolean isVerifyConnectionCalled() {
    return this.verifyConnectionCalled;
  }

  /**
   * Resets the singleton to the default test environment.
   */
  public void reset() {
    this.replacementConfig = null;
    this.replacementDB = null;
    this.validConnection = false;
    this.sessionID = null;
    this.message = null;
    this.file = null;
    this.attachments = null;
    this.commitID = null;
    this.commitTime = null;
    this.sendToPublicWBBCalled = false;
    this.scheduleNextCommitCalled = false;
    this.sendToAllPeersCalled = false;
  }

  /**
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#runCommit()
   */
  @Override
  public void runCommit() {
    this.commitTime = "none";
    this.runCommitCalled = true;
  }

  /**
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#runCommit(java.lang.String)
   */
  @Override
  public void runCommit(String commitTime) {
    this.commitTime = commitTime;
    this.runCommitCalled = true;
  }

  /**
   * Schedule a job to run the next daily commit. Dummy test stub.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#scheduleNextCommit()
   */
  @Override
  public void scheduleNextCommit() {
    this.scheduleNextCommitCalled = true;
  }

  /**
   * Dummy method to pretend sending the specified message.
   * 
   * @param sessionID
   *          The session for the response channel.
   * @param message
   *          The message to send.
   * @param warnIfNoResponseChannel
   *          if trues logs a warning if there is no response channel
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#sendAndClose(java.lang.String, java.lang.String, boolean)
   */
  @Override
  public void sendAndClose(String sessionID, String message, boolean warnIfNoResponseChannel) throws IOException {
    // Save off the message detail for testing.
    this.sessionID = sessionID;
    this.message = message;
  }

  /**
   * Sends the round 2 commit data file to all peers for consolidation. Dummy test stub.
   * 
   * @param file
   *          The file to send.
   * @param attachments
   *          The file attachments.
   * @param commitID
   *          The commit id for the commit session.
   * @param commitTime
   *          The commit time for the commit session.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#sendR2CommitFileToAllPeers(java.lang.String, java.lang.String, java.lang.String,
   *      java.lang.String)
   */
  @Override
  public void sendR2CommitFileToAllPeers(String file, String attachments, String commitID, String commitTime) {
    this.file = file;
    this.attachments = attachments;
    this.commitID = commitID;
    this.commitTime = commitTime;
  }

  /**
   * Sends a message to all peers. Dummy test stub.
   * 
   * @param message
   *          The message to send.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#sendToAllPeers(java.lang.String)
   */
  @Override
  public void sendToAllPeers(String message) {
    this.sendToAllPeersCalled = true;

    this.message = message;
  }

  /**
   * Sends a message to the Public WBB.
   * 
   * @param msg
   *          The message to send.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#sendToPublicWBB(java.lang.String)
   */
  @Override
  public void sendToPublicWBB(String msg) {
    this.sendToPublicWBBCalled = true;
  }

  /**
   * Sets the configuration information to override that used during initialisation.
   * 
   * @param replacementConfig
   *          Replacement configuration.
   */
  public void setConfig(WBBConfig replacementConfig) {
    this.replacementConfig = replacementConfig;
  }

  /**
   * @param db
   *          the replacement DB.
   */
  public void setDB(ConcurrentDB replacementDB) {
    this.replacementDB = replacementDB;
  }

  /**
   * @param message
   *          the new message.
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @param validConnection
   *          the new validConnection.
   */
  public void setValidConnection(boolean validConnection) {
    this.validConnection = validConnection;
  }

  /**
   * Verifies that the sender's address and optionally the port matches to the required X500 principal.
   * 
   * @param remoteAddress
   *          The sender's address.
   * @param principal
   *          The required principal.
   * @param checkPort
   *          True to check the port as well.
   * @return True if the sender's address and optionally the port match the required principal.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.core.WBBPeer#verifyConnection(java.net.InetSocketAddress, java.security.Principal, boolean)
   */
  @Override
  public boolean verifyConnection(InetSocketAddress remoteAddress, Principal principal, boolean checkPort) {
    this.verifyConnectionCalled = true;

    return this.validConnection;
  }
}
