/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>AlreadySentTimeoutExceptionTest</code> contains tests for the class
 * <code>{@link AlreadySentTimeoutException}</code>.
 */
public class AlreadySentTimeoutExceptionTest {

  /**
   * Run the AlreadySentTimeoutException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadySentTimeoutException_1() throws Exception {

    AlreadySentTimeoutException result = new AlreadySentTimeoutException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the AlreadySentTimeoutException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadySentTimeoutException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    AlreadySentTimeoutException result = new AlreadySentTimeoutException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the AlreadySentTimeoutException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadySentTimeoutException_3() throws Exception {
    Throwable cause = new Throwable();

    AlreadySentTimeoutException result = new AlreadySentTimeoutException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the AlreadySentTimeoutException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadySentTimeoutException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    AlreadySentTimeoutException result = new AlreadySentTimeoutException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the AlreadySentTimeoutException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadySentTimeoutException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    AlreadySentTimeoutException result = new AlreadySentTimeoutException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}