/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>JSONSchemaValidationExceptionTest</code> contains tests for the class
 * <code>{@link JSONSchemaValidationException}</code>.
 */
public class JSONSchemaValidationExceptionTest {

  /**
   * Run the JSONSchemaValidationException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONSchemaValidationException_1() throws Exception {

    JSONSchemaValidationException result = new JSONSchemaValidationException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the JSONSchemaValidationException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONSchemaValidationException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    JSONSchemaValidationException result = new JSONSchemaValidationException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the JSONSchemaValidationException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONSchemaValidationException_3() throws Exception {
    Throwable cause = new Throwable();

    JSONSchemaValidationException result = new JSONSchemaValidationException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the JSONSchemaValidationException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONSchemaValidationException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    JSONSchemaValidationException result = new JSONSchemaValidationException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the JSONSchemaValidationException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONSchemaValidationException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    JSONSchemaValidationException result = new JSONSchemaValidationException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}