/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.Principal;

import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.security.auth.x500.X500Principal;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;

/**
 * The class <code>CustomHandshakeCompletedClientListenerTest</code> contains tests for the class
 * <code>{@link CustomHandshakeCompletedClientListener}</code>.
 */
public class CustomHandshakeCompletedClientListenerTest {

  /**
   * Dummy class for the handshake event.
   */
  private class DummyHandshakeCompletedEvent extends HandshakeCompletedEvent {

    private static final long serialVersionUID = 1L;
    private String            remoteHost       = null;

    public DummyHandshakeCompletedEvent(SSLSocket socket, String remoteHost) {
      super(socket, null);

      this.remoteHost = remoteHost;
    }

    @Override
    public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
      return new X500Principal("CN=" + this.remoteHost + ", OU=Dev, O=SurreyPAV, L=Guildford, S=Surrey, C=GB");
    }
  }

  /**
   * Dummy class for the SSL socket.
   */
  private class DummySSLSocket extends SSLSocket {

    private InetSocketAddress address = null;

    private boolean           closed  = false;

    public DummySSLSocket(InetSocketAddress address) throws IOException {
      super();

      this.address = address;
    }

    @Override
    public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public String[] getEnabledCipherSuites() {
      return null;
    }

    @Override
    public String[] getEnabledProtocols() {
      return null;
    }

    @Override
    public boolean getEnableSessionCreation() {
      return false;
    }

    @Override
    public boolean getNeedClientAuth() {
      return false;
    }

    @Override
    public SocketAddress getRemoteSocketAddress() {
      return this.address;
    }

    @Override
    public SSLSession getSession() {
      return null;
    }

    @Override
    public String[] getSupportedCipherSuites() {
      return null;
    }

    @Override
    public String[] getSupportedProtocols() {
      return null;
    }

    @Override
    public boolean getUseClientMode() {
      return false;
    }

    @Override
    public boolean getWantClientAuth() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }

    @Override
    public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public void setEnabledCipherSuites(String[] suites) {
    }

    @Override
    public void setEnabledProtocols(String[] protocols) {
    }

    @Override
    public void setEnableSessionCreation(boolean flag) {
    }

    @Override
    public void setNeedClientAuth(boolean need) {
    }

    @Override
    public void setUseClientMode(boolean mode) {
    }

    @Override
    public void setWantClientAuth(boolean want) {
    }

    @Override
    public void startHandshake() throws IOException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CustomHandshakeCompletedClientListener(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCustomHandshakeCompletedClientListener_1() throws Exception {
    CustomHandshakeCompletedClientListener result = new CustomHandshakeCompletedClientListener(TestParameters.getInstance()
        .getWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the void handshakeCompleted(HandshakeCompletedEvent) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testHandshakeCompleted_1() throws Exception {
    CustomHandshakeCompletedClientListener fixture = new CustomHandshakeCompletedClientListener(TestParameters.getInstance()
        .getWbbPeer());

    // Create the dummy SSL socket and event which has the correct remote details.
    JSONObject remotePeer = TestParameters.getInstance().getWbbPeers().getJSONObject(1);

    SSLSocket sslSocket = new DummySSLSocket(new InetSocketAddress(remotePeer.getString(WBBConfig.ADDRESS),
        remotePeer.getInt(WBBConfig.PORT)));
    HandshakeCompletedEvent evt = new DummyHandshakeCompletedEvent(sslSocket, remotePeer.getString(WBBConfig.PEER_ID));

    fixture.handshakeCompleted(evt);
    assertFalse(sslSocket.isClosed());
  }

  /**
   * Run the void handshakeCompleted(HandshakeCompletedEvent) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testHandshakeCompleted_2() throws Exception {
    CustomHandshakeCompletedClientListener fixture = new CustomHandshakeCompletedClientListener(TestParameters.getInstance()
        .getWbbPeer());

    // Create the dummy SSL socket and event which has the correct remote details.
    JSONObject remotePeer = TestParameters.getInstance().getWbbPeers().getJSONObject(1);

    SSLSocket sslSocket = new DummySSLSocket(new InetSocketAddress("10.0.0.1", remotePeer.getInt(WBBConfig.PORT)));
    HandshakeCompletedEvent evt = new DummyHandshakeCompletedEvent(sslSocket, remotePeer.getString(WBBConfig.PEER_ID));

    fixture.handshakeCompleted(evt);
    assertTrue(sslSocket.isClosed());
  }
}