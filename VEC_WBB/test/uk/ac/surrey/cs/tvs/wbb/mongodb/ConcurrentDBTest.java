/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.wbb.DummyJSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyRespondedException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.R2AlreadyRunningException;
import uk.ac.surrey.cs.tvs.wbb.messages.CancelMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR2Message;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.StartEVMMessage;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

/**
 * The class <code>ConcurrentDBTest</code> contains tests for the class <code>{@link ConcurrentDB}</code>.
 */
public class ConcurrentDBTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Tidy up the database.
    TestParameters.tidyDatabase();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the database.
    TestParameters.tidyDatabase();

    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the boolean canOrHaveReachedConsensus(DBRecordType,String,int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCanOrHaveReachedConsensus_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Test not having a serial ID record.
      boolean result = fixture.canOrHaveReachedConsensus(type, TestParameters.SERIAL_NO, TestParameters.THRESHOLD,
          TestParameters.THRESHOLD);
      assertTrue(result);

      // Add a record of the serial number.
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      doc.append(fields.getField(DBFieldName.SIGNATURE_COUNT), 1);
      doc.append(fields.getField(DBFieldName.SENT_SIGNATURE), 0);
      submissions.insert(doc);

      // Test no consensus.
      result = fixture.canOrHaveReachedConsensus(type, TestParameters.SERIAL_NO, TestParameters.THRESHOLD, 1);
      assertFalse(result);

      // Test consensus.
      result = fixture
          .canOrHaveReachedConsensus(type, TestParameters.SERIAL_NO, TestParameters.THRESHOLD, TestParameters.THRESHOLD);
      assertTrue(result);

      // Test consensus with checked signatures.
      BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      BasicDBObject update = new BasicDBObject();
      BasicDBObject data = new BasicDBObject();

      data.append(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[1]);
      data.append(fields.getField(DBFieldName.QUEUED_MESSAGE), TestParameters.MESSAGE);
      update.append(DBFields.ADD_TO_SET, new BasicDBObject(fields.getField(DBFieldName.CHECKED_SIGS), data));
      submissions.update(query, update);

      result = fixture
          .canOrHaveReachedConsensus(type, TestParameters.SERIAL_NO, TestParameters.THRESHOLD, TestParameters.THRESHOLD);
      assertTrue(result);

      // Test no consensus with own signature.
      query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      update = new BasicDBObject();
      data = new BasicDBObject();

      data.append(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.SESSION_ID);
      update.append(DBFields.SET, data);
      submissions.update(query, update);

      result = fixture
          .canOrHaveReachedConsensus(type, TestParameters.SERIAL_NO, TestParameters.THRESHOLD, TestParameters.THRESHOLD);
      assertFalse(result);

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean canReachCommitConsensus(int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCanReachCommitConsensus_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no signatures.
    boolean result = fixture.canReachCommitConsensus(TestParameters.THRESHOLD, TestParameters.THRESHOLD);
    assertTrue(result);

    // Add an empty commitment.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    commits.insert(doc);

    // Test no signatures.
    result = fixture.canReachCommitConsensus(TestParameters.THRESHOLD, TestParameters.THRESHOLD);
    assertTrue(result);

    // Add a commitment.
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, data));
    commits.update(query, update);

    // Test no signatures.
    result = fixture.canReachCommitConsensus(TestParameters.THRESHOLD, TestParameters.THRESHOLD);
    assertTrue(result);

    // Add in a signature count.
    query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    update = new BasicDBObject();
    data = new BasicDBObject();

    data.append(DBFields.COMMITMENT_SIG_COUNT, 1);

    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, data));
    commits.update(query, update);

    // Test signature count.
    result = fixture.canReachCommitConsensus(TestParameters.THRESHOLD, TestParameters.THRESHOLD);
    assertTrue(result);

    // Add in some checked signatures.
    query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    update = new BasicDBObject();
    data = new BasicDBObject();

    BasicDBList list = new BasicDBList();
    list.add(new BasicDBObject());
    list.add(new BasicDBObject());

    data.append(DBFields.COMMITMENT_SIG_COUNT, 1);
    data.append(DBFields.COMMITMENT_CHECKED_SIGS, list);

    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, data));
    commits.update(query, update);

    // Test checked signatures.
    result = fixture.canReachCommitConsensus(TestParameters.THRESHOLD, TestParameters.THRESHOLD);
    assertFalse(result);

    // Add in a round 1 signature.
    query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    update = new BasicDBObject();
    data = new BasicDBObject();

    list = new BasicDBList();
    list.add(new BasicDBObject());
    list.add(new BasicDBObject());

    data.append(DBFields.COMMITMENT_SIG_COUNT, 1);
    data.append(DBFields.COMMITMENT_CHECKED_SIGS, list);
    data.append(DBFields.COMMITMENT_ROUND1_SIG, new BasicDBObject());

    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, data));
    commits.update(query, update);

    // Test all signatures.
    result = fixture.canReachCommitConsensus(TestParameters.THRESHOLD, TestParameters.THRESHOLD);
    assertFalse(result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean checkAndSetTimeout(DBRecordType,JSONWBBMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndSetTimeout_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test only POD and GENERAL messages as CANCEL messages cannot timeout.
    for (DBRecordType type : new DBRecordType[] { DBRecordType.POD, DBRecordType.GENERAL }) {
      DBFields fields = DBFields.getInstance(type);
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      JSONWBBMessage message = new DummyJSONWBBMessage();

      // Test no record of the message and the setting of the timeout.
      boolean result = fixture.checkAndSetTimeout(type, message);
      assertTrue(result);

      BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
      DBObject timeout = submissions.findOne(query);

      int value = (Integer) timeout.get(fields.getField(DBFieldName.SENT_TIMEOUT));
      assertEquals(1, value);

      // Add a sent signature.
      query = new BasicDBObject(DBFields.ID, message.getID());
      BasicDBObject update = new BasicDBObject();
      BasicDBObject data = new BasicDBObject();

      data.append(fields.getField(DBFieldName.SENT_SIGNATURE), 1);

      update.append(DBFields.SET, data);
      submissions.update(query, update);

      // Test that an exception is thrown if we try to timeout twice.
      try {
        result = fixture.checkAndSetTimeout(type, message);
        fail("no exception");
      }
      catch (AlreadyRespondedException e) {
        // Correct result.
      }

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean checkCommitR2Threshold(int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitR2Threshold_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    boolean result = fixture.checkCommitR2Threshold(TestParameters.THRESHOLD);
    assertFalse(result);

    // Insert a record but no commit.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    BasicDBObject commit = new BasicDBObject();
    commit.append(TestParameters.DB_ROUND2_PROCESSED_COUNT, 0);
    commit.append(TestParameters.DB_COMMIT_SENT_SIG, 1);

    doc.append(DBFields.COMMITMENT, commit);
    commits.insert(doc);

    // Test no record.
    result = fixture.checkCommitR2Threshold(TestParameters.THRESHOLD);
    assertFalse(result);

    // Update the record to have commitment.
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    BasicDBObject update = new BasicDBObject();

    commit = new BasicDBObject();
    commit.append(TestParameters.DB_ROUND2_PROCESSED_COUNT, TestParameters.THRESHOLD);
    commit.append(TestParameters.DB_COMMIT_SENT_SIG, 0);

    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, commit));
    commits.update(query, update);

    // Test record.
    result = fixture.checkCommitR2Threshold(TestParameters.THRESHOLD);
    assertTrue(result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean checkCommitThreshold(int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitThreshold_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    boolean result = fixture.checkCommitThreshold(TestParameters.THRESHOLD);
    assertFalse(result);

    // Insert a record but no commit.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    BasicDBObject commit = new BasicDBObject();
    commit.append(DBFields.COMMITMENT_SIG_COUNT, 0);
    commit.append(TestParameters.DB_COMMIT_SENT_SIG, 1);

    doc.append(DBFields.COMMITMENT, commit);
    commits.insert(doc);

    // Test no record.
    result = fixture.checkCommitThreshold(TestParameters.THRESHOLD);
    assertFalse(result);

    // Update the record to have commitment.
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    BasicDBObject update = new BasicDBObject();

    commit = new BasicDBObject();
    commit.append(DBFields.COMMITMENT_SIG_COUNT, TestParameters.THRESHOLD);
    commit.append(TestParameters.DB_COMMIT_SENT_SIG, 0);

    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, commit));
    commits.update(query, update);

    // Test record.
    result = fixture.checkCommitThreshold(TestParameters.THRESHOLD);
    assertTrue(result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean checkThresholdAndResponse(DBRecordType,String,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThresholdAndResponse_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      JSONWBBMessage message = new DummyJSONWBBMessage();

      // Test no record of the message and the setting of the sent signature.
      boolean result = fixture.checkThresholdAndResponse(type, message.getID(), TestParameters.THRESHOLD);
      assertTrue(result);

      BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
      DBObject signature = submissions.findOne(query);

      int value = (Integer) signature.get(fields.getField(DBFieldName.SENT_SIGNATURE));
      assertEquals(1, value);

      // Test trying to respond twice.
      result = fixture.checkThresholdAndResponse(type, message.getID(), TestParameters.THRESHOLD);
      assertFalse(result);

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the ConcurrentDB(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testConcurrentDB_1() throws Exception {
    ConcurrentDB result = new ConcurrentDB(TestParameters.TEST_DATABASE);
    assertNotNull(result);
  }

  /**
   * Run the void createNewCommitRecord(JSONWBBMessage,String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateNewCommitRecord_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create the commit message.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    // Create the record.
    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);

    // Test the record was written correctly.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject commit = commits.findOne(query);

    commit = (DBObject) commit.get(DBFields.COMMITMENT);

    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_COMMIT_ID.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_HASH.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_FILE.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_FILE_FULL.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_TIME.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_ATTACHMENTS.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_CURRENT_COMMIT.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_DATETIME.split("\\.")[1]));
    assertTrue(commit.containsField(DBFields.COMMITMENT_COMMIT_SIG_COUNT.split("\\.")[1]));

    assertEquals(TestParameters.SESSION_ID.toString(), commit.get(DBFields.COMMITMENT_COMMIT_COMMIT_ID.split("\\.")[1]));
    assertEquals(signature, commit.get(DBFields.COMMITMENT_COMMIT_MY_ROUND1_SIG.split("\\.")[1]));
    assertEquals(hash, commit.get(DBFields.COMMITMENT_COMMIT_HASH.split("\\.")[1]));
    assertEquals(TestParameters.COMMIT_FILE, commit.get(DBFields.COMMITMENT_COMMIT_FILE.split("\\.")[1]));
    assertEquals(TestParameters.COMMIT_FILE_FULL, commit.get(DBFields.COMMITMENT_COMMIT_FILE_FULL.split("\\.")[1]));
    assertEquals(TestParameters.COMMIT_TIME, commit.get(DBFields.COMMITMENT_COMMIT_TIME.split("\\.")[1]));
    assertEquals(TestParameters.COMMIT_FILE_ZIP, commit.get(DBFields.COMMITMENT_COMMIT_ATTACHMENTS.split("\\.")[1]));
    assertEquals(true, commit.get(DBFields.COMMITMENT_COMMIT_CURRENT_COMMIT.split("\\.")[1]));
    assertEquals(time, commit.get(DBFields.COMMITMENT_COMMIT_DATETIME.split("\\.")[1]));
    assertEquals(1, commit.get(DBFields.COMMITMENT_COMMIT_SIG_COUNT.split("\\.")[1]));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void createNewCommitRecord(JSONWBBMessage,String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyReceivedMessageException.class)
  public void testCreateNewCommitRecord_2() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create the commit message.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    // Create the record.
    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);

    // Test duplicate.
    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);
  }

  /**
   * Run the JSONObject getBallotRecord(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetBallotRecord_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    JSONObject result = fixture.getBallotRecord(TestParameters.SERIAL_NO);
    assertNull(result);

    // Add a record.
    DBCollection ciphers = db.getCollection(DBFields.Collections.CIPHERS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    ciphers.insert(doc);

    // Test record.
    result = fixture.getBallotRecord(TestParameters.SERIAL_NO);
    assertNotNull(result);

    assertTrue(result.has(DBFields.ID));
    assertEquals(TestParameters.SERIAL_NO, result.getString(DBFields.ID));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the String[] getClientCancelMessages(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetClientCancelMessages_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    String[] result = fixture.getClientCancelMessages(TestParameters.SERIAL_NO);
    assertNotNull(result);
    assertEquals(0, result.length);

    // Add a cancel record.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);
    DBFieldsCancel fields = DBFieldsCancel.getInstance();

    BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);

    BasicDBList list = new BasicDBList();
    list.add(TestParameters.PEERS[0]);
    list.add(TestParameters.PEERS[1]);

    doc.append(fields.getField(DBFieldName.CLIENT_MESSAGE), list);
    submissions.insert(doc);

    // Test retrieval.
    result = fixture.getClientCancelMessages(TestParameters.SERIAL_NO);
    assertNotNull(result);
    assertEquals(2, result.length);
    assertEquals(TestParameters.PEERS[0], result[0]);
    assertEquals(TestParameters.PEERS[1], result[1]);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the String getClientMessage(DBRecordType,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetClientMessage_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      // Test no record.
      String result = fixture.getClientMessage(type, TestParameters.SERIAL_NO);
      assertNull(result);

      // Add a record.
      BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      doc.append(fields.getField(DBFieldName.CLIENT_MESSAGE), TestParameters.MESSAGE);
      submissions.insert(doc);

      // Test record.
      result = fixture.getClientMessage(type, TestParameters.SERIAL_NO);
      assertNotNull(result);
      assertEquals(TestParameters.MESSAGE, result);

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the ArrayList<JSONWBBMessage> getCommitData(int,File,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitData_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no commit messages.
    File commitFull = new File(TestParameters.COMMIT_FOLDER, TestParameters.COMMIT_FILE_FULL);
    commitFull.getParentFile().mkdirs();

    ArrayList<JSONWBBMessage> result = fixture.getCommitData(TestParameters.THRESHOLD, commitFull.getAbsoluteFile(),
        TestParameters.COMMIT_TIME);
    assertNotNull(result);

    // Test that the file exists and it is empty.
    assertTrue(commitFull.exists());
    assertTrue(TestParameters.isEmpty(commitFull));

    // Add a POD message.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);
    DummyJSONWBBMessage message = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    JSONObject object = message.getMsg();
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.FileMessage.FILESIZE, 0l);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    DBFields fields = DBFields.getInstance(DBRecordType.POD);

    BasicDBObject doc = new BasicDBObject(fields.getField(DBFieldName.SIGNATURE_COUNT), TestParameters.THRESHOLD);
    doc.append(fields.getField(DBFieldName.COMMIT_TIME), TestParameters.COMMIT_TIME);
    doc.append(fields.getField(DBFieldName.CLIENT_MESSAGE), object.toString());
    submissions.insert(doc);

    // Test that the POD message is included.
    result = fixture.getCommitData(TestParameters.THRESHOLD, commitFull.getAbsoluteFile(), TestParameters.COMMIT_TIME);
    assertNotNull(result);

    // Test that the POD message is included and in the file.
    assertEquals(1, result.size());
    JSONObject commit = result.get(0).getMsg();

    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, commit.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.FileMessage.ID));
    assertEquals(0l, commit.getLong(MessageFields.FileMessage.FILESIZE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.Commit.COMMIT_ID));

    assertTrue(commitFull.exists());
    assertTrue(TestParameters.compareFile(commitFull, new JSONObject[] { object }, fields.getField(DBFieldName.CLIENT_MESSAGE)));

    // Remove the POD message.
    submissions.remove(new BasicDBObject());

    // Add a CANCEL message.
    fields = DBFields.getInstance(DBRecordType.CANCEL);

    BasicDBList list = new BasicDBList();
    list.add(object.toString());

    doc = new BasicDBObject(fields.getField(DBFieldName.SIGNATURE_COUNT), TestParameters.THRESHOLD);
    doc.append(fields.getField(DBFieldName.COMMIT_TIME), TestParameters.COMMIT_TIME);
    doc.append(fields.getField(DBFieldName.CLIENT_MESSAGE), list);
    submissions.insert(doc);

    // Test that the CANCEL message is included.
    result = fixture.getCommitData(TestParameters.THRESHOLD, commitFull.getAbsoluteFile(), TestParameters.COMMIT_TIME);
    assertNotNull(result);

    // Test that the CANCEL message is included and in the file.
    assertEquals(1, result.size());
    commit = result.get(0).getMsg();

    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, commit.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.FileMessage.ID));
    assertEquals(0l, commit.getLong(MessageFields.FileMessage.FILESIZE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.Commit.COMMIT_ID));

    assertTrue(commitFull.exists());
    assertTrue(TestParameters.compareFile(commitFull, new JSONObject[] { object }, fields.getField(DBFieldName.CLIENT_MESSAGE)));

    // Remove the CANCEL message.
    submissions.remove(new BasicDBObject());

    // Add a GENERAL message.
    fields = DBFields.getInstance(DBRecordType.GENERAL);

    doc = new BasicDBObject(fields.getField(DBFieldName.SIGNATURE_COUNT), TestParameters.THRESHOLD);
    doc.append(fields.getField(DBFieldName.COMMIT_TIME), TestParameters.COMMIT_TIME);
    doc.append(fields.getField(DBFieldName.CLIENT_MESSAGE), object.toString());
    submissions.insert(doc);

    // Test that the GENERAL message is included.
    result = fixture.getCommitData(TestParameters.THRESHOLD, commitFull.getAbsoluteFile(), TestParameters.COMMIT_TIME);
    assertNotNull(result);

    // Test that the GENERAL message is included and in the file.
    assertEquals(1, result.size());
    commit = result.get(0).getMsg();

    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, commit.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.FileMessage.ID));
    assertEquals(0l, commit.getLong(MessageFields.FileMessage.FILESIZE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.Commit.COMMIT_ID));

    assertTrue(commitFull.exists());
    assertTrue(TestParameters.compareFile(commitFull, new JSONObject[] { object }, fields.getField(DBFieldName.CLIENT_MESSAGE)));

    // Add a conflicting commit with the same ID.
    DBCollection conflicts = db.getCollection(DBFields.Collections.COMMIT_CONFLICTS);

    doc = new BasicDBObject(fields.getField(DBFieldName.SIGNATURE_COUNT), TestParameters.THRESHOLD);
    doc.append(fields.getField(DBFieldName.COMMIT_TIME), TestParameters.COMMIT_TIME);
    doc.append(DBFields.CONFLICT_MESSAGE, object.toString());
    conflicts.insert(doc);

    result = fixture.getCommitData(TestParameters.THRESHOLD, commitFull.getAbsoluteFile(), TestParameters.COMMIT_TIME);
    assertNotNull(result);

    // Test that ONLY the GENERAL message is included and in the file.
    assertEquals(1, result.size());
    commit = result.get(0).getMsg();

    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, commit.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.FileMessage.ID));
    assertEquals(0l, commit.getLong(MessageFields.FileMessage.FILESIZE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.Commit.COMMIT_ID));

    assertTrue(commitFull.exists());
    assertTrue(TestParameters.compareFile(commitFull, new JSONObject[] { object }, fields.getField(DBFieldName.CLIENT_MESSAGE)));

    // Remove the GENERAL message, but keep the conflict.
    submissions.remove(new BasicDBObject());

    result = fixture.getCommitData(TestParameters.THRESHOLD, commitFull.getAbsoluteFile(), TestParameters.COMMIT_TIME);
    assertNotNull(result);

    // Test that ONLY the conflict message is included and in the file.
    assertEquals(1, result.size());
    commit = result.get(0).getMsg();

    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, commit.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.FileMessage.ID));
    assertEquals(0l, commit.getLong(MessageFields.FileMessage.FILESIZE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.Commit.COMMIT_ID));

    assertTrue(commitFull.exists());
    assertTrue(TestParameters.compareFile(commitFull, new JSONObject[] { object }, fields.getField(DBFieldName.CLIENT_MESSAGE)));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the JSONObject getCommitTable() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTable_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no commits.
    JSONObject result = fixture.getCommitTable();
    assertNotNull(result);

    assertEquals(1, JSONObject.getNames(result).length);
    assertTrue(result.has(MessageFields.Commit.COMMITS));

    JSONArray array = result.getJSONArray(MessageFields.Commit.COMMITS);
    assertEquals(0, array.length());

    // Insert a current commit.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject current = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    commits.insert(current);

    // Test current commit.
    result = fixture.getCommitTable();
    assertNotNull(result);

    assertEquals(2, JSONObject.getNames(result).length);
    assertTrue(result.has(MessageFields.Commit.CURRENT_COMMIT));
    assertTrue(result.has(MessageFields.Commit.COMMITS));

    assertEquals(new JSONObject(current.toString()).toString(), result.getJSONObject(MessageFields.Commit.CURRENT_COMMIT)
        .toString());

    array = result.getJSONArray(MessageFields.Commit.COMMITS);
    assertEquals(0, array.length());

    // Add in another commit.
    BasicDBObject other = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    commits.insert(other);

    result = fixture.getCommitTable();
    assertNotNull(result);

    assertEquals(2, JSONObject.getNames(result).length);
    assertTrue(result.has(MessageFields.Commit.CURRENT_COMMIT));
    assertTrue(result.has(MessageFields.Commit.COMMITS));

    assertEquals(new JSONObject(current.toString()).toString(), result.getJSONObject(MessageFields.Commit.CURRENT_COMMIT)
        .toString());

    array = result.getJSONArray(MessageFields.Commit.COMMITS);
    assertEquals(1, array.length());

    assertEquals(new JSONObject(other.toString()).toString(), array.get(0).toString());

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the String getCurrentCommitField(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = NoCommitExistsException.class)
  public void testGetCurrentCommitField_1() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test without a current commit.
    fixture.getCurrentCommitField(DBFields.ID);
  }

  /**
   * Run the String getCurrentCommitField(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCurrentCommitField_2() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Insert a current commitment.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);

    // Test with a current commit.
    String result = fixture.getCurrentCommitField(DBFields.COMMITMENT_COMMIT_COMMIT_ID.split("\\.")[1]);
    assertNotNull(result);
    assertEquals(TestParameters.SESSION_ID.toString(), result);
  }

  /**
   * Run the String getCurrentCommitHash() method test.
   * 
   * @throws Exception
   */
  @Test(expected = NoCommitExistsException.class)
  public void testGetCurrentCommitHash_1() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test without a current commit.
    fixture.getCurrentCommitHash();
  }

  /**
   * Run the String getCurrentCommitHash() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCurrentCommitHash_2() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Insert a current commitment.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);

    // Test with a current commit.
    String result = fixture.getCurrentCommitHash();
    assertNotNull(result);
    assertEquals(hash, result);
  }

  /**
   * Run the ArrayList<CommitR1Message> getCurrentCommitSigsToCheck() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCurrentCommitSigsToCheck_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test without a current commit.
    ArrayList<CommitR1Message> result = fixture.getCurrentCommitSigsToCheck();
    assertNotNull(result);
    assertEquals(0, result.size());

    // Add in a current set of signatures to check which is empty.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    BasicDBList commit = new BasicDBList();

    doc.append(DBFields.COMMITMENT, new BasicDBObject(DBFields.COMMITMENT_SIGS_TO_CHECK, commit));
    commits.insert(doc);

    // Test with a current commit.
    result = fixture.getCurrentCommitSigsToCheck();
    assertNotNull(result);
    assertEquals(0, result.size());

    // Delete the records.
    commits.remove(new BasicDBObject());

    // Add in a current set of signatures to check which is not empty.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    commit = new BasicDBList();
    commit.add(new BasicDBObject(DBFields.COMMITMENT_MESSAGE, message.getMsg().toString()));

    doc.append(DBFields.COMMITMENT, new BasicDBObject(DBFields.COMMITMENT_SIGS_TO_CHECK, commit));
    commits.insert(doc);

    // Test with a current commit.
    result = fixture.getCurrentCommitSigsToCheck();
    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(new JSONObject(message.getMsg().toString()).toString(), result.get(0).getMsg().toString());

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the String getField(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetField_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    String result = fixture.getField(TestParameters.SERIAL_NO, DBFields.ID);
    assertNull(result);

    // Add a record.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    submissions.insert(doc);

    // Test record.
    result = fixture.getField(TestParameters.SERIAL_NO, DBFields.ID);
    assertNotNull(result);
    assertEquals(TestParameters.SERIAL_NO, result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the ArrayList<JSONWBBMessage> getR2Queue() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetR2Queue_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no round 2 commit messages.
    ArrayList<JSONWBBMessage> result = fixture.getR2Queue();
    assertNotNull(result);
    assertEquals(0, result.size());

    // Add in a current commitment with no list.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    commits.insert(doc);

    // Test empty 2 commit messages.
    result = fixture.getR2Queue();
    assertNotNull(result);
    assertEquals(0, result.size());

    // Remove the commitments.
    commits.remove(new BasicDBObject());

    // Add in a current commitment with a list.
    DummyJSONWBBMessage message = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    JSONObject object = message.getMsg();
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.FileMessage.FILESIZE, 0l);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);

    BasicDBList list = new BasicDBList();
    list.add(new BasicDBObject(DBFields.COMMITMENT_MESSAGE, object.toString()));

    doc.append(DBFields.COMMITMENT_R2_QUEUE, list);
    commits.insert(doc);

    // Test empty 2 commit messages.
    result = fixture.getR2Queue();
    assertNotNull(result);
    assertEquals(1, result.size());

    JSONObject commit = result.get(0).getMsg();

    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, commit.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.FileMessage.ID));
    assertEquals(0l, commit.getLong(MessageFields.FileMessage.FILESIZE));
    assertEquals(TestParameters.SESSION_ID.toString(), commit.getString(MessageFields.Commit.COMMIT_ID));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the JSONObject getRecord(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetRecord_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    JSONObject result = fixture.getRecord(TestParameters.SERIAL_NO);
    assertNull(result);

    // Add a record.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    submissions.insert(doc);

    // Test record.
    result = fixture.getRecord(TestParameters.SERIAL_NO);
    assertNotNull(result);
    assertEquals(TestParameters.SERIAL_NO, result.getString(DBFields.ID));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the ArrayList<PeerMessage> getSigsToCheck(DBRecordType,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetSigsToCheck_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Test with no signatures to check.
      ArrayList<PeerMessage> result = fixture.getSigsToCheck(type, TestParameters.SERIAL_NO);
      assertNotNull(result);
      assertEquals(0, result.size());

      // Add a submission.
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      submissions.insert(doc);

      // Test with no signatures to check.
      result = fixture.getSigsToCheck(type, TestParameters.SERIAL_NO);
      assertNotNull(result);
      assertEquals(0, result.size());

      // Remove the submission.
      submissions.remove(new BasicDBObject());

      // Add a submission with signatures to check.
      DummyJSONWBBMessage message = new DummyJSONWBBMessage(TestParameters.MESSAGE);

      JSONObject object = message.getMsg();
      object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
      object.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
      object.put(MessageFields.FileMessage.FILESIZE, 0l);
      object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

      doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);

      BasicDBList list = new BasicDBList();
      list.add(new BasicDBObject(fields.getField(DBFieldName.QUEUED_MESSAGE), object.toString()));

      doc.append(fields.getField(DBFieldName.SIGNATURES_TO_CHECK), list);
      submissions.insert(doc);

      // Test with signatures to check.
      result = fixture.getSigsToCheck(type, TestParameters.SERIAL_NO);
      assertNotNull(result);
      assertEquals(1, result.size());

      JSONObject signature = result.get(0).getMsg();

      assertEquals(TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING, signature.getString(MessageFields.JSONWBBMessage.TYPE));
      assertEquals(TestParameters.SESSION_ID.toString(), signature.getString(MessageFields.FileMessage.ID));
      assertEquals(0l, signature.getLong(MessageFields.FileMessage.FILESIZE));
      assertEquals(TestParameters.SESSION_ID.toString(), signature.getString(MessageFields.Commit.COMMIT_ID));

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean isAlreadyStored(String,DBRecordType,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testIsAlreadyStored_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Test no record.
      boolean result = fixture.isAlreadyStored(TestParameters.SERIAL_NO, type, TestParameters.THRESHOLD);
      assertFalse(result);

      // Add a submission below threshold.
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      doc.append(fields.getField(DBFieldName.SIGNATURE_COUNT), TestParameters.THRESHOLD - 1);
      submissions.insert(doc);

      // Test no record.
      result = fixture.isAlreadyStored(TestParameters.SERIAL_NO, type, TestParameters.THRESHOLD);
      assertFalse(result);

      // Delete the records.
      submissions.remove(new BasicDBObject());

      // Add a submission above threshold.
      doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
      doc.append(fields.getField(DBFieldName.SIGNATURE_COUNT), TestParameters.THRESHOLD);
      submissions.insert(doc);

      // Test record.
      result = fixture.isAlreadyStored(TestParameters.SERIAL_NO, type, TestParameters.THRESHOLD);
      assertTrue(result);

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean isCommitInR2() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testIsCommitInR2_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no record.
    boolean result = fixture.isCommitInR2();
    assertFalse(result);

    // Add a record but false.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    commits.insert(doc);

    // Test record but not round 2.
    result = fixture.isCommitInR2();
    assertFalse(result);

    // Delete the records.
    commits.remove(new BasicDBObject());

    // Add a record and true.
    doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    doc.append(DBFields.COMMITMENT, new BasicDBObject(TestParameters.DB_COMMIT_RUNNING_R2, true));
    commits.insert(doc);

    // Test record but not round 2.
    result = fixture.isCommitInR2();
    assertTrue(result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void queueR2Commit(JSONWBBMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testQueueR2Commit_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a test message.
    DummyJSONWBBMessage message = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    JSONObject object = message.getMsg();
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.FileMessage.FILESIZE, 0l);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    // Test no previous record.
    fixture.queueR2Commit(message);

    // Test that the record was added.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject commit = commits.findOne(query);

    BasicDBList list = (BasicDBList) commit.get(DBFields.COMMITMENT_R2_QUEUE);
    assertNotNull(list);
    assertEquals(1, list.size());

    Iterator<Object> iterator = list.iterator();

    while (iterator.hasNext()) {
      BasicDBObject record = (BasicDBObject) iterator.next();

      assertTrue(record.containsField(DBFields.FROM_PEER));
      assertTrue(record.containsField(DBFields.COMMITMENT_MESSAGE));

      assertEquals(message.getMsg().get(MessageFields.FROM_PEER), record.get(DBFields.FROM_PEER));
      assertEquals(message.getMsg().toString(), record.get(DBFields.COMMITMENT_MESSAGE));
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void queueR2Commit(JSONWBBMessage) method test.
   * 
   * @throws Exception
   */
  @Test(expected = R2AlreadyRunningException.class)
  public void testQueueR2Commit_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a test message.
    DummyJSONWBBMessage message = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    JSONObject object = message.getMsg();
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.FileMessage.FILESIZE, 0l);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    // Add the message.
    fixture.queueR2Commit(message);

    // Set the commit as running.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    data.append(TestParameters.DB_COMMIT_RUNNING_R2, true);
    update.append(DBFields.SET, new BasicDBObject(DBFields.COMMITMENT, data));
    commits.update(query, update);

    // Try again with the same message now the commit is running - should case an exception.
    fixture.queueR2Commit(message);
  }

  /**
   * Run the boolean setCommitSentSig(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyRespondedException.class)
  public void testSetCommitSentSig_1() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);
    // TODO this now returns an exception if it has already been set
    // Test with no current commitment.
    assertTrue(fixture.setCommitSentSig("rubbish"));
    
    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean setCommitSentSig(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetCommitSentSig_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Insert a current commitment.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);

    // Test with a current commitment.
    fixture.setCommitSentSig("rubbish");

    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);
    record = (DBObject) record.get(DBFields.COMMITMENT);

    assertEquals(1, record.get(TestParameters.DB_COMMIT_SENT_SIG));
    assertEquals("rubbish", record.get(TestParameters.DB_COMMIT_SK2_SIG));
    assertEquals(false, record.get(TestParameters.DB_COMMIT_CURRENT_COMMIT));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void setFieldInRecord(String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetFieldInRecord_1() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    fixture.setFieldInRecord(TestParameters.SERIAL_NO, TestParameters.DB_COMMIT_SK2_SIG, "rubbish");

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void setFieldInRecord(String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetFieldInRecord_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a record.
    JSONObject newRecord = new JSONObject();
    newRecord.put(DBFields.ID, TestParameters.SERIAL_NO);
    DBObject obj = (DBObject) JSON.parse(newRecord.toString());

    submissions.insert(obj);

    // Test the record exists.
    BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    DBObject record = submissions.findOne(query);
    assertNotNull(record);
    assertEquals(TestParameters.SERIAL_NO, record.get(DBFields.ID));
    assertNull(record.get(TestParameters.DB_COMMIT_SK2_SIG));

    // Update the record.
    fixture.setFieldInRecord(TestParameters.SERIAL_NO, TestParameters.DB_COMMIT_SK2_SIG, "rubbish");

    // Test the new field exists.
    query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    record = submissions.findOne(query);
    assertNotNull(record);
    assertEquals(TestParameters.SERIAL_NO, record.get(DBFields.ID));
    assertEquals("rubbish", record.get(TestParameters.DB_COMMIT_SK2_SIG));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean setResponseAndCloseCommit(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = NoCommitExistsException.class)
  public void testSetResponseAndCloseCommit_1() throws Exception {
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test with no commit record.
    fixture.setResponseAndCloseCommit("rubbish");

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean setResponseAndCloseCommit(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetResponseAndCloseCommit_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Insert a current commitment.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hash = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hash);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    fixture.createNewCommitRecord(message, TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL,
        TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME, TestParameters.COMMIT_DESC);
    fixture.setCommitSentSig("rubbish");

    // Test with with commit record.
    assertTrue(fixture.setResponseAndCloseCommit("rubbish"));

    // Test that the current commit is gone.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);
    record = (DBObject) record.get(DBFields.COMMITMENT);
    assertNull(record);

    // Test that the commitment has been saved.
    query = new BasicDBObject(TestParameters.DB_RESPONSE, "rubbish");
    record = commits.findOne(query);
    assertNotNull(record);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean shouldRunR2Commit() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testShouldRunR2Commit_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test no previous record.
    boolean result = fixture.shouldRunR2Commit();
    assertFalse(result);

    // Add a record but false.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject doc = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    commits.insert(doc);

    // Test record but not running.
    result = fixture.shouldRunR2Commit();
    assertTrue(result);

    // Test already running.
    result = fixture.shouldRunR2Commit();
    assertFalse(result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitCipher(JSONObject) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitCipher_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a simple cipher.
    JSONObject cipher = new JSONObject();
    cipher.put(MessageFields.Cipher.ID, TestParameters.SERIAL_NO);
    cipher.put(MessageFields.UUID, TestParameters.SESSION_ID); // Dummy field.

    // Add the cipher.
    fixture.submitCipher(cipher);

    // Test that the cipher has been stored.
    DBCollection ciphers = db.getCollection(DBFields.Collections.CIPHERS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    DBObject record = ciphers.findOne(query);
    assertNotNull(record);
    assertEquals(TestParameters.SESSION_ID.toString(), record.get(MessageFields.UUID));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingClientCancellationMessage(CancelMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingClientCancellationMessage_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a cancel message.
    CancelMessage message = new CancelMessage(TestParameters.MESSAGE);

    fixture.submitIncomingClientCancellationMessage(message);

    // Test that the message has been stored.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
    DBObject record = submissions.findOne(query);
    assertNotNull(record);

    DBFields fields = DBFieldsCancel.getInstance();
    BasicDBList list = (BasicDBList) record.get(fields.getField(DBFieldName.CLIENT_MESSAGE));
    assertNotNull(list);
    assertEquals(1, list.size());
    assertEquals(message.getMsg().toString(), list.get(0).toString());

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingClientCancellationMessage(CancelMessage) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyReceivedMessageException.class)
  public void testSubmitIncomingClientCancellationMessage_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a cancel message.
    CancelMessage message = new CancelMessage(TestParameters.MESSAGE);

    fixture.submitIncomingClientCancellationMessage(message);

    // Add the message.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    // Add in a signature.
    BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();

    DBFields fields = DBFieldsCancel.getInstance();
    data.append(fields.getField(DBFieldName.SK2_SIGNATURE), TestParameters.signData(TestParameters.SESSION_ID.toString()));
    update.append(DBFields.SET, data);
    submissions.update(query, update);

    // Test that the message duplicate is spotted.
    fixture.submitIncomingClientCancellationMessage(message);
  }

  /**
   * Run the void submitIncomingClientMessage(DBRecordType,JSONWBBMessage,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingClientMessage_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      JSONWBBMessage message = new DummyJSONWBBMessage();
      String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());

      // Test no previous record.
      fixture.submitIncomingClientMessage(type, message, signature);

      // Test that the record is stored.
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
      DBObject record = submissions.findOne(query);
      assertNotNull(record);

      assertEquals(1, record.get(fields.getField(DBFieldName.SIGNATURE_COUNT)));
      assertEquals(message.getMsg().toString(), record.get(fields.getField(DBFieldName.CLIENT_MESSAGE)));
      assertEquals(signature, record.get(fields.getField(DBFieldName.MY_SIGNATURE)));
      assertEquals(message.getCommitTime(), record.get(fields.getField(DBFieldName.COMMIT_TIME)));

      // Attempt to store the message again.
      try {
        fixture.submitIncomingClientMessage(type, message, signature);
        fail("no exception");
      }
      catch (AlreadyReceivedMessageException e) {
        // Correct result.
      }

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingCommitMessage(DBRecordType,JSONWBBMessage,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingCommitMessage_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Repeat for submissions and conflicts.
      for (String collectionName : new String[] { DBFields.Collections.SUBMISSIONS, DBFields.Collections.COMMIT_CONFLICTS }) {
        DBCollection collection = db.getCollection(collectionName);

        JSONWBBMessage message = new DummyJSONWBBMessage();

        // Test no previous record.
        fixture.submitIncomingCommitMessage(type, message, collectionName.equals(DBFields.Collections.COMMIT_CONFLICTS));

        // Test that the record is stored.
        BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
        DBObject record = collection.findOne(query);
        assertNotNull(record);

        assertEquals(DBFields.COMMITMENT_COMMIT_R2, record.get(fields.getField(DBFieldName.MY_SIGNATURE)));

        if (type != DBRecordType.CANCEL) {
          assertEquals(message.getMsg().toString(), record.get(fields.getField(DBFieldName.CLIENT_MESSAGE)));
          assertEquals(message.getCommitTime(), record.get(fields.getField(DBFieldName.COMMIT_TIME)));
        }
        else {
          BasicDBList list = (BasicDBList) record.get(fields.getField(DBFieldName.CLIENT_MESSAGE));
          assertNotNull(list);
          assertEquals(1, list.size());
          assertEquals(message.getMsg().toString(), list.get(0).toString());
        }

        // Attempt to store the message again.
        try {
          fixture.submitIncomingCommitMessage(type, message, collectionName.equals(DBFields.Collections.COMMIT_CONFLICTS));
          fail("no exception");
        }
        catch (AlreadyReceivedMessageException e) {
          // Correct result.
        }

        // Delete the records.
        collection.remove(new BasicDBObject());
      }
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerCommitR1Checked(CommitR1Message,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerCommitR1Checked_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a commit message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message message = new CommitR1Message(object);

    // Test invalid.
    fixture.submitIncomingPeerCommitR1Checked(message, false);

    // Test that the commit has been stored.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);

    BasicDBObject commit = (BasicDBObject) record.get(DBFields.COMMITMENT);
    BasicDBList list = (BasicDBList) commit.get(DBFields.COMMITMENT_CHECKED_SIGS);
    assertNotNull(list);
    record = (DBObject) list.get(0);

    assertEquals(message.getFromPeer(), record.get(DBFields.FROM_PEER));
    assertEquals(message.getMsg().toString(), record.get(DBFields.COMMITMENT_MESSAGE));

    // Attempt to store the message again.
    try {
      fixture.submitIncomingPeerCommitR1Checked(message, false);
      fail("no exception");
    }
    catch (AlreadyReceivedMessageException e) {
      // Correct result.
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerCommitR1Checked(CommitR1Message,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerCommitR1Checked_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a commit message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message message = new CommitR1Message(object);

    // Test valid.
    fixture.submitIncomingPeerCommitR1Checked(message, true);

    // Test that the commit has been stored.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);

    BasicDBObject commit = (BasicDBObject) record.get(DBFields.COMMITMENT);
    assertEquals(1, commit.get(DBFields.COMMITMENT_SIG_COUNT));

    BasicDBList list = (BasicDBList) commit.get(DBFields.COMMITMENT_CHECKED_SIGS);
    assertNotNull(list);
    record = (DBObject) list.get(0);

    assertEquals(message.getFromPeer(), record.get(DBFields.FROM_PEER));
    assertEquals(message.getMsg().toString(), record.get(DBFields.COMMITMENT_MESSAGE));

    // Attempt to store the message again.
    try {
      fixture.submitIncomingPeerCommitR1Checked(message, true);
      fail("no exception");
    }
    catch (AlreadyReceivedMessageException e) {
      // Correct result.
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerCommitR1UnChecked(CommitR1Message) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerCommitR1UnChecked_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a commit message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message message = new CommitR1Message(object);

    // Test valid.
    fixture.submitIncomingPeerCommitR1UnChecked(message);

    // Test that the commit has been stored.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);

    BasicDBObject commit = (BasicDBObject) record.get(DBFields.COMMITMENT);

    BasicDBList list = (BasicDBList) commit.get(TestParameters.DB_SIGS_TO_CHECK);
    assertNotNull(list);
    record = (DBObject) list.get(0);

    assertEquals(message.getFromPeer(), record.get(DBFields.FROM_PEER));
    assertEquals(message.getMsg().toString(), record.get(DBFields.COMMITMENT_MESSAGE));

    // Attempt to store the message again.
    try {
      fixture.submitIncomingPeerCommitR1UnChecked(message);
      fail("no exception");
    }
    catch (AlreadyReceivedMessageException e) {
      // Correct result.
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerCommitR2Checked(CommitR2Message) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerCommitR2Checked_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create a commit message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.CommitR2.FILESIZE, 0l);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message message = new CommitR2Message(object);

    // Test valid.
    fixture.submitIncomingPeerCommitR2Checked(message);

    // Test that the commit has been stored.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);

    BasicDBObject commit = (BasicDBObject) record.get(DBFields.COMMITMENT);
    assertEquals(1, commit.get(TestParameters.DB_ROUND2_PROCESSED_COUNT));

    BasicDBList list = (BasicDBList) commit.get(TestParameters.DB_R2_COMMITS);
    assertNotNull(list);
    record = (DBObject) list.get(0);

    assertEquals(message.getFromPeer(), record.get(DBFields.FROM_PEER));
    assertEquals(message.getMsg().toString(), record.get(DBFields.COMMITMENT_MESSAGE));

    // Attempt to store the message again.
    try {
      fixture.submitIncomingPeerCommitR2Checked(message);
      fail("no exception");
    }
    catch (AlreadyReceivedMessageException e) {
      // Correct result.
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerMessageChecked(DBRecordType,PeerMessage,boolean,boolean,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerMessageChecked_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Repeat for submissions and conflicts.
      for (String collectionName : new String[] { DBFields.Collections.SUBMISSIONS, DBFields.Collections.COMMIT_CONFLICTS }) {
        DBCollection collection = db.getCollection(collectionName);

        // Cancel is never in conflict.
        if (type == DBRecordType.CANCEL) {
          collection = db.getCollection(DBFields.Collections.SUBMISSIONS);
        }

        JSONObject object = new JSONObject(TestParameters.MESSAGE);
        object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
        object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

        CommitR1Message message = new CommitR1Message(object);

        // Test no previous record and invalid.
        fixture.submitIncomingPeerMessageChecked(type, message, false, false,
            collectionName.equals(DBFields.Collections.COMMIT_CONFLICTS));

        // Test that the record is stored.
        BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
        DBObject record = collection.findOne(query);
        assertNotNull(record);

        BasicDBList list = (BasicDBList) record.get(fields.getField(DBFieldName.CHECKED_SIGS));
        assertNotNull(list);
        assertEquals(1, list.size());
        record = (DBObject) list.get(0);

        assertEquals(message.getFromPeer(), record.get(fields.getField(DBFieldName.FROM_PEER)));
        assertEquals(message.getMsg().toString(), record.get(fields.getField(DBFieldName.QUEUED_MESSAGE)));

        // Attempt to store the message again.
        try {
          fixture.submitIncomingPeerMessageChecked(type, message, false, false,
              collectionName.equals(DBFields.Collections.COMMIT_CONFLICTS));
          fail("no exception");
        }
        catch (AlreadyReceivedMessageException e) {
          // Correct result.
        }

        // Delete the records.
        collection.remove(new BasicDBObject());
      }
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerMessageChecked(DBRecordType,PeerMessage,boolean,boolean,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerMessageChecked_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Repeat for submissions and conflicts.
      for (String collectionName : new String[] { DBFields.Collections.SUBMISSIONS, DBFields.Collections.COMMIT_CONFLICTS }) {
        DBCollection collection = db.getCollection(collectionName);

        // Cancel is never in conflict.
        if (type == DBRecordType.CANCEL) {
          collection = db.getCollection(DBFields.Collections.SUBMISSIONS);
        }

        JSONObject object = new JSONObject(TestParameters.MESSAGE);
        object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
        object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

        CommitR1Message message = new CommitR1Message(object);

        // Test no previous record and valid.
        fixture.submitIncomingPeerMessageChecked(type, message, true, false,
            collectionName.equals(DBFields.Collections.COMMIT_CONFLICTS));

        // Test that the record is stored.
        BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
        DBObject record = collection.findOne(query);
        assertNotNull(record);
        assertEquals(1, record.get(fields.getField(DBFieldName.SIGNATURE_COUNT)));

        BasicDBList list = (BasicDBList) record.get(fields.getField(DBFieldName.CHECKED_SIGS));
        assertNotNull(list);
        assertEquals(1, list.size());
        record = (DBObject) list.get(0);

        assertEquals(message.getFromPeer(), record.get(fields.getField(DBFieldName.FROM_PEER)));
        assertEquals(message.getMsg().toString(), record.get(fields.getField(DBFieldName.QUEUED_MESSAGE)));

        // Attempt to store the message again.
        try {
          fixture.submitIncomingPeerMessageChecked(type, message, true, false,
              collectionName.equals(DBFields.Collections.COMMIT_CONFLICTS));
          fail("no exception");
        }
        catch (AlreadyReceivedMessageException e) {
          // Correct result.
        }

        // Delete the records.
        collection.remove(new BasicDBObject());
      }
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitIncomingPeerMessageUnchecked(DBRecordType,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerMessageUnchecked_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test all record types with no record of the serial number.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      JSONObject object = new JSONObject(TestParameters.MESSAGE);
      object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
      object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

      CommitR1Message message = new CommitR1Message(object);

      // Test no previous record and valid.
      fixture.submitIncomingPeerMessageUnchecked(type, message);

      // Test that the record is stored.
      BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
      DBObject record = submissions.findOne(query);
      assertNotNull(record);

      BasicDBList list = (BasicDBList) record.get(fields.getField(DBFieldName.SIGNATURES_TO_CHECK));
      assertNotNull(list);
      assertEquals(1, list.size());
      record = (DBObject) list.get(0);

      assertEquals(message.getFromPeer(), record.get(fields.getField(DBFieldName.FROM_PEER)));
      assertEquals(message.getMsg().toString(), record.get(fields.getField(DBFieldName.QUEUED_MESSAGE)));

      // Attempt to store the message again.
      try {
        fixture.submitIncomingPeerMessageUnchecked(type, message);
        fail("no exception");
      }
      catch (AlreadyReceivedMessageException e) {
        // Correct result.
      }

      // Delete the records.
      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the String submitMyCancellationSignature(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitMyCancellationSignature_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());

    // Test valid.
    String result = fixture.submitMyCancellationSignature(TestParameters.SERIAL_NO, signature);
    assertNotNull(result);

    // Test that the cancellation has been stored.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);
    DBFieldsCancel fields = DBFieldsCancel.getInstance();

    BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    DBObject record = submissions.findOne(query);
    assertNotNull(record);

    assertEquals(signature, record.get(fields.getField(DBFieldName.MY_SIGNATURE)));
    assertEquals(1, record.get(fields.getField(DBFieldName.SIGNATURE_COUNT)));

    // Test duplicate.
    result = fixture.submitMyCancellationSignature(TestParameters.SERIAL_NO, null);
    assertNotNull(result);

    assertEquals(signature, record.get(fields.getField(DBFieldName.MY_SIGNATURE)));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the String submitMyCancellationSignatureSK2(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitMyCancellationSignatureSK2_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());

    // Test valid.
    String result = fixture.submitMyCancellationSignatureSK2(TestParameters.SERIAL_NO, signature);
    assertNotNull(result);

    // Test that the cancellation has been stored.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);
    DBFieldsCancel fields = DBFieldsCancel.getInstance();

    BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    DBObject record = submissions.findOne(query);
    assertNotNull(record);

    assertEquals(signature, record.get(fields.getField(DBFieldName.SK2_SIGNATURE)));

    // Test duplicate.
    result = fixture.submitMyCancellationSignatureSK2(TestParameters.SERIAL_NO, null);
    assertNotNull(result);

    assertEquals(signature, record.get(fields.getField(DBFieldName.SK2_SIGNATURE)));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitPostTimeoutMessage(JSONWBBMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitPostTimeoutMessage_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    JSONWBBMessage message = new DummyJSONWBBMessage();

    fixture.submitPostTimeoutMessage(message);

    // Test the message was stored.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);
    DBFields fields = DBFields.getInstance();

    BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
    DBObject record = submissions.findOne(query);

    BasicDBList list = (BasicDBList) record.get(fields.getField(DBFieldName.POST_TIMEOUT));
    assertNotNull(list);
    assertEquals(1, list.size());
    record = (DBObject) list.get(0);

    assertEquals(message.getMsg().toString(), record.get(fields.getField(DBFieldName.QUEUED_MESSAGE)));
  }

  /**
   * Run the void submitStartEVMMessage(StartEVMMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitStartEVMMessage_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    StartEVMMessage message = new StartEVMMessage(TestParameters.MESSAGE);

    // Test no previous record.
    fixture.submitStartEVMMessage(message);

    // Test that the record is stored.
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    BasicDBObject query = new BasicDBObject(DBFields.ID, message.getID());
    DBObject record = submissions.findOne(query);
    assertNotNull(record);

    // Attempt to store the message again.
    try {
      fixture.submitStartEVMMessage(message);
      fail("no exception");
    }
    catch (AlreadyReceivedMessageException e) {
      // Correct result.
    }

    // Delete the records.
    submissions.remove(new BasicDBObject());

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void submitStartEVMMessage(StartEVMMessage) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MaxTimeoutExceeded.class)
  public void testSubmitStartEVMMessage_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    StartEVMMessage message = new StartEVMMessage(TestParameters.MESSAGE);

    // Test timeout.
    BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    doc.append(DBFieldsPOD.BALLOT_TIMEOUT, System.currentTimeMillis() - 100000);
    submissions.insert(doc);

    fixture.submitStartEVMMessage(message);
  }

  /**
   * Run the void submitStartEVMMessage(StartEVMMessage) method test.
   * 
   * @throws Exception
   */
  @Test(expected = ClientMessageExistsException.class)
  public void testSubmitStartEVMMessage_3() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);
    DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

    StartEVMMessage message = new StartEVMMessage(TestParameters.MESSAGE);

    // Test record but no data.
    DBFields generalFields = DBFields.getInstance();
    BasicDBObject doc = new BasicDBObject(DBFields.ID, TestParameters.SERIAL_NO);
    doc.append(generalFields.getField(DBFieldName.CLIENT_MESSAGE), "rubbish");
    submissions.insert(doc);

    fixture.submitStartEVMMessage(message);
  }

  /**
   * Run the boolean timeoutSent(DBRecordType,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testTimeoutSent_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Test only POD and GENERAL messages as CANCEL messages cannot timeout.
    for (DBRecordType type : new DBRecordType[] { DBRecordType.POD, DBRecordType.GENERAL }) {
      JSONWBBMessage message = new DummyJSONWBBMessage(TestParameters.MESSAGE);

      // Test no timeout.
      boolean result = fixture.timeoutSent(type, message.getID());
      assertFalse(result);

      // Add a timeout.
      fixture.checkAndSetTimeout(type, message);

      // Test no timeout.
      result = fixture.timeoutSent(type, message.getID());
      assertTrue(result);

      // Delete the records.
      DBCollection submissions = db.getCollection(DBFields.Collections.SUBMISSIONS);

      submissions.remove(new BasicDBObject());
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void updateCurrentCommitRecord(String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateCurrentCommitRecord_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    ConcurrentDB fixture = new ConcurrentDB(TestParameters.TEST_DATABASE);

    // Create the commit message.
    JSONObject object = new JSONObject();

    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String hashR1 = TestParameters.signData(signature);
    long time = System.currentTimeMillis();

    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.Commit.HASH, hashR1);
    object.put(MessageFields.Commit.DATETIME, time);

    JSONWBBMessage message = new DummyJSONWBBMessage(object);

    // Create the commit record.
    String commitFileR1 = "R1" + TestParameters.COMMIT_FILE;
    String commitFileFullR1 = "R1" + TestParameters.COMMIT_FILE_FULL;
    String commitAttachmentsR1 = "R1" + TestParameters.COMMIT_FILE_ZIP;

    fixture.createNewCommitRecord(message, commitFileR1, commitFileFullR1, commitAttachmentsR1, TestParameters.COMMIT_TIME,
        TestParameters.COMMIT_DESC);

    // Update it.
    String hash = TestParameters.signData(TestParameters.SESSION_ID.toString());
    fixture.updateCurrentCommitRecord(TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_FULL, TestParameters.COMMIT_FILE_ZIP,
        hash);

    // Test that the record has been updated.
    DBCollection commits = db.getCollection(DBFields.Collections.COMMITS);
    BasicDBObject query = new BasicDBObject(DBFields.ID, DBFields.CURRENT_COMMIT);
    DBObject record = commits.findOne(query);
    assertNotNull(record);
    record = (DBObject) record.get(DBFields.COMMITMENT);

    assertEquals(commitFileR1, record.get(TestParameters.DB_COMMIT_FILE_R1));
    assertEquals(hashR1, record.get(TestParameters.DB_COMMIT_HASH_R1));
    assertEquals(commitFileFullR1, record.get(TestParameters.DB_COMMIT_FILE_FULL_R1));
    assertEquals(commitAttachmentsR1, record.get(TestParameters.DB_COMMIT_ATTACHMENTS_R1));

    assertEquals(TestParameters.COMMIT_FILE, record.get(DBFields.COMMITMENT_FILE));
    assertEquals(hash, record.get(DBFields.COMMIT_HASH));
    assertEquals(TestParameters.COMMIT_FILE_FULL, record.get(DBFields.COMMITMENT_FILE_FULL));
    assertEquals(TestParameters.COMMIT_FILE_ZIP, record.get(DBFields.COMMITMENT_ATTACHMENTS));

    TestParameters.getInstance().closeDB();
  }
}