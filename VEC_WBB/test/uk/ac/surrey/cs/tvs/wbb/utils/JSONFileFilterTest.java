/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * The class <code>JSONFileFilterTest</code> contains tests for the class <code>{@link JSONFileFilter}</code>.
 */
public class JSONFileFilterTest {

  /**
   * Run the boolean accept(File,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAccept_1() throws Exception {
    JSONFileFilter fixture = new JSONFileFilter();

    assertTrue(fixture.accept(null, "file.json"));
    assertFalse(fixture.accept(null, "file"));
    assertFalse(fixture.accept(null, "file.txt"));
  }

  /**
   * Run the JSONFileFilter() constructor test.
   */
  @Test
  public void testJSONFileFilter_1() throws Exception {
    JSONFileFilter result = new JSONFileFilter();
    assertNotNull(result);
  }
}