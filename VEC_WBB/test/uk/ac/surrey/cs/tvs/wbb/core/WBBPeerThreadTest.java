/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocket;
import javax.security.cert.X509Certificate;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>WBBPeerThreadTest</code> contains tests for the class <code>{@link WBBPeerThread}</code>.
 */
public class WBBPeerThreadTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends SSLSocket {

    private ByteArrayInputStream inputStream = null;

    private boolean              closed      = false;

    public DummySocket(String input) {
      super();

      if (input == null) {
        this.inputStream = new ByteArrayInputStream(new byte[0]);
      }
      else {
        this.inputStream = new ByteArrayInputStream(input.getBytes());
      }
    }

    @Override
    public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public String[] getEnabledCipherSuites() {
      return null;
    }

    @Override
    public String[] getEnabledProtocols() {
      return null;
    }

    @Override
    public boolean getEnableSessionCreation() {
      return false;
    }

    @Override
    public InputStream getInputStream() throws IOException {
      return this.inputStream;
    }

    @Override
    public boolean getNeedClientAuth() {
      return false;
    }

    @Override
    public SocketAddress getRemoteSocketAddress() {
      return new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.INTERNAL_PORT);
    }

    @Override
    public SSLSession getSession() {
      return new SSLSession() {

        @Override
        public int getApplicationBufferSize() {
          return 0;
        }

        @Override
        public String getCipherSuite() {
          return null;
        }

        @Override
        public long getCreationTime() {
          return 0;
        }

        @Override
        public byte[] getId() {
          return null;
        }

        @Override
        public long getLastAccessedTime() {
          return 0;
        }

        @Override
        public Certificate[] getLocalCertificates() {
          return null;
        }

        @Override
        public Principal getLocalPrincipal() {
          return null;
        }

        @Override
        public int getPacketBufferSize() {
          return 0;
        }

        @Override
        public X509Certificate[] getPeerCertificateChain() throws SSLPeerUnverifiedException {
          return null;
        }

        @Override
        public Certificate[] getPeerCertificates() throws SSLPeerUnverifiedException {
          return null;
        }

        @Override
        public String getPeerHost() {
          return null;
        }

        @Override
        public int getPeerPort() {
          return 0;
        }

        @Override
        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
          return TestParameters.PRINCIPAL;
        }

        @Override
        public String getProtocol() {
          return null;
        }

        @Override
        public SSLSessionContext getSessionContext() {
          return null;
        }

        @Override
        public Object getValue(String name) {
          return null;
        }

        @Override
        public String[] getValueNames() {
          return null;
        }

        @Override
        public void invalidate() {
        }

        @Override
        public boolean isValid() {
          return false;
        }

        @Override
        public void putValue(String name, Object value) {
        }

        @Override
        public void removeValue(String name) {
        }
      };
    }

    @Override
    public String[] getSupportedCipherSuites() {
      return null;
    }

    @Override
    public String[] getSupportedProtocols() {
      return null;
    }

    @Override
    public boolean getUseClientMode() {
      return false;
    }

    @Override
    public boolean getWantClientAuth() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }

    @Override
    public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public void setEnabledCipherSuites(String[] suites) {
    }

    @Override
    public void setEnabledProtocols(String[] protocols) {
    }

    @Override
    public void setEnableSessionCreation(boolean flag) {
    }

    @Override
    public void setNeedClientAuth(boolean need) {
    }

    @Override
    public void setUseClientMode(boolean mode) {
    }

    @Override
    public void setWantClientAuth(boolean want) {
    }

    @Override
    public void startHandshake() throws IOException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Tidy up the sockets.
    TestParameters.tidySockets();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Tidy up the sockets.
    TestParameters.tidySockets();
  }

  /**
   * Run the void forceShutdown() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testForceShutdown_1() throws Exception {
    DummySocket socket = new DummySocket(TestParameters.LINE);

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, null);

    fixture.forceShutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    // Create a valid peer message.
    MessageDigest messageDigest = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    messageDigest.update(TestParameters.LINE.getBytes());

    String digest = IOUtils.encodeData(EncodingType.BASE64, messageDigest.digest());
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, "rubbish");
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.PeerFile.ATTACHMENT_SIZE, 0);

    DummySocket socket = new DummySocket(object.toString() + "\n" + TestParameters.LINE);

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test valid peer message and valid host: with attachments and not a valid digest.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    // Create a valid internal message.
    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummySocket socket = new DummySocket(object.toString() + "\n");

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test valid message but not valid host.
    TestParameters.getInstance().getWbbPeer().setValidConnection(false);

    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    // Create a valid internal message.
    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummySocket socket = new DummySocket(object.toString() + "\n");

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test valid message and valid host.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_4() throws Exception {
    // Create a valid peer message.
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummySocket socket = new DummySocket(object.toString() + "\n");

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test valid peer message and valid host: no attachments and not a valid digest.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_5() throws Exception {
    // Create a valid peer message.
    MessageDigest messageDigest = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    messageDigest.update(TestParameters.LINE.getBytes());

    String digest = IOUtils.encodeData(EncodingType.BASE64, messageDigest.digest());
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, "rubbish");
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummySocket socket = new DummySocket(object.toString() + "\n" + TestParameters.LINE);

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test valid peer message and valid host: no attachments and valid digest.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_6() throws Exception {
    // Create a valid peer message.
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, "rubbish");
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.PeerFile.ATTACHMENT_SIZE, 0);

    DummySocket socket = new DummySocket(object.toString() + "\n" + TestParameters.LINE);

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test valid peer message and valid host: with attachments and not a valid digest.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_7() throws Exception {
    DummySocket socket = new DummySocket(null);

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    // Test null input.
    fixture.run();

    assertTrue(socket.isClosed());

    fixture.shutdown();
  }

  /**
   * Run the void shutdown() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testShutdown_1() throws Exception {
    DummySocket socket = new DummySocket(TestParameters.LINE);

    WBBPeerThread fixture = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());

    fixture.shutdown();

    // Make sure we don't read any data.
    fixture.run();
  }

  /**
   * Run the WBBPeerThread(SSLSocket,WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testWBBPeerThread_1() throws Exception {
    DummySocket socket = new DummySocket(TestParameters.LINE);

    WBBPeerThread result = new WBBPeerThread(TestParameters.getInstance().getWbbPeer(), socket, new AtomicInteger());
    assertNotNull(result);

    result.shutdown();
  }
}