/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;

/**
 * The class <code>VoteMessageTest</code> contains tests for the class <code>{@link VoteMessage}</code>.
 */
public class VoteMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    VoteMessage fixture = new VoteMessage(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + TestParameters.DISTRICT + TestParameters.PERMUTATION + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    VoteMessage fixture = new VoteMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + TestParameters.DISTRICT + TestParameters.PERMUTATION + signature
        + TestParameters.SENDER_CERTIFICATE + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    // No type.
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    VoteMessage fixture = new VoteMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, "rubbish" };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    VoteMessage fixture = new VoteMessage(object);

    // Test valid schema but invalid serial signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };
    String signature = TestParameters.signData("rubbish");

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    VoteMessage fixture = new VoteMessage(object);

    // Test valid schema but invalid EBM signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_4() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT, "rubbish" };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    VoteMessage fixture = new VoteMessage(object);

    // Test invalid start EVM.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_5() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };

    String signature = TestParameters.signBLSData(data,TestParameters.CLIENT_EVM_KEYSTORE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_EVM_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    VoteMessage fixture = new VoteMessage(object);

    // Test valid.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testVoteMessage_1() throws Exception {
    VoteMessage result = new VoteMessage("");
    assertNull(result);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testVoteMessage_2() throws Exception {
    VoteMessage result = new VoteMessage("{}");
    assertNull(result);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVoteMessage_3() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(TestParameters.DEVICE));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    VoteMessage result = new VoteMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.VOTE_MESSAGE, result.type);
    assertEquals(TestParameters.PREFERENCES, result.msg.get(MessageFields.VoteMessage.INTERNAL_PREFS));
    assertTrue(result.getInternalSignableContent().contains(TestParameters.SENDER_CERTIFICATE));
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVoteMessage_4() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(TestParameters.DEVICE));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PREFERENCES);

    VoteMessage result = new VoteMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.VOTE_MESSAGE, result.type);
    assertEquals(TestParameters.PREFERENCES, result.msg.get(MessageFields.VoteMessage.INTERNAL_PREFS));
    assertTrue(result.getInternalSignableContent().contains(TestParameters.SENDER_CERTIFICATE));
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testVoteMessage_5() throws Exception {
    VoteMessage result = new VoteMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVoteMessage_6() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(TestParameters.DEVICE));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    VoteMessage result = new VoteMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.VOTE_MESSAGE, result.type);
    assertEquals(TestParameters.PREFERENCES, result.msg.get(MessageFields.VoteMessage.INTERNAL_PREFS));
    assertTrue(result.getInternalSignableContent().contains(TestParameters.SENDER_CERTIFICATE));
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVoteMessage_7() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(TestParameters.DEVICE));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PREFERENCES);

    VoteMessage result = new VoteMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.VOTE_MESSAGE, result.type);
    assertEquals(TestParameters.PREFERENCES, result.msg.get(MessageFields.VoteMessage.INTERNAL_PREFS));
    assertTrue(result.getInternalSignableContent().contains(TestParameters.SENDER_CERTIFICATE));
  }
}