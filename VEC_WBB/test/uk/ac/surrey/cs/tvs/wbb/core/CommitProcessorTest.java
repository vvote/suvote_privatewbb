/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.DummyJSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message;
import uk.ac.surrey.cs.tvs.wbb.messages.FileMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.types.CommitFiles;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.utils.CommitTimeUtils;

/**
 * The class <code>CommitProcessorTest</code> contains tests for the class <code>{@link CommitProcessor}</code>.
 */
public class CommitProcessorTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Reset the WBB.
    TestParameters.getInstance().getWbbPeer().reset();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void checkAndStoreCommitMessage(String,CommitR1Message,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreCommitMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CommitProcessor fixture = new CommitProcessor(TestParameters.getInstance().getWbbPeer());

    TestParameters.signData(TestParameters.SESSION_ID.toString());
    CommitTimeUtils commitTime = new CommitTimeUtils(TestParameters.COMMIT_TIME_HOURS, TestParameters.COMMIT_TIME_MINUTES);
    String hash = TestParameters.signData(TestParameters.SESSION_ID.toString());
    String messageSignature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash),
        commitTime.getPreviousCommitID());

    // Create a commit message.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, messageSignature);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    CommitR1Message commit = new CommitR1Message(message);

    // Test storing the message.
    fixture.checkAndStoreCommitMessage(hash, commit, commitTime.getPreviousCommitID(),TestParameters.COMMIT_DESC);
    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertTrue(db.isValid());
    assertEquals(commit, db.getMessage());
  }

  /**
   * Run the void checkAndStoreCommitMessage(String,CommitR1Message,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreCommitMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CommitProcessor fixture = new CommitProcessor(TestParameters.getInstance().getWbbPeer());

    // Calculate the commit hash with the wrong key.
    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer()
        .getSK2());
    CommitTimeUtils commitTime = new CommitTimeUtils(TestParameters.COMMIT_TIME_HOURS, TestParameters.COMMIT_TIME_MINUTES);
    String hash = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, signature), commitTime.getPreviousCommitID());

    // Create a commit message.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, hash);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    CommitR1Message commit = new CommitR1Message(message);

    // Test storing the message.
    fixture.checkAndStoreCommitMessage(TestParameters.signData(TestParameters.SESSION_ID.toString()), commit,
        commitTime.getPreviousCommitID(),TestParameters.COMMIT_DESC);
    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertFalse(db.isValid());
    assertEquals(commit, db.getMessage());
  }

  /**
   * Run the void checkCommitThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitThreshold_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 1);
    db.setStored(true);

    // Test when the threshold has been reached.
    CommitProcessor.checkCommitThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(db.isCheckCommitThresholdCalled());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertTrue(db.isSetCommitSentSigCalled());
  }

  /**
   * Run the void checkCommitThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitThreshold_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 1);
    db.addException("getCurrentCommitField", 0);

    // Test when the threshold has been reached but an exception retrieving the commit data.
    CommitProcessor.checkCommitThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(db.isCheckCommitThresholdCalled());

    assertFalse(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertFalse(db.isSetCommitSentSigCalled());
    assertFalse(TestParameters.getInstance().getWbbPeer().isScheduleNextCommitCalled());
  }

  /**
   * Run the void checkCommitThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitThreshold_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 2); // Not enough.
    db.setCanReachConsensus(true);
    db.setRunCommitR2(false);

    // Test when the threshold has not been reached and consensus is possible.
    CommitProcessor.checkCommitThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(db.isCheckCommitThresholdCalled());
    assertTrue(db.isCanReachCommitConsensusCalled());
    assertFalse(db.isShouldRunR2CommitCalled());

    assertNull(TestParameters.getInstance().getWbbPeer().getFile());
    assertNull(TestParameters.getInstance().getWbbPeer().getAttachments());
    assertNull(TestParameters.getInstance().getWbbPeer().getCommitID());
    //TODO CJC: I don't understand what this assertion is aim at? getCommitTime always returns a string, it should never be null
    //assertNull(TestParameters.getInstance().getWbbPeer().getCommitTime());
  }

  /**
   * Run the void checkCommitThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitThreshold_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 2); // Not enough.
    db.setCanReachConsensus(false);
    db.setRunCommitR2(false);

    // Test when the threshold has not been reached and consensus is not possible.
    CommitProcessor.checkCommitThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(db.isCheckCommitThresholdCalled());
    assertTrue(db.isCanReachCommitConsensusCalled());
    assertTrue(db.isShouldRunR2CommitCalled());

    assertNull(TestParameters.getInstance().getWbbPeer().getFile());
    assertNull(TestParameters.getInstance().getWbbPeer().getAttachments());
    assertNull(TestParameters.getInstance().getWbbPeer().getCommitID());
    //TODO CJC: I don't understand what this assertion is aim at? getCommitTime always returns a string, it should never be null
    //assertNull(TestParameters.getInstance().getWbbPeer().getCommitTime());
  }

  /**
   * Run the void checkCommitThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckCommitThreshold_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 2); // Not enough.
    db.setCanReachConsensus(false);
    db.setRunCommitR2(true);

    // Generate some messages to process.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    messages.add(new DummyJSONWBBMessage());
    messages.add(new DummyJSONWBBMessage());
    db.setMessages(messages);

    // Test when the threshold has not been reached and consensus is not possible with round 2.
    CommitProcessor.checkCommitThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(db.isCheckCommitThresholdCalled());
    assertTrue(db.isCanReachCommitConsensusCalled());
    assertTrue(db.isShouldRunR2CommitCalled());

    assertNotNull(TestParameters.getInstance().getWbbPeer().getFile());
    assertNotNull(TestParameters.getInstance().getWbbPeer().getAttachments());
    assertNotNull(TestParameters.getInstance().getWbbPeer().getCommitID());
    assertNotNull(TestParameters.getInstance().getWbbPeer().getCommitTime());

    assertEquals(DBFields.COMMITMENT_FILE_FULL, TestParameters.getInstance().getWbbPeer().getFile());
    assertEquals(DBFields.COMMITMENT_ATTACHMENTS, TestParameters.getInstance().getWbbPeer().getAttachments());
    assertEquals(DBFields.COMMITMENT_ID, TestParameters.getInstance().getWbbPeer().getCommitID());
    assertEquals(DBFields.COMMITMENT_TIME, TestParameters.getInstance().getWbbPeer().getCommitTime());

    // Test that the messages were processed after waiting for execution to complete.
    ExecutorService executor = TestParameters.getInstance().getWbbPeer().getCommitExecutor();
    executor.shutdown();
    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

    for (JSONWBBMessage message : messages) {
      assertTrue(((DummyJSONWBBMessage) message).isProcessMessageCalled());
    }
  }

  /**
   * Run the CommitProcessor(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitProcessor_1() throws Exception {
    CommitProcessor result = new CommitProcessor(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the CommitProcessor(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitProcessor_2() throws Exception {
    CommitTimeUtils commitTime = new CommitTimeUtils(TestParameters.COMMIT_TIME_HOURS, TestParameters.COMMIT_TIME_MINUTES);

    CommitProcessor result = new CommitProcessor(TestParameters.getInstance().getWbbPeer(), commitTime.getCommitTime());
    assertNotNull(result);
  }

  /**
   * Run the HashMap<CommitFiles, File> createCommitFiles(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateCommitFiles_1() throws Exception {
    HashMap<CommitFiles, File> result = CommitProcessor.createCommitFiles(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);

    // Test that each file has been added to the hash map and exists in the folder structure.
    assertEquals(4, result.keySet().size());

    assertTrue(result.keySet().contains(CommitFiles.COMMIT_FOLDER));
    assertTrue(result.keySet().contains(CommitFiles.COMMIT_FILE_FULL));
    assertTrue(result.keySet().contains(CommitFiles.COMMIT_FILE_FULL_ZIP));
    assertTrue(result.keySet().contains(CommitFiles.COMMIT_FILE));

    String commitFolder = new File(TestParameters.getInstance().getWbbPeer().getConfig().getString(WBBConfig.COMMIT_DIRECTORY))
        .toString();

    assertEquals(commitFolder, result.get(CommitFiles.COMMIT_FOLDER).toString());

    assertTrue(result.get(CommitFiles.COMMIT_FILE_FULL).toString()
        .startsWith(commitFolder + System.getProperty("file.separator") + TestParameters.COMMIT_FILE_FULL_PREFIX));
    assertTrue(result.get(CommitFiles.COMMIT_FILE_FULL_ZIP).toString()
        .startsWith(commitFolder + System.getProperty("file.separator") + TestParameters.COMMIT_FILE_ZIP_PREFIX));
    assertTrue(result.get(CommitFiles.COMMIT_FILE).toString()
        .startsWith(commitFolder + System.getProperty("file.separator") + TestParameters.COMMIT_FILE_PREFIX));

    assertTrue(result.get(CommitFiles.COMMIT_FILE_FULL).toString().endsWith(TestParameters.COMMIT_FILE_FULL_EXT));
    assertTrue(result.get(CommitFiles.COMMIT_FILE_FULL_ZIP).toString().endsWith(TestParameters.COMMIT_FILE_ZIP_EXT));
    assertTrue(result.get(CommitFiles.COMMIT_FILE).toString().endsWith(TestParameters.COMMIT_FILE_EXT));

    assertTrue(result.get(CommitFiles.COMMIT_FILE_FULL).exists());
    assertTrue(result.get(CommitFiles.COMMIT_FILE_FULL_ZIP).exists());
    assertTrue(result.get(CommitFiles.COMMIT_FILE).exists());
  }

  /**
   * Run the byte[] prepareCommitFiles(WBBPeer,String,HashMap<CommitFiles,File>) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPrepareCommitFiles_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Generate some messages to process.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    messages.add(new DummyJSONWBBMessage());
    messages.add(new DummyJSONWBBMessage());
    db.setMessages(messages);

    HashMap<CommitFiles, File> commitFiles = CommitProcessor.createCommitFiles(TestParameters.getInstance().getWbbPeer());

    // Test preparing commit files which have no associated files.
    byte[] result = CommitProcessor.prepareCommitFiles(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance()
        .getWbbPeer().getPreviousCommitTime(), commitFiles);
    assertNotNull(result);

    // Make sure the full file and the commit file have the same content.
    assertTrue(TestParameters.compareFile(commitFiles.get(CommitFiles.COMMIT_FILE_FULL), commitFiles.get(CommitFiles.COMMIT_FILE)));
  }

  /**
   * Run the byte[] prepareCommitFiles(WBBPeer,String,HashMap<CommitFiles,File>) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPrepareCommitFiles_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Generate a file message.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.UPLOAD_FILE);
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    messages.add(new FileMessage(message));
    db.setMessages(messages);

    // Create the test file.
    File testFile = new File(TestParameters.getInstance().getWbbPeer().getUploadDir(), TestParameters.UPLOAD_FILE);
    TestParameters.createTestFile(testFile);

    HashMap<CommitFiles, File> commitFiles = CommitProcessor.createCommitFiles(TestParameters.getInstance().getWbbPeer());

    // Test preparing commit files which have associated files.
    byte[] result = CommitProcessor.prepareCommitFiles(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance()
        .getWbbPeer().getPreviousCommitTime(), commitFiles);
    assertNotNull(result);

    // Make sure the full file and the commit file have the same content.
    assertTrue(TestParameters.compareFile(commitFiles.get(CommitFiles.COMMIT_FILE_FULL), commitFiles.get(CommitFiles.COMMIT_FILE)));

    // Make sure the file has been added to the ZIP.
    File zipFolder = new File(TestParameters.ZIP_FOLDER);

    TestParameters.extractZip(commitFiles.get(CommitFiles.COMMIT_FILE_FULL_ZIP), zipFolder);
    assertTrue(TestParameters.compareFile(testFile, new File(zipFolder, TestParameters.UPLOAD_FILE)));
  }

  /**
   * Run the byte[] prepareCommitFiles(WBBPeer,String,HashMap<CommitFiles,File>) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPrepareCommitFiles_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Generate a file message.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.getInstance().getWbbPeer().getCommitUploadDir().toString());
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.UPLOAD_FILE);
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    messages.add(new FileMessage(message));
    db.setMessages(messages);

    // Create the test file from the safe upload.
    File testFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.UPLOAD_FILE);
    TestParameters.createTestFile(testFile);

    HashMap<CommitFiles, File> commitFiles = CommitProcessor.createCommitFiles(TestParameters.getInstance().getWbbPeer());

    // Test preparing commit files which have associated files.
    byte[] result = CommitProcessor.prepareCommitFiles(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance()
        .getWbbPeer().getPreviousCommitTime(), commitFiles);
    assertNotNull(result);

    // Make sure the full file and the commit file have the same content.
    assertTrue(TestParameters.compareFile(commitFiles.get(CommitFiles.COMMIT_FILE_FULL), commitFiles.get(CommitFiles.COMMIT_FILE)));

    // Make sure the file has been added to the ZIP.
    File zipFolder = new File(TestParameters.ZIP_FOLDER);

    TestParameters.extractZip(commitFiles.get(CommitFiles.COMMIT_FILE_FULL_ZIP), zipFolder);
    assertTrue(TestParameters.compareFile(testFile, new File(zipFolder, TestParameters.UPLOAD_FILE)));
  }

  /**
   * Run the JSONObject prepareCommitR1(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPrepareCommitR1_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Generate some messages to process.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    messages.add(new DummyJSONWBBMessage());
    messages.add(new DummyJSONWBBMessage());
    db.setMessages(messages);

    // Test preparing commit round 1 files.
    JSONObject result = CommitProcessor.prepareCommitR1(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance()
        .getWbbPeer().getPreviousCommitTime(),TestParameters.COMMIT_DESC);
    assertNotNull(result);

    assertNotNull(db.getMessage());
    assertNotNull(db.getCommitFile());
    assertNotNull(db.getCommitFileFull());
    assertNotNull(db.getCommitAttachments());
    assertNotNull(db.getCommitTime());

    assertTrue(result.has(MessageFields.TYPE));
    assertTrue(result.has(MessageFields.Commit.COMMIT_ID));
    assertTrue(result.has(MessageFields.Commit.HASH));
    assertTrue(result.has(MessageFields.Commit.SIGNATURE));
    assertTrue(result.has(MessageFields.Commit.COMMIT_TIME));
    assertTrue(result.has(MessageFields.Commit.PEER_ID));
    assertTrue(result.has(MessageFields.Commit.DATETIME));

    assertEquals(CommitR1Message.TYPE_STRING, result.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getPreviousCommitTime(),
        result.getString(MessageFields.Commit.COMMIT_TIME));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), result.getString(MessageFields.Commit.PEER_ID));

    assertEquals(db.getMessage().getMsg().toString(), result.toString());

    String commitFolder = new File(TestParameters.getInstance().getWbbPeer().getConfig().getString(WBBConfig.COMMIT_DIRECTORY))
        .toString();

    assertTrue(new File(commitFolder, db.getCommitFile()).exists());
    assertTrue(new File(commitFolder, db.getCommitFileFull()).exists());
    assertTrue(new File(commitFolder, db.getCommitAttachments()).exists());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Generate some messages to process.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    messages.add(new DummyJSONWBBMessage());
    messages.add(new DummyJSONWBBMessage());
    db.setMessages(messages);

    // And one which needs a signature check.
    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    CommitTimeUtils commitTime = new CommitTimeUtils(TestParameters.COMMIT_TIME_HOURS, TestParameters.COMMIT_TIME_MINUTES);
    String hash = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, signature), commitTime.getPreviousCommitID());

    // Create a commit message.
    ArrayList<CommitR1Message> signatureChecks = new ArrayList<CommitR1Message>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, hash);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    signatureChecks.add(new CommitR1Message(message));
    db.setCommitMessages(signatureChecks);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 1);
    db.setStored(true);

    CommitProcessor fixture = new CommitProcessor(TestParameters.getInstance().getWbbPeer());

    // Test successful run through.
    fixture.run();

    // Test that the commit message has been generated. The files and content are tested elsewhere.
    assertNotNull(db.getMessage());
    assertNotNull(db.getCommitFile());
    assertNotNull(db.getCommitFileFull());
    assertNotNull(db.getCommitAttachments());
    assertNotNull(db.getCommitTime());

    // Test that the message has been sent to all peers.
    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());
    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertTrue(db.isSetCommitSentSigCalled());

    String response = TestParameters.getInstance().getWbbPeer().getMessage();
    assertNotNull(response);
    assertEquals(message.toString(), db.getMessage().getMsg().toString());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Generate some messages to process.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    messages.add(new DummyJSONWBBMessage());
    messages.add(new DummyJSONWBBMessage());
    db.setMessages(messages);

    // And one which needs a signature check.
    String signature = TestParameters.signData(TestParameters.SESSION_ID.toString());
    CommitTimeUtils commitTime = new CommitTimeUtils(TestParameters.COMMIT_TIME_HOURS, TestParameters.COMMIT_TIME_MINUTES);
    String hash = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, signature), commitTime.getPreviousCommitID());

    // Create a commit message.
    ArrayList<CommitR1Message> signatureChecks = new ArrayList<CommitR1Message>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, hash);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    signatureChecks.add(new CommitR1Message(message));
    db.setCommitMessages(signatureChecks);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setThreshold(TestParameters.getInstance().getWbbPeer().getPeerCount() + 1);
    db.setStored(true);

    CommitProcessor fixture = new CommitProcessor(TestParameters.getInstance().getWbbPeer());

    db.addException("submitIncomingPeerCommitR1Checked", 0);

    // Test already received message.
    fixture.run();

    // Test that the commit message has been generated. The files and content are tested elsewhere.
    assertNotNull(db.getMessage());
    assertNotNull(db.getCommitFile());
    assertNotNull(db.getCommitFileFull());
    assertNotNull(db.getCommitAttachments());
    assertNotNull(db.getCommitTime());

    // Test that the message has been sent to all peers.
    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());
    assertTrue(db.isSubmitIncomingPeerCommitR1CheckedCalled());
    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertTrue(db.isSetCommitSentSigCalled());

    String response = TestParameters.getInstance().getWbbPeer().getMessage();
    assertNotNull(response);
    assertEquals(message.toString(), db.getMessage().getMsg().toString());
  }

  /**
   * Run the void sendAndStoreCompletedCommitSK2(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = NoCommitExistsException.class)
  public void testSendAndStoreCompletedCommitSK2_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());

    CommitProcessor.sendAndStoreCompletedCommitSK2(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void sendAndStoreCompletedCommitSK2(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendAndStoreCompletedCommitSK2_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    db.setHash(TestParameters.SESSION_ID.toString().getBytes());
    db.setStored(true);

    CommitProcessor.sendAndStoreCompletedCommitSK2(TestParameters.getInstance().getWbbPeer());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertTrue(db.isSetCommitSentSigCalled());
  }
}