/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>DBFieldsTest</code> contains tests for the class <code>{@link DBFields}</code>.
 */
public class DBFieldsTest {

  /**
   * Run the String getField(DBFieldName) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetField_1() throws Exception {
    DBFields fixture = DBFields.getInstance();
    assertNotNull(fixture);

    for (DBFieldName field : DBFieldName.values()) {
      String result = fixture.getField(field);
      String expected = null;

      switch (field) {
        case CHECKED_SIGS:
          expected = TestParameters.DB_CHECKED_SIGS;
          break;
        case MY_SIGNATURE:
          expected = TestParameters.DB_MYSIG;
          break;
        case SENT_SIGNATURE:
          expected = TestParameters.DB_SENT_SIG;
          break;
        case SIGNATURE_COUNT:
          expected = TestParameters.DB_SIG_COUNT;
          break;
        case SENT_TIMEOUT:
          expected = TestParameters.DB_SENT_TIMEOUT;
          break;
        case CLIENT_MESSAGE:
          expected = TestParameters.DB_MESSAGE;
          break;
        case SIGNATURES_TO_CHECK:
          expected = TestParameters.DB_SIGS_TO_CHECK;
          break;
        case QUEUED_MESSAGE:
          expected = TestParameters.DB_MSG;
          break;
        case FROM_PEER:
          expected = TestParameters.DB_FROM_PEER;
          break;
        case COMMIT_TIME:
          expected = TestParameters.DB_COMMIT_TIME;
          break;
        case POST_TIMEOUT:
          expected = TestParameters.DB_POST_TIMEOUT;
          break;
        default:
          expected = null;
          break;
      }

      assertEquals(expected, result);
    }
  }

  /**
   * Run the DBFields getInstance() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInstance_1() throws Exception {
    DBFields result = DBFields.getInstance();
    assertNotNull(result);
  }

  /**
   * Run the DBFields getInstance(DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInstance_2() throws Exception {
    for (DBRecordType type : DBRecordType.values()) {
      DBFields result = DBFields.getInstance(type);

      switch (type) {
        case POD:
          assertTrue(result instanceof DBFieldsPOD);
          break;
        case CANCEL:
          assertTrue(result instanceof DBFieldsCancel);
          break;
        default:
        case GENERAL:
          assertTrue(result instanceof DBFields);
          break;
      }
    }
  }
}