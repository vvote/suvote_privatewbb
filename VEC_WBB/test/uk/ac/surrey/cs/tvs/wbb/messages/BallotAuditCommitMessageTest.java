/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;

/**
 * The class <code>BallotAuditCommitMessageTest</code> contains tests for the class <code>{@link BallotAuditCommitMessage}</code>.
 */
public class BallotAuditCommitMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the BallotAuditCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testBallotAuditCommitMessage_1() throws Exception {
    BallotAuditCommitMessage result = new BallotAuditCommitMessage("{}");
    assertNull(result);
  }

  /**
   * Run the BallotAuditCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotAuditCommitMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid message with no filename and digest.
    BallotAuditCommitMessage result = new BallotAuditCommitMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_AUDIT_COMMIT, result.type);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the BallotAuditCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotAuditCommitMessage_3() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.SERIAL_NO));

    // Test valid message.
    BallotAuditCommitMessage result = new BallotAuditCommitMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_AUDIT_COMMIT, result.type);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
  }

  /**
   * Run the BallotAuditCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testBallotAuditCommitMessage_4() throws Exception {
    BallotAuditCommitMessage result = new BallotAuditCommitMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the BallotAuditCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotAuditCommitMessage_5() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid message with no filename and digest.
    BallotAuditCommitMessage result = new BallotAuditCommitMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_AUDIT_COMMIT, result.type);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the BallotAuditCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotAuditCommitMessage_6() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.SERIAL_NO));

    // Test valid message.
    BallotAuditCommitMessage result = new BallotAuditCommitMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_AUDIT_COMMIT, result.type);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotAuditCommitMessage fixture = new BallotAuditCommitMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + digest + TestParameters.SENDER_CERTIFICATE + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    BallotAuditCommitMessage fixture = new BallotAuditCommitMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, MessageTypes.BALLOT_AUDIT_COMMIT);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);

    BallotAuditCommitMessage fixture = new BallotAuditCommitMessage(object);

    // Test signature validation failure.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, MessageTypes.BALLOT_AUDIT_COMMIT);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);

    BallotAuditCommitMessage fixture = new BallotAuditCommitMessage(object);

    // Test signature validation failure.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean preProcessMessage(WBBPeer,BoundedBufferedInputStream,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreProcessMessage_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object
        .put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, TestParameters.getInstance().getWbbPeer().getCommitUploadDir().toString());

    BallotAuditCommitMessage fixture = new BallotAuditCommitMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.DATA_FILE);

    TestParameters.createTestFile(file);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing. Content of super-method tested elsewhere.
    boolean result = fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), new Socket());
    assertFalse(result);
  }
}