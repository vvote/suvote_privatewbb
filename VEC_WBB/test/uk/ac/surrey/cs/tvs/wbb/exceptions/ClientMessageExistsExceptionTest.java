/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>ClientMessageExistsExceptionTest</code> contains tests for the class
 * <code>{@link ClientMessageExistsException}</code>.
 */
public class ClientMessageExistsExceptionTest {

  /**
   * Run the ClientMessageExistsException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClientMessageExistsException_1() throws Exception {

    ClientMessageExistsException result = new ClientMessageExistsException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the ClientMessageExistsException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClientMessageExistsException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    ClientMessageExistsException result = new ClientMessageExistsException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the ClientMessageExistsException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClientMessageExistsException_3() throws Exception {
    Throwable cause = new Throwable();

    ClientMessageExistsException result = new ClientMessageExistsException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the ClientMessageExistsException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClientMessageExistsException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    ClientMessageExistsException result = new ClientMessageExistsException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the ClientMessageExistsException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClientMessageExistsException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    ClientMessageExistsException result = new ClientMessageExistsException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}