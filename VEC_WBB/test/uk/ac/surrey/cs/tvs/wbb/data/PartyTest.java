/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/**
 * The class <code>PartyTest</code> contains tests for the class <code>{@link Party}</code>.
 */
public class PartyTest {

  /**
   * Run the Party(JSONObject,Region) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testParty_1() throws Exception {
    JSONArray candidates = new JSONArray("[{\"name\":\"Bob\"},{\"name\":\"Sid\"}]");

    JSONObject party1 = new JSONObject();
    party1.put("preferredName", "party1");
    party1.put("hasBallotBox", true);
    party1.put("ungrouped", true);
    party1.put("candidates", candidates);

    Party result = new Party(party1, null);
    assertNotNull(result);

    assertTrue(result.hasBallotBox());
    assertTrue(result.isUngrouped());
    assertNull(result.getRegion());

    List<Candidate> candidateList = result.getAllCandidates();
    assertEquals(candidates.length(), candidateList.size());

    String candidateString = "[";

    for (int i = 0; i < candidates.length(); i++) {
      Candidate candidate = new Candidate(candidates.getJSONObject(i));
      candidateString += candidate.toString();

      assertEquals(candidate.toString(), candidateList.get(i).toString());

      if (i < candidates.length() - 1) {
        candidateString += ", ";
      }
    }
    candidateString += "]";

    assertEquals("Party: party1, " + candidateString, result.toString());
  }

  /**
   * Run the Party(JSONObject,Region) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testParty_2() throws Exception {
    JSONArray candidates = new JSONArray();

    JSONObject party1 = new JSONObject();
    party1.put("preferredName", "party1");
    party1.put("hasBallotBox", true);
    party1.put("ungrouped", true);
    party1.put("candidates", candidates);

    Party result = new Party(party1, null);
    assertNotNull(result);

    assertTrue(result.hasBallotBox());
    assertTrue(result.isUngrouped());

    List<Candidate> candidateList = result.getAllCandidates();
    assertEquals(candidates.length(), candidateList.size());

    String candidateString = "[";

    for (int i = 0; i < candidates.length(); i++) {
      Candidate candidate = new Candidate(candidates.getJSONObject(i));
      candidateString += candidate.toString();

      assertEquals(candidate.toString(), candidateList.get(i).toString());

      if (i < candidates.length() - 1) {
        candidateString += ", ";
      }
    }
    candidateString += "]";

    assertEquals("Party: party1, " + candidateString, result.toString());
  }
}