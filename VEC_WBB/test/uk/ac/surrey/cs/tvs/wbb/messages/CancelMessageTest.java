/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * The class <code>CancelMessageTest</code> contains tests for the class <code>{@link CancelMessage}</code>.
 */
public class CancelMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CancelMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCancelMessage_1() throws Exception {
    CancelMessage result = new CancelMessage("");
    assertNull(result);
  }

  /**
   * Run the CancelMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCancelMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    // Test valid message.
    CancelMessage result = new CancelMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.CANCEL_MESSAGE, result.type);
  }

  /**
   * Run the CancelMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCancelMessage_3() throws Exception {
    CancelMessage result = new CancelMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the CancelMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCancelMessage_4() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    // Test valid message.
    CancelMessage result = new CancelMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.CANCEL_MESSAGE, result.type);
  }

  /**
   * Run the void checkAndStoreCancelMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreCancelMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CancelMessage fixture = new CancelMessage(TestParameters.MESSAGE);

    // Create a peer message.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    // Test invalid signature.
    fixture.checkAndStoreCancelMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertFalse(db.isValid());
  }

  /**
   * Run the void checkAndStoreCancelMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreCancelMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CancelMessage fixture = new CancelMessage(TestParameters.MESSAGE);

    // Create a peer message.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(fixture.getInternalSignableContent()));
    PeerMessage peerMessage = new PeerMessage(message);

    // Test valid signature.
    fixture.checkAndStoreCancelMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());
  }

  /**
   * Run the void checkAndStoreCancelMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreCancelMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CancelMessage fixture = new CancelMessage(TestParameters.MESSAGE);

    // Create a peer message.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(fixture.getInternalSignableContent()));
    PeerMessage peerMessage = new PeerMessage(message);

    // Test already sent timeout.
    db.addException("submitIncomingPeerMessageChecked", 1);
    fixture.checkAndStoreCancelMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    CancelMessage fixture = new CancelMessage(TestParameters.MESSAGE);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID();
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    CancelMessage fixture = new CancelMessage(TestParameters.MESSAGE);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID();
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String signature = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    // Missing type.
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, signature);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_2() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE);
    String authsignature = TestParameters.signBLSData(data,"./testdata/cancelKeyStore.bks","CancelAuth");

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID,"CancelAuth");
    object.put(MessageFields.CancelMessage.AUTH_SIG, authsignature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    CancelMessage fixture = new CancelMessage(object);

    // Test incorrect serial signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    CancelMessage fixture = new CancelMessage(object);

    // Test incorrect cancel machine signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_4() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    CancelMessage fixture = new CancelMessage(object);

    // Test incorrect auth signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_5() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE);
    String authsignature = TestParameters.signBLSData(data,"./testdata/cancelKeyStore.bks","CancelAuth");

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, "CancelAuth");
    object.put(MessageFields.CancelMessage.AUTH_SIG, authsignature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    CancelMessage fixture = new CancelMessage(object);

    // Test valid.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    // No type.
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    JSONObject record = new JSONObject();

    // Test not already stored and invalid message.
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_10() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    //String signature = TestParameters.signData(data);
    String clientSig = TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE);
    String authsignature = TestParameters.signBLSData(data,"./testdata/cancelKeyStore.bks","CancelAuth");

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, clientSig);
    object.put(MessageFields.CancelMessage.AUTH_ID, "CancelAuth");
    object.put(MessageFields.CancelMessage.AUTH_SIG, authsignature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));
    record.put(fields.getField(DBFieldName.CHECKED_SIGS), new JSONArray());

    JSONArray clients = new JSONArray();
    clients.put(fixture.toString());

    JSONObject client = new JSONObject();

    // Test not already stored, valid message and valid signature. Row retrieved with client message but not existing and already
    // received.
    db.setStored(false);
    db.setJsonObject(client);
    db.addException("submitIncomingCommitMessage", 0);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

    assertTrue(db.isSubmitIncomingCommitMessageCalled());
    assertEquals(DBRecordType.CANCEL, db.getRecordType());

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isValid());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    JSONObject record = new JSONObject();

    // Test not already stored, valid message but no signature.
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), "rubbish");

    // Test not already stored, valid message but invalid signature.
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(TestParameters.INTERNAL_CONTENT));

    // Test not already stored, valid message but signature which is not correct.
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));

    // Test not already stored, valid message and valid signature but malformed message.
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    //String signature = TestParameters.signData(data);
    String clientSig = TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE);
    String authsignature = TestParameters.signBLSData(data,"./testdata/cancelKeyStore.bks","CancelAuth");

    
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, clientSig);
    object.put(MessageFields.CancelMessage.AUTH_ID, "CancelAuth");
    object.put(MessageFields.CancelMessage.AUTH_SIG, authsignature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));
    record.put(fields.getField(DBFieldName.CHECKED_SIGS), new JSONArray());

    // Test not already stored, valid message and valid signature.
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

    assertTrue(db.isSubmitIncomingCommitMessageCalled());
    assertEquals(DBRecordType.CANCEL, db.getRecordType());

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isValid());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    //String signature = TestParameters.signData(data);
    String clientSig = TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE);
    String authsignature = TestParameters.signBLSData(data,"./testdata/cancelKeyStore.bks","CancelAuth");

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, clientSig);
    object.put(MessageFields.CancelMessage.AUTH_ID, "CancelAuth");
    object.put(MessageFields.CancelMessage.AUTH_SIG, authsignature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));
    record.put(fields.getField(DBFieldName.CHECKED_SIGS), new JSONArray());

    JSONArray clients = new JSONArray();
    clients.put(fixture.toString());

    JSONObject client = new JSONObject();
    client.put(fields.getField(DBFieldName.CLIENT_MESSAGE), clients);
    record.put(fields.getField(DBFieldName.CLIENT_MESSAGE), clients);

    // Test not already stored, valid message and valid signature. Row retrieved with client message.
    db.setStored(false);
    db.setJsonObject(client);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isValid());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_8() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));

    JSONArray clients = new JSONArray();
    clients.put(fixture.toString());

    JSONObject client = new JSONObject();
    client.put(fields.getField(DBFieldName.CLIENT_MESSAGE), clients);
    record.put(fields.getField(DBFieldName.CLIENT_MESSAGE), new JSONArray());

    // Test not already stored, valid message and valid signature. Row retrieved with client message but not matched.
    db.setStored(false);
    db.setJsonObject(client);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_9() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    //String signature = TestParameters.signData(data);
    String clientSig = TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE);
    String authsignature = TestParameters.signBLSData(data,"./testdata/cancelKeyStore.bks","CancelAuth");

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, clientSig);
    object.put(MessageFields.CancelMessage.AUTH_ID, "CancelAuth");
    object.put(MessageFields.CancelMessage.AUTH_SIG, authsignature);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create the commit record.
    DBFields fields = DBFields.getInstance(DBRecordType.CANCEL);

    JSONObject record = new JSONObject();
    record.put(DBFields.ID, TestParameters.SERIAL_NO);
    record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));
    record.put(fields.getField(DBFieldName.CHECKED_SIGS), new JSONArray());

    JSONArray clients = new JSONArray();
    clients.put(fixture.toString());

    JSONObject client = new JSONObject();

    // Test not already stored, valid message and valid signature. Row retrieved with client message but not existing.
    db.setStored(false);
    db.setJsonObject(client);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

    assertTrue(db.isSubmitIncomingCommitMessageCalled());
    assertEquals(DBRecordType.CANCEL, db.getRecordType());

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isValid());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);

    // Test accepted with no consensus reachable.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());

    assertTrue(TestParameters.getInstance().getWbbPeer().getMessage().contains(TestParameters.NO_CONSENSUS_POSSIBLE_MESSAGE));
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);

    // Test accepted with consensus reachable with exception.
    db.addException("submitIncomingPeerMessageChecked", 0);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);

    // Test accepted with consensus reachable with no client cancel messages.
    db.setReachedConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Create signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);

    // Test accepted with consensus reachable with client cancel messages.
    db.setReachedConsensus(true);
    db.setJsonObject(fixture.getMsg());

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitMyCancellationSignatureSK2Called());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = UnknownDBException.class)
  public void teststoreIncomingMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Test already received, null SK2.
    db.addException("submitIncomingClientCancellationMessage", 0);
    db.setDbField(null);

    fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void teststoreIncomingMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Test already received, SK2 exists.
    db.addException("submitIncomingClientCancellationMessage", 0);
    db.setDbField(TestParameters.SENDER_CERTIFICATE);

    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientCancellationMessageCalled());
    assertEquals(fixture, db.getMessage());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
    assertTrue(TestParameters.getInstance().getWbbPeer().getMessage().contains(TestParameters.SENDER_CERTIFICATE));
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void teststoreIncomingMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Test with db exception.
    db.addException("submitMyCancellationSignature", 0);

    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientCancellationMessageCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isSubmitMyCancellationSignatureCalled());
    assertNotNull(db.getSignature());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
    assertTrue(TestParameters.getInstance().getWbbPeer().getMessage().contains(TestParameters.UNKNOWN_DB_ERROR_MESSAGE));
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void teststoreIncomingMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CancelMessage fixture = new CancelMessage(object);

    // Test accepted.
    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertTrue(result);

    assertTrue(db.isSubmitIncomingClientCancellationMessageCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isSubmitMyCancellationSignatureCalled());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the void submitIncomingPeerMessageChecked(WBBPeer,PeerMessage,boolean,boolean,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerMessageChecked_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.CancelMessage.AUTH_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    CancelMessage fixture = new CancelMessage(object);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(message);

    // Test database method is called.
    fixture.submitIncomingPeerMessageChecked(TestParameters.getInstance().getWbbPeer(), peerMessage, true, false, false);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());
  }
}