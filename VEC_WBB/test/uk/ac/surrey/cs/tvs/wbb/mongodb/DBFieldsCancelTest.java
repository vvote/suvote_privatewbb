/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>DBFieldsCancelTest</code> contains tests for the class <code>{@link DBFieldsCancel}</code>.
 */
public class DBFieldsCancelTest {

  /**
   * Run the String getField(DBFieldName) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetField_1() throws Exception {
    DBFieldsCancel fixture = DBFieldsCancel.getInstance();
    assertNotNull(fixture);

    for (DBFieldName field : DBFieldName.values()) {
      String result = fixture.getField(field);
      String expected = null;

      switch (field) {
        case CHECKED_SIGS:
          expected = TestParameters.DB_CHECKED_SIGS_CANCEL;
          break;
        case MY_SIGNATURE:
          expected = TestParameters.DB_MYSIG_CANCEL;
          break;
        case SENT_SIGNATURE:
          expected = TestParameters.DB_SENT_SIG_CANCEL;
          break;
        case SIGNATURE_COUNT:
          expected = TestParameters.DB_SIG_COUNT_CANCEL;
          break;
        case CLIENT_MESSAGE:
          expected = TestParameters.DB_MESSAGE_CANCEL;
          break;
        case SIGNATURES_TO_CHECK:
          expected = TestParameters.DB_SIGS_TO_CHECK_CANCEL;
          break;
        case SK2_SIGNATURE:
          expected = TestParameters.DB_SK2_SIGNATURE_CANCEL;
          break;
        case COMMIT_TIME:
          expected = TestParameters.DB_COMMIT_TIME;
          break;
        case QUEUED_MESSAGE:
          expected = TestParameters.DB_MSG;
          break;
        case FROM_PEER:
          expected = TestParameters.DB_FROM_PEER;
          break;
        case POST_TIMEOUT:
          expected = TestParameters.DB_POST_TIMEOUT;
          break;
        default:
          expected = null;
          break;
      }

      assertEquals(expected, result);
    }
  }

  /**
   * Run the DBFieldsCancel getInstance() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInstance_1() throws Exception {
    DBFieldsCancel result = DBFieldsCancel.getInstance();
    assertNotNull(result);
  }
}