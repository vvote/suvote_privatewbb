/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.DummyJSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>ExternalMessageProcessorTest</code> contains tests for the class <code>{@link ExternalMessageProcessor}</code>.
 */
public class ExternalMessageProcessorTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the ExternalMessageProcessor(WBBPeer,LinkedBlockingQueue<JSONWBBMessage>,ConcurrentHashMap<String,LinkedBlockingQueue<
   * JSONWBBMessage>>) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testExternalMessageProcessor_1() throws Exception {
    //CJC: Changed as a result of the updated ExternalMessageProcessor
    ExternalMessageProcessor result = new ExternalMessageProcessor(TestParameters.getInstance().getWbbPeer(),null, null);
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
  //CJC: Changed as a result of the updated ExternalMessageProcessor
    ExternalMessageProcessor fixture = new ExternalMessageProcessor(TestParameters.getInstance().getWbbPeer(),null, null);

    // Test no message.
    fixture.run();
    assertNull(fixture.msg);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    //CJC: Changed as a result of the updated ExternalMessageProcessor
   
    // Test 1 message which fails processing.
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();
    message.setProcessMessage(false);
    ExternalMessageProcessor fixture = new ExternalMessageProcessor(TestParameters.getInstance().getWbbPeer(),message, message.getID());

    fixture.run();
    assertEquals(message, fixture.msg);
    assertTrue(message.isProcessMessageCalled());
   
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    //CJC: Changed as a result of the updated ExternalMessageProcessor
    
    // Test 1 message which is processed successfully.
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();
    message.setProcessMessage(true);
    
    ExternalMessageProcessor fixture = new ExternalMessageProcessor(TestParameters.getInstance().getWbbPeer(), message, message.getID());

    fixture.run();
    assertEquals(message, fixture.msg);
    assertTrue(message.isProcessMessageCalled());

  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_4() throws Exception {
   
  //CJC: Changed as a result of the updated ExternalMessageProcessor
    // Test 1 message which fails to process with an exception.
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();
    message.addException("processMessage", 0);
    
    ExternalMessageProcessor fixture = new ExternalMessageProcessor(TestParameters.getInstance().getWbbPeer(), message,message.getID());

    fixture.run();
    assertEquals(message, fixture.msg);
    assertTrue(message.isProcessMessageCalled());
    
  }
// CJC Removed because the ExternalMessageProcessor no longer controls the processing of multiple messages
//  /**
//   * Run the void run() method test.
//   * 
//   * @throws Exception
//   */
//  @Test
//  public void testRun_5() throws Exception {
//    EmptyEqualLinkedBlockingQueue<JSONWBBMessage> queue = new EmptyEqualLinkedBlockingQueue<JSONWBBMessage>();
//    ConcurrentHashMap<String, EmptyEqualLinkedBlockingQueue<JSONWBBMessage>> serialQueue = new ConcurrentHashMap<String, EmptyEqualLinkedBlockingQueue<JSONWBBMessage>>();
//
//    
//    // Test 2 messages which are processed successfully.
//    DummyJSONWBBMessage message1 = new DummyJSONWBBMessage();
//    message1.setProcessMessage(true);
//    queue.add(message1);
//
//    DummyJSONWBBMessage message2 = new DummyJSONWBBMessage();
//    message2.setProcessMessage(true);
//    message2.setID(message1.getID());
//    queue.add(message2);
//
//    serialQueue.put(message1.getID(), queue);
//
//    fixture.run();
//
//    // Wait for all messages to have finished processing.
//    ExecutorService executor = TestParameters.getInstance().getWbbPeer().getExternalMessageExecutor();
//    executor.shutdown();
//    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
//
//    assertEquals(message1, fixture.msg);
//    assertTrue(message1.isProcessMessageCalled());
//
//    assertFalse(queue.contains(message1));
//    assertFalse(queue.contains(message2));
//
//    assertFalse(serialQueue.containsKey(message1.getID()));
//  }
}