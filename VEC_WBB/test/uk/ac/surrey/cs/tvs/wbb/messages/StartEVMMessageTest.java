/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;

/**
 * The class <code>StartEVMMessageTest</code> contains tests for the class <code>{@link StartEVMMessage}</code>.
 */
public class StartEVMMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the JSONObject constructResponseAndSign(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructResponseAndSign_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);

    StartEVMMessage fixture = new StartEVMMessage(object);

    JSONObject result = fixture.constructResponseAndSign(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);

    String data = fixture.getTypeString() + fixture.getID() + TestParameters.DISTRICT;
    TVSSignature verify = new TVSSignature(SignatureType.BLS, TestParameters.getInstance().getWbbPeer()
        .getCertificateFromCerts(CertStore.PEER, TestParameters.SIGNING_CERTIFICATE));
    verify.update(data);
    verify.setPartial(true);
    assertTrue(result.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(result.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(TestParameters.SERIAL_NO, result.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(fixture.getTypeString(), result.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.PEERS[0], result.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(verify.verify(result.getString(MessageFields.JSONWBBMessage.PEER_SIG), EncodingType.BASE64));
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID() + TestParameters.DISTRICT;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID() + TestParameters.DISTRICT;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the void performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData("rubbish"));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test valid schema but invalid serial signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test valid schema but invalid EBM signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_4() throws Exception {
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.MESSAGE_TYPE_START_EVM_STRING, TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signBLSData(data,TestParameters.CLIENT_EVM_KEYSTORE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_EVM_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test valid.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = CommitProcessingException.class)
  public void testProcessAsPartOfCommitR2_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    StartEVMMessage fixture = new StartEVMMessage(object);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), new JSONObject(), TestParameters.PEERS[1],
        TestParameters.SAFE_UPLOAD_DIRECTORY);
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.MESSAGE_TYPE_START_EVM_STRING, TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test already received.
    db.addException("submitStartEVMMessage", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitStartEVMMessageCalled());
    assertEquals(fixture, db.getMessage());
    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.MESSAGE_TYPE_START_EVM_STRING, TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test max timeout.
    db.addException("submitStartEVMMessage", 1);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitStartEVMMessageCalled());
    assertEquals(fixture, db.getMessage());
    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.MESSAGE_TYPE_START_EVM_STRING, TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test client message exists.
    db.addException("submitStartEVMMessage", 2);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitStartEVMMessageCalled());
    assertEquals(fixture, db.getMessage());
    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.MESSAGE_TYPE_START_EVM_STRING, TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_START_EVM_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));

    StartEVMMessage fixture = new StartEVMMessage(object);

    // Test valid.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitStartEVMMessageCalled());
    assertEquals(fixture, db.getMessage());
    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());

    JSONObject response = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    assertNotNull(result);

    TVSSignature verify = new TVSSignature(SignatureType.BLS, TestParameters.getInstance().getWbbPeer()
        .getCertificateFromCerts(CertStore.PEER, TestParameters.SIGNING_CERTIFICATE));
    verify.update(fixture.getTypeString() + fixture.getID() + TestParameters.DISTRICT);
    verify.setPartial(true);
    assertTrue(response.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(response.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(response.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(response.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(TestParameters.SERIAL_NO, response.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(fixture.getTypeString(), response.getString(MessageFields.JSONWBBMessage.TYPE));
    assertEquals(TestParameters.PEERS[0], response.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(verify.verify(response.getString(MessageFields.JSONWBBMessage.PEER_SIG), EncodingType.BASE64));
  }

  /**
   * Run the StartEVMMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testStartEVMMessage_1() throws Exception {
    StartEVMMessage result = new StartEVMMessage("");
    assertNull(result);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testStartEVMMessage_2() throws Exception {
    StartEVMMessage result = new StartEVMMessage("{}");
    assertNull(result);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStartEVMMessage_3() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    StartEVMMessage result = new StartEVMMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.START_EVM, result.type);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testStartEVMMessage_4() throws Exception {
    StartEVMMessage result = new StartEVMMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the VoteMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStartEVMMessage_5() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    StartEVMMessage result = new StartEVMMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.START_EVM, result.type);
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = IOException.class)
  public void testStoreIncomingMessage_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    StartEVMMessage fixture = new StartEVMMessage(object);

    fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), "rubbish");
  }

  /**
   * Run the void submitIncomingPeerMessageChecked(WBBPeer,PeerMessage,boolean,boolean,boolean) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyReceivedMessageException.class)
  public void testSubmitIncomingPeerMessageChecked_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    StartEVMMessage fixture = new StartEVMMessage(object);

    fixture.submitIncomingPeerMessageChecked(TestParameters.getInstance().getWbbPeer(), new PeerMessage(TestParameters.MESSAGE),
        false, false, false);
  }
}