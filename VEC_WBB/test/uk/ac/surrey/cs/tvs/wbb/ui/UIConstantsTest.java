/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * The class <code>UIConstantsTest</code> contains tests for the class <code>{@link UIConstants}</code>.
 */
public class UIConstantsTest {

  /**
   * Run the UIConstants() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUIConstants_1() throws Exception {
    UIConstants result = new UIConstants();
    assertNotNull(result);
  }
}