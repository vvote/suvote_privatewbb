/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>CommandListenerTest</code> contains tests for the class <code>{@link CommandListener}</code>.
 */
public class CommandListenerTest {

  /**
   * Dummy WebSocket class for testing.
   */
  private class DummyWebSocket extends WebSocket {

    private String sent = null;

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    public String getSent() {
      return this.sent;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String sent) throws NotYetConnectedException {
      this.sent = sent;
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Logger used to see if messages have been logged.
   */
  private static final Logger logger = LoggerFactory.getLogger(CommandListenerTest.class);

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CommandListener(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommandListener_1() throws Exception {
    CommandListener result = new CommandListener(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    fixture.startCL();
    // TODO as per the request in the review, the thread starting has moved out of CommandListener constructor, need to call startCL
    // to start the commandlistener
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test logging without a level.
    message.put(TestParameters.TYPE, TestParameters.START_LOG);
    fixture.processMessage(message, socket);

    // Test debug.
    logger.debug(TestParameters.LOG_MESSAGE);
    String data = socket.getSent();
    assertNull(data);

    // Test warning.
    logger.warn(TestParameters.LOG_MESSAGE);
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.LOG_MESSAGE));
    assertTrue(result.has(TestParameters.LOG_LEVEL));
    assertTrue(result.has(TestParameters.THREAD));
    assertTrue(result.has(TestParameters.DATETIME));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.LOGGING);
    assertEquals(TestParameters.LOG_MESSAGE, result.getString(TestParameters.LOG_MESSAGE));
    assertEquals(TestParameters.WARN, result.getString(TestParameters.LOG_LEVEL));
    assertEquals(TestParameters.MAIN, result.getString(TestParameters.THREAD));
    assertTrue(result.getLong(TestParameters.DATETIME) > 0);

    // Test turning logging off.
    socket = new DummyWebSocket(); // Make sure we don't look at any old data.
    message.put(TestParameters.TYPE, TestParameters.STOP_LOG);
    fixture.processMessage(message, socket);

    // Test warning.
    logger.warn(TestParameters.LOG_MESSAGE);
    data = socket.getSent();
    assertNull(data);
    fixture.shutdown();
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    fixture.startCL();
    // TODO as per the request in the review, the thread starting has moved out of CommandListener constructor, need to call startCL
    // to start the commandlistener
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test logging with a level.
    message.put(TestParameters.TYPE, TestParameters.START_LOG);
    message.put(TestParameters.LEVEL, TestParameters.DEBUG);
    fixture.processMessage(message, socket);

    // Test debug.
    logger.debug(TestParameters.LOG_MESSAGE);
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.LOG_MESSAGE));
    assertTrue(result.has(TestParameters.LOG_LEVEL));
    assertTrue(result.has(TestParameters.THREAD));
    assertTrue(result.has(TestParameters.DATETIME));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.LOGGING);
    assertEquals(TestParameters.LOG_MESSAGE, result.getString(TestParameters.LOG_MESSAGE));
    assertEquals(TestParameters.DEBUG, result.getString(TestParameters.LOG_LEVEL));
    assertEquals(TestParameters.MAIN, result.getString(TestParameters.THREAD));
    assertTrue(result.getLong(TestParameters.DATETIME) > 0);

    // Test warning.
    logger.warn(TestParameters.LOG_MESSAGE);
    result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.LOG_MESSAGE));
    assertTrue(result.has(TestParameters.LOG_LEVEL));
    assertTrue(result.has(TestParameters.THREAD));
    assertTrue(result.has(TestParameters.DATETIME));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.LOGGING);
    assertEquals(TestParameters.LOG_MESSAGE, result.getString(TestParameters.LOG_MESSAGE));
    assertEquals(TestParameters.WARN, result.getString(TestParameters.LOG_LEVEL));
    assertEquals(TestParameters.MAIN, result.getString(TestParameters.THREAD));
    assertTrue(result.getLong(TestParameters.DATETIME) > 0);

    // Test turning logging off.
    socket = new DummyWebSocket(); // Make sure we don't look at any old data.
    message.put(TestParameters.TYPE, TestParameters.STOP_LOG);
    fixture.processMessage(message, socket);

    // Test warning.
    logger.warn(TestParameters.LOG_MESSAGE);
    String data = socket.getSent();
    assertNull(data);
    fixture.shutdown();
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test statistics.
    message.put(TestParameters.TYPE, TestParameters.START_MEMORY);
    message.put(TestParameters.DELAY, Integer.toString(TestParameters.SLEEP_INTERVAL));
    fixture.processMessage(message, socket);

    // Wait for enough time for something to have happened.
    int count = 0;

    while (count < 2) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    // Test that statistics were sent.
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.MEMORY));
    assertTrue(result.has(TestParameters.MAX_MEMORY));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.MEMORY);
    assertTrue(result.getLong(TestParameters.MEMORY) > 0);
    assertTrue(result.getLong(TestParameters.MAX_MEMORY) > 0);

    // Shutdown.
    message.put(TestParameters.TYPE, TestParameters.STOP_MEMORY);
    fixture.processMessage(message, socket);
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test statistics.
    message.put(TestParameters.TYPE, TestParameters.START_MEMORY);
    message.put(TestParameters.DELAY, "rubbish"); // Default delay.
    fixture.processMessage(message, socket);

    // Wait for enough time for something to have happened.
    int count = 0;

    while (count < 2) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    // Test that statistics were sent.
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.MEMORY));
    assertTrue(result.has(TestParameters.MAX_MEMORY));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.MEMORY);
    assertTrue(result.getLong(TestParameters.MEMORY) > 0);
    assertTrue(result.getLong(TestParameters.MAX_MEMORY) > 0);

    // Shutdown.
    message.put(TestParameters.TYPE, TestParameters.STOP_MEMORY);
    fixture.processMessage(message, socket);
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_5() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test peer statistics.
    message.put(TestParameters.TYPE, TestParameters.START_PEER);
    message.put(TestParameters.DELAY, Integer.toString(TestParameters.SLEEP_INTERVAL));
    fixture.processMessage(message, socket);

    // Wait for enough time for something to have happened.
    int count = 0;

    while (count < 2) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    // Test that statistics were sent.
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.CHANNELS));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.PEER_STAT);
    assertTrue(result.getInt(TestParameters.CHANNELS) >= 0);

    // Shutdown.
    message.put(TestParameters.TYPE, TestParameters.STOP_PEER);
    fixture.processMessage(message, socket);
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_6() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test peer statistics.
    message.put(TestParameters.TYPE, TestParameters.START_PEER);
    message.put(TestParameters.DELAY, "rubbish"); // Default delay.
    fixture.processMessage(message, socket);

    // Wait for enough time for something to have happened.
    int count = 0;

    while (count < 2) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    // Test that statistics were sent.
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.CHANNELS));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.PEER_STAT);
    assertTrue(result.getInt(TestParameters.CHANNELS) >= 0);

    // Shutdown.
    message.put(TestParameters.TYPE, TestParameters.STOP_PEER);
    fixture.processMessage(message, socket);
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject commit = new JSONObject();
    commit.put(TestParameters.DESC_FIELD, TestParameters.SESSION_ID.toString());
    db.setCommitTable(commit.toString());

    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test commit table.
    message.put(TestParameters.TYPE, TestParameters.GET_COMMIT);
    fixture.processMessage(message, socket);

    // Test that the commit data was sent.
    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.DESC_FIELD));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.COMMIT);
    assertEquals(TestParameters.SESSION_ID.toString(), result.getString(TestParameters.DESC_FIELD));
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_8() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());
    DummyWebSocket socket = new DummyWebSocket();
    JSONObject message = new JSONObject();

    // Test without a command.
    fixture.processMessage(message, socket);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());

    // Test commit.
    String input = UIConstants.CLICommand.CLI_COMMIT + "\n";
    System.setIn(new ByteArrayInputStream(input.getBytes()));

    // Put the CommandListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for some processing.
    TestParameters.wait(2);

    // Shutdown.
    fixture.shutdown();

    // Wait for the thread to exit.
    thread.join();

    // Test the result.
    assertTrue(TestParameters.getInstance().getWbbPeer().isRunCommitCalled());
    assertEquals("none", TestParameters.getInstance().getWbbPeer().getCommitTime());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());

    // Test commit.
    String input = UIConstants.CLICommand.CLI_COMMIT + " rubbish" + "\n";
    System.setIn(new ByteArrayInputStream(input.getBytes()));

    // Put the CommandListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for some processing.
    TestParameters.wait(2);

    // Shutdown.
    fixture.shutdown();

    // Wait for the thread to exit.
    thread.join();

    // Test the result.
    assertTrue(TestParameters.getInstance().getWbbPeer().isRunCommitCalled());
    assertEquals("rubbish", TestParameters.getInstance().getWbbPeer().getCommitTime());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    CommandListener fixture = new CommandListener(TestParameters.getInstance().getWbbPeer());

    // Test commit.
    String input = UIConstants.CLICommand.CLI_SHUTDOWN + "\n";
    System.setIn(new ByteArrayInputStream(input.getBytes()));

    // Put the CommandListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for some processing.
    TestParameters.wait(2);

    // Wait for the thread to exit.
    thread.join();

    // Only get here if we've shutdown.
  }
}