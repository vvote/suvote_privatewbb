/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.config.CertStore;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB;

/**
 * The class <code>WBBPeerTest</code> contains tests for the class <code>{@link WBBPeer}</code>.
 */
public class WBBPeerTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    public String getOutputStreamData() {
      return this.outputStream.toString();
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Tidy up the sockets.
    TestParameters.tidySockets();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Tidy up the sockets.
    TestParameters.tidySockets();
  }

  /**
   * Run the void addResponseChannel(UUID,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddResponseChannel_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    Socket socket = new Socket();

    // Test adding a response channel.
    fixture.addResponseChannel(TestParameters.SESSION_ID, socket);
    assertEquals(1, fixture.getResponseChannelCount());
    assertTrue(fixture.sessionIDExists(TestParameters.SESSION_ID));

    // Test removing an unknown response channel.
    assertNull(fixture.getRemoveResponseChannel(UUID.randomUUID().toString()));

    // Test removing a known response channel.
    assertNotNull(fixture.getRemoveResponseChannel(TestParameters.SESSION_ID.toString()));
    assertFalse(fixture.sessionIDExists(TestParameters.SESSION_ID));

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the String getCertificateName(Principal) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCertificateName_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    String result = fixture.getCertificateName(TestParameters.PRINCIPAL);
    assertNotNull(result);

    assertEquals(TestParameters.PEERS_SSL[0], result);

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    // No arguments.
    WBBPeer.main(new String[0]);

    // Configuration file argument. Needs to be a different peer to the others used because we cannot close the sockets.
    WBBPeer.main(new String[] { TestParameters.PEER_CONFIGURATIONS[1] });
  }

  /**
   * Run the void runCommit() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunCommit_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    fixture.runCommit();
    fixture.runCommit(fixture.getCommitTime());

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void scheduleNextCommit() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testScheduleNextCommit_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    fixture.scheduleNextCommit();

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendAndClose(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendAndClose_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    // Test no socket.
    fixture.sendAndClose(TestParameters.SESSION_ID.toString(), TestParameters.LINE);

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendAndClose(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendAndClose_2() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    // Test no socket.
    fixture.sendAndClose(TestParameters.SESSION_ID.toString(), TestParameters.LINE, false);

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendAndClose(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendAndClose_3() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    // Create a dummy socket to receive the message.
    DummySocket socket = new DummySocket();

    // Add the socket as a response channel.
    fixture.addResponseChannel(TestParameters.SESSION_ID, socket);

    // Test sending the message.
    fixture.sendAndClose(TestParameters.SESSION_ID.toString(), TestParameters.LINE);

    assertEquals(TestParameters.LINE + System.getProperty("line.separator"), socket.getOutputStreamData());
    assertTrue(socket.isClosed());

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendR2CommitFileToAllPeers(String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendR2CommitFileToAllPeers_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    // Create the commit file and attachments.
    File commitFile = new File(fixture.getConfig().getString(WBBConfig.COMMIT_DIRECTORY) + TestParameters.COMMIT_FILE);
    TestParameters.createCommitFile(commitFile);

    // Send without attachments.
    fixture.sendR2CommitFileToAllPeers(TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME,
        TestParameters.SESSION_ID.toString());

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendR2CommitFileToAllPeers(String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendR2CommitFileToAllPeers_2() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    // Create the commit file and attachments.
    File commitFile = new File(fixture.getConfig().getString(WBBConfig.COMMIT_DIRECTORY) + TestParameters.COMMIT_FILE);
    TestParameters.createCommitFile(commitFile);

    File attachementsFile = new File(fixture.getConfig().getString(WBBConfig.COMMIT_DIRECTORY) + TestParameters.COMMIT_FILE_ZIP);
    TestParameters.createTestFile(attachementsFile);

    // Send with attachments.
    fixture.sendR2CommitFileToAllPeers(TestParameters.COMMIT_FILE, TestParameters.COMMIT_FILE_ZIP, TestParameters.COMMIT_TIME,
        TestParameters.SESSION_ID.toString());

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendToAllPeers(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendToAllPeers_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    fixture.sendToAllPeers(TestParameters.LINE);

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the void sendToPublicWBB(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendToPublicWBB_1() throws Exception {
    final DummyConcurrentDB db = new DummyConcurrentDB();

    // Replace the database so that we can influence testing.
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]) {

      @Override
      public ConcurrentDB getDB() {
        return db;
      }
    };

    fixture.sendToPublicWBB(TestParameters.LINE);

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the boolean verifyConnection(InetSocketAddress,Principal) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testVerifyConnection_1() throws Exception {
    WBBPeer fixture = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);

    // Test valid.
    InetSocketAddress remoteAddress = new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.INTERNAL_PORT);
    
    boolean result = fixture.verifyConnection(remoteAddress, TestParameters.PRINCIPAL);
    assertTrue(result);

    // Test invalid host name.
    remoteAddress = new InetSocketAddress("10.0.0.1", TestParameters.INTERNAL_PORT);
    result = fixture.verifyConnection(remoteAddress, TestParameters.PRINCIPAL);
    assertFalse(result);

    // Test invalid port.
    remoteAddress = new InetSocketAddress(TestParameters.LISTENER_ADDRESS, 99);
    result = fixture.verifyConnection(remoteAddress, TestParameters.PRINCIPAL);
    assertFalse(result);

    // Test invalid port but we don't care.
    remoteAddress = new InetSocketAddress(TestParameters.LISTENER_ADDRESS, 99);
    result = fixture.verifyConnection(remoteAddress, TestParameters.PRINCIPAL, false);
    assertTrue(result);

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    fixture.getPeerServerSocket().close();
    fixture.getPeerExternalServerSocket().close();
  }

  /**
   * Run the WBBPeer(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testWBBPeer_1() throws Exception {
    WBBPeer result = new WBBPeer(TestParameters.PEER_CONFIGURATIONS[0]);
    assertNotNull(result);

    // Test that all of the required parameters and items have been initialised.
    assertNotNull(result.getConfig());
    assertEquals(TestParameters.PEERS[0], result.getID());
    assertNotNull(result.getDB());
    assertNotNull(result.getJSONValidator());
    System.out.println(result.getAddress());
    assertEquals(TestParameters.LISTENER_ADDRESS, result.getAddress());
    assertEquals(TestParameters.INTERNAL_PORT, result.getInternalPort());

    assertNotNull(result.getPeerExternalServerSocket());
    assertNotNull(result.getPeerServerSocket());

    assertNotNull(result.getCertificateKeyStore(CertStore.PEER));
    assertNotNull(result.getCertificateFromCerts(CertStore.PEER, TestParameters.SENDER_CERTIFICATE));
    assertNotNull(result.getSK1());
    assertNotNull(result.getSK2());

    assertNotNull(result.getCommitExecutor());
    assertNotNull(result.getScheduledExecutor());
    assertNotNull(result.getExternalListenerExecutor());
    assertNotNull(result.getExternalMessageExecutor());
    assertNotNull(result.getInternalMessageExecutor());
    assertNotNull(result.getTimeoutManager());

    assertEquals(TestParameters.THRESHOLD, result.getThreshold());
    assertEquals(TestParameters.THRESHOLD - 1, result.getPeerCount());

    assertEquals(new File(TestParameters.UPLOAD_DIRECTORY), result.getUploadDir());
    assertEquals(TestParameters.FILE_UPLOAD_TIMEOUT, result.getUploadTimeout());

    assertNotNull(result.getCommitTime());
    assertNotNull(result.getPreviousCommitTime());
    assertEquals(new File(TestParameters.SAFE_UPLOAD_DIRECTORY), result.getCommitUploadDir());

    // Shutdown the sockets used by the peer so we can re-create them in the next test.
    result.getPeerServerSocket().close();
    result.getPeerExternalServerSocket().close();
  }
}