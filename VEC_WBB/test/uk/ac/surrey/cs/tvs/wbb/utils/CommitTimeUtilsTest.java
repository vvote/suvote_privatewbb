/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

/**
 * The class <code>CommitTimeUtilsTest</code> contains tests for the class <code>{@link CommitTimeUtils}</code>.
 */
public class CommitTimeUtilsTest {

  /**
   * Run the CommitTimeUtils(int,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitTimeUtils_1() throws Exception {
    // Find a time just a little ahead of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours += 1;
    minutes += 10;

    CommitTimeUtils result = new CommitTimeUtils(hours, minutes);
    assertNotNull(result);
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTime_1() throws Exception {
    // Find a time just a little ahead of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours += 1;

    CommitTimeUtils fixture = new CommitTimeUtils(hours, minutes);

    // Test that the commit time is yesterday's.
    String result = fixture.getCommitTime();
    assertNotNull(result);

    time = Long.parseLong(result);

    int resultDays = (int) TimeUnit.MILLISECONDS.toDays(time);
    int resultHours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(resultDays));
    int resultMinutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(resultDays) - TimeUnit.HOURS
        .toMinutes(resultHours));

    assertEquals(days - 1, resultDays);
    assertEquals(hours, resultHours);
    assertEquals(minutes, resultMinutes);
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTime_2() throws Exception {
    // Find a time just a little behind of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours -= 1;

    CommitTimeUtils fixture = new CommitTimeUtils(hours, minutes);

    // Test that the commit time is tomorrow's.
    String result = fixture.getCommitTime();
    assertNotNull(result);

    time = Long.parseLong(result);

    int resultDays = (int) TimeUnit.MILLISECONDS.toDays(time);
    int resultHours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(resultDays));
    int resultMinutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(resultDays) - TimeUnit.HOURS
        .toMinutes(resultHours));

    assertEquals(days, resultDays);
    assertEquals(hours, resultHours);
    assertEquals(minutes, resultMinutes);
  }

  /**
   * Run the long getMilisecondsTillTime(int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetMilisecondsTillTime_1() throws Exception {
    // Find a time just a little ahead of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours += 1;
    minutes += 10;

    // Test that the number of milliseconds to the specified time is under 1 hour and 10 minutes.
    long result = CommitTimeUtils.getMillisecondsTillTime(hours, minutes);
    assertNotNull(result);

    assertTrue(result > 0);
    assertTrue(result <= (((1 * 60) + 10) * 60 * 1000));
  }

  /**
   * Run the long getMilisecondsTillTime(int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetMilisecondsTillTime_2() throws Exception {
    // Find a time just a little behind of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours -= 1;
    minutes += 10;

    // Test that the number of milliseconds to the specified time is under 23 hour and 10 minutes.
    long result = CommitTimeUtils.getMillisecondsTillTime(hours, minutes);
    assertNotNull(result);

    assertTrue(result > (((1 * 60) + 10) * 60 * 1000));
    assertTrue(result <= (((23 * 60) + 10) * 60 * 1000));
  }

  /**
   * Run the String getPreviousCommitID() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetPreviousCommitID_1() throws Exception {
    // Find a time just a little ahead of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours += 1;

    CommitTimeUtils fixture = new CommitTimeUtils(hours, minutes);

    // Test that the previous commit is 2 days ago since today's commit has not been passed.
    String result = fixture.getPreviousCommitID();
    assertNotNull(result);

    time = Long.parseLong(result);

    int resultDays = (int) TimeUnit.MILLISECONDS.toDays(time);
    int resultHours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(resultDays));
    int resultMinutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(resultDays) - TimeUnit.HOURS
        .toMinutes(resultHours));

    assertEquals(days - 2, resultDays);
    assertEquals(hours, resultHours);
    assertEquals(minutes, resultMinutes);
  }

  /**
   * Run the String getPreviousCommitID() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetPreviousCommitID_2() throws Exception {
    // Find a time just a little behind of the current time.
    long time = System.currentTimeMillis();
    int days = (int) TimeUnit.MILLISECONDS.toDays(time);
    int hours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(days));
    int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));

    hours -= 1;

    CommitTimeUtils fixture = new CommitTimeUtils(hours, minutes);

    // Test that the previous commit is 2 days ago since today's commit has not been passed.
    String result = fixture.getPreviousCommitID();
    assertNotNull(result);

    time = Long.parseLong(result);

    int resultDays = (int) TimeUnit.MILLISECONDS.toDays(time);
    int resultHours = (int) (TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(resultDays));
    int resultMinutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.DAYS.toMinutes(resultDays) - TimeUnit.HOURS
        .toMinutes(resultHours));

    assertEquals(days - 1, resultDays);
    assertEquals(hours, resultHours);
    assertEquals(minutes, resultMinutes);
  }
}