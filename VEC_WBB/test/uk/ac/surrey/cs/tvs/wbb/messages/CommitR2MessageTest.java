/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;

/**
 * The class <code>CommitR2MessageTest</code> contains tests for the class <code>{@link CommitR2Message}</code>.
 */
public class CommitR2MessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Reset the WBB.
    TestParameters.getInstance().getWbbPeer().reset();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CommitR2Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCommitR2Message_1() throws Exception {
    CommitR2Message result = new CommitR2Message("");
    assertNotNull(result);
  }

  /**
   * Run the CommitR2Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitR2Message_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);

    CommitR2Message result = new CommitR2Message(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_FILE_MESSAGE, result.type);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the CommitR2Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitR2Message_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CommitR2Message result = new CommitR2Message(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_FILE_MESSAGE, result.type);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(TestParameters.PEERS[0], result.getFromPeer());
  }

  /**
   * Run the CommitR2Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCommitR2Message_4() throws Exception {
    CommitR2Message result = new CommitR2Message(new JSONObject());
    assertNotNull(result);
  }

  /**
   * Run the CommitR2Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitR2Message_5() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);

    CommitR2Message result = new CommitR2Message(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_FILE_MESSAGE, result.type);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the CommitR2Message(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitR2Message_6() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CommitR2Message result = new CommitR2Message(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_FILE_MESSAGE, result.type);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(TestParameters.PEERS[0], result.getFromPeer());
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test with a from peer already defined.
    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test with a from peer null.
    JSONObject internal = fixture.getMsg();
    internal.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.CommitR2.ATTACHMENT_SIZE, 99);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test valid schema but invalid signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.CommitR2.ATTACHMENT_SIZE, 99);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test valid schema and signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test not in commit round 2 and queued.
    db.setRound2(false);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isQueueR2CommitCalled());
    assertEquals(fixture, db.getMessage());

    assertFalse(db.isUpdateCurrentCommitRecordCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test not in commit round 2 and queued with already received.
    db.addException("queueR2Commit", 0);
    db.setRound2(false);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isQueueR2CommitCalled());
    assertEquals(fixture, db.getMessage());

    assertFalse(db.isUpdateCurrentCommitRecordCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test in commit round 2, valid commit file but no safe upload directory.
    db.setRound2(true);

    // Create a file containing commit records.
    File commitFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.COMMIT_FILE);

    TestParameters.createCommitFile(commitFile);

    fixture.processMessage(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test in commit round 2, valid commit file with safe upload directory, no threshold.
    db.setRound2(true);
    db.setThreshold(TestParameters.THRESHOLD + 1);

    // Create a file containing commit records.
    File commitFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.COMMIT_FILE);

    TestParameters.createCommitFile(commitFile);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerCommitR2CheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckCommitR2ThresholdCalled());

    assertFalse(db.isUpdateCurrentCommitRecordCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.CommitR2.ATTACHMENT_SIZE, 99);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test in commit round 2, valid commit file with safe upload directory, already received, no threshold.
    db.setRound2(true);
    db.setThreshold(TestParameters.THRESHOLD + 1);
    db.addException("submitIncomingPeerCommitR2Checked", 0);

    // Create a file containing commit records.
    File commitFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.COMMIT_FILE);

    TestParameters.createCommitFile(commitFile);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerCommitR2CheckedCalled());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test in commit round 2, valid commit file with safe upload directory, with threshold.
    db.setRound2(true);
    //TODO CJC: changed this because otherwise the threshold will be met and the methods called (was -1)
    db.setThreshold(TestParameters.THRESHOLD);

    // Create a file containing commit records.
    File commitFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.COMMIT_FILE);

    TestParameters.createCommitFile(commitFile);

    // Generate a file message for the commit.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.getInstance().getWbbPeer().getCommitUploadDir().toString());
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.UPLOAD_FILE);
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    messages.add(new FileMessage(message));
    db.setMessages(messages);
    
    // Has commit processing exception.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerCommitR2CheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckCommitR2ThresholdCalled());

    assertFalse(db.isUpdateCurrentCommitRecordCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test in commit round 2, valid commit file with safe upload directory, with threshold.
    db.setRound2(true);
    db.setThreshold(TestParameters.THRESHOLD - 1);

    String hash = TestParameters.signData(TestParameters.PEERS[0]);
    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Create a file containing commit records.
    File commitFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.COMMIT_FILE);

    TestParameters.createCommitFile(commitFile);

    // Generate a file message for the commit.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.UPLOAD_FILE);
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    messages.add(new FileMessage(message));
    db.setMessages(messages);
    db.setStored(true);

    // Create the test file from the safe upload.
    File testFile = new File(TestParameters.SAFE_UPLOAD_DIRECTORY, TestParameters.UPLOAD_FILE);
    TestParameters.createTestFile(testFile);

    // Update completes successfully.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerCommitR2CheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckCommitR2ThresholdCalled());

    assertTrue(db.isUpdateCurrentCommitRecordCalled());

    assertTrue(db.getCommitFile().startsWith(TestParameters.COMMIT_FILE_PREFIX));
    assertTrue(db.getCommitFileFull().startsWith(TestParameters.COMMIT_FILE_FULL_PREFIX));
    assertTrue(db.getCommitAttachments().startsWith(TestParameters.COMMIT_FILE_ZIP_PREFIX));

    assertTrue(db.getCommitFile().endsWith(TestParameters.COMMIT_FILE_EXT));
    assertTrue(db.getCommitFileFull().endsWith(TestParameters.COMMIT_FILE_FULL_EXT));
    assertTrue(db.getCommitAttachments().endsWith(TestParameters.COMMIT_FILE_ZIP_EXT));

    assertNotNull(db.getCommitHash());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertTrue(db.isSetCommitSentSigCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_8() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, digest);
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test in commit round 2, valid commit file with safe upload directory, with threshold.
    db.setRound2(true);
    db.setThreshold(TestParameters.THRESHOLD - 1);

    String hash = TestParameters.signData(TestParameters.PEERS[0]);
    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    // Create a file containing commit records.
    File commitFile = new File(TestParameters.getInstance().getWbbPeer().getCommitUploadDir(), TestParameters.COMMIT_FILE);

    TestParameters.createCommitFile(commitFile);

    // Generate a file message for the commit.
    ArrayList<JSONWBBMessage> messages = new ArrayList<JSONWBBMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.UPLOAD_FILE);
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    messages.add(new FileMessage(message));
    db.setMessages(messages);

    // Create the test file from the safe upload.
    File testFile = new File(TestParameters.SAFE_UPLOAD_DIRECTORY, TestParameters.UPLOAD_FILE);
    TestParameters.createTestFile(testFile);

    // Update already received.
    db.addException("updateCurrentCommitRecord", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerCommitR2CheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckCommitR2ThresholdCalled());

    assertTrue(db.isUpdateCurrentCommitRecordCalled());

    assertTrue(db.getCommitFile().startsWith(TestParameters.COMMIT_FILE_PREFIX));
    assertTrue(db.getCommitFileFull().startsWith(TestParameters.COMMIT_FILE_FULL_PREFIX));
    assertTrue(db.getCommitAttachments().startsWith(TestParameters.COMMIT_FILE_ZIP_PREFIX));

    assertTrue(db.getCommitFile().endsWith(TestParameters.COMMIT_FILE_EXT));
    assertTrue(db.getCommitFileFull().endsWith(TestParameters.COMMIT_FILE_FULL_EXT));
    assertTrue(db.getCommitAttachments().endsWith(TestParameters.COMMIT_FILE_ZIP_EXT));

    assertNotNull(db.getCommitHash());

    assertFalse(TestParameters.getInstance().getWbbPeer().isSendToPublicWBBCalled());
    assertFalse(db.isSetCommitSentSigCalled());
    assertFalse(TestParameters.getInstance().getWbbPeer().isScheduleNextCommitCalled());
  }

  /**
   * Run the void setFilename(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetFilename_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    CommitR2Message fixture = new CommitR2Message(object.toString());

    fixture.setFilename(TestParameters.COMMIT_FILE_FULL);

    assertEquals(TestParameters.COMMIT_FILE_FULL, fixture.getFilename());
    assertEquals(TestParameters.COMMIT_FILE_FULL, fixture.getMsg().getString(MessageFields.CommitR2.INTERNAL_FILENAME));
  }

  /**
   * Run the boolean updateDigestAndVerify(String,WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateDigestAndVerify_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, "rubbish");
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test updating with an invalid digest.
    boolean result = fixture.updateDigestAndVerify(TestParameters.getInstance().getWbbPeer(), "rubbish");
    assertFalse(result);

    assertEquals("rubbish", fixture.getMsg().getString(MessageFields.CommitR2.INTERNAL_DIGEST));
  }

  /**
   * Run the boolean updateDigestAndVerify(String,WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateDigestAndVerify_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    byte[] data = IOUtils.decodeData(EncodingType.BASE64, digest);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING);
    object.put(MessageFields.CommitR2.COMMIT_ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.CommitR2.FILESIZE, 99);
    object.put(MessageFields.CommitR2.FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.CommitR2.INTERNAL_FILENAME, TestParameters.COMMIT_FILE);
    object.put(MessageFields.PeerFile.SAFE_UPLOAD_DIR, TestParameters.SAFE_UPLOAD_DIRECTORY);
    object.put(MessageFields.CommitR2.DIGEST, "rubbish");
    object.put(MessageFields.CommitR2.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.CommitR2.PEER_SIGNATURE, TestParameters.signData(data, ""));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR2Message fixture = new CommitR2Message(object);

    // Test updating with a valid digest.
    boolean result = fixture.updateDigestAndVerify(TestParameters.getInstance().getWbbPeer(), digest);
    assertTrue(result);

    assertEquals(digest, fixture.getMsg().getString(MessageFields.CommitR2.INTERNAL_DIGEST));
  }
}