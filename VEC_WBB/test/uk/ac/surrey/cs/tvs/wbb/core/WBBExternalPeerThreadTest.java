/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.KeyStore;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocket;
import javax.security.cert.X509Certificate;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.MapMaker;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.VoteMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.collection.EmptyEqualLinkedBlockingQueue;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;

/**
 * The class <code>WBBExternalPeerThreadTest</code> contains tests for the class <code>{@link WBBExternalPeerThread}</code>.
 */
public class WBBExternalPeerThreadTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends SSLSocket {

    private ByteArrayInputStream  inputStream  = null;
    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    public DummySocket(String input) {
      super();

      if (input == null) {
        this.inputStream = new ByteArrayInputStream(new byte[0]);
      }
      else {
        this.inputStream = new ByteArrayInputStream(input.getBytes());
      }
    }

    @Override
    public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public String[] getEnabledCipherSuites() {
      return null;
    }

    @Override
    public String[] getEnabledProtocols() {
      return null;
    }

    @Override
    public boolean getEnableSessionCreation() {
      return false;
    }

    @Override
    public InputStream getInputStream() throws IOException {
      return this.inputStream;
    }

    @Override
    public boolean getNeedClientAuth() {
      return false;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    public String getOutputStreamData() {
      return this.outputStream.toString();
    }

    @Override
    public SocketAddress getRemoteSocketAddress() {
      return new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.INTERNAL_PORT);
    }

    @Override
    public SSLSession getSession() {
      return new SSLSession() {

        @Override
        public int getApplicationBufferSize() {
          return 0;
        }

        @Override
        public String getCipherSuite() {
          return null;
        }

        @Override
        public long getCreationTime() {
          return 0;
        }

        @Override
        public byte[] getId() {
          return null;
        }

        @Override
        public long getLastAccessedTime() {
          return 0;
        }

        @Override
        public Certificate[] getLocalCertificates() {
          return null;
        }

        @Override
        public Principal getLocalPrincipal() {
          return null;
        }

        @Override
        public int getPacketBufferSize() {
          return 0;
        }

        @Override
        public X509Certificate[] getPeerCertificateChain() throws SSLPeerUnverifiedException {
          return null;
        }

        @Override
        public Certificate[] getPeerCertificates() throws SSLPeerUnverifiedException {
          return null;
        }

        @Override
        public String getPeerHost() {
          return null;
        }

        @Override
        public int getPeerPort() {
          return 0;
        }

        @Override
        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
          return TestParameters.PRINCIPAL;
        }

        @Override
        public String getProtocol() {
          return null;
        }

        @Override
        public SSLSessionContext getSessionContext() {
          return null;
        }

        @Override
        public Object getValue(String name) {
          return null;
        }

        @Override
        public String[] getValueNames() {
          return null;
        }

        @Override
        public void invalidate() {
        }

        @Override
        public boolean isValid() {
          return false;
        }

        @Override
        public void putValue(String name, Object value) {
        }

        @Override
        public void removeValue(String name) {
        }
      };
    }

    @Override
    public String[] getSupportedCipherSuites() {
      return null;
    }

    @Override
    public String[] getSupportedProtocols() {
      return null;
    }

    @Override
    public boolean getUseClientMode() {
      return false;
    }

    @Override
    public boolean getWantClientAuth() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }

    @Override
    public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public void setEnabledCipherSuites(String[] suites) {
    }

    @Override
    public void setEnabledProtocols(String[] protocols) {
    }

    @Override
    public void setEnableSessionCreation(boolean flag) {
    }

    @Override
    public void setNeedClientAuth(boolean need) {
    }

    @Override
    public void setUseClientMode(boolean mode) {
    }

    @Override
    public void setWantClientAuth(boolean want) {
    }

    @Override
    public void startHandshake() throws IOException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    DummySocket socket = new DummySocket(null);
    ConcurrentHashMap<String, SerialExecutor> serialQueue = new ConcurrentHashMap<String, SerialExecutor>();

    WBBExternalPeerThread fixture = new WBBExternalPeerThread(TestParameters.getInstance().getWbbPeer(), socket, serialQueue);

    // Test null input.
    fixture.run();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    DummySocket socket = new DummySocket(TestParameters.ZIP_CIPHER + "\n");
    ConcurrentHashMap<String, SerialExecutor> serialQueue = new ConcurrentHashMap<String, SerialExecutor>();

    WBBExternalPeerThread fixture = new WBBExternalPeerThread(TestParameters.getInstance().getWbbPeer(), socket, serialQueue);

    // Test empty message.
    fixture.run();

    assertTrue(socket.getOutputStreamData().contains(TestParameters.ERROR_MESSAGE));
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    // Create a vote message.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    // Missing district.
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummySocket socket = new DummySocket(object.toString() + "\n");
    ConcurrentHashMap<String, SerialExecutor> serialQueue = new ConcurrentHashMap<String, SerialExecutor>();

    WBBExternalPeerThread fixture = new WBBExternalPeerThread(TestParameters.getInstance().getWbbPeer(), socket, serialQueue);

    // Test invalid message.
    fixture.run();

    assertTrue(socket.getOutputStreamData().contains(TestParameters.ERROR_MESSAGE));
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_4() throws Exception {
    // Create a vote message.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };

    TVSKeyStore keyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    keyStore.load("./testdata/TestEVMOneSigningKey.json", "".toCharArray());

    String signature = TestParameters.signData(data, keyStore.getBLSPrivateKey("clientSigningKey"));

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    // We need an interned value that we can stop referencing, hence it can't be a fixed string, so we use Random to generate a
    // serial number

    Random rand = new Random();

    object.put(MessageFields.JSONWBBMessage.ID, "TestDevice1:" + rand.nextInt(10));
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, "TestEVMOne");
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    DummySocket socket = new DummySocket(object.toString() + "\n");
    // CJC Changed to handle new way of controlling order of serial number processing
    // Make sure the queue isn't empty to prevent the executor running.
    ConcurrentMap<String, SerialExecutor> serialMap = new MapMaker().weakKeys().makeMap();
    JSONWBBMessage msg = JSONWBBMessage.parseMessage(object);

    String internID = msg.getID().intern();
    SerialExecutor se = new SerialExecutor(TestParameters.getInstance().getWbbPeer().getExternalMessageExecutor());
    serialMap.putIfAbsent(internID, se);

    WBBExternalPeerThread fixture = new WBBExternalPeerThread(TestParameters.getInstance().getWbbPeer(), socket, serialMap);

    assertEquals(1, serialMap.size());
    // Test valid message.
    fixture.run();
    //Clear references to internedID
    internID = null;
    msg = null;
    object = null;
    fixture = null;
    //Perform a Garbage Collection
    System.gc();
    //Check that value was removed - we have to use this to force an update, size() will be incorrect until the map is internally updated
    assertEquals(0, serialMap.keySet().toArray().length);
  }

  /**
   * Run the WBBExternalPeerThread(Socket,WBBPeer,ConcurrentHashMap<String,LinkedBlockingQueue<JSONWBBMessage>>) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testWBBExternalPeerThread_1() throws Exception {
    DummySocket socket = new DummySocket(TestParameters.LINE);
    ConcurrentHashMap<String, SerialExecutor> serialQueue = new ConcurrentHashMap<String, SerialExecutor>();

    WBBExternalPeerThread result = new WBBExternalPeerThread(TestParameters.getInstance().getWbbPeer(), socket, serialQueue);
    assertNotNull(result);
  }
}