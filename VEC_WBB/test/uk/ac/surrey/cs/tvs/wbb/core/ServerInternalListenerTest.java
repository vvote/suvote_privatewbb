/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;

import javax.net.ssl.SSLSocket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message;

/**
 * The class <code>ServerInternalListenerTest</code> contains tests for the class <code>{@link ServerInternalListener}</code>.
 */
public class ServerInternalListenerTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    ServerInternalListener fixture = new ServerInternalListener(TestParameters.getInstance().getWbbPeer());

    // Put the ServerExternalListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for the socket to start up.
    TestParameters.wait(1);

    // Connect to the socket without a valid connection.
    TestParameters.getInstance().getWbbPeer().setValidConnection(false);

    SSLSocket socket = (SSLSocket) TestParameters.getInstance().getClientSSLSocketFactory().createSocket();
    socket.setEnabledCipherSuites(new String[] { TestParameters.CIPHERS });
    socket.setUseClientMode(true);

    socket.connect(new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.INTERNAL_PORT));

    // Write some data.
    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
    bos.write("{}\n".getBytes());

    // Do not flush of close as this blocks on the invalid connection. There is no way to test that the connection is invalid.

    socket.close();

    // Shutdown.
    fixture.shutdown();

    // Force the socket to close and unblock the accept.
    TestParameters.getInstance().getWbbPeer().getPeerExternalServerSocket().close();

    // Wait for the thread to finish.
    thread.join();
    assertFalse(thread.isAlive());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ServerInternalListener fixture = new ServerInternalListener(TestParameters.getInstance().getWbbPeer());

    // Put the ServerExternalListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for the socket to start up.
    TestParameters.wait(1);

    // Connect to the socket and send some data with a valid connection.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    SSLSocket socket = (SSLSocket) TestParameters.getInstance().getClientSSLSocketFactory().createSocket();
    socket.setEnabledCipherSuites(new String[] { TestParameters.CIPHERS });
    socket.setUseClientMode(true);

    socket.connect(new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.INTERNAL_PORT));

    // Create a valid internal message.
    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    CommitR1Message sendMessage = new CommitR1Message(object.toString());

    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    String send = sendMessage.getMsg().toString() + "\n";

    // Send the message.
    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
    bos.write(send.getBytes());
    bos.flush();

    // No response is received so set a timeout.
    socket.setSoTimeout(2000);

    // Wait for a response.
    BufferedInputStream bis = null;

    try {
      bis = new BufferedInputStream(socket.getInputStream());
      byte[] buffer = new byte[TestParameters.BUFFER_BOUND];
      bis.read(buffer);

      fail("received data");
    }
    catch (SocketTimeoutException e) {
      // Correct response.
    }
    finally {
      if (bis != null) {
        bis.close();
      }
    }

    bos.close();
    socket.close();

    // Shutdown.
    fixture.shutdown();
  }

  /**
   * Run the ServerInternalListener(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testServerInternalListener_1() throws Exception {
    ServerInternalListener result = new ServerInternalListener(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }
}