/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>UnknownDBExceptionTest</code> contains tests for the class <code>{@link UnknownDBException}</code>.
 */
public class UnknownDBExceptionTest {

  /**
   * Run the UnknownDBException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUnknownDBException_1() throws Exception {

    UnknownDBException result = new UnknownDBException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the UnknownDBException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUnknownDBException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    UnknownDBException result = new UnknownDBException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the UnknownDBException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUnknownDBException_3() throws Exception {
    Throwable cause = new Throwable();

    UnknownDBException result = new UnknownDBException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the UnknownDBException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUnknownDBException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    UnknownDBException result = new UnknownDBException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the UnknownDBException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUnknownDBException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    UnknownDBException result = new UnknownDBException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}