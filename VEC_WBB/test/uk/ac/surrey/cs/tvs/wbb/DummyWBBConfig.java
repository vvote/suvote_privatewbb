/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.util.HashMap;
import java.util.Map;

import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;

/**
 * This class defines a dummy WBBConfig.
 */
public class DummyWBBConfig extends WBBConfig {

  /** Map holding the dummy configuration values. */
  Map<String, Object> map = new HashMap<String, Object>();

  /**
   * Default constructor which initialises the configuration without a config file.
   * 
   * @throws JSONSchemaValidationException
   */
  public DummyWBBConfig() throws JSONSchemaValidationException {
    super("rubbish");
  }

  /**
   * Gets an int property value
   * 
   * @param key
   *          name of property to get
   * @return int value of the requested property or 0 if it does not exist or is not an integer
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfig#getInt(java.lang.String)
   */
  @Override
  public int getInt(String key) {
    // Only return what has been put into the map.
    return (int) this.map.get(key);
  }

  /**
   * Gets a string property value
   * 
   * @param key
   *          name of property to get
   * @return String value of the property or empty string if property does not exist or is not a string
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfig#getString(java.lang.String)
   */
  @Override
  public String getString(String key) {
    // Only return what has been put into the map.
    return (String) this.map.get(key);
  }

  /**
   * Puts configuration information into the object.
   * 
   * @param key
   *          The configuration key.
   * @param value
   *          The value.
   */
  public void put(String key, Object value) {
    this.map.put(key, value);
  }

}
