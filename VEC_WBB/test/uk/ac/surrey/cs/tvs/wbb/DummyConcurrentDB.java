/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyRespondedException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadySentTimeoutException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.ClientMessageExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.NoCommitExistsException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.R2AlreadyRunningException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.messages.CancelMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message;
import uk.ac.surrey.cs.tvs.wbb.messages.CommitR2Message;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.StartEVMMessage;
import uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * This class defines a dummy ConcurrentDB.
 */
public class DummyConcurrentDB extends ConcurrentDB {

  /** Has the message been stored or not? */
  private boolean                    stored                                        = false;

  /** The last commit file. */
  private String                     commitFile                                    = null;

  /** The last commit full file. */
  private String                     commitFileFull                                = null;

  /** The last commit attachments. */
  private String                     commitAttachments                             = null;

  /** The last commit time. */
  private String                     commitTime                                    = null;

  /** The last cipher. */
  private JSONObject                 cipher                                        = null;

  /** Respond with a timeout? */
  private boolean                    timeout                                       = false;

  /** Respond with whether consensus can be reached? */
  private boolean                    canReachConsensus                             = false;

  /** Respond with whether consensus has been reached? */
  private boolean                    reachedConsensus                              = false;

  /** Respond with the following JSON object. */
  private JSONObject                 jsonObject                                    = null;

  /** The commit table data to send. */
  private String                     commitTable                                   = null;

  /** The commit threshold. */
  private int                        threshold                                     = 0;

  /** The current hash. */
  private byte[]                     hash                                          = null;

  /** Should commit round 2 be run? */
  private boolean                    runCommitR2                                   = false;

  /** Response with commit round 2 status. */
  private boolean                    round2                                        = false;

  /** Messages to respond with. */
  private ArrayList<JSONWBBMessage>  messages                                      = null;

  /** The commit messages to respond with. */
  private ArrayList<CommitR1Message> commitMessages                                = null;

  /** Signatures to check to respond with. */
  private ArrayList<PeerMessage>     sigsToCheck                                   = null;

  /** Database field to return. */
  private String                     dbField                                       = null;

  /** Map of methods to exceptions that should be raised. */
  private Map<String, Integer>       exceptionsMap                                 = new HashMap<String, Integer>();

  /** The last received DB record type. */
  private DBRecordType               recordType                                    = null;

  /** The last received message. */
  private JSONWBBMessage             message                                       = null;

  /** The last received signature. */
  private String                     signature                                     = null;

  /** The last received serial number. */
  private String                     serialNo                                      = null;

  /** Use the last stored message when the client message is retrieved? */
  private boolean                    useStoredMessage                              = false;

  /** The last received validity. */
  private boolean                    valid                                         = false;

  /** The last received commit hash. */
  private String                     commitHash                                    = null;

  /** The last received response. */
  private String                     response                                      = null;

  /** The last received value. */
  private String                     value                                         = null;

  /** The last received field. */
  private String                     field                                         = null;

  /** Has the getClientCancelMessages been called? */
  private boolean                    getClientCancelMessagesCalled                 = false;

  /** Has the isAlreadyStored method been called? */
  private boolean                    isAlreadyStoredCalled                         = false;

  /** Has the submitIncomingPeerMessageChecked method been called? */
  private boolean                    submitIncomingPeerMessageCheckedCalled        = false;

  /** Has the submitIncomingPeerMessageUnchecked method been called? */
  private boolean                    submitIncomingPeerMessageUncheckedCalled      = false;

  /** Has the submitIncomingPeerCommitR1Checked method been called? */
  private boolean                    submitIncomingPeerCommitR1CheckedCalled       = false;

  /** Has the submitPostTimeoutMessage method been called? */
  private boolean                    submitPostTimeoutMessageCalled                = false;

  /** Has the canOrHaveReachedConsensus method been called? */
  private boolean                    canOrHaveReachedConsensusCalled               = false;

  /** Has the getClientMessage method been called? */
  private boolean                    getClientMessageCalled                        = false;

  /** Has the checkThresholdAndResponse method been called? */
  private boolean                    checkThresholdAndResponseCalled               = false;

  /** Has the submitIncomingClientMessage method been called? */
  private boolean                    submitIncomingClientMessageCalled             = false;

  /** Has the checkCommitThreshold method been called? */
  private boolean                    checkCommitThresholdCalled                    = false;

  /** Has the setCommitSentSig method been called? */
  private boolean                    setCommitSentSigCalled                        = false;

  /** Has the canReachCommitConsensus method been called? */
  private boolean                    canReachCommitConsensusCalled                 = false;

  /** Has the submitMyCancellationSignature method been called? */
  private boolean                    submitMyCancellationSignatureCalled           = false;

  /** Has the shouldRunR2Commit method been called? */
  private boolean                    shouldRunR2CommitCalled                       = false;

  /** Has the getCurrentCommitSigsToCheck method been called? */
  private boolean                    getCurrentCommitSigsToCheckCalled             = false;

  /** Has the getBallotRecord method been called? */
  private boolean                    getBallotRecordCalled                         = false;

  /** Has the getBallotRecord method been called? */
  private boolean                    getSigsToCheckCalled                          = false;

  /** Has the submitMyCancellationSignatureSK2 method been called? */
  private boolean                    submitMyCancellationSignatureSK2Called        = false;

  /** Has the submitCipher method been called? */
  private boolean                    submitCipherCalled                            = false;

  /** Has the getRecord method been called? */
  private boolean                    getRecordCalled                               = false;

  /** Has the submitIncomingCommitMessage method been called? */
  private boolean                    submitIncomingCommitMessageCalled             = false;

  /** Has the submitIncomingClientCancellationMessage method been called? */
  private boolean                    submitIncomingClientCancellationMessageCalled = false;

  /** Has the submitIncomingPeerCommitR1UnChecked method been called? */
  private boolean                    submitIncomingPeerCommitR1UnCheckedCalled     = false;

  /** Has the queueR2Commit method been called? */
  private boolean                    queueR2CommitCalled                           = false;

  /** Has the submitIncomingPeerCommitR2Checked method been called? */
  private boolean                    submitIncomingPeerCommitR2CheckedCalled       = false;

  /** Has the checkCommitR2Threshold method been called? */
  private boolean                    checkCommitR2ThresholdCalled                  = false;

  /** Has the updateCurrentCommitRecord method been called? */
  private boolean                    updateCurrentCommitRecordCalled               = false;

  /** Has the timeoutSent method been called? */
  private boolean                    timeoutSentCalled                             = false;

  /** Has the setResponseAndCloseCommit method been called? */
  private boolean                    setResponseAndCloseCommitCalled               = false;

  /** Has the submitStartEVMMessage method been called? */
  private boolean                    submitStartEVMMessageCalled                   = false;

  /**
   * Default constructor which does not connect to a database.
   */
  public DummyConcurrentDB() {
    super("rubbish");
  }

  /**
   * Adds an exception to be raised for a method.
   * 
   * @param method
   *          The method to raise the exception.
   * @param exception
   *          The index to the exception.
   */
  public void addException(String method, int exception) {
    this.exceptionsMap.put(method, exception);
  }

  /**
   * Checks if we have reached consensus on a cancellation message and if not, whether it is possible to do so. Dummy test stub.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String of serial number to check
   * @param threshold
   *          integer of the threshold we need to reach
   * @param peerCount
   *          integer of total number of peers
   * @return boolean - true if have or can reach consensus, false otherwise
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#canOrHaveReachedConsensus(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      java.lang.String, int, int)
   */
  @Override
  public boolean canOrHaveReachedConsensus(DBRecordType recordType, String serialNo, int threshold, int peerCount) {
    this.canOrHaveReachedConsensusCalled = true;
    this.serialNo = serialNo;

    return this.canReachConsensus;
  }

  /**
   * Checks whether it is still possible to reach consensus on R1 of the commitment procedure. Dummy test stub.
   * 
   * @param threshold
   *          the threshold of peers needed to reach consensus
   * @param peerCount
   *          the number of peers in total
   * @return boolean - true if we can still reach commitment, false if not
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#canReachCommitConsensus(int, int)
   */
  @Override
  public boolean canReachCommitConsensus(int threshold, int peerCount) {
    this.canReachCommitConsensusCalled = true;

    return this.canReachConsensus;
  }

  /**
   * Checks whether we have already responded to a message and if not marks it as timed out. This method returns the required test
   * value.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param message
   *          JSONWBBMessage from client that the timeout has occurred on
   * @return boolean - true if we have set timeout, false if we already set timeout.
   * @throws AlreadyRespondedException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#checkAndSetTimeout(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage)
   */
  @Override
  public boolean checkAndSetTimeout(DBRecordType recordType, JSONWBBMessage message) throws AlreadyRespondedException {
    this.recordType = recordType;
    this.message = message;

    if (this.exceptionsMap.containsKey("checkAndSetTimeout")) {
      switch (this.exceptionsMap.get("checkAndSetTimeout")) {
        case 0:
          throw new AlreadyRespondedException("Testing");
        default:
      }
    }

    return this.timeout;
  }

  /**
   * Checks whether we have received a threshold of R2 messages and have not already sent a signature. Dummy test stub.
   * 
   * @param threshold
   *          integer of the threshold to check
   * @return boolean - true if we have reached consensus, false if not
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#checkCommitR2Threshold(int)
   */
  @Override
  public boolean checkCommitR2Threshold(int threshold) {
    this.checkCommitR2ThresholdCalled = true;

    return threshold >= this.threshold;
  }

  /**
   * Checks if we have reached the threshold in R1 of the commit signature and have not sent an SK2 signature. Dummy test stub.
   * 
   * @param threshold
   *          integer threshold to check
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#checkCommitThreshold(int)
   */
  @Override
  public boolean checkCommitThreshold(int threshold) {
    this.checkCommitThresholdCalled = true;

    return threshold >= this.threshold;
  }

  /**
   * Checks whether we have reached a consensus on a message and if we have sent a response. Dummy test stub.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to check
   * @param threshold
   *          integer threshold to check
   * @return boolean - true if we have reached consensus and should send response, otherwise false
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#checkThresholdAndResponse(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      java.lang.String, int)
   */
  @Override
  public boolean checkThresholdAndResponse(DBRecordType recordType, String serialNo, int threshold) {
    this.checkThresholdAndResponseCalled = true;
    this.serialNo = serialNo;

    return this.reachedConsensus;
  }

  /**
   * Checks whether we have reached a consensus on a message and if we have sent a response. Dummy test stub.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to check
   * @param threshold
   *          integer threshold to check
   * @param timeout
   *          integer timeout for Print on Demand - zero otherwise
   * @return boolean - true if we have reached consensus and should send response, otherwise false
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#checkThresholdAndResponse(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      java.lang.String, int, int)
   */
  @Override
  public boolean checkThresholdAndResponse(DBRecordType recordType, String serialNo, int threshold, int timeout) {
    this.checkThresholdAndResponseCalled = true;
    this.serialNo = serialNo;

    return this.reachedConsensus;
  }

  /**
   * Creates a new currentCommit record. Dummy test stub.
   * 
   * @param messageToSend
   *          the JSONWBBMessage that we will send to other peers (R1 commit)
   * @param commitFile
   *          the JSON file with the commit data in it
   * @param commitFileFull
   *          the Full database file with everything in it
   * @param commitAttachments
   *          -the location of any attachments - files we will be send with the commitment
   * @param commitTime
   *          - the CommitTime this relates to
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#createNewCommitRecord(uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage,
   *      java.lang.String, java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public void createNewCommitRecord(JSONWBBMessage messageToSend, String commitFile, String commitFileFull,
      String commitAttachments, String commitTime, String commitDesc) throws JSONException, AlreadyReceivedMessageException {
    this.message = messageToSend;
    this.commitFile = commitFile;
    this.commitFileFull = commitFileFull;
    this.commitAttachments = commitAttachments;
    this.commitTime = commitTime;
  }

  @Override
  public JSONObject getBallotRecord(String serial) throws JSONException {
    this.getBallotRecordCalled = true;
    this.serialNo = serial;

    return this.jsonObject;
  }

  /**
   * @return The last received cipher.
   */
  public JSONObject getCipher() {
    return this.cipher;
  }

  /**
   * Gets a String array of client cancellation messages. Dummy test stub.
   * 
   * @param serialNo
   *          String serial number to lookup
   * @return String[] of client messages in JSON String format or array of length 0 if none exist
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getClientCancelMessages(java.lang.String)
   */
  @Override
  public String[] getClientCancelMessages(String serialNo) {
    this.getClientCancelMessagesCalled = true;
    this.serialNo = serialNo;

    List<String> results = new ArrayList<String>();

    // Use a specific value or the last stored message?
    if (this.useStoredMessage) {
      if ((this.message != null) && (this.message.getMsg() != null)) {
        results.add(this.message.getMsg().toString());
      }
    }
    else if (this.jsonObject != null) {
      results.add(this.jsonObject.toString());
    }

    return results.toArray(new String[0]);
  }

  /**
   * Gets the String JSON representation of the original client message. Dummy test stub.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to lookup
   * @return String of JSON of the original message or null if the message does not exist
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getClientMessage(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      java.lang.String)
   */
  @Override
  public String getClientMessage(DBRecordType recordType, String serialNo) {
    this.getClientMessageCalled = true;
    this.serialNo = serialNo;

    String result = null;

    // Use a specific value or the last stored message?
    if (this.useStoredMessage) {
      if ((this.message != null) && (this.message.getMsg() != null)) {
        result = this.message.getMsg().toString();
      }
    }
    else if (this.jsonObject != null) {
      result = this.jsonObject.toString();
    }

    return result;
  }

  /**
   * @return the commitAttachments.
   */
  public String getCommitAttachments() {
    return this.commitAttachments;
  }

  /**
   * Finds all data related to a particular commitTime and prepares the data for commitment. Dummy test stub.
   * 
   * @param threshold
   *          integer of the threshold
   * @param commitFull
   *          File to write full commit data to
   * @param commitTime
   *          String of CommitTime we are making the commitment on
   * @return ArrayList of JSONWBBMessages that should be committed
   * @throws MessageJSONException
   * @throws IOException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getCommitData(int, java.io.File, java.lang.String)
   */
  @Override
  public ArrayList<JSONWBBMessage> getCommitData(int threshold, File commitFull, String commitTime) throws MessageJSONException,
      IOException {
    // Write the messages to the file.
    BufferedWriter bw = new BufferedWriter(new FileWriter(commitFull));

    for (JSONWBBMessage message : this.messages) {
      bw.write(message.toString());
      bw.newLine();
    }

    bw.close();

    return this.messages;
  }

  /**
   * @return the commitFile.
   */
  public String getCommitFile() {
    return this.commitFile;
  }

  /**
   * @return the commitFileFull.
   */
  public String getCommitFileFull() {
    return this.commitFileFull;
  }

  /**
   * @return the last received commit hash.
   */
  public String getCommitHash() {
    return this.commitHash;
  }

  /**
   * @return The dummy commit table.
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getCommitTable()
   */
  @Override
  public JSONObject getCommitTable() throws JSONException {
    return new JSONObject(this.commitTable);
  }

  /**
   * @return the commitTime.
   */
  public String getCommitTime() {
    return this.commitTime;
  }

  /**
   * Utility method for getting a field from the CurrentCommit document. Dummy test stub.
   * 
   * @param field
   *          String of field name to get
   * @return String of field value of null
   * @throws NoCommitExistsException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getCurrentCommitField(java.lang.String)
   */
  @Override
  public String getCurrentCommitField(String field) throws NoCommitExistsException {
    if (this.exceptionsMap.containsKey("getCurrentCommitField")) {
      switch (this.exceptionsMap.get("getCurrentCommitField")) {
        case 0:
          throw new NoCommitExistsException("Testing");
        default:
      }
    }
    if(field.equals("commitDesc")){
      return null;
    }
    return field;
  }

  /**
   * Gets a String of the Base64 encoding of the hash of our own data for the current commitment. Dummy test stub
   * 
   * @return Base64 String of the hash of our own data
   * @throws NoCommitExistsException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getCurrentCommitHash()
   */
  @Override
  public String getCurrentCommitHash() throws NoCommitExistsException {
    if (this.exceptionsMap.containsKey("getCurrentCommitHash")) {
      switch (this.exceptionsMap.get("getCurrentCommitHash")) {
        case 0:
          throw new NoCommitExistsException("Testing");
        default:
      }
    }

    return Base64.encodeBase64String(this.hash);
  }

  /**
   * Gets an ArrayList of CommitR1Messages that need checking. Dummy test stub.
   * 
   * @return ArrayList of CommitR1Messages that need checking
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getCurrentCommitSigsToCheck()
   */
  @Override
  public ArrayList<CommitR1Message> getCurrentCommitSigsToCheck() throws MessageJSONException {
    this.getCurrentCommitSigsToCheckCalled = true;

    return this.commitMessages;
  }

  /**
   * @return the field.
   */
  public String getField() {
    return this.field;
  }

  /**
   * Gets a field from the submissions for the specified serial number. Dummy test stub.
   * 
   * @param serialNo
   *          The target serial number.
   * @param field
   *          The field to retrieve.
   * @return The field or null if it or the record do not exist.
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getField(java.lang.String, java.lang.String)
   */
  @Override
  public String getField(String serialNo, String field) {
    this.serialNo = serialNo;

    return this.dbField;
  }

  /**
   * @return the last received message.
   */
  public JSONWBBMessage getMessage() {
    return this.message;
  }

  /**
   * Gets the R2Queue in the form of an ArrayList of JSONWBBMessages. Dummy test stub.
   * 
   * @return ArrayList of JSONWBBMessages of the R2Queue or an empty ArrayList if no messages are queued
   * @throws MessageJSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getR2Queue()
   */
  @Override
  public ArrayList<JSONWBBMessage> getR2Queue() throws MessageJSONException {
    return this.messages;
  }

  /**
   * Gets a record as a JSONObject. This returns all data in the document for that serial no. Dummy test stub.
   * 
   * @param serial
   *          String of serial number of look for
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#getRecord(java.lang.String)
   */
  @Override
  public JSONObject getRecord(String serial) throws JSONException {
    this.getRecordCalled = true;
    this.serialNo = serial;

    return this.jsonObject;
  }

  /**
   * @return the last received DB record type.
   */
  public DBRecordType getRecordType() {
    return this.recordType;
  }

  /**
   * @return the response.
   */
  public String getResponse() {
    return this.response;
  }

  /**
   * @return the last received serial number.
   */
  public String getSerialNo() {
    return this.serialNo;
  }

  /**
   * @return the last received signature.
   */
  public String getSignature() {
    return this.signature;
  }

  /**
   * Gets an ArrayList of PeerMessages that need to be checked. Dummy test stub.
   * 
   * @param recordType
   *          DBRecordType specifying what type of record (POD, Cancel, General) that we are checking
   * @param serialNo
   *          String serial number to lookup
   * @return ArrayList of PeerMessages to check, or an empty list
   * @throws MessageJSONException
   */
  @Override
  public ArrayList<PeerMessage> getSigsToCheck(DBRecordType recordType, String serialNo) throws MessageJSONException {
    this.getSigsToCheckCalled = true;
    this.serialNo = serialNo;

    return this.sigsToCheck;
  }

  /**
   * @return the value.
   */
  public String getValue() {
    return this.value;
  }

  /**
   * /** Called during commit to check if the database already contains a record for that serial number, for the appropriate
   * DBRecordType with a threshold of signatures. Dummy test stub.
   * 
   * Cancel, POD and Standard messages related to the same serial and stored in the same document (record), as such the type that is
   * being checked is specified
   * 
   * @param serialNo
   *          String of serial number to look up
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param threshold
   *          integer threshold it must be >= to be valid
   * @return boolean - true if threshold met, false if not
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#isAlreadyStored(java.lang.String,
   *      uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType, int)
   */
  @Override
  public boolean isAlreadyStored(String serialNo, DBRecordType type, int threshold) {
    this.isAlreadyStoredCalled = true;
    this.serialNo = serialNo;

    return this.stored;
  }

  /**
   * @return whether the isAlreadyStored has been called.
   */
  public boolean isAlreadyStoredCalled() {
    return this.isAlreadyStoredCalled;
  }

  /**
   * @return whether the canOrHaveReachedConsensus method has been called.
   */
  public boolean isCanOrHaveReachedConsensusCalled() {
    return this.canOrHaveReachedConsensusCalled;
  }

  /**
   * @return whether the canReachCommitConsensus method has been called.
   */
  public boolean isCanReachCommitConsensusCalled() {
    return this.canReachCommitConsensusCalled;
  }

  /**
   * @return whether the checkCommitR2Threshold method has been called.
   */
  public boolean isCheckCommitR2ThresholdCalled() {
    return this.checkCommitR2ThresholdCalled;
  }

  /**
   * @return whether the checkCommitThreshold method has been called.
   */
  public boolean isCheckCommitThresholdCalled() {
    return this.checkCommitThresholdCalled;
  }

  /**
   * @return whether the checkThresholdAndResponseCalled method has been called
   */
  public boolean isCheckThresholdAndResponseCalled() {
    return this.checkThresholdAndResponseCalled;
  }

  /**
   * Check if we are currently running R2 of the commit protocol. Dummy test stub.
   * 
   * @return boolean - true if R2 is running, false if not
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#isCommitInR2()
   */
  @Override
  public boolean isCommitInR2() {
    return this.round2;
  }

  /**
   * @return whether the getBallotRecordCalled method has been called.
   */
  public boolean isGetBallotRecordCalled() {
    return this.getBallotRecordCalled;
  }

  /**
   * @return whether the getClientCancelMessages has been called.
   */
  public boolean isGetClientCancelMessagesCalled() {
    return this.getClientCancelMessagesCalled;
  }

  /**
   * @return whether the getClientMessageCalled method has been called.
   */
  public boolean isGetClientMessageCalled() {
    return this.getClientMessageCalled;
  }

  /**
   * @return whether the getCurrentCommitSigsToCheck method has been called.
   */
  public boolean isGetCurrentCommitSigsToCheckCalled() {
    return this.getCurrentCommitSigsToCheckCalled;
  }

  /**
   * @return whether the getRecord method has been called.
   */
  public boolean isGetRecordCalled() {
    return this.getRecordCalled;
  }

  /**
   * @return whether the getSigsToCheck method has been called.
   */
  public boolean isGetSigsToCheckCalled() {
    return this.getSigsToCheckCalled;
  }

  /**
   * @return whether the queueR2Commit method has been called.
   */
  public boolean isQueueR2CommitCalled() {
    return this.queueR2CommitCalled;
  }

  /**
   * @return whether the setCommitSentSig method has been called.
   */
  public boolean isSetCommitSentSigCalled() {
    return this.setCommitSentSigCalled;
  }

  /**
   * @return whether the setResponseAndCloseCommit method has been called.
   */
  public boolean isSetResponseAndCloseCommitCalled() {
    return this.setResponseAndCloseCommitCalled;
  }

  /**
   * @return whether the shouldRunR2Commit method has been called.
   */
  public boolean isShouldRunR2CommitCalled() {
    return this.shouldRunR2CommitCalled;
  }

  /**
   * @return whether the submitCipher method has been called.
   */
  public boolean isSubmitCipherCalled() {
    return this.submitCipherCalled;
  }

  /**
   * @return whether the submitIncomingClientCancellationMessage method has been called.
   */
  public boolean isSubmitIncomingClientCancellationMessageCalled() {
    return this.submitIncomingClientCancellationMessageCalled;
  }

  /**
   * @return whether the submitIncomingClientMessage method has been called.
   */
  public boolean isSubmitIncomingClientMessageCalled() {
    return this.submitIncomingClientMessageCalled;
  }

  /**
   * @return whether the submitIncomingCommitMessage has been called.
   */
  public boolean isSubmitIncomingCommitMessageCalled() {
    return this.submitIncomingCommitMessageCalled;
  }

  /**
   * @return whether the submitIncomingPeerCommitR1Checked method has been called.
   */
  public boolean isSubmitIncomingPeerCommitR1CheckedCalled() {
    return this.submitIncomingPeerCommitR1CheckedCalled;
  }

  /**
   * @return whether the submitIncomingPeerCommitR1UnChecked method has been called.
   */
  public boolean isSubmitIncomingPeerCommitR1UnCheckedCalled() {
    return this.submitIncomingPeerCommitR1UnCheckedCalled;
  }

  /**
   * @return whether the submitIncomingPeerCommitR2Checked method has been called.
   */
  public boolean isSubmitIncomingPeerCommitR2CheckedCalled() {
    return this.submitIncomingPeerCommitR2CheckedCalled;
  }

  /**
   * @return whether the submitIncomingPeerMessageChecked method has been called.
   */
  public boolean isSubmitIncomingPeerMessageCheckedCalled() {
    return this.submitIncomingPeerMessageCheckedCalled;
  }

  /**
   * @return whether the submitIncomingPeerMessageUnchecked method has been called.
   */
  public boolean isSubmitIncomingPeerMessageUncheckedCalled() {
    return this.submitIncomingPeerMessageUncheckedCalled;
  }

  /**
   * @return whether the submitMyCancellationSignature method has been called.
   */
  public boolean isSubmitMyCancellationSignatureCalled() {
    return this.submitMyCancellationSignatureCalled;
  }

  /**
   * @return whether the submitMyCancellationSignatureSK2 method has been called.
   */
  public boolean isSubmitMyCancellationSignatureSK2Called() {
    return this.submitMyCancellationSignatureSK2Called;
  }

  /**
   * @return whether the submitPostTimeoutMessage method has been called.
   */
  public boolean isSubmitPostTimeoutMessageCalled() {
    return this.submitPostTimeoutMessageCalled;
  }

  /**
   * @return whether the submitStartEVMMessage method has been called.
   */
  public boolean isSubmitStartEVMMessageCalled() {
    return this.submitStartEVMMessageCalled;
  }

  /**
   * @return whether the timeoutSent method has been called.
   */
  public boolean isTimeoutSentCalled() {
    return this.timeoutSentCalled;
  }

  /**
   * @return whether the updateCurrentCommitRecord method has been called.
   */
  public boolean isUpdateCurrentCommitRecordCalled() {
    return this.updateCurrentCommitRecordCalled;
  }

  /**
   * @return the valid.
   */
  public boolean isValid() {
    return this.valid;
  }

  /**
   * Called when a Peer sends an R2 commitment before we have finished creating our own commitment. Dummy test stub.
   * 
   * @param commitMessage
   *          JSONWBBMessage to store
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#queueR2Commit(uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage)
   */
  @Override
  public void queueR2Commit(JSONWBBMessage commitMessage) throws JSONException, AlreadyReceivedMessageException,
      R2AlreadyRunningException {
    this.queueR2CommitCalled = true;
    this.message = commitMessage;

    if (this.exceptionsMap.containsKey("queueR2Commit")) {
      switch (this.exceptionsMap.get("queueR2Commit")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        case 1:
          throw new R2AlreadyRunningException("Testing");
        default:
      }
    }
  }

  /**
   * @param canReachConsensus
   *          the new canReachConsensus.
   */
  public void setCanReachConsensus(boolean canReachConsensus) {
    this.canReachConsensus = canReachConsensus;
  }

  /**
   * @param closed
   *          the new closed.
   */
  public void setClosed(boolean closed) {
  }

  /**
   * @param commitMessages
   *          the new commitMessages.
   */
  public void setCommitMessages(ArrayList<CommitR1Message> commitMessages) {
    this.commitMessages = commitMessages;
  }

  /**
   * Marks that we have sent a signature under SK2 and close the currentCommitment. Dummy test stub.
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#setCommitSentSig(java.lang.String)
   */
  @Override
  public boolean setCommitSentSig(String mySK2Signature) {
    this.setCommitSentSigCalled = true;
    this.signature = mySK2Signature;

    return this.stored;
  }

  /**
   * @param commitTable
   *          the new commit table.
   */
  public void setCommitTable(String commitTable) {
    this.commitTable = commitTable;
  }

  /**
   * @param dbField
   *          the dbField to set
   */
  public void setDbField(String dbField) {
    this.dbField = dbField;
  }

  /**
   * Gets a Ballot Cipher record as a JSONObject. This returns all data in the document for that serial no
   * 
   * @param serial
   *          String of serial number of look for
   * @return JSONObject containing the document or null if it doesn't exist
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#setFieldInRecord(java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public void setFieldInRecord(String serial, String field, String value) throws UnknownDBException {
    this.serialNo = serial;
    this.field = field;
    this.value = value;
  }

  /**
   * @param hash
   *          the new hash.
   */
  public void setHash(byte[] hash) {
    this.hash = hash;
  }

  /**
   * @param jsonObject
   *          the new jsonObject.
   */
  public void setJsonObject(JSONObject jsonObject) {
    this.jsonObject = jsonObject;
  }

  /**
   * @param messages
   *          the new messages.
   */
  public void setMessages(ArrayList<JSONWBBMessage> messages) {
    this.messages = messages;
  }

  /**
   * @param reachedConsensus
   *          the new reachedConsensus.
   */
  public void setReachedConsensus(boolean reachedConsensus) {
    this.reachedConsensus = reachedConsensus;
  }

  /**
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#setResponseAndCloseCommit(java.lang.String)
   */
  @Override
  public boolean setResponseAndCloseCommit(String response) throws NoCommitExistsException {
    this.setResponseAndCloseCommitCalled = true;
    this.response = response;

    if (this.exceptionsMap.containsKey("setResponseAndCloseCommit")) {
      switch (this.exceptionsMap.get("setResponseAndCloseCommit")) {
        case 0:
          throw new NoCommitExistsException("Testing");
        default:
      }
    }

    return true;
  }

  /**
   * @param round2
   *          the round2 to set
   */
  public void setRound2(boolean round2) {
    this.round2 = round2;
  }

  /**
   * @param runCommitR2
   *          should commit round 2 be run?
   */
  public void setRunCommitR2(boolean runCommitR2) {
    this.runCommitR2 = runCommitR2;
  }

  /**
   * Sets the sigs to check to return.
   * 
   * @param sigsToCheck
   *          The list of sigs to check.
   */
  public void setSigsToCheck(ArrayList<PeerMessage> sigsToCheck) {
    this.sigsToCheck = sigsToCheck;
  }

  /**
   * Sets whether stored response.
   * 
   * @param stored
   *          Respond with stored or not?
   */
  public void setStored(boolean stored) {
    this.stored = stored;
  }

  /**
   * @param threshold
   *          the new threshold.
   */
  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }

  /**
   * @param timeout
   *          the new timeout.
   */
  public void setTimeout(boolean timeout) {
    this.timeout = timeout;
  }

  /**
   * @param useStoredMessage
   *          use the last stored message when the client message is retrieved?
   */
  public void setUseStoredMessage(boolean useStoredMessage) {
    this.useStoredMessage = useStoredMessage;
  }

  /**
   * Checks whether R2 of the commit procedure should be run. Dummy test stub.
   * 
   * @return boolean true if R2 should be started, false if R2 has already been started
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#shouldRunR2Commit()
   */
  @Override
  public boolean shouldRunR2Commit() {
    this.shouldRunR2CommitCalled = true;

    return this.runCommitR2;
  }

  @Override
  public void submitCipher(JSONObject cipher) throws AlreadyReceivedMessageException, JSONException {
    this.submitCipherCalled = true;
    this.cipher = cipher;

    if (this.exceptionsMap.containsKey("submitCipher")) {
      switch (this.exceptionsMap.get("submitCipher")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

  /**
   * Records an incoming Client Cancellation message. Dummy test stub.
   * 
   * @param msg
   *          CancelMessage to store
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingClientCancellationMessage(uk.ac.surrey.cs.tvs.wbb.messages.CancelMessage)
   */
  @Override
  public void submitIncomingClientCancellationMessage(CancelMessage msg) throws AlreadyReceivedMessageException {
    this.submitIncomingClientCancellationMessageCalled = true;
    this.message = msg;

    if (this.exceptionsMap.containsKey("submitIncomingClientCancellationMessage")) {
      switch (this.exceptionsMap.get("submitIncomingClientCancellationMessage")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

  /**
   * Stores an incoming client message (generally from an EBM). Dummy test stub.
   * 
   * Checks that the message has not already been used or cancelled
   * 
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param message
   *          JSONWBBMessage to store
   * @param signature
   *          signature from this peer of the message
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingClientMessage(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage, java.lang.String)
   */
  @Override
  public void submitIncomingClientMessage(DBRecordType recordType, JSONWBBMessage message, String signature)
      throws AlreadyReceivedMessageException {
    this.submitIncomingClientMessageCalled = true;
    this.recordType = recordType;
    this.message = message;
    this.signature = signature;

    if (this.exceptionsMap.containsKey("submitIncomingClientMessage")) {
      switch (this.exceptionsMap.get("submitIncomingClientMessage")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

  /**
   * Submits a generic client message during R2 commit. Dummy test stub.
   * 
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param msg
   *          JSONWBBMessage we should submit
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingCommitMessage(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage, boolean)
   */
  @Override
  public void submitIncomingCommitMessage(DBRecordType recordType, JSONWBBMessage msg, boolean isConflict)
      throws AlreadyReceivedMessageException {
    this.submitIncomingCommitMessageCalled = true;
    this.recordType = recordType;
    this.message = msg;

    if (this.exceptionsMap.containsKey("submitIncomingCommitMessage")) {
      switch (this.exceptionsMap.get("submitIncomingCommitMessage")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

  /**
   * Records an R1 Commit message received from another peer. Dummy test stub.
   * 
   * If isValid is set to true the commitSigCount will be incremented
   * 
   * @param commitMessage
   *          CommitR1Message to store
   * @param valid
   *          boolean - true if valid, false if invalid
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingPeerCommitR1Checked(uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message,
   *      boolean)
   */
  @Override
  public void submitIncomingPeerCommitR1Checked(CommitR1Message commitMessage, boolean valid)
      throws AlreadyReceivedMessageException {
    this.submitIncomingPeerCommitR1CheckedCalled = true;
    this.message = commitMessage;
    this.valid = valid;

    if (this.exceptionsMap.containsKey("submitIncomingPeerCommitR1Checked")) {
      switch (this.exceptionsMap.get("submitIncomingPeerCommitR1Checked")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

  /**
   * Records an unchecked R1 Commit message received from another peer. Dummy test stub.
   * 
   * @param cMsg
   *          CommitR1Message to store
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingPeerCommitR1UnChecked(uk.ac.surrey.cs.tvs.wbb.messages.CommitR1Message)
   */
  @Override
  public void submitIncomingPeerCommitR1UnChecked(CommitR1Message cMsg) throws AlreadyReceivedMessageException,
      CommitSignatureExistsException {
    this.submitIncomingPeerCommitR1UnCheckedCalled = true;
    this.message = cMsg;

    if (this.exceptionsMap.containsKey("submitIncomingPeerCommitR1UnChecked")) {
      switch (this.exceptionsMap.get("submitIncomingPeerCommitR1UnChecked")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        case 1:
          throw new CommitSignatureExistsException("Testing");
        default:
      }
    }
  }

  /**
   * Submits a checked R2 message from another peer. Dummy test stub.
   * 
   * @param cMsg
   *          PeerFileMessage (R2 message) to record
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingPeerCommitR2Checked(uk.ac.surrey.cs.tvs.wbb.messages.CommitR2Message)
   */
  @Override
  public void submitIncomingPeerCommitR2Checked(CommitR2Message cMsg) throws AlreadyReceivedMessageException {
    this.submitIncomingPeerCommitR2CheckedCalled = true;
    this.message = cMsg;

    if (this.exceptionsMap.containsKey("submitIncomingPeerCommitR2Checked")) {
      switch (this.exceptionsMap.get("submitIncomingPeerCommitR2Checked")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

  /**
   * Stores an incoming Peer SK1 message. Dummy method for testing.
   * 
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param pMsg
   *          PeerMessage to store
   * @param isValid
   *          boolean is pMsg valid
   * @param ignoreTimeout
   *          - if true the timeout block will be overridden
   * @param isConflict
   *          - if true the record will be stored in the conflict collection
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingPeerMessageChecked(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage, boolean, boolean, boolean)
   */
  @Override
  public void submitIncomingPeerMessageChecked(DBRecordType recordType, PeerMessage peerMessage, boolean valid,
      boolean ignoreTimeout, boolean isConflict) throws AlreadyReceivedMessageException, AlreadySentTimeoutException {
    this.submitIncomingPeerMessageCheckedCalled = true;
    this.message = peerMessage;
    this.valid = valid;

    if (this.exceptionsMap.containsKey("submitIncomingPeerMessageChecked")) {
      switch (this.exceptionsMap.get("submitIncomingPeerMessageChecked")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        case 1:
          throw new AlreadySentTimeoutException("Testing");
        default:
      }
    }
  }

  /**
   * Stores an incoming Peer SK1 signature for later checking. Dummy test stub.
   * 
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param peerMessage
   *          PeerMessage to store
   * @throws ClientMessageExistsException
   * @throws AlreadyReceivedMessageException
   * @throws AlreadySentTimeoutException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitIncomingPeerMessageUnchecked(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType,
   *      uk.ac.surrey.cs.tvs.wbb.messages.PeerMessage)
   */
  @Override
  public void submitIncomingPeerMessageUnchecked(DBRecordType recordType, PeerMessage peerMessage)
      throws ClientMessageExistsException, AlreadyReceivedMessageException, AlreadySentTimeoutException {
    this.submitIncomingPeerMessageUncheckedCalled = true;
    this.message = peerMessage;

    if (this.exceptionsMap.containsKey("submitIncomingPeerMessageUnchecked")) {
      switch (this.exceptionsMap.get("submitIncomingPeerMessageUnchecked")) {
        case 0:
          throw new ClientMessageExistsException("Testing");
        case 1:
          throw new AlreadyReceivedMessageException("Testing");
        case 2:
          throw new AlreadySentTimeoutException("Testing");
        default:
      }
    }
  }

  /**
   * Stores the SK1 cancellation signature generated by this peer. Dummy test stub.
   * 
   * @param id
   *          String serial number to store cancellation against.
   * @param mySig
   *          Base64 String of signature under SK1 of the cancellation
   * @return String of the signature in the database
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitMyCancellationSignature(java.lang.String, java.lang.String)
   */
  @Override
  public String submitMyCancellationSignature(String id, String mySig) throws UnknownDBException {
    this.submitMyCancellationSignatureCalled = true;
    this.signature = mySig;

    if (this.exceptionsMap.containsKey("submitMyCancellationSignature")) {
      switch (this.exceptionsMap.get("submitMyCancellationSignature")) {
        case 0:
          throw new UnknownDBException("Testing");
        default:
      }
    }

    return mySig;
  }

  /**
   * Stores the SK2 cancellation signature generated by the peer. Dummy test stub.
   * 
   * @param id
   *          String of serial number
   * @param mySig
   *          Base64 of the SK2 signature of the cancellation
   * @return Base64 String of mySig or if a signature was already stored in the database the stored signature
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitMyCancellationSignatureSK2(java.lang.String, java.lang.String)
   */
  @Override
  public String submitMyCancellationSignatureSK2(String id, String mySig) throws UnknownDBException {
    this.submitMyCancellationSignatureSK2Called = true;
    this.signature = mySig;

    if (this.exceptionsMap.containsKey("submitMyCancellationSignatureSK2")) {
      switch (this.exceptionsMap.get("submitMyCancellationSignatureSK2")) {
        case 0:
          throw new UnknownDBException("Testing");
        default:
      }
    }

    return mySig;
  }

  /**
   * Records a message we have received following a timeout. Dummy test method.
   * 
   * @param message
   *          JSONWBBMessage to store
   * @throws UnknownDBException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitPostTimeoutMessage(uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage)
   */
  @Override
  public void submitPostTimeoutMessage(JSONWBBMessage message) throws UnknownDBException {
    this.submitPostTimeoutMessageCalled = true;
    this.message = message;

    if (this.exceptionsMap.containsKey("submitPostTimeoutMessage")) {
      switch (this.exceptionsMap.get("submitPostTimeoutMessage")) {
        case 0:
          throw new UnknownDBException("Testing");
        default:
      }
    }
  }

  /**
   * Stores a StartEVM message in the database. Dummy test stub
   * 
   * @param message
   *          StartEVMMessage to store
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#submitStartEVMMessage(uk.ac.surrey.cs.tvs.wbb.messages.StartEVMMessage)
   */
  @Override
  public void submitStartEVMMessage(StartEVMMessage message) throws AlreadyReceivedMessageException, MaxTimeoutExceeded,
      ClientMessageExistsException {
    this.submitStartEVMMessageCalled = true;
    this.message = message;

    if (this.exceptionsMap.containsKey("submitStartEVMMessage")) {
      switch (this.exceptionsMap.get("submitStartEVMMessage")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        case 1:
          throw new MaxTimeoutExceeded("Testing");
        case 2:
          throw new ClientMessageExistsException("Testing");
        default:
      }
    }
  }

  /**
   * Checks if the timeout for this message has been sent. Dummy test stub.
   * 
   * @param type
   *          DBRecordType of the record to check (POD, Cancel, Standard)
   * @param serialNo
   *          of message to check
   * @return boolean true if it has been sent, false if not
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#timeoutSent(uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType, java.lang.String)
   */
  @Override
  public boolean timeoutSent(DBRecordType recordType, String serialNo) {
    this.timeoutSentCalled = true;
    this.recordType = recordType;
    this.serialNo = serialNo;

    return this.timeout;
  }

  /**
   * Updates the commit record with the specified information. Dummy test stub.
   * 
   * @param commitFile
   *          The commit file.
   * @param commitFileFull
   *          Whether the commit file is full.
   * @param commitAttachments
   *          The commit attachments.
   * @param hash
   *          Hash of the data.
   * @see uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB#updateCurrentCommitRecord(java.lang.String, java.lang.String,
   *      java.lang.String, java.lang.String)
   */
  @Override
  public void updateCurrentCommitRecord(String commitFile, String commitFileFull, String commitAttachments, String hash)
      throws JSONException, AlreadyReceivedMessageException {
    this.updateCurrentCommitRecordCalled = true;
    this.commitFile = commitFile;
    this.commitFileFull = commitFileFull;
    this.commitAttachments = commitAttachments;
    this.commitHash = hash;

    if (this.exceptionsMap.containsKey("updateCurrentCommitRecord")) {
      switch (this.exceptionsMap.get("updateCurrentCommitRecord")) {
        case 0:
          throw new AlreadyReceivedMessageException("Testing");
        default:
      }
    }
  }

}
