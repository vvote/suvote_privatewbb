/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.security.MessageDigest;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;

/**
 * The class <code>FileMessageTest</code> contains tests for the class <code>{@link FileMessage}</code>.
 */
public class FileMessageTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    public String getOutputStreamData() {
      return this.outputStream.toString();
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the FileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testFileMessage_1() throws Exception {
    // Test invalid source message.
    FileMessage result = new FileMessage("");
    assertNull(result);
  }

  /**
   * Run the FileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFileMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid source message.
    FileMessage result = new FileMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.FILE_MESSAGE, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the FileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFileMessage_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);

    // Test valid source message with filename and digest.
    FileMessage result = new FileMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.FILE_MESSAGE, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.fileName);
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(digest, result.digest);
  }

  /**
   * Run the FileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testFileMessage_4() throws Exception {
    // Test invalid source message.
    FileMessage result = new FileMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the FileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFileMessage_5() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid source message.
    FileMessage result = new FileMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.FILE_MESSAGE, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the FileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFileMessage_6() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);

    // Test valid source message with filename and digest.
    FileMessage result = new FileMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.FILE_MESSAGE, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.fileName);
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(digest, result.digest);
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);

    FileMessage fixture = new FileMessage(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + digest + TestParameters.SENDER_CERTIFICATE + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);

    FileMessage fixture = new FileMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + digest + TestParameters.SENDER_CERTIFICATE + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);

    FileMessage fixture = new FileMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));

    FileMessage fixture = new FileMessage(object);

    // Test signature validation failure.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));

    FileMessage fixture = new FileMessage(object);

    // Test with valid signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_1() throws Exception {
    // Create the test file with known content for the digest.
    File file = new File(TestParameters.DATA_FILE);
    file.getParentFile().mkdirs();
    String directory = file.getParent();
    String filename = file.getName();

    TestParameters.createTestFile(file);

    // Create the message.
    MessageDigest md = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    md.update(TestParameters.LINE.getBytes());

    String digest = IOUtils.encodeData(EncodingType.BASE64, md.digest());

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, filename);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));

    FileMessage fixture = new FileMessage(object);

    // Test adding the file to the digest with incorrect signature.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), directory);

    assertEquals(digest, fixture.getMsg().getString(MessageFields.FileMessage.INTERNAL_DIGEST));
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_2() throws Exception {
    // Create the test file with known content for the digest.
    File file = new File(TestParameters.DATA_FILE);
    file.getParentFile().mkdirs();
    String directory = file.getParent();
    String filename = file.getName();

    TestParameters.createTestFile(file);

    // Create the message.
    MessageDigest md = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    md.update(TestParameters.LINE.getBytes());

    String digest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, filename);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));

    FileMessage fixture = new FileMessage(object);

    // Test adding the file to the digest with correct signature.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), directory);

    assertEquals(digest, fixture.getMsg().getString(MessageFields.FileMessage.INTERNAL_DIGEST));
    assertEquals(directory, fixture.getMsg().getString(MessageFields.FileMessage.SAFE_UPLOAD_DIR));
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_3() throws Exception {
    // Create the test file with known content for the digest.
    File file = new File(TestParameters.DATA_FILE);
    file.getParentFile().mkdirs();
    String directory = file.getParent();
    String filename = file.getName();

    // Create the message.
    MessageDigest md = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    md.update(TestParameters.LINE.getBytes());

    String digest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, filename);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));

    FileMessage fixture = new FileMessage(object);

    // Test missing file.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), directory);
  }

  /**
   * Run the boolean preProcessMessage(WBBPeer,BoundedBufferedInputStream,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreProcessMessage_1() throws Exception {
    // Create the message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(TestParameters.DATA_FILE));

    FileMessage fixture = new FileMessage(object);

    // Dummy input file.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);

    DummySocket socket = new DummySocket();
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test digest failure.
    boolean result = fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket);
    assertFalse(result);

    assertTrue(socket.isClosed());
    assertTrue(socket.getOutputStreamData().contains(TestParameters.SIGNATURE_CHECK_FAILED_MESSAGE));
  }

  /**
   * Run the boolean preProcessMessage(WBBPeer,BoundedBufferedInputStream,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreProcessMessage_2() throws Exception {
    // Create the test file with known content for the digest.
    File file = new File(TestParameters.DATA_FILE);
    file.getParentFile().mkdirs();
    String filename = file.getName();

    TestParameters.createTestFile(file);

    // Create the message.
    MessageDigest md = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    md.update(TestParameters.LINE.getBytes());

    String digest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, filename);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));

    FileMessage fixture = new FileMessage(object);

    DummySocket socket = new DummySocket();
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test digest failure.
    boolean result = fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket);
    assertTrue(result);
  }

  /**
   * Run the void setFilename(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetFilename_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);

    FileMessage fixture = new FileMessage(object);

    assertEquals(TestParameters.DATA_FILE, fixture.fileName);
    assertEquals(TestParameters.DATA_FILE, fixture.getFilename());

    // Test setting the filename.
    fixture.setFilename(TestParameters.COMMIT_FILE_FULL);

    assertEquals(TestParameters.COMMIT_FILE_FULL, fixture.fileName);
    assertEquals(TestParameters.COMMIT_FILE_FULL, fixture.getFilename());
    assertEquals(TestParameters.COMMIT_FILE_FULL, fixture.getMsg().getString(MessageFields.FileMessage.INTERNAL_FILENAME));
  }

  /**
   * Run the boolean updateDigestAndVerify(String,WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateDigestAndVerify_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(TestParameters.DESC_FIELD, TestParameters.DEVICE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));

    FileMessage fixture = new FileMessage(object);

    // Test with valid signature.
    boolean result = fixture.updateDigestAndVerify(TestParameters.getInstance().getWbbPeer(), digest);
    assertTrue(result);
  }
}