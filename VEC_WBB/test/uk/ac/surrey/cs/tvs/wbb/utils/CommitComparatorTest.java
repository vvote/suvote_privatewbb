/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.messages.AuditMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.CancelMessage;

/**
 * The class <code>CommitComparatorTest</code> contains tests for the class <code>{@link CommitComparator}</code>.
 */
public class CommitComparatorTest {

  /**
   * Run the CommitComparator() constructor test.
   */
  @Test
  public void testCommitComparator_1() throws Exception {
    CommitComparator result = new CommitComparator();
    assertNotNull(result);
  }

  /**
   * Run the int compare(JSONWBBMessage,JSONWBBMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompare_1() throws Exception {
    CommitComparator fixture = new CommitComparator();

    // Create a series of messages of different types and serial numbers.
    AuditMessage a1 = new AuditMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceOne:1\"}"));
    AuditMessage a2 = new AuditMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceOne:2\"}"));
    AuditMessage a3 = new AuditMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceThree:1\"}"));
    AuditMessage a4 = new AuditMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceThree:2\"}"));

    CancelMessage c1 = new CancelMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceOne:1\"}"));
    CancelMessage c2 = new CancelMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceOne:2\"}"));
    CancelMessage c3 = new CancelMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceThree:1\"}"));
    CancelMessage c4 = new CancelMessage(new JSONObject("{\"" + MessageFields.JSONWBBMessage.ID + "\"=\"TestDeviceThree:2\"}"));

    assertEquals(0, fixture.compare(a1, a1));
    assertEquals(0, fixture.compare(a2, a2));
    assertEquals(0, fixture.compare(a3, a3));
    assertEquals(0, fixture.compare(a4, a4));

    assertTrue(fixture.compare(a1, a2) < 0);
    assertTrue(fixture.compare(a3, a4) < 0);
    assertTrue(fixture.compare(a1, a3) < 0);
    assertTrue(fixture.compare(a2, a4) < 0);

    assertTrue(fixture.compare(a2, a1) > 0);
    assertTrue(fixture.compare(a4, a3) > 0);
    assertTrue(fixture.compare(a3, a1) > 0);
    assertTrue(fixture.compare(a4, a2) > 0);

    assertEquals(0, fixture.compare(c1, c1));
    assertEquals(0, fixture.compare(c2, c2));
    assertEquals(0, fixture.compare(c3, c3));
    assertEquals(0, fixture.compare(c4, c4));

    assertTrue(fixture.compare(c1, c2) < 0);
    assertTrue(fixture.compare(c3, c4) < 0);
    assertTrue(fixture.compare(c1, c3) < 0);
    assertTrue(fixture.compare(c2, c4) < 0);

    assertTrue(fixture.compare(c2, c1) > 0);
    assertTrue(fixture.compare(c4, c3) > 0);
    assertTrue(fixture.compare(c3, c1) > 0);
    assertTrue(fixture.compare(c4, c2) > 0);

    assertTrue(fixture.compare(a1, c1) < 0);
    assertTrue(fixture.compare(a2, c2) < 0);
    assertTrue(fixture.compare(a3, c3) < 0);
    assertTrue(fixture.compare(a4, c4) < 0);

    assertTrue(fixture.compare(c1, a1) > 0);
    assertTrue(fixture.compare(c2, a2) > 0);
    assertTrue(fixture.compare(c3, a3) > 0);
    assertTrue(fixture.compare(c4, a4) > 0);
  }
}