/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * The class <code>BallotGenCommitMessageTest</code> contains tests for the class <code>{@link BallotGenCommitMessage}</code>.
 */
public class BallotGenCommitMessageTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the BallotGenCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException.class)
  public void testBallotGenCommitMessage_1() throws Exception {
    // Test invalid source message.
    BallotGenCommitMessage result = new BallotGenCommitMessage("");
    assertNull(result);
  }

  /**
   * Run the BallotGenCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenCommitMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid source message.
    BallotGenCommitMessage result = new BallotGenCommitMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_GEN_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the BallotGenCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenCommitMessage_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    // Test valid source message with filename and digest.
    BallotGenCommitMessage result = new BallotGenCommitMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_GEN_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.fileName);
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(digest, result.digest);
  }

  /**
   * Run the BallotGenCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException.class)
  public void testBallotGenCommitMessage_4() throws Exception {
    // Test invalid source message.
    BallotGenCommitMessage result = new BallotGenCommitMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the BallotGenCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenCommitMessage_5() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid source message.
    BallotGenCommitMessage result = new BallotGenCommitMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_GEN_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the BallotGenCommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenCommitMessage_6() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    // Test valid source message with filename and digest.
    BallotGenCommitMessage result = new BallotGenCommitMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.BALLOT_GEN_COMMIT, result.type);
    assertEquals(99, result.fileSize);
    assertEquals(99, result.getFileSize());
    assertEquals(TestParameters.DATA_FILE, result.fileName);
    assertEquals(TestParameters.DATA_FILE, result.getFilename());
    assertEquals(digest, result.digest);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + digest + TestParameters.SENDER_CERTIFICATE + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.TYPE, MessageTypes.BALLOT_GEN_COMMIT);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Test missing digest for signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.TYPE, MessageTypes.BALLOT_GEN_COMMIT);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Test signature validation failure.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_4() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };
      
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.TYPE, MessageTypes.BALLOT_GEN_COMMIT);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Test with valid signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean preProcessMessage(WBBPeer,BoundedBufferedInputStream,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreProcessMessage_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.DATA_FILE);

    TestParameters.createTestFile(file);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with an empty ZIP file. Just make sure the super method is called.
    DummySocket socket = new DummySocket();

    boolean result = fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket);
    assertFalse(result);
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message not stored because already received.
    db.addException("submitIncomingClientMessage", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(fixture, db.getMessage());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message stored and signatures to check with no output directory.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);

    db.setSigsToCheck(sigsToCheck);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(peerMessage, db.getMessage());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message stored and signatures to check already received with no output directory.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);

    db.setSigsToCheck(sigsToCheck);
    db.addException("submitIncomingPeerMessageChecked", 0);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(peerMessage, db.getMessage());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message stored and signatures to check already received with an output directory which has no files.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);

    db.setSigsToCheck(sigsToCheck);

    // Create the output directory by pre-processing.
    File file = new File(TestParameters.DATA_FILE);
    TestParameters.createTestFile(file);
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with an empty ZIP file. Just make sure the super method is called.
    DummySocket socket = new DummySocket();

    fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(new FileInputStream(file)),
        socket);

    // Test the processing.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(peerMessage, db.getMessage());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message stored and signatures to check already received with an output directory which has one file.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);

    db.setSigsToCheck(sigsToCheck);

    // Create the output directory by pre-processing.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();
    DummySocket socket = new DummySocket();

    fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(new FileInputStream(file)),
        socket);

    // Test the processing.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitCipherCalled());
    assertEquals(new JSONObject(TestParameters.ZIP_CIPHER).toString(), db.getCipher().toString());

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(peerMessage, db.getMessage());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message stored and signatures to check already received with an output directory which has one file with a ciphe which has
    // already been received.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);

    db.setSigsToCheck(sigsToCheck);
    db.addException("submitCipher", 0);

    // Create the output directory by pre-processing.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();
    DummySocket socket = new DummySocket();

    fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(new FileInputStream(file)),
        socket);

    // Test the processing.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitCipherCalled());
    assertEquals(new JSONObject(TestParameters.ZIP_CIPHER).toString(), db.getCipher().toString());

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(peerMessage, db.getMessage());
    assertNotNull(db.getSignature());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String digest = TestParameters.signData(TestParameters.SERIAL_NO);
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DEVICE, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);
    object.put(MessageFields.FileMessage.DIGEST, digest);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.MixRandomCommit.PRINTER_ID, TestParameters.DEVICE);

    BallotGenCommitMessage fixture = new BallotGenCommitMessage(object);

    // Message stored and signatures to check already received with an output directory which has one file no threshold.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);

    db.setSigsToCheck(sigsToCheck);
    db.setReachedConsensus(true);

    // Create the output directory by pre-processing.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();
    DummySocket socket = new DummySocket();

    fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(new FileInputStream(file)),
        socket);

    // Test the processing.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isGetClientMessageCalled());

    assertTrue(db.isSubmitCipherCalled());
    assertEquals(new JSONObject(TestParameters.ZIP_CIPHER).toString(), db.getCipher().toString());

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(peerMessage, db.getMessage());
    assertNotNull(db.getSignature());
  }
}