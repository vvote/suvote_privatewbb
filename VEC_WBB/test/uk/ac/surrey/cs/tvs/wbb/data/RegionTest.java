/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/**
 * The class <code>RegionTest</code> contains tests for the class <code>{@link Region}</code>.
 */
public class RegionTest {

  /**
   * Run the Region(JSONObject,HashMap<String,District>,JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRegion_1() throws Exception {
    JSONArray candidates = new JSONArray("[{\"name\":\"Bob\"},{\"name\":\"Sid\"}]");

    JSONArray parties = new JSONArray();

    JSONObject party1 = new JSONObject();
    party1.put("preferredName", "party1");
    party1.put("hasBallotBox", true);
    party1.put("ungrouped", true);
    party1.put("candidates", candidates);
    parties.put(party1);

    JSONObject party2 = new JSONObject();
    party2.put("preferredName", "party2");
    party2.put("hasBallotBox", false);
    party2.put("ungrouped", false);
    party2.put("candidates", candidates);
    parties.put(party2);

    JSONArray districts = new JSONArray();

    JSONObject district1 = new JSONObject();
    district1.put("district", "district1");
    district1.put("candidates", candidates);
    districts.put(district1);

    JSONObject district2 = new JSONObject();
    district2.put("district", "district2");
    district2.put("candidates", candidates);
    districts.put(district2);

    JSONObject region = new JSONObject();
    region.put("region", "region1");
    region.put("parties", parties);
    region.put("districts", districts);

    JSONObject candidateCount = new JSONObject();
    candidateCount.put("la", candidates.length());
    candidateCount.put("lc_atl", candidates.length());
    candidateCount.put("lc_btl", candidates.length() + candidates.length());

    JSONObject districtConf = new JSONObject();
    districtConf.put("district1", candidateCount);
    districtConf.put("district2", candidateCount);

    HashMap<String, District> districtMap = new HashMap<String, District>();
    Region result = new Region(region, districtMap, districtConf);
    assertNotNull(result);

    assertEquals("Region: " + result.getName() + " Grouped Parties: [" + new Party(party1, null) + ", " + new Party(party2, null)
        + "] Ungrouped Parties: [] Districts: {" + district1.getString("district") + "="
        + new District(district1, null, districtConf) + ", " + district2.getString("district") + "="
        + new District(district2, null, districtConf) + "}", result.toString());
    assertEquals("region1", result.getName());

    assertTrue(result.validateConsistency());
  }

  /**
   * Run the boolean validateConsistency() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateConsistency_1() throws Exception {
    JSONArray candidates = new JSONArray("[{\"name\":\"Bob\"},{\"name\":\"Sid\"}]");

    JSONArray parties = new JSONArray();

    JSONObject party1 = new JSONObject();
    party1.put("preferredName", "party1");
    party1.put("hasBallotBox", true);
    party1.put("ungrouped", true);
    party1.put("candidates", candidates);
    parties.put(party1);

    JSONObject party2 = new JSONObject();
    party2.put("preferredName", "party2");
    party2.put("hasBallotBox", false);
    party2.put("ungrouped", false);
    party2.put("candidates", candidates);
    parties.put(party2);

    JSONArray districts = new JSONArray();

    JSONObject district1 = new JSONObject();
    district1.put("district", "district1");
    district1.put("candidates", candidates);
    districts.put(district1);

    JSONObject district2 = new JSONObject();
    district2.put("district", "district2");
    district2.put("candidates", candidates);
    districts.put(district2);

    JSONObject region = new JSONObject();
    region.put("region", "region1");
    region.put("parties", parties);
    region.put("districts", districts);

    JSONObject candidateCount = new JSONObject();
    candidateCount.put("la", candidates.length());
    candidateCount.put("lc_atl", candidates.length());
    candidateCount.put("lc_btl", candidates.length()); // Wrong.

    JSONObject districtConf = new JSONObject();
    districtConf.put("district1", candidateCount);
    districtConf.put("district2", candidateCount);

    HashMap<String, District> districtMap = new HashMap<String, District>();
    Region fixture = new Region(region, districtMap, districtConf);

    boolean result = fixture.validateConsistency();
    assertEquals(false, result);
  }
}