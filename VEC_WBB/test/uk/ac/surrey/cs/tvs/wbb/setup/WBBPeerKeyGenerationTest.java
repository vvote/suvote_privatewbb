/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.setup;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.mongodb.ConcurrentDB;

/**
 * The class <code>ConcurrentDBTest</code> contains tests for the class <code>{@link ConcurrentDB}</code>.
 */
public class WBBPeerKeyGenerationTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void generateKeyAndCSR(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateKeyAndCSR_1() throws Exception {
    WBBPeerKeyGeneration fixture = new WBBPeerKeyGeneration(TestParameters.PEERS[0]);

    fixture.generateKeyAndCSR(TestParameters.CSR_FOLDER, TestParameters.OUTPUT_KEYSTORE);

    // Test certificate creation. We don't test the content.
    File sk1File = new File(TestParameters.CSR_FOLDER, TestParameters.PEERS[0] + "_SigningSK1.csr");
    //File sk2File = new File(TestParameters.CSR_FOLDER, TestParameters.PEERS[0] + "_SigningSK2.csr");
    File sslFile = new File(TestParameters.CSR_FOLDER, TestParameters.PEERS[0] + "_SSL.csr");

    assertTrue(sk1File.exists());
    //assertTrue(sk2File.exists());
    assertTrue(sslFile.exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    String[] args = new String[] {};

    WBBPeerKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    String[] args = new String[] { "rubbish" };

    WBBPeerKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    String[] args = new String[] { "generate", TestParameters.PEERS[0], TestParameters.CSR_FOLDER, TestParameters.OUTPUT_KEYSTORE };

    WBBPeerKeyGeneration.main(args);

    // Test certificate creation. We don't test the content.
    File sk1File = new File(TestParameters.CSR_FOLDER, TestParameters.PEERS[0] + "_SigningSK1.csr");
    //File sk2File = new File(TestParameters.CSR_FOLDER, TestParameters.PEERS[0] + "_SigningSK2.csr");
    File sslFile = new File(TestParameters.CSR_FOLDER, TestParameters.PEERS[0] + "_SSL.csr");

    assertTrue(sk1File.exists());
    //assertTrue(sk2File.exists());
    assertTrue(sslFile.exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_4() throws Exception {
    String[] args = new String[] { "import", TestParameters.PEERS[0], TestParameters.OUTPUT_KEYSTORE, TestParameters.CSR_FOLDER,
        "rubbish" };

    // Just test that something happens - the actual import is tested elsewhere.
    WBBPeerKeyGeneration.main(args);
  }

  /**
   * Run the WBBPeerKeyGeneration(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testWBBPeerKeyGeneration_1() throws Exception {
    WBBPeerKeyGeneration result = new WBBPeerKeyGeneration(TestParameters.PEERS[0]);
    assertNotNull(result);
  }
}