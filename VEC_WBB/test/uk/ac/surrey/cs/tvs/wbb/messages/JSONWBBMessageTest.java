/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.VoteMessage;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.DummyJSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.CommitProcessingException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldsCancel;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldsPOD;
import uk.ac.surrey.cs.tvs.wbb.validation.JSONSchema;

/**
 * The class <code>JSONWBBMessageTest</code> contains tests for the class <code>{@link JSONWBBMessage}</code>.
 */
public class JSONWBBMessageTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    public String getOutputStreamData() {
      return this.outputStream.toString();
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void addAdditionalContentToRespone(WBBPeer,JSONObject) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddAdditionalContentToRespone_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test adding in the commit time.
    JSONObject response = new JSONObject();
    assertFalse(response.has(MessageFields.JSONWBBMessage.COMMIT_TIME));

    fixture.addAdditionalContentToResponse(response);
    assertTrue(response.has(MessageFields.JSONWBBMessage.COMMIT_TIME));
    assertEquals(TestParameters.COMMIT_TIME, response.getString(MessageFields.JSONWBBMessage.COMMIT_TIME));
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    // Test that the signature was valid and that the call to the database was made.
    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isValid());
    assertEquals(peerMessage.toString(), db.getMessage().toString());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with an invalid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData("rubbish"));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    // Test that the signature was invalid and that the call to the database was made.
    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertFalse(db.isValid());
    assertEquals(peerMessage.toString(), db.getMessage().toString());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with a missing signature encoding (but the field is there).
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, "rubbish");
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    // Test that the call to the database was not made.
    assertFalse(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertFalse(db.isValid());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyReceivedMessageException.class)
  public void testCheckAndStoreMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    // Test throwing an AlreadyReceivedMessageException.
    db.addException("submitIncomingPeerMessageChecked", 0);
    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    // Test throwing an AlreadySentTimeoutException.
    db.addException("submitIncomingPeerMessageChecked", 1);
    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);
    assertTrue(db.isSubmitPostTimeoutMessageCalled());
    assertEquals(peerMessage.toString(), db.getMessage().toString());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    // Test throwing an AlreadySentTimeoutException and an UnknownDBException.
    db.addException("submitIncomingPeerMessageChecked", 1);
    db.addException("submitPostTimeoutMessage", 0);
    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);
    assertTrue(db.isSubmitPostTimeoutMessageCalled());
    assertEquals(peerMessage.toString(), db.getMessage().toString());
  }

  /**
   * Run the boolean checkPeerMessageSignature(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPeerMessageSignature_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    boolean result = fixture.checkPeerMessageSignature(TestParameters.getInstance().getWbbPeer(), peerMessage);
    assertTrue(result);
  }

  /**
   * Run the boolean checkPeerMessageSignature(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPeerMessageSignature_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    // Test a PeerMessage with an invalid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData("rubbish"));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    boolean result = fixture.checkPeerMessageSignature(TestParameters.getInstance().getWbbPeer(), peerMessage);
    assertFalse(result);
  }

  /**
   * Run the void constructResponseAndSend(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructResponseAndSend_1() throws Exception {
    // Test all message types.
    for (Message type : Message.values()) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      fixture.constructResponseAndSend(TestParameters.getInstance().getWbbPeer());
      assertTrue(fixture.isGetExternalSignableContentCalled());

      JSONObject result = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

      String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();
      assertEquals(TestParameters.SESSION_ID.toString(), sessionID);

      assertTrue(result.has(MessageFields.JSONWBBMessage.ID));
      assertTrue(result.has(MessageFields.JSONWBBMessage.TYPE));
      assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_ID));
      assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_SIG));

      assertEquals(fixture.getID(), result.getString(MessageFields.JSONWBBMessage.ID));
      assertEquals(TestParameters.getInstance().getWbbPeer().getID(), result.getString(MessageFields.JSONWBBMessage.PEER_ID));
      assertNotNull(result.getString(MessageFields.JSONWBBMessage.PEER_SIG)); // TODO verify signature?

      String expected = null;

      switch (type) {
        case AUDIT_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING;
          break;
        case BALLOT_AUDIT_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_AUDIT_COMMIT_STRING;
          break;
        case BALLOT_GEN_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_GEN_COMMIT_STRING;
          break;
        case CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING;
          break;
        case COMMIT_R1:
          expected = TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING;
          break;
        case FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING;
          break;
        case GENERIC:
          expected = TestParameters.MESSAGE_TYPE_GENERIC_STRING;
          break;
        case MIX_RANDOM_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING;
          break;
        case PEER_CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_CANCEL_MESSAGE_STRING;
          break;
        case PEER_FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING;
          break;
        case PEER_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING;
          break;
        case PEER_POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING;
          break;
        case POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING;
          break;
        case VOTE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING;
          break;
        case START_EVM:
          expected = TestParameters.MESSAGE_TYPE_START_EVM_STRING;
          break;
        case HEARTBEAT:
          expected = TestParameters.MESSAGE_TYPE_HEARTBEAT_STRING;
          break;
        default:
          expected = TestParameters.MESSAGE_TYPE_DEFAULT_STRING;
          break;
      }

      assertEquals(expected, result.getString(MessageFields.JSONWBBMessage.TYPE));
    }
  }

  /**
   * Run the JSONObject constructResponseAndSign(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructResponseAndSign_1() throws Exception {
    // Test all message types.
    for (Message type : Message.values()) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      JSONObject result = fixture.constructResponseAndSign(TestParameters.getInstance().getWbbPeer());
      assertNotNull(result);
      assertTrue(fixture.isGetExternalSignableContentCalled());

      assertTrue(result.has(MessageFields.JSONWBBMessage.ID));
      assertTrue(result.has(MessageFields.JSONWBBMessage.TYPE));
      assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_ID));
      assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_SIG));

      assertEquals(fixture.getID(), result.getString(MessageFields.JSONWBBMessage.ID));
      assertEquals(TestParameters.getInstance().getWbbPeer().getID(), result.getString(MessageFields.JSONWBBMessage.PEER_ID));
      assertNotNull(result.getString(MessageFields.JSONWBBMessage.PEER_SIG)); // TODO verify signature?

      String expected = null;

      switch (type) {
        case AUDIT_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING;
          break;
        case BALLOT_AUDIT_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_AUDIT_COMMIT_STRING;
          break;
        case BALLOT_GEN_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_GEN_COMMIT_STRING;
          break;
        case CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING;
          break;
        case COMMIT_R1:
          expected = TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING;
          break;
        case FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING;
          break;
        case GENERIC:
          expected = TestParameters.MESSAGE_TYPE_GENERIC_STRING;
          break;
        case MIX_RANDOM_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING;
          break;
        case PEER_CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_CANCEL_MESSAGE_STRING;
          break;
        case PEER_FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING;
          break;
        case PEER_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING;
          break;
        case PEER_POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING;
          break;
        case POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING;
          break;
        case VOTE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING;
          break;
        case START_EVM:
          expected = TestParameters.MESSAGE_TYPE_START_EVM_STRING;
          break;
        case HEARTBEAT:
          expected = TestParameters.MESSAGE_TYPE_HEARTBEAT_STRING;
          break;
        default:
          expected = TestParameters.MESSAGE_TYPE_DEFAULT_STRING;
          break;
      }

      assertEquals(expected, result.getString(MessageFields.JSONWBBMessage.TYPE));
    }
  }

  /**
   * Run the JSONObject constructResponseWithoutSignature(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructResponseWithoutSignature_1() throws Exception {
    // Test all message types.
    for (Message type : Message.values()) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      JSONObject result = fixture.constructResponseWithoutSignature(TestParameters.getInstance().getWbbPeer());
      assertNotNull(result);

      assertTrue(result.has(MessageFields.JSONWBBMessage.ID));
      assertTrue(result.has(MessageFields.JSONWBBMessage.TYPE));
      assertTrue(result.has(MessageFields.JSONWBBMessage.PEER_ID));

      assertEquals(fixture.getID(), result.getString(MessageFields.JSONWBBMessage.ID));
      assertEquals(TestParameters.getInstance().getWbbPeer().getID(), result.getString(MessageFields.JSONWBBMessage.PEER_ID));

      String expected = null;

      switch (type) {
        case AUDIT_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING;
          break;
        case BALLOT_AUDIT_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_AUDIT_COMMIT_STRING;
          break;
        case BALLOT_GEN_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_GEN_COMMIT_STRING;
          break;
        case CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING;
          break;
        case COMMIT_R1:
          expected = TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING;
          break;
        case FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING;
          break;
        case GENERIC:
          expected = TestParameters.MESSAGE_TYPE_GENERIC_STRING;
          break;
        case MIX_RANDOM_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING;
          break;
        case PEER_CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_CANCEL_MESSAGE_STRING;
          break;
        case PEER_FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING;
          break;
        case PEER_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING;
          break;
        case PEER_POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING;
          break;
        case POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING;
          break;
        case VOTE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING;
          break;
        case START_EVM:
          expected = TestParameters.MESSAGE_TYPE_START_EVM_STRING;
          break;
        case HEARTBEAT:
          expected = TestParameters.MESSAGE_TYPE_HEARTBEAT_STRING;
          break;
        default:
          expected = TestParameters.MESSAGE_TYPE_DEFAULT_STRING;
          break;
      }

      assertEquals(expected, result.getString(MessageFields.JSONWBBMessage.TYPE));
    }
  }

  /**
   * Run the String createInternalSignature(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateInternalSignature_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    String result = fixture.createInternalSignature(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
    assertTrue(fixture.isGetInternalSignableContentCalled());

    // TODO verify signature?
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTime_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage("{}");

    String result = fixture.getCommitTime();
    assertNotNull(result);
    assertEquals("", result);
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTime_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);

    String result = fixture.getCommitTime();
    assertNotNull(result);
    assertEquals(TestParameters.COMMIT_TIME, result);
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTime_3() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage("{}");
    fixture.msg = message;

    String result = fixture.getCommitTime();
    assertNotNull(result);
    assertEquals(TestParameters.COMMIT_TIME, result);
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCommitTime_4() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, -1);

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage("{}");
    fixture.msg = message;

    String result = fixture.getCommitTime();
    assertNull(result);
  }

  /**
   * Run the String getPeerTypeFromClientType() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetPeerTypeFromClientType_1() throws Exception {
    // Test all message types.
    for (Message type : Message.values()) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      String result = fixture.getPeerTypeFromClientType();
      assertNotNull(result);

      String expected = null;

      switch (type) {
        case CANCEL_MESSAGE:
          expected = TestParameters.CLIENT_PEER_CANCEL_STRING;
          break;
        case POD_MESSAGE:
          expected = TestParameters.CLIENT_PEER_POD_STRING;
          break;
        default:
          expected = TestParameters.CLIENT_PEER_STRING;
          break;
      }

      assertEquals(expected, result);
    }
  }

  /**
   * Run the Message getType() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetType_1() throws Exception {
    // Test all message types.
    for (Message type : Message.values()) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      Message result = fixture.getType();
      assertNotNull(result);
      assertEquals(type, result);
    }
  }

  /**
   * Run the String getTypeString() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetTypeString_1() throws Exception {
    // Test all message types.
    for (Message type : Message.values()) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      String result = fixture.getTypeString();
      assertNotNull(result);

      String expected = null;

      switch (type) {
        case AUDIT_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING;
          break;
        case BALLOT_AUDIT_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_AUDIT_COMMIT_STRING;
          break;
        case BALLOT_GEN_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_BALLOT_GEN_COMMIT_STRING;
          break;
        case CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_CANCEL_MESSAGE_STRING;
          break;
        case COMMIT_R1:
          expected = TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING;
          break;
        case FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING;
          break;
        case GENERIC:
          expected = TestParameters.MESSAGE_TYPE_GENERIC_STRING;
          break;
        case MIX_RANDOM_COMMIT:
          expected = TestParameters.MESSAGE_TYPE_MIX_RANDOM_COMMIT_STRING;
          break;
        case PEER_CANCEL_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_CANCEL_MESSAGE_STRING;
          break;
        case PEER_FILE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_FILE_MESSAGE_STRING;
          break;
        case PEER_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING;
          break;
        case PEER_POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING;
          break;
        case POD_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING;
          break;
        case VOTE_MESSAGE:
          expected = TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING;
          break;
        case START_EVM:
          expected = TestParameters.MESSAGE_TYPE_START_EVM_STRING;
          break;
        case HEARTBEAT:
          expected = TestParameters.MESSAGE_TYPE_HEARTBEAT_STRING;
          break;
        default:
          expected = TestParameters.MESSAGE_TYPE_DEFAULT_STRING;
          break;
      }

      assertEquals(expected, result);
    }
  }

  /**
   * Run the void JSONWBBMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONWBBMessage_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    DummyJSONWBBMessage result = new DummyJSONWBBMessage(message);
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
    assertEquals(message.toString(), result.getMsg().toString());
  }

  /**
   * Run the void JSONWBBMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONWBBMessage_2() throws Exception {
    DummyJSONWBBMessage result = new DummyJSONWBBMessage(new JSONObject());
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
  }

  /**
   * Run the void JSONWBBMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testJSONWBBMessage_3() throws Exception {
    JSONObject message = new JSONObject();
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, -1);

    DummyJSONWBBMessage result = new DummyJSONWBBMessage(message);
    assertNull(result);
  }

  /**
   * Run the void JSONWBBMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONWBBMessage_4() throws Exception {
    DummyJSONWBBMessage result = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
  }

  /**
   * Run the void JSONWBBMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONWBBMessage_5() throws Exception {
    DummyJSONWBBMessage result = new DummyJSONWBBMessage("{}");
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
  }

  /**
   * Run the void JSONWBBMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testJSONWBBMessage_6() throws Exception {
    JSONObject message = new JSONObject();
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, -1);

    DummyJSONWBBMessage result = new DummyJSONWBBMessage(message.toString());
    assertNull(result);
  }

  /**
   * Run the void JSONWBBMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONWBBMessage_7() throws Exception {
    JSONObject message = new JSONObject();
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    DummyJSONWBBMessage result = new DummyJSONWBBMessage(message.toString());
    assertNotNull(result);
  }

  /**
   * Run the List<JSONWBBMessage> loadCommitMessages(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testLoadCommitMessages_1() throws Exception {
    List<JSONWBBMessage> result = JSONWBBMessage.loadCommitMessages(TestParameters.getInstance().getWbbPeer(), new JSONObject());
    assertNotNull(result);

    assertEquals(0, result.size());
  }

  /**
   * Run the List<JSONWBBMessage> loadCommitMessages(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testLoadCommitMessages_2() throws Exception {
    // Create commit messages of all types.
    JSONArray messages = new JSONArray();

    for (Message type : Message.values()) {
      // Ignore generic messages.
      if (!type.equals(Message.GENERIC)) {
        DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
        fixture.type = type;

        // Cover all message fields.
        JSONObject message = fixture.getMsg();
        message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
        message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
        message.put(MessageFields.FileMessage.FILESIZE, 0l);
        message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

        messages.put(message.toString());
      }
    }

    JSONObject record = new JSONObject();
    record.put(DBFieldsPOD.getInstance().getField(DBFieldName.CLIENT_MESSAGE), messages.get(0));
    record.put(DBFields.getInstance().getField(DBFieldName.CLIENT_MESSAGE), messages.get(2));
    record.put(DBFieldsCancel.getInstance().getField(DBFieldName.CLIENT_MESSAGE), messages);

    List<JSONWBBMessage> result = JSONWBBMessage.loadCommitMessages(TestParameters.getInstance().getWbbPeer(), record);
    assertNotNull(result);
    assertEquals(2, result.size()); // No cancel messages should be included because they fail validation.

    assertEquals(new JSONObject(messages.getString(0)).toString(), result.get(0).getMsg().toString());
    assertEquals(new JSONObject(messages.getString(2)).toString(), result.get(1).getMsg().toString());
  }

  /**
   * Run the List<JSONWBBMessage> loadCommitMessages(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testLoadCommitMessages_3() throws Exception {
    List<JSONWBBMessage> result = JSONWBBMessage.loadCommitMessages(TestParameters.getInstance().getWbbPeer(), new JSONObject());
    assertNotNull(result);

    assertEquals(0, result.size());
  }

  /**
   * Run the List<JSONWBBMessage> loadCommitMessages(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testLoadCommitMessages_4() throws Exception {
    // Create commit messages of all types.
    JSONArray messages = new JSONArray();

    for (Message type : Message.values()) {
      // Ignore generic messages.
      if (!type.equals(Message.GENERIC)) {
        DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
        fixture.type = type;

        // Cover all message fields.
        JSONObject message = fixture.getMsg();
        message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
        message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
        message.put(MessageFields.FileMessage.FILESIZE, 0l);
        message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

        messages.put(message.toString());
      }
    }

    JSONObject record = new JSONObject();
    record.put(DBFieldsPOD.getInstance().getField(DBFieldName.CLIENT_MESSAGE), messages.get(0));
    record.put(DBFields.getInstance().getField(DBFieldName.CLIENT_MESSAGE), messages.get(2));
    record.put(DBFieldsCancel.getInstance().getField(DBFieldName.CLIENT_MESSAGE), messages);

    List<JSONWBBMessage> result = JSONWBBMessage.loadCommitMessages(TestParameters.getInstance().getWbbPeer(), record);
    assertNotNull(result);
    assertEquals(2, result.size()); // No cancel messages should be included because they fail validation.

    assertEquals(new JSONObject(messages.getString(0)).toString(), result.get(0).getMsg().toString());
    assertEquals(new JSONObject(messages.getString(2)).toString(), result.get(1).getMsg().toString());
  }

  /**
   * Run the JSONWBBMessage parseMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testParseMessage_1() throws Exception {
    // Attempt to parse messages of all types. We do not create valid messages here as these are tested with the message class
    // implementations.
    for (Message type : Message.values()) {
      // Ignore generic messages.
      if (!type.equals(Message.GENERIC)) {
        DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
        fixture.type = type;

        // Cover all message fields.
        JSONObject message = fixture.getMsg();
        message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
        message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
        message.put(MessageFields.FileMessage.FILESIZE, 0l);
        message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

        JSONWBBMessage result = JSONWBBMessage.parseMessage(message);
        assertNotNull(result);
      }
    }
  }

  /**
   * Run the JSONWBBMessage parseMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParseMessage_2() throws Exception {
    // Try a generic message.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.GENERIC;

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    JSONWBBMessage result = JSONWBBMessage.parseMessage(message);
    assertNotNull(result);
  }

  /**
   * Run the JSONWBBMessage parseMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParseMessage_3() throws Exception {
    // Test no type.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.GENERIC;

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    JSONWBBMessage result = JSONWBBMessage.parseMessage(message);
    assertNull(result);
  }

  /**
   * Run the JSONWBBMessage parseMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testParseMessage_4() throws Exception {
    // Attempt to parse messages of all types. We do not create valid messages here as these are tested with the message class
    // implementations.
    for (Message type : Message.values()) {
      // Ignore generic messages.
      if (!type.equals(Message.GENERIC)) {
        DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
        fixture.type = type;

        // Cover all message fields.
        JSONObject message = fixture.getMsg();
        message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
        message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
        message.put(MessageFields.FileMessage.FILESIZE, 0l);
        message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

        JSONWBBMessage result = JSONWBBMessage.parseMessage(message.toString());
        assertNotNull(result);
      }
    }
  }

  /**
   * Run the JSONWBBMessage parseMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParseMessage_5() throws Exception {
    // Try a generic message.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.GENERIC;

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    JSONWBBMessage result = JSONWBBMessage.parseMessage(message.toString());
    assertNotNull(result);
  }

  /**
   * Run the JSONWBBMessage parseMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParseMessage_6() throws Exception {
    // Test no type.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.GENERIC;

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    JSONWBBMessage result = JSONWBBMessage.parseMessage(message.toString());
    assertNull(result);
  }

  /**
   * Run the PeerMessage parsePeerMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testParsePeerMessage_1() throws Exception {
    // Attempt to parse messages of all peer types. We do not create valid messages here as these are tested with the message class
    // implementations.
    for (Message type : new Message[] { Message.PEER_MESSAGE, Message.PEER_CANCEL_MESSAGE, Message.PEER_POD_MESSAGE,
        Message.COMMIT_R1, Message.PEER_FILE_MESSAGE }) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      // Cover all message fields.
      JSONObject message = fixture.getMsg();
      message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
      message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
      message.put(MessageFields.FileMessage.FILESIZE, 0l);
      message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

      PeerMessage result = JSONWBBMessage.parsePeerMessage(message);
      assertNotNull(result);
    }
  }

  /**
   * Run the PeerMessage parsePeerMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParsePeerMessage_2() throws Exception {
    // Test an unknown message.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.GENERIC;

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    PeerMessage result = JSONWBBMessage.parsePeerMessage(message);
    assertNotNull(result);
  }

  /**
   * Run the PeerMessage parsePeerMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParsePeerMessage_3() throws Exception {
    // Test no type.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    PeerMessage result = JSONWBBMessage.parsePeerMessage(message);
    assertNotNull(result);
  }

  /**
   * Run the PeerMessage parsePeerMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testParsePeerMessage_4() throws Exception {
    // Attempt to parse messages of all peer types. We do not create valid messages here as these are tested with the message class
    // implementations.
    for (Message type : new Message[] { Message.PEER_MESSAGE, Message.PEER_CANCEL_MESSAGE, Message.PEER_POD_MESSAGE,
        Message.COMMIT_R1, Message.PEER_FILE_MESSAGE }) {
      DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
      fixture.type = type;

      // Cover all message fields.
      JSONObject message = fixture.getMsg();
      message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
      message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
      message.put(MessageFields.FileMessage.FILESIZE, 0l);
      message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

      PeerMessage result = JSONWBBMessage.parsePeerMessage(message.toString());
      assertNotNull(result);
    }
  }

  /**
   * Run the PeerMessage parsePeerMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParsePeerMessage_5() throws Exception {
    // Test an unknown message.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.GENERIC;

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    PeerMessage result = JSONWBBMessage.parsePeerMessage(message.toString());
    assertNotNull(result);
  }

  /**
   * Run the PeerMessage parsePeerMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testParsePeerMessage_6() throws Exception {
    // Test no type.
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    // Cover all message fields.
    JSONObject message = fixture.getMsg();
    message.put(MessageFields.FileMessage.ID, TestParameters.SESSION_ID.toString());
    message.put(MessageFields.FileMessage.FILESIZE, 0l);
    message.put(MessageFields.Commit.COMMIT_ID, TestParameters.SESSION_ID.toString());

    PeerMessage result = JSONWBBMessage.parsePeerMessage(message.toString());
    assertNotNull(result);
  }

  /**
   * Run the boolean preProcessMessage(WBBPeer,BoundedBufferedInputStream,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreProcessMessage_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    boolean result = fixture.preProcessMessage(TestParameters.getInstance().getWbbPeer(), null, null);
    assertTrue(result);
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = CommitProcessingException.class)
  public void testProcessAsPartOfCommitR2_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), null, null, null);
  }

  /**
   * Run the void sendMessageAndClose(String,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessageAndClose_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    DummySocket socket = new DummySocket();
    assertFalse(socket.isClosed());

    fixture.sendMessageAndClose(TestParameters.MESSAGE, socket);
    assertTrue(socket.isClosed());
    assertEquals(TestParameters.MESSAGE + System.getProperty("line.separator"), socket.getOutputStreamData());
  }

  /**
   * Run the void setCommitTime(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetCommitTime_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage("{}");

    String result = fixture.getCommitTime();
    assertNotNull(result);
    assertEquals("", result);

    fixture.setCommitTime(TestParameters.COMMIT_TIME);
    result = fixture.getCommitTime();
    assertNotNull(result);
    assertEquals(TestParameters.COMMIT_TIME, result);
  }

  /**
   * Run the String toString() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testToString_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);

    String result = fixture.toString();
    assertNotNull(result);

    // Update the expected message's ID, which is dynamically generated.
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.ID, new JSONObject(result).getString(MessageFields.JSONWBBMessage.ID));

    assertEquals(message.toString(), result);
  }

  /**
   * Run the String toString() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testToString_2() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.msg = null;

    String result = fixture.toString();
    assertNotNull(result);
    assertTrue(result.startsWith(DummyJSONWBBMessage.class.getName()));
  }

  /**
   * Run the boolean validateSchema(JsonSchema) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateSchema_1() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.FILE_MESSAGE;

    JSONObject message = fixture.getMsg();
    message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());

    message.put(MessageFields.FileMessage.ID, "");
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(fixture.getTypeString()));
    message.put(MessageFields.FileMessage.SENDER_ID, fixture.getTypeString());
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message.put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(fixture.getTypeString()));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    // Test just one schema. Message content and schemas are tested with the implementing message classes.
    boolean result = fixture
        .validateSchema(TestParameters.getInstance().getWbbPeer().getJSONValidator().getSchema(JSONSchema.FILE));
    assertTrue(result);
  }

  /**
   * Run the boolean validateSchema(JsonSchema) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateSchema_2() throws Exception {
    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(TestParameters.MESSAGE);
    fixture.type = Message.FILE_MESSAGE;

    JSONObject message = fixture.getMsg();
    message.put(MessageFields.JSONWBBMessage.TYPE, fixture.getTypeString());

    // Test an invalid message.
    boolean result = fixture
        .validateSchema(TestParameters.getInstance().getWbbPeer().getJSONValidator().getSchema(JSONSchema.FILE));
    assertFalse(result);
  }

  /**
   * Run the boolean validateSerialSignature(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateSerialSignature_1() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };
    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);
    fixture.getMsg().put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO); // Needs resetting.

    boolean result = fixture.validateSerialSignature(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);
  }

  /**
   * Run the boolean validateSerialSignature(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateSerialSignature_2() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, "rubbish" };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };
    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    DummyJSONWBBMessage fixture = new DummyJSONWBBMessage(message);
    fixture.getMsg().put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO); // Needs resetting.

    boolean result = fixture.validateSerialSignature(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);
  }
}