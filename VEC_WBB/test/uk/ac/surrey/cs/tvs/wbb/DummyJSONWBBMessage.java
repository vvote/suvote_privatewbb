/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.UnknownDBException;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;

/**
 * This class defines a dummy JSONWBBMessage.
 */
public class DummyJSONWBBMessage extends JSONWBBMessage {

  /** Unique count of messages to give each message a unique ID. */
  private static int           uniqueId                         = 0;

  /** Map of methods to exceptions that should be raised. */
  private Map<String, Integer> exceptionsMap                    = new HashMap<String, Integer>();

  /** Was the message successfully processed? */
  private boolean              processMessage                   = false;

  /** Has the getExternalSignableContent method been called? */
  private boolean              getExternalSignableContentCalled = false;

  /** Has the getInternalSignableContent method been called? */
  private boolean              getInternalSignableContentCalled = false;

  /** Has the processMessage method been called? */
  private boolean              processMessageCalled             = false;

  /**
   * Default constructor allowing a test message to be created.
   * 
   * @throws MessageJSONException
   */
  public DummyJSONWBBMessage() throws MessageJSONException {
    this(TestParameters.MESSAGE);
  }

  /**
   * Constructor requiring the source message.
   * 
   * @param msg
   *          JSON message.
   * @throws MessageJSONException
   */
  public DummyJSONWBBMessage(JSONObject msg) throws MessageJSONException {
    super(msg);

    this.id = TestParameters.SERIAL_NO + String.valueOf(uniqueId);
    uniqueId++;

    try {
      this.msg.put(MessageFields.JSONWBBMessage.ID, this.id);
    }
    catch (JSONException e) {
    }
  }

  /**
   * Constructor requiring the source message as a String.
   * 
   * @param msgString
   *          String message.
   * @throws MessageJSONException
   */
  public DummyJSONWBBMessage(String msgString) throws MessageJSONException {
    super(msgString);

    this.id = TestParameters.SERIAL_NO + String.valueOf(uniqueId);
    uniqueId++;

    try {
      this.msg.put(MessageFields.JSONWBBMessage.ID, this.id);
    }
    catch (JSONException e) {
    }
  }

  /**
   * Adds an exception to be raised for a method.
   * 
   * @param method
   *          The method to raise the exception.
   * @param exception
   *          The index to the exception.
   */
  public void addException(String method, int exception) {
    this.exceptionsMap.put(method, exception);
  }

  /**
   * Checks whether we have reached a consensus on a message and sends the response. Dummy test stub.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message sent. Consensus may not have been reached in which case an error response was sent.
   * @throws MessageJSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws JSONException
   * @throws InvalidKeyException
   * @throws IOException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#checkThreshold(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean checkThreshold(WBBPeer peer) throws MessageJSONException, NoSuchAlgorithmException, SignatureException,
      JSONException, InvalidKeyException, IOException, TVSSignatureException {
    return false;
  }

  /**
   * Defines the external content that should be signed by message. Dummy test stub.
   * 
   * @return a String of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getExternalSignableContent()
   */
  @Override
  public String getExternalSignableContent() throws JSONException {
    this.getExternalSignableContentCalled = true;

    return TestParameters.EXTERNAL_CONTENT;
  }

  /**
   * Defines the internal content that should be signed by message. Dummy test stub.
   * 
   * @return a String of the signable content
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#getInternalSignableContent()
   */
  @Override
  public String getInternalSignableContent() throws JSONException {
    this.getInternalSignableContentCalled = true;

    return TestParameters.INTERNAL_CONTENT;
  }

  /**
   * @return whether the getExternalSignableContent method has been called.
   */
  public boolean isGetExternalSignableContentCalled() {
    return this.getExternalSignableContentCalled;
  }

  /**
   * @return whether the getInternalSignableContent method has been called.
   */
  public boolean isGetInternalSignableContentCalled() {
    return this.getInternalSignableContentCalled;
  }

  /**
   * @return whether the processMessage method has been called.
   */
  public boolean isProcessMessageCalled() {
    return this.processMessageCalled;
  }

  /**
   * Perform validation. Dummy test stub,
   * 
   * @param peer
   *          the Peer this is running on - needed for accessing keys
   * @return boolean result of validation
   * @throws MessageVerificationException
   * @throws JSONSchemaValidationException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#performValidation(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
  }

  /**
   * Processes a peer message. Dummy test stub.
   * 
   * @param peer
   *          The WBB peer.
   * @return True if the message was successfully processed.
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws SignatureException
   * @throws InvalidKeyException
   * @throws MessageJSONException
   * @throws IOException
   * @throws UnknownDBException
   * @throws TVSSignatureException
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage#processMessage(uk.ac.surrey.cs.tvs.wbb.core.WBBPeer)
   */
  @Override
  public boolean processMessage(WBBPeer peer) throws JSONException, NoSuchAlgorithmException, SignatureException,
      InvalidKeyException, MessageJSONException, IOException, UnknownDBException, TVSSignatureException {
    this.processMessageCalled = true;

    if (this.exceptionsMap.containsKey("processMessage")) {
      switch (this.exceptionsMap.get("processMessage")) {
        case 0:
          throw new JSONException("Testing");
        default:
      }
    }

    return this.processMessage;
  }

  /**
   * Allows the ID to be overridden for testing.
   * 
   * @param id
   *          The new ID.
   */
  public void setID(String id) {
    this.id = id;
  }

  /**
   * @param processMessage
   *          the new processMessage.
   */
  public void setProcessMessage(boolean processMessage) {
    this.processMessage = processMessage;
  }

  /**
   * @param type
   *          The new message type.
   */
  public void setType(Message type) {
    this.type = type;
  }
}
