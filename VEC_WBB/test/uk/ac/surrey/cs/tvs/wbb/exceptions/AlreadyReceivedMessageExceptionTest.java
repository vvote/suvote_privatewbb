/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>AlreadyReceivedMessageExceptionTest</code> contains tests for the class
 * <code>{@link AlreadyReceivedMessageException}</code>.
 */
public class AlreadyReceivedMessageExceptionTest {

  /**
   * Run the AlreadyReceivedMessageException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadyReceivedMessageException_1() throws Exception {

    AlreadyReceivedMessageException result = new AlreadyReceivedMessageException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the AlreadyReceivedMessageException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadyReceivedMessageException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    AlreadyReceivedMessageException result = new AlreadyReceivedMessageException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the AlreadyReceivedMessageException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadyReceivedMessageException_3() throws Exception {
    Throwable cause = new Throwable();

    AlreadyReceivedMessageException result = new AlreadyReceivedMessageException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the AlreadyReceivedMessageException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadyReceivedMessageException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    AlreadyReceivedMessageException result = new AlreadyReceivedMessageException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the AlreadyReceivedMessageException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAlreadyReceivedMessageException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    AlreadyReceivedMessageException result = new AlreadyReceivedMessageException(message, cause, enableSuppression,
        writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.AlreadyReceivedMessageException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}