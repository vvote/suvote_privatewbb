/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.json.JSONObject;
import org.junit.Test;

/**
 * The class <code>CandidateTest</code> contains tests for the class <code>{@link Candidate}</code>.
 */
public class CandidateTest {

  /**
   * Run the Candidate(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidate_1() throws Exception {
    JSONObject candidate = new JSONObject();
    candidate.put("name", "rubbish");

    Candidate result = new Candidate(candidate);
    assertNotNull(result);

    assertEquals("\"rubbish\"", result.toString());
  }
}