/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>CommitResponseTest</code> contains tests for the class <code>{@link CommitResponse}</code>.
 */
public class CommitResponseTest {

  /**
   * Run the CommitResponse(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitResponse_1() throws Exception {
    CommitResponse result = new CommitResponse(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the void responseFromWait(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testResponseFromWait_1() throws Exception {
    CommitResponse fixture = new CommitResponse(TestParameters.getInstance().getWbbPeer());

    fixture.responseFromWait(null);
  }

  /**
   * Run the void responseFromWait(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testResponseFromWait_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CommitResponse fixture = new CommitResponse(TestParameters.getInstance().getWbbPeer());

    db.addException("setResponseAndCloseCommit", 0);
    fixture.responseFromWait("rubbish");

    assertTrue(TestParameters.getInstance().getWbbPeer().isScheduleNextCommitCalled());
  }

  /**
   * Run the void responseFromWait(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testResponseFromWait_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    CommitResponse fixture = new CommitResponse(TestParameters.getInstance().getWbbPeer());

    fixture.responseFromWait("rubbish");

    assertTrue(db.isSetResponseAndCloseCommitCalled());
    assertEquals("rubbish", db.getResponse());

    assertTrue(TestParameters.getInstance().getWbbPeer().isScheduleNextCommitCalled());
  }
}