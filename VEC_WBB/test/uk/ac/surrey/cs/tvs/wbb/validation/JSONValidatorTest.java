/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.validation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.DummyWBBConfig;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;

/**
 * The class <code>JSONValidatorTest</code> contains tests for the class <code>{@link JSONValidator}</code>.
 */
public class JSONValidatorTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the JSONValidator(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONValidator_1() throws Exception {
    JSONValidator result = new JSONValidator(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);

    // Test each schema.
    for (JSONSchema schemaId : JSONSchema.values()) {
      String schema = result.getSchema(schemaId);
      assertNotNull(schema);
    }
  }

  /**
   * Run the JSONValidator(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONValidator_2() throws Exception {
    // Replace the configuration information so that there is no schema list.
    DummyWBBConfig config = new DummyWBBConfig();
    config.put(WBBConfig.SCHEMA_LIST, "rubbish");

    TestParameters.getInstance().getWbbPeer().setConfig(config);

    JSONValidator result = new JSONValidator(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);

    // Test each schema.
    for (JSONSchema schemaId : JSONSchema.values()) {
      String schema = result.getSchema(schemaId);
      assertNull(schema);
    }
  }
}