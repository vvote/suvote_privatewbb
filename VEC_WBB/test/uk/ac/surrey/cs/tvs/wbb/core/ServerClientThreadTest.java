/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.net.ssl.SSLSocketFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>ServerClientThreadTest</code> contains tests for the class <code>{@link ServerClientThread}</code>.
 */
public class ServerClientThreadTest {

  /**
   * Dummy ServerClient class.
   */
  private class DummyServiceClient extends ServerClient {

    private boolean connectCalled = false;

    public DummyServiceClient(String address, int port, SSLSocketFactory factory, WBBPeer peer) {
      super(address, port, factory, peer);
    }

    @Override
    public void connect() {
      this.connectCalled = true;
    }

    public boolean isConnectCalled() {
      return this.connectCalled;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    DummyServiceClient serverClient = new DummyServiceClient("", 0, null, TestParameters.getInstance().getWbbPeer());

    ServerClientThread fixture = new ServerClientThread(serverClient);

    fixture.run();
    assertTrue(serverClient.isConnectCalled());
  }

  /**
   * Run the ServerClientThread(ServerClient) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testServerClientThread_1() throws Exception {
    DummyServiceClient serverClient = new DummyServiceClient("", 0, null, TestParameters.getInstance().getWbbPeer());

    ServerClientThread result = new ServerClientThread(serverClient);
    assertNotNull(result);
  }
}