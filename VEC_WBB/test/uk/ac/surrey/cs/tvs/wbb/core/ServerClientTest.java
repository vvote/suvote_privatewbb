/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>ServerClientTest</code> contains tests for the class <code>{@link ServerClient}</code>.
 */
public class ServerClientTest {

  /**
   * Dummy class to listen on the server thread and accept SSL connections.
   */
  private class DummySSLServer implements Runnable {

    SSLSocket socket = null;

    @Override
    public void run() {
      SSLServerSocket serverSocket = null;
      try {
        serverSocket = (SSLServerSocket) TestParameters.getInstance().getServerSSLSocketFactory().createServerSocket();

        serverSocket.setEnabledCipherSuites(new String[] { TestParameters.CIPHERS });
        serverSocket.setNeedClientAuth(true);
        serverSocket.setUseClientMode(false);
        serverSocket.setReuseAddress(true);

        InetSocketAddress address = new InetSocketAddress(TestParameters.TEST_PORT);
        serverSocket.bind(address);
        this.socket = (SSLSocket) serverSocket.accept();

        // Read all available data until closed.
        int data = this.socket.getInputStream().read();

        while (data != -1) {
          data = this.socket.getInputStream().read();
        }
      }
      catch (KeyManagementException | NoSuchAlgorithmException | IOException | PeerSSLInitException e) {
        // Ignore.
      }
      finally {
        if (serverSocket != null) {
          try {
            serverSocket.close();
          }
          catch (IOException e) {
            // Ignore.
          }
        }
      }
    }

    public void shutdown() throws IOException {
      this.socket.close();
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void connect() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConnect_1() throws Exception {
    final ServerClient fixture = new ServerClient(TestParameters.LISTENER_ADDRESS, TestParameters.TEST_PORT, TestParameters
        .getInstance().getClientSSLSocketFactory(), TestParameters.getInstance().getWbbPeer());

    // Create a listener to accept the connection.
    DummySSLServer server = new DummySSLServer();
    Thread serverThread = new Thread(server);
    serverThread.start();

    // Wait for the socket to start up.
    TestParameters.wait(1);

    // Test connect with invalid connection.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.setCloseAfterSend(true);
    fixture.setMaxRetries(1);
    fixture.setWaitForMessageTimeout(2000);

    fixture.connect();

    // Wait for the connect to timeout.
    TestParameters.wait(2);

    assertTrue(TestParameters.getInstance().getWbbPeer().isVerifyConnectionCalled());

    // Shutdown the server.
    server.shutdown();

    // Wait for the thread to finish.
    serverThread.join();
    assertFalse(serverThread.isAlive());
  }

  /**
   * Run the void connect() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConnect_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    final ServerClient fixture = new ServerClient(TestParameters.LISTENER_ADDRESS, TestParameters.TEST_PORT, TestParameters
        .getInstance().getClientSSLSocketFactory(), TestParameters.getInstance().getWbbPeer());

    // Create a listener to accept the connection.
    DummySSLServer server = new DummySSLServer();
    Thread serverThread = new Thread(server);
    serverThread.start();

    // Wait for the socket to start up.
    TestParameters.wait(1);

    // Create a valid internal message.
    String hash = TestParameters.COMMIT_TIME;
    String signature = TestParameters.signData(IOUtils.decodeData(EncodingType.BASE64, hash), TestParameters.COMMIT_TIME);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_COMMIT_R1_STRING);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.Commit.DATETIME, System.currentTimeMillis());
    object.put(MessageFields.Commit.HASH, TestParameters.signData(TestParameters.PEERS[0]));
    object.put(MessageFields.Commit.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.SIGNATURE, signature);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    db.setHash(IOUtils.decodeData(EncodingType.BASE64, hash));

    fixture.writeMessage(object.toString());

    // Test connect with valid connection.
    TestParameters.getInstance().getWbbPeer().setValidConnection(true);

    fixture.setCloseAfterSend(true);
    fixture.setMaxRetries(1);
    fixture.setWaitForMessageTimeout(2000);

    fixture.connect();

    // Wait for the connect to timeout.
    TestParameters.wait(2);

    assertTrue(TestParameters.getInstance().getWbbPeer().isVerifyConnectionCalled());

    // Shutdown the server.
    server.shutdown();

    // Wait for the thread to finish.
    serverThread.join();
    assertFalse(serverThread.isAlive());
  }

  /**
   * Run the ServerClient(String,int,SSLSocketFactory,WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testServerClient_1() throws Exception {
    ServerClient result = new ServerClient(TestParameters.LISTENER_ADDRESS, TestParameters.EXTERNAL_PORT, TestParameters
        .getInstance().getClientSSLSocketFactory(), TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);

    assertEquals(TestParameters.LISTENER_ADDRESS, result.getAddress());
    assertEquals(TestParameters.EXTERNAL_PORT, result.getPort());
  }
}