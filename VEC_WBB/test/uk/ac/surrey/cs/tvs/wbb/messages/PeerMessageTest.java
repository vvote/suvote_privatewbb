/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;

/**
 * The class <code>PeerMessageTest</code> contains tests for the class <code>{@link PeerMessage}</code>.
 */
public class PeerMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage fixture = new PeerMessage(message);

    String result = fixture.getExternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage fixture = new PeerMessage(message);

    // Test with a from peer already defined.
    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);

    PeerMessage fixture = new PeerMessage(message);

    // Test with a from peer null.
    JSONObject internal = fixture.getMsg();
    internal.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage fixture = new PeerMessage(message);

    String result = fixture.getInternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the void PeerMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerMessage_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    PeerMessage result = new PeerMessage(message);
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
    assertEquals(message.toString(), result.getMsg().toString());
  }

  /**
   * Run the void PeerMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerMessage_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage result = new PeerMessage(message);
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
    assertEquals(message.toString(), result.getMsg().toString());
  }

  /**
   * Run the void PeerMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testPeerMessage_3() throws Exception {
    JSONObject message = new JSONObject("{}");

    PeerMessage result = new PeerMessage(message);
    assertNull(result);
  }

  /**
   * Run the void PeerMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerMessage_4() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    PeerMessage result = new PeerMessage(message.toString());
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
    assertEquals(message.toString(), result.getMsg().toString());
  }

  /**
   * Run the void PeerMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerMessage_5() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage result = new PeerMessage(message.toString());
    assertNotNull(result);

    assertEquals("", result.getCommitTime());
    assertEquals(message.toString(), result.getMsg().toString());
  }

  /**
   * Run the void PeerMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testPeerMessage_6() throws Exception {
    JSONObject message = new JSONObject("{}");

    PeerMessage result = new PeerMessage(message.toString());
    assertNull(result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));

    PeerMessage fixture = new PeerMessage(message);

    // Test a valid peer message.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);

    PeerMessage fixture = new PeerMessage(message);

    // Test an invalid peer message.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test the message not already having been stored with consensus.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.setReachedConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());

    // Test that the message was sent with the correct content.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();
    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);

    assertTrue(sent.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(fixture.getID(), sent.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertNotNull(sent.getString(MessageFields.JSONWBBMessage.PEER_SIG));
    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, sent.getString(MessageFields.JSONWBBMessage.TYPE));
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_10() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");
    PeerMessage fixture = new PeerMessage(message);

    // Test the message not already having been stored with no consensus and it can be reached.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.setReachedConsensus(false);
    db.setCanReachConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isCanOrHaveReachedConsensusCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());

    // Test that no message was sent.
    assertNull(TestParameters.getInstance().getWbbPeer().getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_11() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test the message not already having been stored with no client message retrieved.
    db.setJsonObject(null);
    db.setUseStoredMessage(false);
    db.setReachedConsensus(true);
    db.setCanReachConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertFalse(db.isCanOrHaveReachedConsensusCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());

    // Test that no message was sent.
    assertNull(TestParameters.getInstance().getWbbPeer().getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test already received message.
    db.setJsonObject(null);
    db.addException("submitIncomingPeerMessageUnchecked", 1);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertFalse(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test client message exists with consensus.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.addException("submitIncomingPeerMessageUnchecked", 0);
    db.setReachedConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());

    // Test that the message was sent with the correct content.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();
    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);

    assertTrue(sent.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(fixture.getID(), sent.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertNotNull(sent.getString(MessageFields.JSONWBBMessage.PEER_SIG));
    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, sent.getString(MessageFields.JSONWBBMessage.TYPE));
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test client message exists with consensus.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.addException("submitIncomingPeerMessageUnchecked", 0);
    db.addException("submitIncomingPeerMessageChecked", 0);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertFalse(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test client message already sent.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.addException("submitIncomingPeerMessageUnchecked", 2);
    db.setReachedConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertFalse(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isSubmitPostTimeoutMessageCalled());
    assertFalse(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test client message already sent.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.addException("submitIncomingPeerMessageUnchecked", 2);
    db.addException("submitPostTimeoutMessage", 0);
    db.setReachedConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertFalse(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isSubmitPostTimeoutMessageCalled());
    assertFalse(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test client message exists with consensus.
    db.setJsonObject(message);
    db.setUseStoredMessage(false);
    db.addException("submitIncomingPeerMessageUnchecked", 0);
    db.setReachedConsensus(true);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isGetClientMessageCalled());
    assertFalse(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertTrue(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());

    // Test that the message was sent with the correct content.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();
    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);

    assertTrue(sent.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(fixture.getID(), sent.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertNotNull(sent.getString(MessageFields.JSONWBBMessage.PEER_SIG));
    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, sent.getString(MessageFields.JSONWBBMessage.TYPE));
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_8() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test client message exists with consensus.
    db.setJsonObject(message);
    db.setUseStoredMessage(false);
    db.addException("submitIncomingPeerMessageUnchecked", 0);
    db.addException("submitIncomingPeerMessageChecked", 0);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isGetClientMessageCalled());
    assertFalse(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertFalse(db.isCheckThresholdAndResponseCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_9() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    message.put(MessageFields.FileMessage.FILESIZE, 0);
    message.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    message.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    message
        .put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    message.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    message.put(MessageFields.FileMessage.DIGEST, "");
    message.put(TestParameters.DESC_FIELD, "");

    PeerMessage fixture = new PeerMessage(message);

    // Test the message not already having been stored with no consensus and it cannot be reached.
    db.setJsonObject(null);
    db.setUseStoredMessage(true);
    db.setReachedConsensus(false);
    db.setCanReachConsensus(false);
    TestParameters.getInstance().getWbbPeer().setMessage(null);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isGetClientMessageCalled());
    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isCanOrHaveReachedConsensusCalled());

    assertEquals(fixture.getID(), db.getSerialNo());
    assertEquals(fixture, db.getMessage());

    // Test that the message was sent with the correct content.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();
    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);

    assertTrue(sent.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.TYPE));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_ID));
    assertTrue(sent.has(MessageFields.JSONWBBMessage.PEER_SIG));

    assertEquals(fixture.getID(), sent.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.JSONWBBMessage.PEER_ID));
    assertNotNull(sent.getString(MessageFields.JSONWBBMessage.PEER_SIG));
    assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.JSONWBBMessage.TYPE));
  }

  /**
   * Run the void setFromPeer(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetFromPeer_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage fixture = new PeerMessage(message);

    // Test with a from peer already defined.
    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);

    // Test setting the peer.
    fixture.setFromPeer(TestParameters.PEERS[2]);

    result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[2], result);

    JSONObject internal = fixture.getMsg();
    assertEquals(TestParameters.PEERS[2], internal.getString(MessageFields.FROM_PEER));
  }
}