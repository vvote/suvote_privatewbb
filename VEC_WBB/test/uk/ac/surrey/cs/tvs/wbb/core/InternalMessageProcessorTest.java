/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.DummyJSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>InternalMessageProcessorTest</code> contains tests for the class <code>{@link InternalMessageProcessor}</code>.
 */
public class InternalMessageProcessorTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the InternalMessageProcessor(JSONWBBMessage,WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testInternalMessageProcessor_1() throws Exception {
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();

    InternalMessageProcessor result = new InternalMessageProcessor(message, TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();
    message.setProcessMessage(false);

    InternalMessageProcessor fixture = new InternalMessageProcessor(message, TestParameters.getInstance().getWbbPeer());

    // Test failure to process message.
    fixture.run();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();
    message.setProcessMessage(true);

    InternalMessageProcessor fixture = new InternalMessageProcessor(message, TestParameters.getInstance().getWbbPeer());

    // Test successful processing of a message.
    fixture.run();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    DummyJSONWBBMessage message = new DummyJSONWBBMessage();
    message.addException("processMessage", 0);

    InternalMessageProcessor fixture = new InternalMessageProcessor(message, TestParameters.getInstance().getWbbPeer());

    // Test failure to process message with exception.
    fixture.run();
  }
}