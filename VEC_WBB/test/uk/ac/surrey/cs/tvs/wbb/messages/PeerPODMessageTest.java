/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;

/**
 * The class <code>PeerPODMessageTest</code> contains tests for the class <code>{@link PeerPODMessage}</code>.
 */
public class PeerPODMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test missing from peer.
    String result = fixture.getFromPeer();
    assertNull(result);
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFromPeer_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);

    PeerPODMessage fixture = new PeerPODMessage(message);
    fixture.getMsg().put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    // Test valid from peer.
    String result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);

    // Test again.
    result = fixture.getFromPeer();
    assertNotNull(result);

    assertEquals(TestParameters.PEERS[1], result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test no internal content.
    String result = fixture.getInternalSignableContent();
    assertNull(result);
  }

  /**
   * Run the PeerPODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testPeerPODMessage_1() throws Exception {
    // Test invalid source message.
    PeerPODMessage result = new PeerPODMessage("");
    assertNull(result);
  }

  /**
   * Run the PeerPODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerPODMessage_2() throws Exception {
    // Test valid source message.
    PeerPODMessage result = new PeerPODMessage(TestParameters.MESSAGE);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_POD_MESSAGE, result.type);
  }

  /**
   * Run the PeerPODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerPODMessage_3() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    // Test valid source message with from peer.
    PeerPODMessage result = new PeerPODMessage(message.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_POD_MESSAGE, result.type);
    assertEquals(TestParameters.PEERS[1], result.getFromPeer());
  }

  /**
   * Run the PeerPODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testPeerPODMessage_4() throws Exception {
    // Test invalid source message.
    PeerPODMessage result = new PeerPODMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the PeerPODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerPODMessage_5() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);

    // Test valid source message.
    PeerPODMessage result = new PeerPODMessage(message);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_POD_MESSAGE, result.type);
  }

  /**
   * Run the PeerPODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerPODMessage_6() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    // Test valid source message with from peer.
    PeerPODMessage result = new PeerPODMessage(message);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.PEER_POD_MESSAGE, result.type);
    assertEquals(TestParameters.PEERS[1], result.getFromPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_2() throws Exception {
    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[1]));
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test valid schema.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[1]));
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with no client message and no threshold.
    db.setJsonObject(null);
    db.setReachedConsensus(false);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_10() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with client message.
    db.setJsonObject(fixture.getMsg());
    db.setReachedConsensus(false);
    db.setCanReachConsensus(false);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    message.put(MessageFields.PeerMessage.TYPE, TestParameters.MESSAGE_TYPE_PEER_POD_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[1]));
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with no client message and already received.
    db.setJsonObject(null);
    db.setReachedConsensus(false);
    db.addException("submitIncomingPeerMessageUnchecked", 1);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertFalse(db.isCheckThresholdAndResponseCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with no client message and message exists, have not reached consensus with exception.
    db.setJsonObject(null);
    db.setReachedConsensus(false);
    db.addException("submitIncomingPeerMessageUnchecked", 0);
    db.addException("submitIncomingPeerMessageChecked", 0);
    db.setUseStoredMessage(true);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertFalse(db.isValid());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with no client message and message exists, have not reached consensus, but can reach consensus.
    db.setJsonObject(null);
    db.setReachedConsensus(false);
    db.addException("submitIncomingPeerMessageUnchecked", 0);
    db.setUseStoredMessage(true);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isCanOrHaveReachedConsensusCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with no client message and message exists, have not reached consensus with exception.
    db.setJsonObject(null);
    db.setReachedConsensus(false);
    db.addException("submitIncomingPeerMessageUnchecked", 2);
    db.setUseStoredMessage(true);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isSubmitPostTimeoutMessageCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with no client message and message exists, have not reached consensus with exception.
    db.setJsonObject(null);
    db.setReachedConsensus(false);
    db.addException("submitIncomingPeerMessageUnchecked", 2);
    db.addException("submitPostTimeoutMessage", 0);
    db.setUseStoredMessage(true);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerMessageUncheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isSubmitPostTimeoutMessageCalled());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with client message.
    db.setJsonObject(fixture.getMsg());
    db.setReachedConsensus(false);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertTrue(db.isCanOrHaveReachedConsensusCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_8() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with client message.
    db.setJsonObject(fixture.getMsg());
    db.addException("submitIncomingPeerMessageChecked", 0);
    db.setReachedConsensus(false);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_9() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    // Create a vote message and parcel it up as a Peer POD message so that it can be parsed.
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };

    String signature = TestParameters.signData(data);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    message.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    message.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    message.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    message.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    message.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    message.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.SERIAL_NO));

    PeerPODMessage fixture = new PeerPODMessage(message);

    // Test with client message.
    db.setJsonObject(fixture.getMsg());
    db.setReachedConsensus(true);
    db.setCanReachConsensus(true);

    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(fixture, db.getMessage());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }
}