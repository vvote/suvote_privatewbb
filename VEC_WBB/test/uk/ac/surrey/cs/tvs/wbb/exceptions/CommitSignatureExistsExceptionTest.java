/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>CommitSignatureExistsExceptionTest</code> contains tests for the class
 * <code>{@link CommitSignatureExistsException}</code>.
 */
public class CommitSignatureExistsExceptionTest {

  /**
   * Run the CommitSignatureExistsException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitSignatureExistsException_1() throws Exception {

    CommitSignatureExistsException result = new CommitSignatureExistsException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the CommitSignatureExistsException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitSignatureExistsException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    CommitSignatureExistsException result = new CommitSignatureExistsException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CommitSignatureExistsException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitSignatureExistsException_3() throws Exception {
    Throwable cause = new Throwable();

    CommitSignatureExistsException result = new CommitSignatureExistsException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the CommitSignatureExistsException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitSignatureExistsException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    CommitSignatureExistsException result = new CommitSignatureExistsException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CommitSignatureExistsException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitSignatureExistsException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    CommitSignatureExistsException result = new CommitSignatureExistsException(message, cause, enableSuppression,
        writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.CommitSignatureExistsException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}