/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.security.MessageDigest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;

/**
 * The class <code>AuditMessageTest</code> contains tests for the class <code>{@link AuditMessage}</code>.
 */
public class AuditMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the AuditMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testAuditMessage_1() throws Exception {
    // Test invalid source message.
    AuditMessage result = new AuditMessage("");
    assertNull(result);
  }

  /**
   * Run the AuditMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAuditMessage_2() throws Exception {
    // Test valid source message.
    AuditMessage result = new AuditMessage(TestParameters.MESSAGE);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.AUDIT_MESSAGE, result.type);
  }

  /**
   * Run the AuditMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAuditMessage_3() throws Exception {
    // Test valid source with adjusted permutations (dummy).
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    AuditMessage result = new AuditMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.AUDIT_MESSAGE, result.type);
    assertTrue(result.getInternalSignableContent().contains(TestParameters.INTERNAL_PERMUTATION));
  }

  /**
   * Run the AuditMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException.class)
  public void testAuditMessage_4() throws Exception {
    // Test invalid source message.
    AuditMessage result = new AuditMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the AuditMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAuditMessage_5() throws Exception {
    // Test valid source message.
    AuditMessage result = new AuditMessage(new JSONObject(TestParameters.MESSAGE));
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.AUDIT_MESSAGE, result.type);
  }

  /**
   * Run the AuditMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testAuditMessage_6() throws Exception {
    // Test valid source with adjusted permutations (dummy).
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    AuditMessage result = new AuditMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.AUDIT_MESSAGE, result.type);
    assertTrue(result.getInternalSignableContent().contains(TestParameters.INTERNAL_PERMUTATION));
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);

    AuditMessage fixture = new AuditMessage(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID() + null;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    AuditMessage fixture = new AuditMessage(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID() + TestParameters.INTERNAL_PERMUTATION;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    AuditMessage fixture = new AuditMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID() + TestParameters.COMMIT_TIME + null;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    AuditMessage fixture = new AuditMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getTypeString() + fixture.getID() + TestParameters.COMMIT_TIME + TestParameters.INTERNAL_PERMUTATION;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    AuditMessage fixture = new AuditMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    // Test invalid serial signatures.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { "rubbish" };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    // Test invalid audit signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_4() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.AuditMessage.INTERNAL_REDUCED_PERMUTATION, TestParameters.INTERNAL_PERMUTATION);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data, TestParameters.CLIENT_POD_KEYSTORE));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    // Test valid with adjusted permutations.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    // Test valid but no adjusted permutations and no database records.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    // Test valid but no adjusted permutations and database records but with rubbish.
    JSONObject ballots = new JSONObject();
    ballots.put(MessageFields.PODMessage.BALLOT_REDUCTIONS, "rubbish");
    db.setJsonObject(ballots);

    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data, TestParameters.CLIENT_POD_KEYSTORE));
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    // Test valid but no adjusted permutations and valid database records.
    JSONObject ballots = new JSONObject();
    JSONObject reduction = new JSONObject();
    reduction.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 0);

    JSONArray array = new JSONArray();
    JSONArray reductionArray = new JSONArray();
    reductionArray.put(reduction);
    array.put(reductionArray);
    array.put(reductionArray);
    array.put(reductionArray);

    ballots.put(MessageFields.PODMessage.BALLOT_REDUCTIONS, array);
    db.setJsonObject(ballots);

    fixture.performValidation(TestParameters.getInstance().getWbbPeer());

    assertTrue(db.isGetBallotRecordCalled());
    assertTrue(db.isGetClientMessageCalled());
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    String signature = TestParameters.signData(TestParameters.DEVICE);

    // Test failure to store.
    db.addException("submitIncomingClientMessage", 0);

    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    String signature = TestParameters.signData(TestParameters.DEVICE);

    // Test successful store but no database record.
    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());
    AuditMessage fixture = new AuditMessage(object);

    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject permutation = new JSONObject();
    // TODO CJC Added this to correctly calculate the permutation commitment, whilst AuditMessage.Permutation is used in both
    // messages the contents is different. I've added a todo in the code to also update the MessageField to better clarify that it
    // is different even though the field names are the same
    MessageDigest md = MessageDigest.getInstance("SHA256");
    md.update(TestParameters.PERMUTATION.getBytes());
    // permutation.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PERMUTATION);
    permutation.put(MessageFields.AuditMessage.PERMUTATION, IOUtils.encodeData(EncodingType.BASE64, md.digest()));
    db.setJsonObject(permutation);

    // TODO This fails because the BouncyCastle Commitment method now throws an exception if the lengths aren't correct (this is a
    // change between 1.49 and 1.50). The issue is that the permutation is too short - since the witness value is fixed (it is
    // jointly generated by the randomness servers) it means the length of the message must also be fixed. Therefore the message
    // must be 32 bytes in length. This isn't a problem when the message is longer than 32 bytes because we hash the value in
    // advanced (which is what occurs in reality). However, in this test case the message is too short and would normally caused an
    // ArrayIndexOutOfBoundsException during the generation of the original commitment. We therefore have two options, update the
    // test to reflect the real world scenario or modify the code to enforce the minimum length as well as the maximum length. This
    // would either require generating more random data for the permutation commitment, which we don't want to do, or hashing the
    // shorter messages to ensure they are the correct length.

    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);

    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    assertEquals(DBFields.AUDIT_FAILURE_STATUS, db.getField());
    assertNotNull(db.getValue());
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    byte[] commitment = CryptoUtils.generateCommitment(TestParameters.PADDED_PERMUTATION.getBytes(), TestParameters.getInstance()
        .getWitness());

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING,
        TestParameters.PERMUTATION, TestParameters.getInstance().getEncodedWitness() };

    // Add in the fields for validation.
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_AUDIT_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.AuditMessage.PERMUTATION, TestParameters.PADDED_PERMUTATION);
    object.put(MessageFields.AuditMessage.COMMIT_WITNESS, TestParameters.getInstance().getEncodedWitness());

    AuditMessage fixture = new AuditMessage(object);

    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject permutation = new JSONObject();
    permutation.put(MessageFields.AuditMessage.PERMUTATION, IOUtils.encodeData(EncodingType.BASE64, commitment));
    db.setJsonObject(permutation);

    // Test successful store with database record but valid commitment.
    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertTrue(result);
  }
}