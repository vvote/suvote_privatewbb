/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.junit.Test;

/**
 * The class <code>MsgSenderTest</code> contains tests for the class <code>{@link MsgSender}</code>.
 */
public class MsgSenderTest {

  /**
   * Dummy implementation of the abstract MsgSender.
   */
  private class DummyMsgSender extends MsgSender {

    public DummyMsgSender(WebSocket ws, long delay) {
      super(ws, delay);
    }

    @Override
    public void run() {
    }
  }

  /**
   * Dummy web socket class. Does nothing.
   */
  private class DummyWebSocket extends WebSocket {

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String arg0) throws NotYetConnectedException {
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Run the void shutdown() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testShutdown_1() throws Exception {
    DummyWebSocket webSocket = new DummyWebSocket();
    long delay = 99l;

    DummyMsgSender fixture = new DummyMsgSender(webSocket, delay);
    assertEquals(webSocket, fixture.ws);
    assertEquals(delay, fixture.delay);
    assertFalse(fixture.shutdown);

    fixture.shutdown();

    assertTrue(fixture.shutdown);

  }
}