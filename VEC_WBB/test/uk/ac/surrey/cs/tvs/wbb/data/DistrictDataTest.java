/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import static org.junit.Assert.assertNotNull;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>DistrictDataTest</code> contains tests for the class <code>{@link DistrictData}</code>.
 */
public class DistrictDataTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the DistrictData(String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testDistrictData_1() throws Exception {
    // Create the region file.
    JSONArray candidates = new JSONArray("[{\"name\":\"Bob\"},{\"name\":\"Sid\"}]");

    JSONArray parties = new JSONArray();

    JSONObject party1 = new JSONObject();
    party1.put("preferredName", "party1");
    party1.put("hasBallotBox", true);
    party1.put("ungrouped", true);
    party1.put("candidates", candidates);
    parties.put(party1);

    JSONObject party2 = new JSONObject();
    party2.put("preferredName", "party2");
    party2.put("hasBallotBox", false);
    party2.put("ungrouped", false);
    party2.put("candidates", candidates);
    parties.put(party2);

    JSONArray districts = new JSONArray();

    JSONObject district1 = new JSONObject();
    district1.put("district", "district1");
    district1.put("candidates", candidates);
    districts.put(district1);

    JSONObject district2 = new JSONObject();
    district2.put("district", "district2");
    district2.put("candidates", candidates);
    districts.put(district2);

    JSONObject region = new JSONObject();
    region.put("region", "region1");
    region.put("parties", parties);
    region.put("districts", districts);

    JSONObject regions = new JSONObject();
    regions.put(region.getString("region"), region);

    IOUtils.writeJSONToFile(regions, TestParameters.OUTPUT_REGION_FILE);

    // Create the district configuration file.
    JSONObject candidateCount = new JSONObject();
    candidateCount.put("la", candidates.length());
    candidateCount.put("lc_atl", candidates.length());
    candidateCount.put("lc_btl", candidates.length() + candidates.length());

    JSONObject districtConf = new JSONObject();
    districtConf.put("district1", candidateCount);
    districtConf.put("district2", candidateCount);

    IOUtils.writeJSONToFile(districtConf, TestParameters.OUTPUT_DISTRICT_FILE);

    // Test loading the data.
    DistrictData result = new DistrictData(TestParameters.OUTPUT_REGION_FILE, TestParameters.OUTPUT_DISTRICT_FILE);
    assertNotNull(result);
  }
}