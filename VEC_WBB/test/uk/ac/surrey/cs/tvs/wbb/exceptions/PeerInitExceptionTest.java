/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>PeerInitExceptionTest</code> contains tests for the class <code>{@link PeerInitException}</code>.
 */
public class PeerInitExceptionTest {

  /**
   * Run the PeerInitException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerInitException_1() throws Exception {

    PeerInitException result = new PeerInitException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the PeerInitException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerInitException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    PeerInitException result = new PeerInitException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the PeerInitException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerInitException_3() throws Exception {
    Throwable cause = new Throwable();

    PeerInitException result = new PeerInitException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the PeerInitException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerInitException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    PeerInitException result = new PeerInitException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the PeerInitException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerInitException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    PeerInitException result = new PeerInitException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.exceptions.PeerInitException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}