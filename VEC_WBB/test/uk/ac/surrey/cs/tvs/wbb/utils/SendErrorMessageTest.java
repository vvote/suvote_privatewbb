/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.config.ClientErrorMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.AuditMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;

/**
 * The class <code>SendErrorMessageTest</code> contains tests for the class <code>{@link SendErrorMessage}</code>.
 */
public class SendErrorMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the String constructErrorMessage(WBBPeer,String,ClientErrorMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructErrorMessage_1() throws Exception {
    for (ClientErrorMessage message : ClientErrorMessage.values()) {
      String result = SendErrorMessage.constructErrorMessage(TestParameters.getInstance().getWbbPeer(),
          TestParameters.ERROR_MESSAGE, message);
      assertNotNull(result);

      JSONObject response = new JSONObject(result);

      String expectedMessage = null;

      switch (message) {
        case COULD_NOT_PARSE_JSON:
          expectedMessage = TestParameters.COULD_NOT_PARSE_JSON_MESSAGE + TestParameters.ERROR_MESSAGE;
          break;
        case UNKNOWN_MESSAGE_TYPE:
          expectedMessage = TestParameters.UNKNOWN_MESSAGE_TYPE_MESSAGE + TestParameters.ERROR_MESSAGE;
          break;
        case MESSAGE_FAILED_VALIDATION:
          expectedMessage = TestParameters.MESSAGE_FAILED_VALIDATION_MESSAGE + TestParameters.ERROR_MESSAGE;
          break;
        case SIGNATURE_CHECK_FAILED:
          expectedMessage = TestParameters.SIGNATURE_CHECK_FAILED_MESSAGE + TestParameters.ERROR_MESSAGE;
          break;
        case INPUT_EXCEEDED_BOUND:
          expectedMessage = TestParameters.INPUT_EXCEEDED_BOUND_MESSAGE + TestParameters.ERROR_MESSAGE;
          break;
        default:
          expectedMessage = TestParameters.UNKNOWN_ERROR_MESSAGE;
          break;
      }

      assertEquals(TestParameters.ERROR_MESSAGE, response.getString(MessageFields.TYPE));
      assertEquals(TestParameters.getInstance().getWbbPeer().getID(), response.getString(MessageFields.ErrorMessage.PEER_ID));
      assertNotNull(response.get(MessageFields.ErrorMessage.PEER_SIG));
      assertEquals(expectedMessage, response.getString(MessageFields.ErrorMessage.MESSAGE));
    }
  }

  /**
   * Run the SendErrorMessage() constructor test.
   */
  @Test
  public void testSendErrorMessage_1() throws Exception {
    JSONWBBMessage original = new AuditMessage(new JSONObject(TestParameters.MESSAGE));

    for (ClientErrorMessage message : ClientErrorMessage.values()) {
      SendErrorMessage.sendErrorMessage(TestParameters.getInstance().getWbbPeer(), original, message);

      JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
      String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();

      assertEquals(original.getID(), sent.getString(MessageFields.ErrorMessage.ID));
      assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.TYPE));
      assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.ErrorMessage.PEER_ID));
      assertNotNull(sent.get(MessageFields.ErrorMessage.PEER_SIG));

      String expectedMessage = null;

      switch (message) {
        case SERIALNO_USED:
          expectedMessage = TestParameters.SERIALNO_USED_MESSAGE;
          break;
        case TIME_OUT_ON_CONSENSUS:
          expectedMessage = TestParameters.TIME_OUT_ON_CONSENSUS_MESSAGE;
          break;
        case NO_CONSENSUS_POSSIBLE:
          expectedMessage = TestParameters.NO_CONSENSUS_POSSIBLE_MESSAGE;
          break;
        case UNKNOWN_DB_ERROR:
          expectedMessage = TestParameters.UNKNOWN_DB_ERROR_MESSAGE;
          break;
        case SESSION_TIMEOUT:
          expectedMessage = TestParameters.SESSION_TIMEOUT_MESSAGE;
          break;
        case COMMITMENT_FAILURE:
          expectedMessage = TestParameters.COMMITMENT_FAILURE_MESSAGE;
          break;
        case UNKNOWN_SERIAL_NO:
          expectedMessage = TestParameters.UNKNOWN_SERIAL_NO_MESSAGE;
          break;
        default:
          // Do nothing.
      }

      if (expectedMessage != null) {
        assertEquals(expectedMessage, sent.getString(MessageFields.ErrorMessage.MESSAGE));
      }

      assertEquals(TestParameters.SESSION_ID.toString(), sessionID);
    }
  }

  /**
   * Run the void sendTimeoutMessage(WBBPeer,JSONWBBMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendTimeoutMessage_1() throws Exception {
    JSONWBBMessage original = new AuditMessage(new JSONObject(TestParameters.MESSAGE));

    SendErrorMessage.sendTimeoutMessage(TestParameters.getInstance().getWbbPeer(), original);

    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();

    assertEquals(original.getID(), sent.getString(MessageFields.ErrorMessage.ID));
    assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.ErrorMessage.PEER_ID));
    assertEquals(TestParameters.TIMEOUT_MESSAGE, sent.getString(MessageFields.ErrorMessage.MESSAGE));
    assertNotNull(sent.get(MessageFields.ErrorMessage.PEER_SIG));

    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);
  }
}