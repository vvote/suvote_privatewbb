/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.Socket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.messages.AuditMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.PODMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.VoteMessage;

/**
 * The class <code>SubmissionTimeoutTest</code> contains tests for the class <code>{@link SubmissionTimeout}</code>.
 */
public class SubmissionTimeoutTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    // Replace the database so that we can influence testing.
    TestParameters.getInstance().getWbbPeer().setDB(new DummyConcurrentDB());

    JSONWBBMessage original = new AuditMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Test no session ID.
    fixture.run();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new AuditMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test audit message timeout.
    db.setTimeout(true);
    fixture.run();

    // A timeout message should have been sent.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();

    assertEquals(original.getID(), sent.getString(MessageFields.ErrorMessage.ID));
    assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.ErrorMessage.PEER_ID));
    assertEquals(TestParameters.TIMEOUT_MESSAGE, sent.getString(MessageFields.ErrorMessage.MESSAGE));
    assertNotNull(sent.get(MessageFields.ErrorMessage.PEER_SIG));

    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new AuditMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test audit message without timeout.
    db.setTimeout(false);
    fixture.run();
    assertFalse(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new VoteMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test vote message timeout.
    db.setTimeout(true);
    fixture.run();

    // A timeout message should have been sent.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();

    assertEquals(original.getID(), sent.getString(MessageFields.ErrorMessage.ID));
    assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.ErrorMessage.PEER_ID));
    assertEquals(TestParameters.TIMEOUT_MESSAGE, sent.getString(MessageFields.ErrorMessage.MESSAGE));
    assertNotNull(sent.get(MessageFields.ErrorMessage.PEER_SIG));

    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_5() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new VoteMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test vote message without timeout.
    db.setTimeout(false);
    fixture.run();
    assertFalse(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_6() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new PODMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test POD message timeout.
    db.setTimeout(true);
    fixture.run();

    // A timeout message should have been sent.
    JSONObject sent = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    String sessionID = TestParameters.getInstance().getWbbPeer().getSessionID();

    assertEquals(original.getID(), sent.getString(MessageFields.ErrorMessage.ID));
    assertEquals(TestParameters.ERROR_MESSAGE, sent.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), sent.getString(MessageFields.ErrorMessage.PEER_ID));
    assertEquals(TestParameters.TIMEOUT_MESSAGE, sent.getString(MessageFields.ErrorMessage.MESSAGE));
    assertNotNull(sent.get(MessageFields.ErrorMessage.PEER_SIG));

    assertEquals(TestParameters.SESSION_ID.toString(), sessionID);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_7() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new PODMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test POD message without timeout.
    db.setTimeout(false);
    fixture.run();
    assertFalse(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_8() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONWBBMessage original = new PODMessage(new JSONObject(TestParameters.MESSAGE));
    SubmissionTimeout fixture = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID,
        original);

    // Setup a valid session ID.
    TestParameters.getInstance().getWbbPeer().addResponseChannel(TestParameters.SESSION_ID, new Socket());
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));

    // Test POD message with exception.
    db.addException("checkAndSetTimeout", 0);
    fixture.run();
    assertTrue(TestParameters.getInstance().getWbbPeer().sessionIDExists(TestParameters.SESSION_ID));
  }

  /**
   * Run the SubmissionTimeout(WBBPeer,UUID,JSONWBBMessage) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmissionTimeout_1() throws Exception {
    JSONWBBMessage message = new AuditMessage(new JSONObject(TestParameters.MESSAGE));

    SubmissionTimeout result = new SubmissionTimeout(TestParameters.getInstance().getWbbPeer(), TestParameters.SESSION_ID, message);
    assertNotNull(result);
  }
}