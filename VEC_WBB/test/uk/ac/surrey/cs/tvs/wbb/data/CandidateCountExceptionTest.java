/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>CandidateCountExceptionTest</code> contains tests for the class <code>{@link CandidateCountException}</code>.
 */
public class CandidateCountExceptionTest {

  /**
   * Run the CandidateCountException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateCountException_1() throws Exception {

    CandidateCountException result = new CandidateCountException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.data.CandidateCountException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the CandidateCountException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateCountException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    CandidateCountException result = new CandidateCountException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.data.CandidateCountException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CandidateCountException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateCountException_3() throws Exception {
    Throwable cause = new Throwable();

    CandidateCountException result = new CandidateCountException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.data.CandidateCountException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the CandidateCountException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateCountException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    CandidateCountException result = new CandidateCountException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.data.CandidateCountException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CandidateCountException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateCountException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    CandidateCountException result = new CandidateCountException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.wbb.data.CandidateCountException: " + TestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}