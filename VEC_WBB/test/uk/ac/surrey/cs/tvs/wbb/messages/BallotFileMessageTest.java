/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.security.MessageDigest;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;

/**
 * The class <code>BallotFileMessageTest</code> contains tests for the class <code>{@link BallotFileMessage}</code>.
 */
public class BallotFileMessageTest {

  /**
   * Dummy concrete class to test abstract BallotFileMessage.
   */
  private class DummyBallotFileMessage extends BallotFileMessage {

    public DummyBallotFileMessage(JSONObject msg) throws MessageJSONException {
      super(msg);
    }

    public DummyBallotFileMessage(String msgString) throws MessageJSONException {
      super(msgString);
    }
  }

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the boolean BallotFileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotFileMessage_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid message. Message content tested elsewhere.
    BallotFileMessage result = new DummyBallotFileMessage(object.toString());
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.FILE_MESSAGE, result.type);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the boolean BallotFileMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotFileMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);

    // Test valid message. Message content tested elsewhere.
    BallotFileMessage result = new DummyBallotFileMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.FILE_MESSAGE, result.type);
    assertEquals(99, result.getFileSize());
  }

  /**
   * Run the boolean doFileProcessing(WBBPeer,BoundedBufferedInputStream,Socket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoFileProcessing_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.DATA_FILE);

    TestParameters.createTestFile(file);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with an empty ZIP file.
    DummySocket socket = new DummySocket();

    boolean result = fixture.doFileProcessing(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket, TestParameters.LOG_FOLDER);
    assertFalse(result);
  }

  /**
   * Run the boolean doFileProcessing(WBBPeer,BoundedBufferedInputStream,Socket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoFileProcessing_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with a valid ZIP file but invalid digest.
    DummySocket socket = new DummySocket();

    boolean result = fixture.doFileProcessing(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket, TestParameters.LOG_FOLDER);
    assertFalse(result);
    assertTrue(socket.isClosed());
  }

  /**
   * Run the boolean doFileProcessing(WBBPeer,BoundedBufferedInputStream,Socket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoFileProcessing_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with a valid ZIP file and valid digest but invalid signature.
    DummySocket socket = new DummySocket();

    boolean result = fixture.doFileProcessing(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket, TestParameters.LOG_FOLDER);
    assertFalse(result);
    assertTrue(socket.isClosed());
  }

  /**
   * Run the boolean doFileProcessing(WBBPeer,BoundedBufferedInputStream,Socket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoFileProcessing_4() throws Exception {
    MessageDigest md = MessageDigest.getInstance(TestParameters.MESSAGE_DIGEST_ALGORITHM);
    String content = new JSONObject(TestParameters.ZIP_CIPHER).toString() + "\n";
    md.update(content.getBytes());

    // Digest from contents of file.
    String digest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
    String[] data = new String[] { TestParameters.SERIAL_NO, digest };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_POD_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signBLSData(data,TestParameters.CLIENT_POD_KEYSTORE));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Create a dummy input file.
    File file = new File(TestParameters.TEST_GOOD_ZIP_FILE);

    // Create the uploads directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    // Test pre-processing with a valid ZIP file and valid digest and valid signature.
    DummySocket socket = new DummySocket();

    boolean result = fixture.doFileProcessing(TestParameters.getInstance().getWbbPeer(), new BoundedBufferedInputStream(
        new FileInputStream(file)), socket, TestParameters.LOG_FOLDER);
    assertTrue(result);
    assertFalse(socket.isClosed());
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + digest + TestParameters.SENDER_CERTIFICATE + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_1() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.DATA_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Test missing file.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance().getWbbPeer()
        .getUploadDir().getAbsolutePath());
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_2() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.TEST_GOOD_ZIP_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Copy the ZIP into the upload directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    TestParameters.copyFile(new File(TestParameters.TEST_BAD_ZIP_FILE), new File(TestParameters.getInstance().getWbbPeer()
        .getUploadDir(), TestParameters.TEST_GOOD_ZIP_FILE.substring(TestParameters.TEST_GOOD_ZIP_FILE.lastIndexOf("/"))));

    // Test with valid ZIP but invalid content.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance().getWbbPeer()
        .getUploadDir().getAbsolutePath());
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_3() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData("rubbish"));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.TEST_GOOD_ZIP_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Copy the ZIP into the upload directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    TestParameters.copyFile(new File(TestParameters.TEST_GOOD_ZIP_FILE), new File(TestParameters.getInstance().getWbbPeer()
        .getUploadDir(), TestParameters.TEST_GOOD_ZIP_FILE.substring(TestParameters.TEST_GOOD_ZIP_FILE.lastIndexOf("/"))));

    // Test with valid ZIP and valid content but wrong signature.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance().getWbbPeer()
        .getUploadDir().getAbsolutePath());
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_4() throws Exception {
    String digest = TestParameters.signData(TestParameters.SERIAL_NO);

    MessageDigest md = MessageDigest.getInstance("SHA1");
    md.update(TestParameters.LINE.getBytes());

    // Digest from contents of file.
    String[] data = new String[] { TestParameters.SERIAL_NO, IOUtils.encodeData(EncodingType.BASE64, md.digest()) };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data));
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, TestParameters.TEST_GOOD_ZIP_FILE);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, digest);

    BallotFileMessage fixture = new DummyBallotFileMessage(object);

    // Copy the ZIP into the upload directory.
    TestParameters.getInstance().getWbbPeer().getUploadDir().mkdirs();

    TestParameters.copyFile(new File(TestParameters.TEST_GOOD_ZIP_FILE), new File(TestParameters.getInstance().getWbbPeer()
        .getUploadDir(), TestParameters.TEST_GOOD_ZIP_FILE.substring(TestParameters.TEST_GOOD_ZIP_FILE.lastIndexOf("/"))));

    // Test with valid ZIP and valid content and signature.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), TestParameters.getInstance().getWbbPeer()
        .getUploadDir().getAbsolutePath());
  }
}