/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.security.PrivateKey;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.core.WBBPeer;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFieldName;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBFields;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * The class <code>ExternalMessageTest</code> contains tests for the class <code>{@link ExternalMessage}</code>.
 */
public class ExternalMessageTest {

  /**
   * Dummy concrete class to test abstract BallotFileMessage.
   */
  private class DummyExternalMessage extends ExternalMessage {

    private boolean valid = false;

    public DummyExternalMessage(JSONObject msg) throws MessageJSONException {
      super(msg);

      this.id = TestParameters.SERIAL_NO;
    }

    public DummyExternalMessage(String msgString) throws MessageJSONException {
      super(msgString);

      this.id = TestParameters.SERIAL_NO;
    }

    @Override
    public String getExternalSignableContent() throws JSONException {
      return TestParameters.EXTERNAL_CONTENT;
    }

    @Override
    public String getInternalSignableContent() throws JSONException {
      return TestParameters.INTERNAL_CONTENT;
    }

    @Override
    public void performValidation(WBBPeer peer) throws MessageVerificationException, JSONSchemaValidationException {
      if (!this.valid) {
        throw new MessageVerificationException("invalid message");
      }
    }

    public void setValid(boolean valid) {
      this.valid = valid;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the boolean canOrHaveReachedConsensus(WBBPeer,String,int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCanOrHaveReachedConsensus_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test that the database method is called.
    db.setCanReachConsensus(true);

    boolean result = fixture.canOrHaveReachedConsensus(TestParameters.getInstance().getWbbPeer(), TestParameters.THRESHOLD,
        TestParameters.THRESHOLD);
    assertTrue(result);

    assertTrue(db.isCanOrHaveReachedConsensusCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Create a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    // Test valid storage.
    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Create a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    // Test post timeout storage.
    db.addException("submitIncomingPeerMessageChecked", 1);

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(true, db.isValid());

    assertTrue(db.isSubmitPostTimeoutMessageCalled());
    assertEquals(peerMessage, db.getMessage());
  }

  /**
   * Run the void checkAndStoreMessage(WBBPeer,PeerMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAndStoreMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Create a PeerMessage with a valid signature.
    JSONObject signedMessage = new JSONObject(TestParameters.MESSAGE);
    signedMessage.put(TestParameters.PEER_ID_FIELD, TestParameters.getInstance().getWbbPeer().getID());
    signedMessage.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(signedMessage);

    // Test post timeout storage with exception
    db.addException("submitIncomingPeerMessageChecked", 1);
    db.addException("submitPostTimeoutMessage", 0);

    fixture.checkAndStoreMessage(TestParameters.getInstance().getWbbPeer(), peerMessage);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(true, db.isValid());

    assertTrue(db.isSubmitPostTimeoutMessageCalled());
    assertEquals(peerMessage, db.getMessage());
  }

  /**
   * Run the boolean checkMessageFromExternalPeer(WBBPeer,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckMessageFromExternalPeer_1() throws Exception {
    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);
    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);

    boolean result = fixture.checkMessageFromExternalPeer(TestParameters.getInstance().getWbbPeer(), TestParameters.PEERS[0],
        signature);
    assertTrue(result);
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testCheckThreshold_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test with threshold and invalid client message.
    JSONObject object = new JSONObject();
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    db.setJsonObject(object);
    db.setReachedConsensus(true);

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test with threshold and valid client message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(TestParameters.TYPE, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.FileMessage.FILESIZE, 0);
    object.put(MessageFields.FileMessage.SENDER_SIG, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    object.put(MessageFields.FileMessage.SENDER_ID, TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, "");
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, TestParameters.signData(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING));
    object.put(MessageFields.FileMessage.SAFE_UPLOAD_DIR, "");
    object.put(MessageFields.FileMessage.DIGEST, "");
    object.put(TestParameters.DESC_FIELD, "");

    db.setJsonObject(object);
    db.setReachedConsensus(true);

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());

    JSONObject response = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    assertEquals(TestParameters.SERIAL_NO, response.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.MESSAGE_TYPE_FILE_MESSAGE_STRING, response.getString(TestParameters.TYPE));
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test with threshold and missing client message.
    db.setReachedConsensus(true);

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the boolean checkThreshold(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThreshold_4() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test without threshold and cannot reach consensus.
    db.setReachedConsensus(false);
    db.setCanReachConsensus(false);

    boolean result = fixture.checkThreshold(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());

    JSONObject response = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());
    assertEquals(TestParameters.SERIAL_NO, response.getString(MessageFields.JSONWBBMessage.ID));
    assertEquals(TestParameters.ERROR_MESSAGE, response.getString(TestParameters.TYPE));
  }

  /**
   * Run the boolean checkThresholdAndResponse(WBBPeer,String,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThresholdAndResponse_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test that the database method is called.
    db.setReachedConsensus(true);

    boolean result = fixture.checkThresholdAndResponse(TestParameters.getInstance().getWbbPeer(), TestParameters.THRESHOLD);
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the void checkValidPeerMessagesInR2Record(WBBPeer,JSONObject,ArrayList<JSONWBBMessage>,DBFields) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckValidPeerMessagesInR2Record_1() throws Exception {
    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    ArrayList<JSONWBBMessage> validPeerMessages = new ArrayList<JSONWBBMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));

    ExternalMessage validMessage = new DummyExternalMessage(message.toString());

    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData("rubbish"));
    ExternalMessage invalidMessage = new DummyExternalMessage(message.toString());

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      DBFields fields = DBFields.getInstance(type);

      // Create the commit record of signature checks.
      JSONObject record = new JSONObject();
      JSONArray signatures = new JSONArray();

      // Add a valid and an invalid message.
      JSONObject signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), validMessage.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[0]);
      signatures.put(signature);

      signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), invalidMessage.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[0]);
      signatures.put(signature);

      record.put(fields.getField(DBFieldName.CHECKED_SIGS), signatures);

      // Test that only the valid message is extracted.
      fixture.checkValidPeerMessagesInR2Record(TestParameters.getInstance().getWbbPeer(), record, validPeerMessages, fields);

      assertEquals(1, validPeerMessages.size());
      assertEquals(validMessage.getMsg().getString(MessageFields.TYPE),
          validPeerMessages.get(0).getMsg().getString(MessageFields.TYPE));
      assertEquals(validMessage.getMsg().getString(MessageFields.PeerMessage.PEER_ID),
          validPeerMessages.get(0).getMsg().getString(MessageFields.PeerMessage.PEER_ID));
      assertEquals(validMessage.getMsg().getString(MessageFields.PeerMessage.PEER_SIG), validPeerMessages.get(0).getMsg()
          .getString(MessageFields.PeerMessage.PEER_SIG));

      validPeerMessages.clear();
    }
  }

  /**
   * Run the JSONObject constructPeerMessage(WBBPeer,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructPeerMessage_1() throws Exception {
    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);

    JSONObject result = fixture.constructPeerMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertNotNull(result);

    assertEquals(TestParameters.CLIENT_PEER_STRING, result.get(MessageFields.PeerMessage.TYPE));
    assertEquals(TestParameters.SERIAL_NO, result.get(MessageFields.PeerMessage.ID));
    assertEquals(signature, result.get(MessageFields.PeerMessage.PEER_SIG));
    assertEquals(TestParameters.PEERS[0], result.get(MessageFields.PeerMessage.PEER_ID));
  }

  /**
   * Run the JSONObject constructSimulatedPeerMessage(String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructSimulatedPeerMessage_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    ExternalMessage fixture = new DummyExternalMessage(object);

    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);

    JSONObject result = fixture.constructSimulatedPeerMessage(TestParameters.SERIAL_NO, signature, TestParameters.PEERS[0]);
    assertNotNull(result);

    assertEquals(TestParameters.CLIENT_PEER_STRING, result.get(MessageFields.PeerMessage.TYPE));
    assertEquals(TestParameters.SERIAL_NO, result.get(MessageFields.PeerMessage.ID));
    assertEquals(signature, result.get(MessageFields.PeerMessage.PEER_SIG));
    assertEquals(TestParameters.PEERS[0], result.get(MessageFields.PeerMessage.PEER_ID));
    assertEquals(TestParameters.PEERS[1], result.get(MessageFields.FROM_PEER));
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_1() throws Exception {
    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      // Create the commit record.
      JSONObject record = new JSONObject();

      // Test not already stored and invalid message.
      fixture.setValid(false);
      db.setStored(false);

      try {
        fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
            TestParameters.LOG_FOLDER, type);
        fail("no exception");
      }
      catch (MessageVerificationException e) {
        // Correct behaviour
      }

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_2() throws Exception {
    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      // Create the commit record.
      JSONObject record = new JSONObject();

      // Test not already stored and valid message with no signature.
      fixture.setValid(true);
      db.setStored(false);

      fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
          TestParameters.LOG_FOLDER, type);

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_3() throws Exception {
    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      DBFields fields = DBFields.getInstance(type);

      // Create the commit record.
      JSONObject record = new JSONObject();
      record.put(fields.getField(DBFieldName.MY_SIGNATURE), "rubbish");

      // Test not already stored and valid message with an invalid signature.
      fixture.setValid(true);
      db.setStored(false);

      try {
        fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
            TestParameters.LOG_FOLDER, type);
        fail("no exception");
      }
      catch (TVSSignatureException e) {
        // Correct result.
      }
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_4() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);

    DummyExternalMessage fixture = new DummyExternalMessage(object);

    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      DBFields fields = DBFields.getInstance(type);

      // Create the commit record.
      JSONObject record = new JSONObject();
      record.put(DBFields.ID, TestParameters.SERIAL_NO);
      record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(TestParameters.INTERNAL_CONTENT));

      JSONArray signatures = new JSONArray();
      JSONObject signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[0]);
      signatures.put(signature);
      record.put(fields.getField(DBFieldName.CHECKED_SIGS), signatures);

      // Test not already stored and valid message with a valid signature but no threshold.
      fixture.setValid(true);
      db.setStored(false);

      fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
          TestParameters.LOG_FOLDER, type);

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_5() throws Exception {
    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer3keys.jks", "", "Peer3_SigningSK1", "");
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[2]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT,key));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[2]);

    DummyExternalMessage fixture = new DummyExternalMessage(object);

    key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer2keys.jks", "", "Peer2_SigningSK1", "");
    object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT,key));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummyExternalMessage fixture1 = new DummyExternalMessage(object);


    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      DBFields fields = DBFields.getInstance(type);

      // Create the commit record.
      // Create the commit record of signature checks.
      JSONObject record = new JSONObject();
      record.put(DBFields.ID, TestParameters.SERIAL_NO);
      record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(TestParameters.INTERNAL_CONTENT));

      JSONArray signatures = new JSONArray();
      JSONObject signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[2]);
      signatures.put(signature);
      signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture1.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[1]);
      
      signatures.put(signature);
      record.put(fields.getField(DBFieldName.CHECKED_SIGS), signatures);
      
      // Test not already stored and valid message with a valid signature and threshold.
      fixture.setValid(true);
      fixture1.setValid(true);
      db.setStored(false);

      fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
          TestParameters.LOG_FOLDER, type);

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

      assertTrue(db.isSubmitIncomingCommitMessageCalled());
      assertEquals(type, db.getRecordType());

      assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
      assertTrue(db.isValid());

      // Check the last submitted message.
      assertEquals(
          new JSONObject(signature.getString(fields.getField(DBFieldName.QUEUED_MESSAGE)))
              .getString(MessageFields.JSONWBBMessage.ID),
          db.getMessage().getMsg().getString(MessageFields.JSONWBBMessage.ID));
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_6() throws Exception {


    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer3keys.jks", "", "Peer3_SigningSK1", "");
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[2]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT,key));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[2]);

    DummyExternalMessage fixture = new DummyExternalMessage(object);

    key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer2keys.jks", "", "Peer2_SigningSK1", "");
    object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT,key));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummyExternalMessage fixture1 = new DummyExternalMessage(object);

    
    
    
    
    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      DBFields fields = DBFields.getInstance(type);

      // Create the commit record.
      // Create the commit record of signature checks.
      JSONObject record = new JSONObject();
      record.put(DBFields.ID, TestParameters.SERIAL_NO);
      record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(TestParameters.INTERNAL_CONTENT));

      JSONArray signatures = new JSONArray();
      JSONObject signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[2]);
      signatures.put(signature);
      
      signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture1.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[1]);
      
      signatures.put(signature);
      record.put(fields.getField(DBFieldName.CHECKED_SIGS), signatures);

      // Test not already stored and valid message with a valid signature and threshold with row.
      fixture.setValid(true);
      db.setStored(false);
      db.setJsonObject(fixture.getMsg());

      fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
          TestParameters.LOG_FOLDER, type);

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

      assertTrue(db.isSubmitIncomingCommitMessageCalled());
      assertEquals(type, db.getRecordType());

      assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
      assertTrue(db.isValid());

      // Check the last submitted message.
      assertEquals(
          new JSONObject(signature.getString(fields.getField(DBFieldName.QUEUED_MESSAGE)))
              .getString(MessageFields.JSONWBBMessage.ID),
          db.getMessage().getMsg().getString(MessageFields.JSONWBBMessage.ID));
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_7() throws Exception {
    
    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer3keys.jks", "", "Peer3_SigningSK1", "");
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[2]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT,key));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[2]);

    DummyExternalMessage fixture = new DummyExternalMessage(object);

    key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer2keys.jks", "", "Peer2_SigningSK1", "");
    object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT,key));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    DummyExternalMessage fixture1 = new DummyExternalMessage(object);
    


    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      DBFields fields = DBFields.getInstance(type);

      // Create the commit record.
      // Create the commit record of signature checks.
      JSONObject record = new JSONObject();
      record.put(DBFields.ID, TestParameters.SERIAL_NO);
      record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(TestParameters.INTERNAL_CONTENT));

      JSONArray signatures = new JSONArray();
      JSONObject signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[2]);
      signatures.put(signature);
      signature = new JSONObject();
      signature.put(fields.getField(DBFieldName.QUEUED_MESSAGE), fixture1.toString());
      signature.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[1]);
      signatures.put(signature);
      
      record.put(fields.getField(DBFieldName.CHECKED_SIGS), signatures);

      // Test not already stored and valid message with a valid signature and threshold with row which has a valid client message
      // but different internal content.
      JSONObject vote = new JSONObject(TestParameters.MESSAGE);
      vote.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
      vote.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
      vote.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
      vote.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(TestParameters.DISTRICT));
      vote.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
      vote.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
      vote.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

      JSONObject client = new JSONObject();
      client.put(fields.getField(DBFieldName.CLIENT_MESSAGE), vote.toString());

      fixture.setValid(true);
      db.setStored(false);
      db.setJsonObject(client);

      fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
          TestParameters.LOG_FOLDER, type);

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

      assertTrue(db.isSubmitIncomingCommitMessageCalled());
      assertEquals(type, db.getRecordType());

      assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
      assertTrue(db.isValid());

      // Check the last submitted message.
      assertEquals(
          new JSONObject(signature.getString(fields.getField(DBFieldName.QUEUED_MESSAGE)))
              .getString(MessageFields.JSONWBBMessage.ID),
          db.getMessage().getMsg().getString(MessageFields.JSONWBBMessage.ID));
    }
  }

  /**
   * Run the void doProcessAsPartOfCommitR2(WBBPeer,JSONObject,String,String,DBRecordType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoProcessAsPartOfCommitR2_8() throws Exception {
    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT, TestParameters.PERMUTATION };
    String senderSignature = TestParameters.signBLSData(data,TestParameters.CLIENT_EVM_KEYSTORE);
    String[] serialData = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };
    String[] evmData = new String[] { JSONWBBMessage.getTypeString(Message.START_EVM), TestParameters.SERIAL_NO,
        TestParameters.DISTRICT };

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.CLIENT_EVM_DEVICE_ID);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, senderSignature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.getSerialSignatures(TestParameters.PEERS[0], serialData));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    object.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[0]);
    object.put(MessageFields.Commit.COMMIT_ID, TestParameters.COMMIT_TIME);
    object.put(MessageFields.VoteMessage.START_EVM_SIG, TestParameters.getSerialSignatures(TestParameters.PEERS[0], evmData));

    VoteMessage fixture = new VoteMessage(object); // Vote message is an ExternalMessage.
    PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer2keys.jks", "", "Peer2_SigningSK1", "");
    JSONObject peerObject = new JSONObject(TestParameters.MESSAGE);
    peerObject.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    peerObject.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[1]);
    peerObject.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(fixture.getInternalSignableContent(),key));
    peerObject.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    PeerMessage peerMessage = new PeerMessage(peerObject);
    
    key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/keys/Peer3keys.jks", "", "Peer3_SigningSK1", "");
    peerObject = new JSONObject(TestParameters.MESSAGE);
    peerObject.put(MessageFields.TYPE, TestParameters.MESSAGE_TYPE_PEER_MESSAGE_STRING);
    peerObject.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[2]);
    peerObject.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(fixture.getInternalSignableContent(),key));
    peerObject.put(MessageFields.FROM_PEER, TestParameters.PEERS[2]);

    PeerMessage peerMessage1 = new PeerMessage(peerObject);
    // Test all record types.
    for (DBRecordType type : DBRecordType.values()) {
      // Replace the database so that we can influence testing.
      DummyConcurrentDB db = new DummyConcurrentDB();
      TestParameters.getInstance().getWbbPeer().setDB(db);

      DBFields fields = DBFields.getInstance(type);

      // Create the commit record.
      // Create the commit record of signature checks.
      JSONObject record = new JSONObject();
      record.put(DBFields.ID, TestParameters.SERIAL_NO);
      record.put(fields.getField(DBFieldName.MY_SIGNATURE), TestParameters.signData(fixture.getInternalSignableContent()));

      JSONArray signatures = new JSONArray();
      JSONObject signatureObject = new JSONObject();
      signatureObject.put(fields.getField(DBFieldName.QUEUED_MESSAGE), peerMessage.toString());
      signatureObject.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[1]);
      signatures.put(signatureObject);
      
      signatureObject = new JSONObject();
      signatureObject.put(fields.getField(DBFieldName.QUEUED_MESSAGE), peerMessage1.toString());
      signatureObject.put(fields.getField(DBFieldName.FROM_PEER), TestParameters.PEERS[2]);
      signatures.put(signatureObject);
      
      record.put(fields.getField(DBFieldName.CHECKED_SIGS), signatures);

      // Test not already stored and valid message with a valid signature and threshold with row which has a valid client message
      // and with the same internal content.
      JSONObject client = new JSONObject();
      client.put(fields.getField(DBFieldName.CLIENT_MESSAGE), fixture.toString());

      db.setStored(false);
      db.setJsonObject(client);

      fixture.doProcessAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
          TestParameters.LOG_FOLDER, type);

      assertTrue(db.isAlreadyStoredCalled());
      assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());

      assertFalse(db.isSubmitIncomingCommitMessageCalled());

      assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
      assertTrue(db.isValid());

      // Check the last submitted message.
      assertEquals(
          new JSONObject(signatureObject.getString(fields.getField(DBFieldName.QUEUED_MESSAGE)))
              .getString(MessageFields.JSONWBBMessage.ID),
          db.getMessage().getMsg().getString(MessageFields.JSONWBBMessage.ID));
    }
  }

  /**
   * Run the boolean BallotFileMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testExternalMessage_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);

    // Test valid message. Message content tested elsewhere.
    ExternalMessage result = new DummyExternalMessage(object.toString());
    assertNotNull(result);
  }

  /**
   * Run the boolean BallotFileMessage(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testExternalMessage_2() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);

    // Test valid message. Message content tested elsewhere.
    ExternalMessage result = new DummyExternalMessage(object);
    assertNotNull(result);
  }

  /**
   * Run the String getClientMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetClientMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test that the database method is called.
    JSONObject object = new JSONObject(TestParameters.ZIP_CIPHER);
    db.setJsonObject(object);

    String result = fixture.getClientMessage(TestParameters.getInstance().getWbbPeer(), TestParameters.SERIAL_NO);
    assertNotNull(result);

    assertTrue(db.isGetClientMessageCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    assertEquals(object.toString(), result);
  }

  /**
   * Run the ArrayList<? extends PeerMessage> getSigsToCheck(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetSigsToCheck_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test that the database method is called.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.PEERS[0]));
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);

    ArrayList<? extends PeerMessage> result = fixture.getSigsToCheck(TestParameters.getInstance().getWbbPeer(),
        TestParameters.SERIAL_NO);
    assertNotNull(result);

    assertTrue(db.isGetSigsToCheckCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    assertEquals(sigsToCheck, result);
  }

  /**
   * Run the void preprocessAsPartofCommitR2(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreprocessAsPartofCommitR2_1() throws Exception {
    ExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Empty method.
    fixture.preprocessAsPartofCommitR2(TestParameters.getInstance().getWbbPeer(), TestParameters.LOG_FOLDER);
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Create the commit record.
    JSONObject record = new JSONObject();

    // Just test that the underlying method is called (which is tested elsewhere).
    fixture.setValid(false);
    db.setStored(false);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), record, TestParameters.PEERS[0],
        TestParameters.LOG_FOLDER);

    assertTrue(db.isAlreadyStoredCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test message stored and signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();
    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, signature);
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);
    db.setStored(true);
    db.setReachedConsensus(false);
    db.setCanReachConsensus(true);

    // Test valid.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());

    JSONObject response = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    assertEquals(TestParameters.CLIENT_PEER_STRING, response.get(MessageFields.PeerMessage.TYPE));
    assertEquals(TestParameters.SERIAL_NO, response.get(MessageFields.PeerMessage.ID));
    assertEquals(TestParameters.PEERS[0], response.get(MessageFields.PeerMessage.PEER_ID));
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);

    ExternalMessage fixture = new DummyExternalMessage(object);

    // Test message stored and signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();
    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, signature);
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);
    db.setStored(true);
    db.setReachedConsensus(false);
    db.setCanReachConsensus(true);
    db.addException("submitIncomingPeerMessageChecked", 0);

    // Test already received.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertTrue(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());

    JSONObject response = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    assertEquals(TestParameters.CLIENT_PEER_STRING, response.get(MessageFields.PeerMessage.TYPE));
    assertEquals(TestParameters.SERIAL_NO, response.get(MessageFields.PeerMessage.ID));
    assertEquals(TestParameters.PEERS[0], response.get(MessageFields.PeerMessage.PEER_ID));
  }

  /**
   * Run the boolean processMessage(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test message stored and signatures to check.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();
    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, signature);
    PeerMessage peerMessage = new PeerMessage(message);

    sigsToCheck.add(peerMessage);
    db.setSigsToCheck(sigsToCheck);
    db.setStored(true);
    db.setReachedConsensus(true);

    // Test valid but with no threshold.
    boolean result = fixture.processMessage(TestParameters.getInstance().getWbbPeer());
    assertFalse(result);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());

    assertTrue(TestParameters.getInstance().getWbbPeer().isSendToAllPeersCalled());

    JSONObject response = new JSONObject(TestParameters.getInstance().getWbbPeer().getMessage());

    assertEquals(TestParameters.CLIENT_PEER_STRING, response.get(MessageFields.PeerMessage.TYPE));
    assertEquals(TestParameters.SERIAL_NO, response.get(MessageFields.PeerMessage.ID));
    assertEquals(TestParameters.PEERS[0], response.get(MessageFields.PeerMessage.PEER_ID));
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);
    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test database method is called.
    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertTrue(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(signature, db.getSignature());
    assertEquals(fixture, db.getMessage());
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String signature = TestParameters.signData(TestParameters.INTERNAL_CONTENT);
    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    // Test database method is called with exception.
    db.addException("submitIncomingClientMessage", 0);

    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.GENERAL, db.getRecordType());
    assertEquals(signature, db.getSignature());
    assertEquals(fixture, db.getMessage());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }

  /**
   * Run the void submitIncomingPeerMessageChecked(WBBPeer,PeerMessage,boolean,boolean,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerMessageChecked_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    DummyExternalMessage fixture = new DummyExternalMessage(TestParameters.MESSAGE);

    JSONObject message = new JSONObject(TestParameters.MESSAGE);
    message.put(MessageFields.FROM_PEER, TestParameters.PEERS[1]);
    message.put(MessageFields.PeerMessage.PEER_ID, TestParameters.PEERS[0]);
    message.put(MessageFields.PeerMessage.PEER_SIG, TestParameters.signData(TestParameters.INTERNAL_CONTENT));
    PeerMessage peerMessage = new PeerMessage(message);

    // Test database method is called.
    fixture.submitIncomingPeerMessageChecked(TestParameters.getInstance().getWbbPeer(), peerMessage, true, false, false);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertTrue(db.isValid());
  }
}