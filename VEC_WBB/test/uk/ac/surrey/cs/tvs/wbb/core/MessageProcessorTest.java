/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>MessageProcessorTest</code> contains tests for the class <code>{@link MessageProcessor}</code>.
 */
public class MessageProcessorTest {

  /**
   * Extended class for testing.
   */
  private class DummyMessageProcessor extends MessageProcessor {

    /**
     * Constructor testing the underlying abstract class' constructor.
     * 
     * @param peer
     */
    public DummyMessageProcessor(WBBPeer peer) {
      super(peer);
    }

    /**
     * Runs the processor.
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
      // Do nothing.
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the MessageProcessor(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMessageProcessor_1() throws Exception {
    MessageProcessor result = new DummyMessageProcessor(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }
}