/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.InetSocketAddress;

import javax.net.ssl.SSLSocket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>ServerExternalListenerTest</code> contains tests for the class <code>{@link ServerExternalListener}</code>.
 */
public class ServerExternalListenerTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Make sure we have fresh WBB sockets.
    TestParameters.getInstance().resetWBB();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    ServerExternalListener fixture = new ServerExternalListener(TestParameters.getInstance().getWbbPeer());

    // Put the ServerExternalListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for the socket to start up.
    TestParameters.wait(1);

    // Connect to the socket and send some data.
    SSLSocket socket = (SSLSocket) TestParameters.getInstance().getClientSSLSocketFactory().createSocket();
    socket.setEnabledCipherSuites(new String[] { TestParameters.CIPHERS });
    socket.setUseClientMode(true);

    socket.connect(new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.EXTERNAL_PORT));

    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
    bos.write("{}\n".getBytes());
    bos.flush();

    // Wait for a response.
    BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
    byte[] buffer = new byte[TestParameters.BUFFER_BOUND];
    bis.read(buffer);

    String result = new String(buffer);

    bos.close();
    bis.close();

    socket.close();

    // We don't care what the response is, just that we got one. Responses will be tested elsewhere.
    JSONObject response = new JSONObject(result);
    assertEquals(TestParameters.ERROR_MESSAGE, response.getString(MessageFields.TYPE));
    assertEquals(TestParameters.getInstance().getWbbPeer().getID(), response.getString(MessageFields.ErrorMessage.PEER_ID));
    assertNotNull(response.get(MessageFields.ErrorMessage.PEER_SIG));
    assertTrue(response.getString(MessageFields.ErrorMessage.MESSAGE).startsWith(TestParameters.COULD_NOT_PARSE_JSON_MESSAGE));

    // Shutdown.
    fixture.shutdown();

    // Force the socket to close and unblock the accept.
    TestParameters.getInstance().getWbbPeer().getPeerExternalServerSocket().close();

    // Wait for the thread to finish.
    thread.join();
    assertFalse(thread.isAlive());
  }

  /**
   * Run the ServerExternalListener(WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testServerExternalListener_1() throws Exception {
    ServerExternalListener result = new ServerExternalListener(TestParameters.getInstance().getWbbPeer());
    assertNotNull(result);
  }
}