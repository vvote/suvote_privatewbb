/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.wbb.DummyConcurrentDB;
import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageVerificationException;
import uk.ac.surrey.cs.tvs.wbb.mongodb.DBRecordType;

/**
 * The class <code>PODMessageTest</code> contains tests for the class <code>{@link PODMessage}</code>.
 */
public class PODMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the boolean canOrHaveReachedConsensus(WBBPeer,String,int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCanOrHaveReachedConsensus_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    PODMessage fixture = new PODMessage(TestParameters.MESSAGE);

    // Test that the underlying db method is called.
    db.setCanReachConsensus(true);

    boolean result = fixture.canOrHaveReachedConsensus(TestParameters.getInstance().getWbbPeer(), TestParameters.THRESHOLD,
        TestParameters.THRESHOLD);
    assertTrue(result);

    assertTrue(db.isCanOrHaveReachedConsensusCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the boolean checkThresholdAndResponse(WBBPeer,String,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThresholdAndResponse_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    PODMessage fixture = new PODMessage(TestParameters.MESSAGE);

    // Test that the underlying db method is called.
    db.setReachedConsensus(true);

    boolean result = fixture.checkThresholdAndResponse(TestParameters.getInstance().getWbbPeer(), TestParameters.THRESHOLD);
    assertTrue(result);

    assertTrue(db.isCheckThresholdAndResponseCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
  }

  /**
   * Run the String getClientMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetClientMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    PODMessage fixture = new PODMessage(TestParameters.MESSAGE);

    // Test that the underlying db method is called.
    JSONObject object = new JSONObject();
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    db.setJsonObject(object);

    String result = fixture.getClientMessage(TestParameters.getInstance().getWbbPeer(), TestParameters.SERIAL_NO);
    assertNotNull(result);

    assertTrue(db.isGetClientMessageCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    assertEquals(object.toString(), result);
  }

  /**
   * Run the String getExternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetExternalSignableContent_1() throws Exception {
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);

    PODMessage fixture = new PODMessage(object);

    // Test content.
    String result = fixture.getExternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + TestParameters.DISTRICT;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getExternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the String getInternalSignableContent() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInternalSignableContent_1() throws Exception {
    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    JSONObject randomness = new JSONObject();
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 1);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 1);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 2);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 2);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 3);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 3);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));

    JSONArray indexes = new JSONArray();
    indexes.put(randomness);

    JSONArray reductions = new JSONArray();
    reductions.put(indexes);
    reductions.put(indexes);

    object.put(MessageFields.PODMessage.BALLOT_REDUCTIONS, reductions);

    PODMessage fixture = new PODMessage(object);

    // Test content.
    String result = fixture.getInternalSignableContent();
    assertNotNull(result);

    String expected = fixture.getID() + TestParameters.DISTRICT + reductions.toString() + TestParameters.COMMIT_TIME;
    assertEquals(expected, result);

    // Test again.
    result = fixture.getInternalSignableContent();
    assertNotNull(result);

    assertEquals(expected, result);
  }

  /**
   * Run the ArrayList<? extends PeerMessage> getSigsToCheck(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetSigsToCheck_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    PODMessage fixture = new PODMessage(TestParameters.MESSAGE);

    // Test that the underlying db method is called.
    ArrayList<PeerMessage> sigsToCheck = new ArrayList<PeerMessage>();
    db.setSigsToCheck(sigsToCheck);

    ArrayList<? extends PeerMessage> result = fixture.getSigsToCheck(TestParameters.getInstance().getWbbPeer(),
        TestParameters.SERIAL_NO);
    assertNotNull(result);

    assertTrue(db.isGetSigsToCheckCalled());
    assertEquals(TestParameters.SERIAL_NO, db.getSerialNo());
    assertEquals(sigsToCheck, result);
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testPerformValidation_1() throws Exception {
    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    // No type.
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, TestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);

    PODMessage fixture = new PODMessage(object);

    // Test schema violation.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_2() throws Exception {
    String signature = TestParameters.signData(TestParameters.DEVICE);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    JSONObject randomness = new JSONObject();
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 1);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 1);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 2);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 2);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 3);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 3);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));

    JSONArray indexes = new JSONArray();
    indexes.put(randomness);

    JSONArray reductions = new JSONArray();
    reductions.put(indexes);
    reductions.put(indexes);

    object.put(MessageFields.PODMessage.BALLOT_REDUCTIONS, reductions);

    PODMessage fixture = new PODMessage(object);

    // Test valid schema but invalid signature.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the boolean performValidation(WBBPeer) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageVerificationException.class)
  public void testPerformValidation_3() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String[] data = new String[] { TestParameters.SERIAL_NO, TestParameters.DISTRICT };

    String signature = TestParameters.signData(data);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, TestParameters.MESSAGE_TYPE_POD_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, signature);
    object.put(TestParameters.SERIAL_SIG_FIELD, TestParameters.signData(TestParameters.DEVICE));

    JSONObject randomness = new JSONObject();
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 1);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 1);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 2);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 2);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, 3);
    randomness.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, 3);
    randomness.put(TestParameters.RANDOMNESS_FIELD, TestParameters.signData(TestParameters.DEVICE));

    JSONArray indexes = new JSONArray();
    indexes.put(randomness);

    JSONArray reductions = new JSONArray();
    reductions.put(indexes);
    reductions.put(indexes);

    object.put(MessageFields.PODMessage.BALLOT_REDUCTIONS, reductions);

    PODMessage fixture = new PODMessage(object);

    // Test valid schema but invalid ballot reductions.
    fixture.performValidation(TestParameters.getInstance().getWbbPeer());
  }

  /**
   * Run the PODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testPODMessage_1() throws Exception {
    // Test invalid source message.
    PODMessage result = new PODMessage("");
    assertNull(result);
  }

  /**
   * Run the PODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPODMessage_2() throws Exception {
    // Test valid source message.
    PODMessage result = new PODMessage(TestParameters.MESSAGE);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.POD_MESSAGE, result.type);
  }

  /**
   * Run the PODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageJSONException.class)
  public void testPODMessage_3() throws Exception {
    // Test invalid source message.
    PODMessage result = new PODMessage(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the PODMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPODMessage_4() throws Exception {
    // Test valid source message.
    JSONObject object = new JSONObject(TestParameters.MESSAGE);

    PODMessage result = new PODMessage(object);
    assertNotNull(result);

    assertEquals(TestParameters.SERIAL_NO, result.id);
    assertEquals(Message.POD_MESSAGE, result.type);
  }

  /**
   * Run the void processAsPartOfCommitR2(WBBPeer,JSONObject,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessAsPartOfCommitR2_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    JSONObject object = new JSONObject(TestParameters.MESSAGE);

    PODMessage fixture = new PODMessage(object);

    // Test that the super method is called. Content of super method tested elsewhere.
    db.setStored(true);

    fixture.processAsPartOfCommitR2(TestParameters.getInstance().getWbbPeer(), new JSONObject(), TestParameters.PEERS[1],
        TestParameters.getInstance().getWbbPeer().getUploadDir().getAbsolutePath());
    assertTrue(db.isAlreadyStoredCalled());
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String signature = TestParameters.signData(TestParameters.DEVICE);
    JSONObject object = new JSONObject(TestParameters.MESSAGE);

    PODMessage fixture = new PODMessage(object);

    // Test that the db method is called.
    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertTrue(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.POD, db.getRecordType());
    assertEquals(fixture, db.getMessage());
    assertEquals(signature, db.getSignature());
  }

  /**
   * Run the boolean storeIncomingMessage(WBBPeer,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStoreIncomingMessage_2() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    String signature = TestParameters.signData(TestParameters.DEVICE);
    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.UUID, TestParameters.SESSION_ID.toString());

    PODMessage fixture = new PODMessage(object);

    // Test that the db method is called with exception.
    db.addException("submitIncomingClientMessage", 0);

    boolean result = fixture.storeIncomingMessage(TestParameters.getInstance().getWbbPeer(), signature);
    assertFalse(result);

    assertTrue(db.isSubmitIncomingClientMessageCalled());
    assertEquals(DBRecordType.POD, db.getRecordType());
    assertEquals(fixture, db.getMessage());
    assertEquals(signature, db.getSignature());

    assertEquals(TestParameters.SESSION_ID.toString(), TestParameters.getInstance().getWbbPeer().getSessionID());
  }

  /**
   * Run the void submitIncomingPeerMessageChecked(WBBPeer,PeerMessage,boolean,boolean,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitIncomingPeerMessageChecked_1() throws Exception {
    // Replace the database so that we can influence testing.
    DummyConcurrentDB db = new DummyConcurrentDB();
    TestParameters.getInstance().getWbbPeer().setDB(db);

    PODMessage fixture = new PODMessage(TestParameters.MESSAGE);
    PeerMessage peerMessage = new PeerMessage(TestParameters.MESSAGE);

    // Test that the underlying db method is called.
    JSONObject object = new JSONObject();
    object.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NO);

    db.setJsonObject(object);

    fixture.submitIncomingPeerMessageChecked(TestParameters.getInstance().getWbbPeer(), peerMessage, true, true, false);

    assertTrue(db.isSubmitIncomingPeerMessageCheckedCalled());
    assertEquals(peerMessage, db.getMessage());
    assertEquals(true, db.isValid());
  }
}