/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;

/**
 * The class <code>PeerStatSenderTest</code> contains tests for the class <code>{@link PeerStatSender}</code>.
 */
public class PeerStatSenderTest {

  /**
   * Dummy WebSocket class for testing.
   */
  private class DummyWebSocket extends WebSocket {

    private String sent = null;

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    public String getSent() {
      return this.sent;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String sent) throws NotYetConnectedException {
      this.sent = sent;
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the PeerStatSender(WebSocket,long,WBBPeer) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerStatSender_1() throws Exception {
    PeerStatSender result = new PeerStatSender(new DummyWebSocket(), TestParameters.SLEEP_INTERVAL, TestParameters.getInstance()
        .getWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    DummyWebSocket socket = new DummyWebSocket();
    PeerStatSender fixture = new PeerStatSender(socket, TestParameters.SLEEP_INTERVAL, TestParameters.getInstance().getWbbPeer());

    // Put the StatSender in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for enough time for something to have happened.
    int count = 0;

    while (count < 2) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    // Shutdown.
    fixture.shutdown();

    // Wait for the thread to finish.
    thread.join();
    assertFalse(thread.isAlive());

    JSONObject result = new JSONObject(socket.getSent());
    assertNotNull(result);

    assertTrue(result.has(TestParameters.TYPE));
    assertTrue(result.has(TestParameters.CHANNELS));

    assertEquals(result.getString(TestParameters.TYPE), TestParameters.PEER_STAT);
    assertTrue(result.getInt(TestParameters.CHANNELS) >= 0);
  }
}