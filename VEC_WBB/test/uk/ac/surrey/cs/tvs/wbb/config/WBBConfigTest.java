/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.security.MessageDigest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.wbb.TestParameters;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;

/**
 * The class <code>WBBConfigTest</code> contains tests for the class <code>{@link WBBConfig}</code>.
 */
public class WBBConfigTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the int getInt(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInt_1() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    int result = fixture.getInt(WBBConfig.BUFFER_BOUND);
    assertEquals(TestParameters.BUFFER_BOUND, result);
  }

  /**
   * Run the int getInt(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetInt_2() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    int result = fixture.getInt("rubbish");
    assertEquals(0, result);
  }

  /**
   * Run the MessageDigest getMessageDigest() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetMessageDigest_1() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    MessageDigest result = fixture.getMessageDigest();
    assertEquals(TestParameters.MESSAGE_DIGEST_ALGORITHM, result.getAlgorithm());
  }

  /**
   * Run the String getString(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetString_1() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    String result = fixture.getString(WBBConfig.DISTRICT_CONFIG);
    assertEquals(TestParameters.DISTRICT_CONFIG_FILE, result);
  }

  /**
   * Run the String getString(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetString_2() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    String result = fixture.getString("rubbish");
    assertNull(result);
  }

  /**
   * Run the int getTime(String,TimeUnit) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetTime_1() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    int result = fixture.getTime(WBBConfig.COMMIT_TIME, TimeUnit.HOURS);
    assertEquals(TestParameters.COMMIT_TIME_HOURS, result);

    result = fixture.getTime(WBBConfig.COMMIT_TIME, TimeUnit.MINUTES);
    assertEquals(TestParameters.COMMIT_TIME_MINUTES, result);

    result = fixture.getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.HOURS);
    assertEquals(TestParameters.RUN_COMMIT_AT_HOURS, result);

    result = fixture.getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.MINUTES);
    assertEquals(TestParameters.RUN_COMMIT_AT_MINUTES, result);

    result = fixture.getTime(WBBConfig.RUN_COMMIT_AT, TimeUnit.SECONDS);
    assertEquals(-1, result);
  }

  /**
   * Run the int getTime(String,TimeUnit) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetTime_2() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    int result = fixture.getTime("rubbish", TimeUnit.HOURS);
    assertEquals(-1, result);
  }

  /**
   * Run the int getTime(String,TimeUnit) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetTime_3() throws Exception {
    WBBConfig fixture = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);

    int result = fixture.getTime("rubbish", TimeUnit.SECONDS);
    assertEquals(-1, result);
  }

  /**
   * Run the WBBConfig(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testWBBConfig_1() throws Exception {
    WBBConfig result = new WBBConfig(TestParameters.PEER_CONFIGURATIONS[0]);
    assertNotNull(result);

    assertEquals(TestParameters.THRESHOLD, result.getThreshold());
    assertEquals(TestParameters.SUBMISSION_TIMEOUT, result.getSubmissionTimeout());
    assertEquals(TestParameters.BUFFER_BOUND, result.getBufferBound());
    assertEquals(TestParameters.SOCKET_FAIL_TIMEOUT_INITIAL, result.getSocketFailTimeoutInitial());
    assertEquals(TestParameters.SOCKET_FAIL_TIMEOUT_MAX, result.getSocketFailTimeoutMax());

    assertEquals(TestParameters.getInstance().getDistrict().toString(), result.getDistrictConf().toString());

    Map<String, JSONObject> resultPeers = result.getPeers();
    JSONArray actualPeers = TestParameters.getInstance().getWbbPeers();
    assertEquals(actualPeers.length(), resultPeers.size());

    for (int i = 0; i < actualPeers.length(); i++) {
      String peerId = actualPeers.getJSONObject(i).getString(WBBConfig.PEER_ID);
      assertTrue(resultPeers.containsKey(peerId));
      assertEquals(actualPeers.getJSONObject(i).toString(), resultPeers.get(peerId).toString());
      assertEquals(actualPeers.getJSONObject(i).toString(), result.getHostDetails(peerId).toString());
    }
  }

  /**
   * Run the WBBConfig(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testWBBConfig_2() throws Exception {
    WBBConfig result = new WBBConfig("rubbish");
    assertNotNull(result);
  }

  /**
   * Run the WBBConfig(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONSchemaValidationException.class)
  public void testWBBConfig_3() throws Exception {
    WBBConfig result = new WBBConfig(TestParameters.DISTRICT_CONFIG_FILE); // Wrong file.
    assertNull(result);
  }
}