package uk.ac.surrey.cs.tvs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 * 
 * @generatedBy CodePro at 09/11/13 18:19
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ uk.ac.surrey.cs.tvs.verifier.AllTests.class, uk.ac.surrey.cs.tvs.wbb.AllTests.class, })
public class AllTests {
}
