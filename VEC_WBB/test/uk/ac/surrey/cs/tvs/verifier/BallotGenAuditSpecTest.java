/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.verifier;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * The class <code>BallotGenAuditSpecTest</code> contains tests for the class <code>{@link BallotGenAuditSpec}</code>.
 */
public class BallotGenAuditSpecTest {

  /**
   * Run the BallotGenAuditSpec(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testBallotGenAuditSpec_1() throws Exception {
    BallotGenAuditSpec result = new BallotGenAuditSpec("");
    assertNull(result);
  }

  /**
   * Run the BallotGenAuditSpec(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testBallotGenAuditSpec_2() throws Exception {
    BallotGenAuditSpec result = new BallotGenAuditSpec(new JSONObject());
    assertNull(result);
  }

  /**
   * Run the BallotGenAuditSpec(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenAuditSpec_3() throws Exception {
    JSONObject config = IOUtils.readJSONObjectFromFile("./testdata/verify/ballotGenAuditSpec.json");

    BallotGenAuditSpec result = new BallotGenAuditSpec(config);
    assertNotNull(result);

    assertNotNull(result.getBallotAuditFile());
    assertNotNull(result.getBallotGenConf());
    assertNotNull(result.getBallotGenFile());
    assertNotNull(result.getBaseEncryptedIds());
    assertNotNull(result.getPublicKeyPath());
    assertNotNull(result.getRandomnessCommits());
  }
}