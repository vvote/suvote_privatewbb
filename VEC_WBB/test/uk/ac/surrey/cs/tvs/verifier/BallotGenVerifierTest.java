/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.verifier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>BallotGenVerifierTest</code> contains tests for the class <code>{@link BallotGenVerifier}</code>.
 */
public class BallotGenVerifierTest {

  /**
   * Run the boolean doVerification() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoVerification_1() throws Exception {
    String spec = IOUtils.readStringFromFile("./testdata/ballotGenAudit/ballotGenAuditSpec.json");
    BallotGenVerifier fixture = new BallotGenVerifier(spec);

    // Test verification failure (wrong files).
    assertFalse(fixture.doVerification());
  }

  /**
   * Run the boolean doVerification() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoVerification_2() throws Exception {
    String spec = IOUtils.readStringFromFile("./testdata/verify/ballotGenAuditSpec.json");
    BallotGenVerifier fixture = new BallotGenVerifier(spec);

    // Test verification success (correct files).
    assertTrue(fixture.doVerification());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    String[] args = new String[] {};

    BallotGenVerifier.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    String[] args = new String[] { "./testdata/ballotGenAudit/ballotGenAuditSpec.json" };

    // Test verification failure; we're just testing that the correct methods are called.
    BallotGenVerifier.main(args);
  }
}