/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.verifier;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.bouncycastle.math.ec.ECPoint;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * The class <code>ECPointComparatorTest</code> contains tests for the class <code>{@link ECPointComparator}</code>.
 */
public class ECPointComparatorTest {

  /**
   * Run the int compare(ECPoint[],ECPoint[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompare_1() throws Exception {
    ECUtils ecUtils = new ECUtils();

    ECPoint plainText = ecUtils.getRandomValue();
    BigInteger privateKey = ecUtils.getRandomInteger(ecUtils.getOrderUpperBound(), new SecureRandom());
    ECPoint publicKey = ecUtils.getG().multiply(privateKey);

    ECPoint[] cipherOne = ecUtils.encrypt(plainText, publicKey);

    ECPointComparator fixture = new ECPointComparator();

    assertEquals(0, fixture.compare(cipherOne, cipherOne));
  }
}