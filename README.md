PrivateWBB
==========

The PrivateWBB is the major component in the system. It receives, signs and validates all the messages from the system. It is involved from the point of randomness generation, through to the final submission of data from the mixnet.

The concept behind the protocol is that it operates in a distributed manner. When a message is submitted it must be individually submitted to all peers. There is an assumption that there will always be a threshold of peers available and online. When a peer receives a message it checks it is valid and is consistent with the state of its local database. For example, if an audit request is sent it checks that a startEVM/Vote/Cancel have not been received already. If the messages is acceptable it stores it in the local database and sends a signature of the message to the other peers. The same process occurs concurrently on the different peers. When a peer receives a message from another peer it checks if it has received a related message from a client. If it has it will validate the received peer message against the message it received. If consistent and valid it will add it to its database. Once a threshold of valid peer messages have been received it returns a share of a threshold signature, on the original data, to the client. If no related message has been received it queues the message for checking when a message is received from a client. 

When the client receives a threshold of messages from the peers it can combine the signature shares and create a single joint signature on the data - this signature proves the data has been received and recorded as valid by a threshold number of peers.

At the end of each day the peers run a commit protocol, in which they joint agree the state of the combined databases. This is a two round protocol involving sending around a hash of the data and possible a copy of the data. The end result is a message sent to the PublicWBB component with a share of a joint signature on the days submissions.

Running
=======
WBBPeer pathToConfigFile  

pathToConfigFile: path to configuration file to use.## This is the README
